require 'test_helper'

class MachineTagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @machine_tag = machine_tags(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get machine_tags_url
    assert_response :success
  end

  test "should get new" do
    get new_machine_tag_url
    assert_response :success
  end

  test "should not get index for regular users" do
    sign_out :user
    sign_in users(:regular)
    get machine_tags_url
    assert_response :redirect
  end

  test "should not get new for regular users" do
    sign_out :user
    sign_in users(:regular)
    get new_machine_tag_url
    assert_response :redirect
  end

  test "should create machine_tag" do
    assert_difference('MachineTag.count') do
      post machine_tags_url, params: { machine_tag: { cad_price: @machine_tag.cad_price, intl_shipping: @machine_tag.intl_shipping, machine_tag_group_id: @machine_tag.machine_tag_group_id, name: @machine_tag.name, price: @machine_tag.price, priority_shipping: @machine_tag.priority_shipping, shipping_price: @machine_tag.shipping_price, weight: @machine_tag.weight } }
    end

    assert_redirected_to machine_tag_url(MachineTag.last)
  end

  test "should show machine_tag" do
    get machine_tag_url(@machine_tag)
    assert_response :success
  end

  test "should get edit" do
    get edit_machine_tag_url(@machine_tag)
    assert_response :success
  end

  test "should not show machine_tag for regular user" do
    sign_out :user
    sign_in users(:regular)
    get machine_tag_url(@machine_tag)
    assert_response :redirect
  end

  test "should not get edit for regular user" do
    sign_out :user
    sign_in users(:regular)
    get edit_machine_tag_url(@machine_tag)
    assert_response :redirect
  end

  test "should update machine_tag" do
    patch machine_tag_url(@machine_tag), params: { machine_tag: { cad_price: @machine_tag.cad_price, intl_shipping: @machine_tag.intl_shipping, machine_tag_group_id: @machine_tag.machine_tag_group_id, name: @machine_tag.name, price: @machine_tag.price, priority_shipping: @machine_tag.priority_shipping, shipping_price: @machine_tag.shipping_price, weight: @machine_tag.weight } }
    assert_redirected_to machine_tag_url(@machine_tag)
  end

  test "can mark machine_tag inactive" do
    assert_difference('@machine_tag.machine_tag_group.active_machine_tag_count', -1) do
      put machine_tag_url(@machine_tag), params: { machine_tag: { active: false } }
      assert_redirected_to machine_tag_url(@machine_tag)
      follow_redirect!
      assert_select 'dd', /No/
    end
  end

  test "should destroy machine_tag" do
    assert_difference('MachineTag.count', -1) do
      delete machine_tag_url(@machine_tag)
    end

    assert_redirected_to machine_tags_url
  end
end
