require 'test_helper'

class HatsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @hat = hats(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get hats_url
    assert_response :success
  end

  test "should get new" do
    get new_hat_url
    assert_response :success
  end

  test "regular users should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_hat_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "anonymous users should not get new" do
    sign_out :user
    get new_hat_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create hat" do
    assert_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: TestData.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end

    assert_redirected_to hat_url(Hat.last)
  end

  test "should not create hat without name" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: "",
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without price" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: "",
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without cad_price" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: "",
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without description" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: "",
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without image" do
    assert_no_difference('Hat.count') do
    # assert_raises Shrine::Error do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image: "",
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without intl_shipping" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: "",
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without keywords" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: "",
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without meta_description" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: "",
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without priority_shipping" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: "",
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without shipping_price" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: "",
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without size" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: "",
        structure: @hat.structure,
        style: @hat.style,
        weight: @hat.weight } }
    end
  end

  test "should not create hat without weight" do
    assert_no_difference('Hat.count') do
      post hats_url, params: { hat: {
        name: @hat.name,
        price: @hat.price,
        cad_price: @hat.cad_price,
        description: @hat.description,
        fastener: @hat.fastener,
        image_data: @hat.image_data,
        intl_shipping: @hat.intl_shipping,
        keywords: @hat.keywords,
        meta_description: @hat.meta_description,
        priority_shipping: @hat.priority_shipping,
        shipping_price: @hat.shipping_price,
        size: @hat.size,
        structure: @hat.structure,
        style: @hat.style,
        weight: "" } }
    end
  end

  test "should show hat" do
    get hat_url(@hat)
    assert_response :success
  end

  test "should get edit" do
    get edit_hat_url(@hat)
    assert_response :success
  end

  test "should update hat" do
    patch hat_url(@hat), params: { hat: {
      cad_price: @hat.cad_price,
      description: @hat.description,
      fastener: @hat.fastener,
      image_data: @hat.image_data,
      intl_shipping: @hat.intl_shipping,
      keywords: @hat.keywords,
      meta_description: @hat.meta_description,
      name: @hat.name,
      price: @hat.price,
      priority_shipping: @hat.priority_shipping,
      shipping_price: @hat.shipping_price,
      size: @hat.size,
      slug: @hat.slug,
      structure: @hat.structure,
      style: @hat.style,
      weight: @hat.weight } }
    assert_redirected_to hat_url(@hat)
  end

  test "should destroy hat" do
    assert_difference('Hat.count', -1) do
      delete hat_url(@hat)
    end

    assert_redirected_to hats_url
  end

  test "can mark hat inactive" do
    patch hat_url(@hat), params: { hat: {
      active: false,
      image_data: @hat.image_data
      }
    }
    assert_redirected_to hat_url(@hat)
    follow_redirect!
    assert_select 'h5', /This item is currently unavailable./
  end
end
