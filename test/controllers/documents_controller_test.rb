require 'test_helper'

class DocumentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 7500, cad_price: 9500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 6500, cad_price: 8500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 4500, cad_price: 6500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 1, price: 2900, cad_price: 3900, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 2500, cad_price: 3500, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 2000, cad_price: 3000, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 1500, cad_price: 2500, tier: 2 }
    }
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "can access index if not logged in" do
    get documentation_url
    assert_response :success
  end
end
