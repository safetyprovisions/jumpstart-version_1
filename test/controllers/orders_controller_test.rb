require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @order = orders(:two)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get orders_url
    assert_response :redirect
    follow_redirect!
    assert_select "div.container", "×
      You need to sign in or sign up before continuing." # Includes the x to close the div
  end

  test "regular user can access" do
    sign_out :user
    sign_in users(:regular)
    get orders_url
    assert_response :success
  end

  test "superadmin can view order" do
    get order_url(@order)
    assert_response :success
  end

  test "regular user can view order" do
    sign_out :user
    sign_in users(:regular)
    get order_url(orders(:three))
    assert_response :success
  end

  test "regular user cannot view someone else's order" do
    sign_out :user
    sign_in users(:regular)
    get order_url(@order)
    assert_redirected_to root_url
    follow_redirect!
    assert_select 'body', /This record does not exist./
  end

  test "can update order" do
    put order_url(@order), params: { order: { shipping_method: "intl_shipping" } }
    assert_redirected_to cart_url
    @order.reload
    assert_equal "intl_shipping", @order.shipping_method
  end

  test "superadmin can view unpaid_orders" do
    get unpaid_orders_url
    assert_response :success
    assert_select 'h1', "Unpaid Orders"
    assert_select 'h3', 2
  end

  test "admin user cannot view unpaid_orders" do
    sign_out :user
    sign_in users(:admin)
    get unpaid_orders_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=unpaid&q%5B%5D=orders"
  end

  test "regular user cannot view unpaid_orders" do
    sign_out :user
    sign_in users(:regular)
    get unpaid_orders_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=unpaid&q%5B%5D=orders"
  end

  test "regular users can change_shipping method" do
    sign_out :user
    sign_in users(:regular)
    get change_shipping_url, params: { shipping_method: 'priority_shipping' }
    assert_redirected_to cart_path
    assert_equal 'priority_shipping', Order.last.shipping_method
  end

  test "regular user can submit a valid code" do
    sign_out :user
    sign_in users(:regular)
    # Create Order
    current_cart = Order.new(user_id: users(:regular).id)
    # Add OrderItems
    @order_item = current_cart.order_items.find_or_initialize_by(
      orderable_id:   online_courses(:one).id,
      orderable_type: 'OnlineCourse'
    )
    @order_item.update(quantity: @order_item.quantity + 1)

    get verify_code_url, params: { coupon_code: coupon_codes(:total).code }
    assert_redirected_to cart_path
    follow_redirect!
    assert_select 'body', /Coupon code added!/
    # TODO This part doesn't pass:
    # assert_equal 3950, current_cart.total('usd')
  end

  test "regular user can't submit an invalid code" do
    sign_out :user
    sign_in users(:regular)
    # Create Order
    current_cart = Order.new(user_id: users(:regular).id)
    # Add OrderItems
    @order_item = current_cart.order_items.find_or_initialize_by(
      orderable_id:   online_courses(:one).id,
      orderable_type: 'OnlineCourse'
    )
    @order_item.update(quantity: @order_item.quantity + 1)

    get verify_code_url, params: { coupon_code: 'somethingmadeup' }
    assert_redirected_to cart_path
    follow_redirect!
    assert_select 'body', /Invalid coupon code./
  end

  test "regular user can't use code without min_purchase_amt" do
    sign_out :user
    sign_in users(:regular)
    # Create Order
    current_cart = Order.new(user_id: users(:regular).id)
    # Don't Add OrderItems…
    get verify_code_url, params: { coupon_code: coupon_codes(:total).code }
    assert_redirected_to cart_path
    follow_redirect!
    assert_select 'body', /Minimum purchase amt must be met/
  end

  test "regular user can't buy online_course with kit code" do
    sign_out :user
    sign_in users(:regular)
    # Create Order
    current_cart = Order.new(user_id: users(:regular).id)
    # Add OrderItems
    @order_item = current_cart.order_items.find_or_initialize_by(
      orderable_id:   online_courses(:one).id,
      orderable_type: 'OnlineCourse'
    )
    @order_item.update(quantity: @order_item.quantity + 1)

    get verify_code_url, params: { coupon_code: coupon_codes(:kits).code }
    assert_redirected_to cart_path
    follow_redirect!
    assert_select 'body', /This coupon code can only be used to purchase one or more DIY Kits./
  end

  test "can remove_coupon_code" do
    sign_out :user
    sign_in users(:regular)
    # Create Order
    current_cart = Order.new(user_id: users(:regular).id)
    # Add OrderItems
    @order_item = current_cart.order_items.find_or_initialize_by(
      orderable_id:   online_courses(:one).id,
      orderable_type: 'OnlineCourse'
    )
    @order_item.update(quantity: @order_item.quantity + 1)

    get verify_code_url, params: { coupon_code: coupon_codes(:total).code }
    assert_redirected_to cart_path
    follow_redirect!
    assert_select 'body', /Coupon code added!/
    get remove_coupon_code_url
    assert_redirected_to cart_path
    follow_redirect!
    assert_select 'body', /Coupon code removed./
    assert_nil current_cart.coupon_code
  end

  # TODO Don't know why the three tests below aren't working. The statuses aren't changing as expected.
  # test "superadmin can mark orders as labeled" do
  #   sign_in companies(:one)
  #   @time_before = @order.label_created
  #   post labeled_url, params: { labeled_order: @order.id }
  #   assert_redirected_to user_url(id: users(:superadmin).id, anchor: "dashboard-manual-labels")
  #   assert_not_equal @time_before, @order.label_created
  # end

  # test "superadmin can mark orders as invoiced" do
  #   sign_in companies(:one)
  #   post invoiced_url, params: { invoiced_order: orders(:to_be_invoiced).id }
  #   assert_redirected_to user_url(id: users(:superadmin).id, anchor: "dashboard-unpaid-orders")
  #   assert_equal "invoiced", orders(:to_be_invoiced).status
  # end

  # test "superadmin can mark orders as paid" do
  #   sign_in companies(:one)
  #   post order_paid_url, params: { paid_order: orders(:unpaid).id }
  #   assert_redirected_to user_url(id: users(:superadmin).id, anchor: "dashboard-unpaid-orders")
  #   assert_equal "paid", orders(:unpaid).status
  # end

  # TODO Is there a way to test the orders#download_validator?

  # TODO Need to soft delete Orders.
  # test "can destroy order" do
  #   assert_difference('AltName.count', -1) do
  #     delete order_url(@order)
  #   end
  #   assert_redirected_to orders_url
  # end
end
