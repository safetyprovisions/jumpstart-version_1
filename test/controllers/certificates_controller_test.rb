require 'test_helper'

class CertificatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @certificate = certificates(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get certificates_url
    assert_response :success
  end

  test "should get new" do
    get new_certificate_url
    assert_response :success
  end

  test "anonymous user should not get index" do
    sign_out :user
    get certificates_url
    assert_redirected_to "http://localhost:3000/search?q=certificates"
  end

  test "anonymous user should not get new" do
    sign_out :user
    get new_certificate_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "regular user should not get index" do
    sign_out :user
    sign_in users(:regular)
    get certificates_url
    assert_redirected_to "http://localhost:3000/search?q=certificates"
  end

  test "regular user should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_certificate_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create certificate" do
    assert_difference('Certificate.count') do
      post certificates_url, params: { certificate: {
        cad_price: @certificate.cad_price,
        description: @certificate.description,
        image: cached_certificate_image.to_json,
        intl_shipping: @certificate.intl_shipping,
        keywords: @certificate.keywords,
        meta_description: @certificate.meta_description,
        name: @certificate.name,
        price: @certificate.price,
        priority_shipping: @certificate.priority_shipping,
        shipping_price: @certificate.shipping_price } }
    end

    assert_redirected_to certificate_url(Certificate.last)
  end

  test "should show certificate" do
    get certificate_url(@certificate)
    assert_response :success
  end

  test "should get edit" do
    get edit_certificate_url(@certificate)
    assert_response :success
  end

  test "should update certificate" do
    patch certificate_url(@certificate), params: { certificate: {
      cad_price: @certificate.cad_price,
      description: @certificate.description,
      image_data: @certificate.image_data,
      intl_shipping: @certificate.intl_shipping,
      keywords: @certificate.keywords,
      meta_description: @certificate.meta_description,
      name: @certificate.name,
      price: @certificate.price,
      priority_shipping: @certificate.priority_shipping,
      shipping_price: @certificate.shipping_price,
      slug: @certificate.slug } }
    # assert_redirected_to certificate_url(@certificate)
    assert_redirected_to "http://localhost:3000/certificates/#{ @certificate.slug }"
  end

  test "should destroy certificate" do
    assert_difference('Certificate.count', -1) do
      delete certificate_url(@certificate)
    end

    assert_redirected_to certificates_url
  end

  test "can mark certificate inactive" do
    patch certificate_url(@certificate), params: { certificate: { active: false } }
    assert_redirected_to certificate_url(@certificate)
    follow_redirect!
    assert_select 'h5', /This item is currently unavailable./
  end
end
