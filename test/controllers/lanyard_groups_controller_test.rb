require 'test_helper'

class LanyardGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @lanyard_group = lanyard_groups(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "superadmin should get index" do
    get lanyard_groups_url
    assert_response :success
  end

  test "regular user should not get index" do
    sign_out :user
    sign_in users(:regular)
    get lanyard_groups_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=lanyard&q%5B%5D=groups"
  end

  test "superadmin should get new" do
    get new_lanyard_group_url
    assert_response :success
  end

  test "regular user should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_lanyard_group_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create lanyard_group" do
    assert_difference('LanyardGroup.count') do
      post lanyard_groups_url, params: { lanyard_group: {
        active: @lanyard_group.active,
        description: @lanyard_group.description,
        image_data: @lanyard_group.image_data,
        keywords: @lanyard_group.keywords,
        meta_description: @lanyard_group.meta_description,
        name: @lanyard_group.name,
        slug: "new-kit-slug" } }
    end

    assert_redirected_to lanyard_group_url(LanyardGroup.last)
  end

  test "should show lanyard_group to superadmin" do
    get lanyard_group_url(@lanyard_group)
    assert_response :success
  end

  test "should show lanyard_group to regular user" do
    sign_out :user
    sign_in users(:regular)
    get lanyard_group_url(@lanyard_group)
    assert_response :success
  end

  test "superadmin should get edit" do
    get edit_lanyard_group_url(@lanyard_group)
    assert_response :success
  end

  test "regular user should not get edit" do
    sign_out :user
    sign_in users(:regular)
    get edit_lanyard_group_url(@lanyard_group)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update lanyard_group" do
    patch lanyard_group_url(@lanyard_group), params: { lanyard_group: {
      active: @lanyard_group.active,
      description: @lanyard_group.description,
      image_data: @lanyard_group.image_data,
      keywords: @lanyard_group.keywords,
      meta_description: @lanyard_group.meta_description,
      name: @lanyard_group.name,
      slug: @lanyard_group.slug } }
    assert_redirected_to lanyard_group_url(@lanyard_group)
  end

  test "can mark lanyard_group inactive" do
    patch lanyard_group_url(@lanyard_group), params: { lanyard_group: { active: false } }
    assert_redirected_to "http://localhost:3000/lanyard_groups/#{ @lanyard_group.slug }"
    follow_redirect!
    assert_select 'h5', /This item is currently unavailable./
  end

  # test "should destroy lanyard_group" do
  #   assert_difference('LanyardGroup.count', -1) do
  #     delete lanyard_group_url(@lanyard_group)
  #   end

  #   assert_redirected_to lanyard_groups_url
  # end
end
