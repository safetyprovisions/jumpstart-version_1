require 'test_helper'

class KeychainsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @keychain = keychains(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get keychains_url
    assert_response :success
  end

  test "should get new" do
    get new_keychain_url
    assert_response :success
  end

  test "should not get index for regular user" do
    sign_out :user
    sign_in users(:regular)
    get keychains_url
    assert_response :redirect
  end

  test "should not get new for regular user" do
    sign_out :user
    sign_in users(:regular)
    get new_keychain_url
    assert_response :redirect
  end

  test "should create keychain" do
    assert_difference('Keychain.count') do
      post keychains_url, params: { keychain: { cad_price: @keychain.cad_price, color: @keychain.color, intl_shipping: @keychain.intl_shipping, keychain_group_id: @keychain.keychain_group_id, name: @keychain.name, price: @keychain.price, priority_shipping: @keychain.priority_shipping, shipping_price: @keychain.shipping_price } }
    end

    assert_redirected_to keychain_url(Keychain.last)
  end

  test "should show keychain" do
    get keychain_url(@keychain)
    assert_response :success
  end

  test "should get edit" do
    get edit_keychain_url(@keychain)
    assert_response :success
  end

  test "should not show keychain for regular user" do
    sign_out :user
    sign_in users(:regular)
    get keychain_url(@keychain)
    assert_response :redirect
  end

  test "should not get edit for regular user" do
    sign_out :user
    sign_in users(:regular)
    get edit_keychain_url(@keychain)
    assert_response :redirect
  end

  test "should update keychain" do
    patch keychain_url(@keychain), params: { keychain: { cad_price: @keychain.cad_price, color: @keychain.color, intl_shipping: @keychain.intl_shipping, keychain_group_id: @keychain.keychain_group_id, name: @keychain.name, price: @keychain.price, priority_shipping: @keychain.priority_shipping, shipping_price: @keychain.shipping_price } }
    assert_redirected_to keychain_url(@keychain)
  end

  test "can mark keychain inactive" do
    assert_difference('@keychain.keychain_group.active_keychain_count', -1) do
      put keychain_url(@keychain), params: { keychain: { active: false } }
      assert_redirected_to keychain_url(@keychain)
      follow_redirect!
      assert_select 'dd', /No/
    end
  end

  test "should destroy keychain" do
    assert_difference('Keychain.count', -1) do
      delete keychain_url(@keychain)
    end

    assert_redirected_to keychains_url
  end
end
