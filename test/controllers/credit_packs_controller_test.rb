require 'test_helper'

class CreditPacksControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @credit_pack = credit_packs(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "can access index if not logged in" do
    sign_out :user
    get credit_packs_url
    assert_response :success
  end

  test "can access index if regular user" do
    sign_out :user
    sign_in users(:regular)
    get credit_packs_url
    assert_response :success
  end

  test "can access if superadmin" do
    get credit_packs_url
    assert_response :success
  end

  test "can access new credit_pack page" do
    get new_credit_pack_url
    assert_response :success
  end

  test "can create a new credit_pack" do
    assert_difference('CreditPack.count') do
      post credit_packs_url, params: { credit_pack: { name: '$79x50', price: 325000, credits: 395000, cad_price: 0, cad_credits: 0 } }
    end

    assert_redirected_to credit_pack_url(CreditPack.last)
  end

  test "cannot create two identical credit_packs" do
    assert_difference('CreditPack.count', 1) do
      post credit_packs_url, params: { credit_pack: { name: '$79x50', price: 325000, credits: 395000, cad_price: 0, cad_credits: 0 } }
      post credit_packs_url, params: { credit_pack: { name: '$79x50', price: 325000, credits: 395000, cad_price: 0, cad_credits: 0 } }
    end
  end

  test "cannot create credit_pack without name" do
    assert_no_difference('CreditPack.count') do
      post credit_packs_url, params: { credit_pack: { name: '', price: 325000, credits: 395000, cad_price: 0, cad_credits: 0 } }
    end
  end

  test "cannot create credit_pack without any price info" do
    assert_no_difference('CreditPack.count') do
      post credit_packs_url, params: { credit_pack: { name: '$79x50', price: 0, credits: 0, cad_price: 0, cad_credits: 0 } }
    end
  end

  test "cannot create credit_pack without all usd price info" do
    assert_no_difference('CreditPack.count') do
      post credit_packs_url, params: { credit_pack: { name: '$79x50', price: 325000, credits: 0, cad_price: 0, cad_credits: 0 } }
    end
  end

  test "cannot create credit_pack without all cad price info" do
    assert_no_difference('CreditPack.count') do
      post credit_packs_url, params: { credit_pack: { name: '$99x50', price: 0, credits: 0, cad_price: 425000, cad_credits: 0 } }
    end
  end
end
