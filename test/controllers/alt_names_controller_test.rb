require 'test_helper'

class AltNamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @alt_name = alt_names(:one)
    @product_group_id = product_groups(:one).id
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get alt_names_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=alt&q%5B%5D=names"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get alt_names_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get alt_names_url
    assert_response :success
  end

  test "can access new alt_name page" do
    get new_alt_name_url
    assert_response :success
  end

  test "can create a new alt_name" do
    assert_difference('AltName.count') do
      post alt_names_url, params: { alt_name: { name: "Alternative Name", product_group_id: @product_group_id } }
    end

    assert_redirected_to alt_name_url(AltName.last)
  end

  test "cannot create two identical alt_names" do
    assert_difference('AltName.count', 1) do
      post alt_names_url, params: { alt_name: { name: "Alternative Name", product_group_id: @product_group_id } }
      post alt_names_url, params: { alt_name: { name: "Alternative Name", product_group_id: @product_group_id } }
    end
  end

  test "cannot create alt_name without title" do
    assert_no_difference('AltName.count') do
      post alt_names_url, params: { alt_name: { name: "", product_group_id: @product_group_id } }
    end
  end

  test "cannot create alt_name without product_group_id" do
    assert_no_difference('AltName.count') do
      post alt_names_url, params: { alt_name: { name: "Alternative Name", product_group_id: '' } }
    end
  end

  test "can view alt_name" do
    get alt_name_url(@alt_name)
    assert_response :success
  end

  test "can edit alt_name" do
    get edit_alt_name_url(@alt_name)
    assert_response :success
  end

  test "can update alt_name" do
    put alt_name_url(@alt_name), params: { alt_name: { name: "New Alternative Name", product_group_id: @product_group_id } }
    assert_redirected_to alt_name_url(@alt_name)
    @alt_name.reload
    assert_equal "New Alternative Name", @alt_name.name
  end

  test "cannot update alt_name with invalid attributes" do
    put alt_name_url(@alt_name), params: { alt_name: { name: ""} }
    assert_response :success
    assert_select "h1", "Edit Alt Name"
  end

  test "can destroy alt_name" do
    assert_difference('AltName.count', -1) do
      delete alt_name_url(@alt_name)
    end
    assert_redirected_to alt_names_url
  end
end
