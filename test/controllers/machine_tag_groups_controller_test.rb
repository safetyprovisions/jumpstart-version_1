require 'test_helper'

class MachineTagGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @machine_tag_group = machine_tag_groups(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get machine_tag_groups_url
    assert_response :success
  end

  test "should not get index for regular users" do
    sign_out :user
    sign_in users(:regular)
    get machine_tag_groups_url
    assert_response :redirect
  end

  test "should get new" do
    get new_machine_tag_group_url
    assert_response :success
  end

  test "should not get new for regular users" do
    sign_out :user
    sign_in users(:regular)
    get new_machine_tag_group_url
    assert_response :redirect
  end

  test "should create machine_tag_group" do
    assert_difference('MachineTagGroup.count') do
      post machine_tag_groups_url, params: { machine_tag_group: {
        description: @machine_tag_group.description,
        image: cached_machine_tag_group_image.to_json,
        keywords: @machine_tag_group.keywords,
        meta_description: @machine_tag_group.meta_description,
        name: @machine_tag_group.name,
        slug: @machine_tag_group.slug }
      }
    end

    assert_redirected_to machine_tag_group_url(MachineTagGroup.last)
  end

  test "should show machine_tag_group to superadmin" do
    get machine_tag_group_url(@machine_tag_group)
    assert_response :success
  end

  test "should show machine_tag_group to regular users" do
    sign_out :user
    sign_in users(:regular)
    get machine_tag_group_url(@machine_tag_group)
    assert_response :success
  end

  test "superadmin should get edit" do
    get edit_machine_tag_group_url(@machine_tag_group)
    assert_response :success
  end

  test "regular user should not get edit" do
    sign_out :user
    sign_in users(:regular)
    get edit_machine_tag_group_url(@machine_tag_group)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update machine_tag_group" do
    patch machine_tag_group_url(@machine_tag_group), params: { machine_tag_group: {
      description: @machine_tag_group.description,
      image_data: @machine_tag_group.image_data,
      keywords: @machine_tag_group.keywords,
      meta_description: @machine_tag_group.meta_description,
      name: @machine_tag_group.name,
      slug: @machine_tag_group.slug } }
    assert_redirected_to machine_tag_group_url(@machine_tag_group)
  end

  # test "should destroy machine_tag_group" do
  #   assert_difference('MachineTagGroup.count', -1) do
  #     delete machine_tag_group_url(@machine_tag_group)
  #   end

  #   assert_redirected_to machine_tag_groups_url
  # end

  test "can mark machine_tag_group inactive" do
    patch machine_tag_group_url(@machine_tag_group), params: { machine_tag_group: { active: false } }
    assert_redirected_to "http://localhost:3000/machine_tag_groups/#{ @machine_tag_group.slug }"
    follow_redirect!
    assert_select 'h5', /This item is currently unavailable./
  end
end
