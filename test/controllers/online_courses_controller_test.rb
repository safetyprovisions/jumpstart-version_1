require 'test_helper'

class OnlineCoursesControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @online_course = online_courses(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get online_courses_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=online&q%5B%5D=courses"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get online_courses_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=online&q%5B%5D=courses"
  end

  test "can access if superadmin" do
    get online_courses_url
    assert_response :success
  end

  test "can access new online_course page" do
    get new_online_course_url
    assert_response :success
  end

  test "can create a new online_course" do
    assert_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end

    assert_redirected_to online_course_url(OnlineCourse.last)
  end

  test "cannot create two identical online_courses" do
    assert_difference('OnlineCourse.count', 1) do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without title" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without price" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: '',
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without cad_price" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: '',
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without language" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without standards" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without product_group_id" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: '',
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without talent_course_id" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without practical_evaluation" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation_data: '',
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without exam" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam_data: '',
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true } }
    end
  end

  test "cannot create online_course without answer_key" do
    assert_no_difference('OnlineCourse.count') do
      post online_courses_url, params: { online_course: {
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: product_groups(:one).id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key_data: '',
        market_type: "usa",
        public: true } }
    end
  end

  test "superadmin can view online_course" do
    get online_course_url(@online_course)
    assert_response :success
  end

  test "regular user cannot view online_course" do
    sign_out :user
    sign_in users(:regular)
    get online_course_url(@online_course)
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=98&q%5B%5D=19&q%5B%5D=96"
  end

  test "superadmin can edit online_course" do
    get edit_online_course_url(@online_course)
    assert_response :success
  end

  test "regular user cannot edit online_course" do
    sign_out :user
    sign_in users(:regular)
    get edit_online_course_url(@online_course)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "can update online_course" do
    patch online_course_url(@online_course), params: { online_course: {
        title: "New Online Course Name",
        # product_group_id: @product_group_id
      }
    }
    assert_redirected_to online_courses_url
    @online_course.reload
    assert_equal "New Online Course Name", @online_course.title
  end

  test "cannot update online_course with invalid attributes" do
    patch online_course_url(@online_course), params: { online_course: { title: ""} }
    assert_response :success
    assert_select "h1", "Edit Online Course"
  end

  test "can mark online_course inactive" do
    assert_difference(@active_online_course_count, -1) do
      patch online_course_url(@online_course), params: { online_course: {
          active: false
        }
      }
      assert_redirected_to online_courses_url
    end
  end

  # test "can destroy online_course" do
  #   assert_difference('OnlineCourse.count', -1) do
  #     delete online_course_url(@online_course)
  #   end
  #   assert_redirected_to online_courses_url
  # end
end
