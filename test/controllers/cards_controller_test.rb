require 'test_helper'

class CardsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @card = cards(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get cards_url
    assert_response :success
  end

  test "should get new" do
    get new_card_url
    assert_response :success
  end

  test "regular users should not get index" do
    sign_out :user
    sign_in users(:regular)
    get cards_url
    assert_redirected_to "http://localhost:3000/search?q=cards"
  end

  test "regular users should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_card_url
    assert_redirected_to "http://localhost:3000/search?q=new"
  end

  test "anonymous users should not get index" do
    sign_out :user
    get cards_url
    assert_redirected_to "http://localhost:3000/search?q=cards"
  end

  test "anonymous users should not get new" do
    sign_out :user
    get new_card_url
    assert_redirected_to "http://localhost:3000/search?q=new"
  end

  test "should create card" do
    assert_difference('Card.count') do
      post cards_url, params: { card: {
        cad_price: @card.cad_price,
        card_group_id: @card.card_group_id,
        intl_shipping: @card.intl_shipping,
        name: "New Card",
        price: @card.price,
        priority_shipping: @card.priority_shipping,
        shipping_price: @card.shipping_price,
        weight: @card.weight } }
    end

    assert_redirected_to card_url(Card.last)
  end

  test "should show card" do
    get card_url(@card)
    assert_response :success
  end

  test "should get edit" do
    get edit_card_url(@card)
    assert_response :success
  end

  test "regular users should not show card" do
    sign_out :user
    sign_in users(:regular)
    get card_url(@card)
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=98&q%5B%5D=19&q%5B%5D=96"
  end

  test "regular users should not get edit" do
    sign_out :user
    sign_in users(:regular)
    get edit_card_url(@card)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "anonymous users should not show card" do
    sign_out :user
    get card_url(@card)
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=98&q%5B%5D=19&q%5B%5D=96"
  end

  test "anonymous users should not get edit" do
    sign_out :user
    get edit_card_url(@card)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update card" do
    patch card_url(@card), params: { card: {
      cad_price: @card.cad_price,
      card_group_id: @card.card_group_id,
      intl_shipping: @card.intl_shipping,
      name: @card.name,
      price: @card.price,
      priority_shipping: @card.priority_shipping,
      shipping_price: @card.shipping_price,
      weight: @card.weight } }
    assert_redirected_to card_url(@card)
  end

  test "can mark card inactive" do
    assert_difference('@card.card_group.active_card_count', -1) do
      put card_url(@card), params: { card: { active: false } }
      assert_redirected_to card_url(@card)
      follow_redirect!
      assert_select 'dd', /No/
    end
  end

  test "should destroy card" do
    assert_difference('Card.count', -1) do
      delete card_url(@card)
    end

    assert_redirected_to cards_url
  end
end
