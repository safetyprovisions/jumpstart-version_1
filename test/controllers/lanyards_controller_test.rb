require 'test_helper'

class LanyardsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @lanyard = lanyards(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "superadmin should get index" do
    get lanyards_url
    assert_response :success
  end

  test "regular user should not get index" do
    sign_out :user
    sign_in users(:regular)
    get lanyards_url
    assert_redirected_to "http://localhost:3000/search?q=lanyards"
  end

  test "superadmin should get new" do
    get new_lanyard_url
    assert_response :success
  end

  test "regular user should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_lanyard_url
    assert_redirected_to "http://localhost:3000/search?q=new"
  end

  test "should create lanyard" do
    assert_difference('Lanyard.count') do
      post lanyards_url, params: { lanyard: {
        active: @lanyard.active,
        cad_price: @lanyard.cad_price,
        intl_shipping: @lanyard.intl_shipping,
        lanyard_group_id: @lanyard.lanyard_group_id,
        name: @lanyard.name,
        price: @lanyard.price,
        priority_shipping: @lanyard.priority_shipping,
        shipping_price: @lanyard.shipping_price,
        weight: @lanyard.weight } }
    end

    assert_redirected_to lanyard_url(Lanyard.last)
  end

  test "should show lanyard to superadmin" do
    get lanyard_url(@lanyard)
    assert_response :success
  end

  test "should not show lanyard to regular user" do
    sign_out :user
    sign_in users(:regular)
    get lanyard_url(@lanyard)
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=98&q%5B%5D=19&q%5B%5D=96"
  end

  test "superadmin should get edit" do
    get edit_lanyard_url(@lanyard)
    assert_response :success
  end

  test "regular user should not get edit" do
    sign_out :user
    sign_in users(:regular)
    get edit_lanyard_url(@lanyard)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update lanyard" do
    patch lanyard_url(@lanyard), params: { lanyard: {
      active: @lanyard.active,
      cad_price: @lanyard.cad_price,
      intl_shipping: @lanyard.intl_shipping,
      lanyard_group_id: @lanyard.lanyard_group_id,
      name: @lanyard.name,
      price: @lanyard.price,
      priority_shipping: @lanyard.priority_shipping,
      shipping_price: @lanyard.shipping_price,
      weight: @lanyard.weight } }
    assert_redirected_to lanyard_url(@lanyard)
  end

  test "can mark lanyard inactive" do
    assert_difference('@lanyard.lanyard_group.active_lanyard_count', -1) do
      put lanyard_url(@lanyard), params: { lanyard: { active: false } }
      assert_redirected_to lanyard_url(@lanyard)
      follow_redirect!
      assert_select 'dd', /No/
    end
  end

  test "should destroy lanyard" do
    assert_difference('Lanyard.count', -1) do
      delete lanyard_url(@lanyard)
    end

    assert_redirected_to lanyards_url
  end
end
