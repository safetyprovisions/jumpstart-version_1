require 'test_helper'

class GroupTrainingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @group_training = group_trainings(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get group_trainings_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=group&q%5B%5D=trainings"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get group_trainings_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get group_trainings_url
    assert_response :success
  end

  test "can access new group_training page" do
    get new_group_training_url
    assert_response :success
  end

  test "can create a new group_training" do
    assert_difference('GroupTraining.count') do
      @online_course = online_courses(:two)
      post group_trainings_url, params: { group_training: { online_course_id: @online_course.id } }
    end

    assert_redirected_to group_training_url(GroupTraining.last)
  end

  test "cannot create two identical group_trainings" do
    @online_course = online_courses(:two)
    assert_difference('GroupTraining.count', 1) do
      post group_trainings_url, params: { group_training: { online_course_id: @online_course.id } }
      post group_trainings_url, params: { group_training: { online_course_id: @online_course.id } }
    end
  end

  test "cannot create group_training without online_course_id" do
    @online_course = online_courses(:two)
    assert_no_difference('GroupTraining.count') do
      post group_trainings_url, params: { group_training: { online_course_id: '' } }
    end
  end

  test "can view group_training" do
    get group_training_url(@group_training)
    assert_response :success
  end

  test "can edit group_training" do
    get edit_group_training_url(@group_training)
    assert_response :success
  end

  test "can update group_training" do
    @online_course = online_courses(:three)
    put group_training_url(@group_training), params: { group_training: { online_course_id: @online_course.id } }
    assert_redirected_to group_training_url(@group_training)
    @group_training.reload
    assert_equal "Online Course Three", @group_training.name
  end

  test "cannot update group_training with invalid attributes" do
    put group_training_url(@group_training), params: { group_training: { online_course_id: 57 } }
    assert_response :success
    assert_select "h1", "Edit Group Training"
  end

  test "can destroy group_training" do
    assert_difference('GroupTraining.count', -1) do
      delete group_training_url(@group_training)
    end
    assert_redirected_to group_trainings_url
  end

  # TODO When Shrine error is fixed, we need to add a test that makes GroupTraining.first.online_course.active == false, and then verifies that the GroupTraining does not show up on the ProductGroup page.
end
