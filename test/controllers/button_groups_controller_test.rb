require 'test_helper'

class ButtonGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @button_group = button_groups(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "superadmin should get index" do
    get button_groups_url
    assert_response :success
  end

  test "regular user should get index" do
    sign_out :user
    sign_in users(:regular)
    get button_groups_url
    assert_response :success
  end

  test "superadmin should get new" do
    get new_button_group_url
    assert_response :success
  end

  test "regular user should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_button_group_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create button_group" do
    assert_difference('ButtonGroup.count') do
      post button_groups_url, params: { button_group: {
        active: @button_group.active,
        description: @button_group.description,
        image_data: @button_group.image_data,
        keywords: @button_group.keywords,
        meta_description: @button_group.meta_description,
        name: @button_group.name
        }
      }
    end

    assert_redirected_to button_group_url(ButtonGroup.last)
  end

  test "should show button_group to superadmin" do
    get button_group_url(@button_group)
    assert_response :success
  end

  test "should show button_group to regular user" do
    sign_out :user
    sign_in users(:regular)
    get button_group_url(@button_group)
    assert_response :success
  end

  test "superadmin should get edit" do
    get edit_button_group_url(@button_group)
    assert_response :success
  end

  test "regular user should not get edit" do
    sign_out :user
    sign_in users(:regular)
    get edit_button_group_url(@button_group)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update button_group" do
    patch button_group_url(@button_group), params: { button_group: { active: @button_group.active, description: @button_group.description, image_data: @button_group.image_data, keywords: @button_group.keywords, meta_description: @button_group.meta_description, name: @button_group.name, slug: @button_group.slug } }
    assert_redirected_to button_group_url(@button_group)
  end

  test "can mark button_group inactive" do
    patch button_group_url(@button_group), params: { button_group: { active: false } }
    assert_redirected_to button_group_url(@button_group)
    follow_redirect!
    assert_select 'h5', /This item is currently unavailable./
  end

  # test "should destroy button_group" do
  #   assert_difference('ButtonGroup.count', -1) do
  #     delete button_group_url(@button_group)
  #   end

  #   assert_redirected_to button_groups_url
  # end
end
