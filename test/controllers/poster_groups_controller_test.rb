require 'test_helper'

class PosterGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @poster_group = poster_groups(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get poster_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_poster_group_url
    assert_response :success
  end

  test "regular user should get index" do
    sign_out :user
    sign_in users(:regular)
    get poster_groups_url
    assert_response :success
  end

  test "regular user should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_poster_group_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create poster_group" do
    assert_difference('PosterGroup.count') do
      post poster_groups_url, params: { poster_group: {
        description: @poster_group.description,
        keywords: @poster_group.keywords,
        meta_description: @poster_group.meta_description,
        name: @poster_group.name,
        image: cached_poster_group_image.to_json } }
    end

    assert_redirected_to poster_group_url(PosterGroup.last)
  end

  test "should show poster_group to superadmin" do
    get poster_group_url(@poster_group)
    assert_response :success
  end

  test "should show poster_group to regular user" do
    sign_out :user
    sign_in users(:regular)
    get poster_group_url(@poster_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_poster_group_url(@poster_group)
    assert_response :success
  end

  test "regular user should not get edit" do
    sign_out :user
    sign_in users(:regular)
    get edit_poster_group_url(@poster_group)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update poster_group" do
    patch poster_group_url(@poster_group), params: { poster_group: {
      description: @poster_group.description,
      keywords: @poster_group.keywords,
      meta_description: @poster_group.meta_description,
      name: @poster_group.name,
      # image: cached_poster_group_image.to_json
      }
    }
    assert_redirected_to poster_group_url(@poster_group)
  end

  # test "should destroy poster_group" do
  #   assert_difference('PosterGroup.count', -1) do
  #     delete poster_group_url(@poster_group)
  #   end

  #   assert_redirected_to poster_groups_url
  # end

  test "can mark poster_group inactive" do
    put poster_group_url(@poster_group), params: { poster_group: { active: false } }
    assert_redirected_to poster_group_url(@poster_group)
    follow_redirect!
    assert_select 'h5', /This training is currently unavailable./
  end
end
