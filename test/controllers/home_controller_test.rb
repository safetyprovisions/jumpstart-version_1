require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  setup do
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "anyone can access root" do
    get root_url
    assert_response :success
  end

  test "anyone can access terms" do
    get terms_url
    assert_response :success
  end

  test "anyone can access privacy" do
    get privacy_url
    assert_response :success
  end

  test "anyone can access about" do
    get about_url
    assert_response :success
  end

  test "anyone can access contact" do
    get contact_us_url
    assert_response :success
  end

  test "anyone can access faq" do
    get faq_url
    assert_response :success
  end

  test "anyone can access training_types" do
    sign_in users(:superadmin)
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 9500, tier: 1 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 50, price: 7500, cad_price: 9500, tier: 1 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 100, price: 7500, cad_price: 9500, tier: 1 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 1, price: 3900, cad_price: 3900, tier: 2 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 2500, cad_price: 3500, tier: 2 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 50, price: 2000, cad_price: 3000, tier: 2 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 100, price: 1500, cad_price: 2500, tier: 2 } }
    sign_out :user
    sign_in users(:regular)
    get training_types_url
    assert_response :success
  end

  test "anyone can access onsite_training" do
    get onsite_request_url
    assert_response :success
  end

  test "anyone can submit send_contact_email (onsite_training request)" do
    post send_contact_email_url, params: { contact_form: { name: "Bob Barker", email: "bobbarker@test.com", phone: "9999999999", company: "Bob's Company", equipment_type: "Stuff", training_type: "operator", number_of_operators: "4", number_of_trainers: "0", location: "ascsdc", note: "eced", nickname: "" } }
    assert_redirected_to onsite_request_url
    follow_redirect!
    assert_select "body", /Your request has been sent./
  end

  test "user can update_preferred_currency" do
    sign_in users(:regular)
    assert_equal "usd", users(:regular).preferred_currency
    post update_preferred_currency_url, params: { source: 'user', url: root_url, preferred_currency: 'cad' }
    assert_redirected_to root_url
    assert_equal "cad", users(:regular).preferred_currency
  end

  test "anyone can access topics page" do
    get training_topics_url
    assert_response :success
  end

  test "anyone can access diy_training_kits page" do
    get diy_training_kits_url
    assert_response :success
  end

  test "anyone can access osha_training_kits page" do
    get osha_training_kits_url
    assert_response :success
  end

  test "anyone can access canadian_training_kits page" do
    get canadian_training_kits_url
    assert_response :success
  end

  test "anyone can access spanish_training_kits page" do
    get spanish_training_kits_url
    assert_response :success
  end

  test "anyone can access tagalog_training_kits page" do
    get tagalog_training_kits_url
    assert_response :success
  end

  test "anyone can access all_online_courses page" do
    get all_online_courses_url
    assert_response :success
  end

  test "anyone can access osha_online_courses page" do
    get osha_online_courses_url
    assert_response :success
  end

  test "anyone can access canadian_online_courses page" do
    get canadian_online_courses_url
    assert_response :success
  end

  test "anyone can access spanish_online_courses page" do
    get spanish_online_courses_url
    assert_response :success
  end

  test "anyone can access tagalog_online_courses page" do
    get tagalog_online_courses_url
    assert_response :success
  end

  test "anyone can access all_train_the_trainer_courses page" do
    get all_train_the_trainer_courses_url
    assert_response :success
  end

  test "anyone can access osha_train_the_trainer_courses page" do
    get osha_train_the_trainer_courses_url
    assert_response :success
  end

  test "anyone can access canadian_train_the_trainer_courses page" do
    get canadian_train_the_trainer_courses_url
    assert_response :success
  end

  test "anyone can access spanish_train_the_trainer_courses page" do
    get spanish_train_the_trainer_courses_url
    assert_response :success
  end

  test "anyone can access tagalog_train_the_trainer_courses page" do
    get tagalog_train_the_trainer_courses_url
    assert_response :success
  end

  test "anyone can access all_group_trainings page" do
    sign_in users(:superadmin)
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 7500, cad_price: 9500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 6500, cad_price: 8500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 4500, cad_price: 6500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 1, price: 2900, cad_price: 3900, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 2500, cad_price: 3500, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 2000, cad_price: 3000, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 1500, cad_price: 2500, tier: 2 }
    }
    sign_out :user
    sign_in users(:regular)
    get all_group_trainings_url
    assert_response :success
  end

  test "anyone can access osha_group_trainings page" do
    sign_in users(:superadmin)
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 7500, cad_price: 9500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 6500, cad_price: 8500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 4500, cad_price: 6500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 1, price: 2900, cad_price: 3900, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 2500, cad_price: 3500, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 2000, cad_price: 3000, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 1500, cad_price: 2500, tier: 2 }
    }
    sign_out :user
    sign_in users(:regular)
    get osha_group_trainings_url
    assert_response :success
  end

  test "anyone can access canadian_group_trainings page" do
    sign_in users(:superadmin)
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 7500, cad_price: 9500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 6500, cad_price: 8500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 4500, cad_price: 6500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 1, price: 2900, cad_price: 3900, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 2500, cad_price: 3500, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 2000, cad_price: 3000, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 1500, cad_price: 2500, tier: 2 }
    }
    sign_out :user
    sign_in users(:regular)
    get canadian_group_trainings_url
    assert_response :success
  end

  test "anyone can access spanish_group_trainings page" do
    sign_in users(:superadmin)
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 7500, cad_price: 9500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 6500, cad_price: 8500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 4500, cad_price: 6500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 1, price: 2900, cad_price: 3900, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 2500, cad_price: 3500, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 2000, cad_price: 3000, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 1500, cad_price: 2500, tier: 2 }
    }
    sign_out :user
    sign_in users(:regular)
    get spanish_group_trainings_url
    assert_response :success
  end

  test "anyone can access tagalog_group_trainings page" do
    sign_in users(:superadmin)
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 7500, cad_price: 9500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 6500, cad_price: 8500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 4500, cad_price: 6500, tier: 1 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 1, price: 2900, cad_price: 3900, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 25, price: 2500, cad_price: 3500, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 50, price: 2000, cad_price: 3000, tier: 2 }
    }
    post bulk_price_levels_url, params: { bulk_price_level:
      { qty: 100, price: 1500, cad_price: 2500, tier: 2 }
    }
    sign_out :user
    sign_in users(:regular)
    get tagalog_group_trainings_url
    assert_response :success
  end

end


# TODO Still need to fix these tests:

  # test "search works" do
  #   post "http://localhost:3000/search?utf8=%E2%9C%93&q=Online+Course+One", params: { q: online_courses(:one).title, utf8: "✓" }
  #   assert_redirected_to "http://localhost:3000/search?utf8=%E2%9C%93&q=Online+Course+One"
  #   follow_redirect!
  #   assert_select "body", /Search Results/
  #   assert_select "body", online_courses(:one).title
  # end
