require 'test_helper'

class BackupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @backup = backups(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get backups_url
    assert_redirected_to "http://localhost:3000/search?q=backups"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get backups_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get backups_url
    assert_response :success
  end

  test "can access new backup page" do
    get new_backup_url
    assert_response :success
  end

  test "can create a new backup" do
    assert_difference('Backup.count') do
      post backups_url, params: { backup: { format: "USB2", price: 2500, cad_price: 3500, shipping_price: 1000, priority_shipping: 1500, intl_shipping: 1500,  } }
    end

    assert_redirected_to backup_url(Backup.last)
  end

  test "cannot create two identical backups" do
    assert_difference('Backup.count', 1) do
      post backups_url, params: { backup: { format: "CD", price: 2500, cad_price: 3500, shipping_price: 1000, priority_shipping: 1500, intl_shipping: 1500,  } }
      post backups_url, params: { backup: { format: "CD", price: 2500, cad_price: 3500, shipping_price: 1000, priority_shipping: 1500, intl_shipping: 1500,  } }
    end
  end

  test "cannot create backup without format" do
    assert_no_difference('Backup.count') do
      post backups_url, params: { backup: { format: "", price: 2500, cad_price: 3500, shipping_price: 1000, priority_shipping: 1500, intl_shipping: 1500,  } }
    end
  end

  test "cannot create backup with price less than 50 cents" do
    assert_no_difference('Backup.count') do
      post backups_url, params: { backup: { format: "USB", price: 49, cad_price: 3500, shipping_price: 1000, priority_shipping: 1500, intl_shipping: 1500,  } }
    end
  end

  test "cannot create backup with cad_price less than 50 cents" do
    assert_no_difference('Backup.count') do
      post backups_url, params: { backup: { format: "USB", price: 2500, cad_price: 49, shipping_price: 1000, priority_shipping: 1500, intl_shipping: 1500,  } }
    end
  end

  test "cannot create backup with shipping_price less than 50 cents" do
    assert_no_difference('Backup.count') do
      post backups_url, params: { backup: { format: "USB", price: 2500, cad_price: 3500, shipping_price: 49, priority_shipping: 1500, intl_shipping: 1500,  } }
    end
  end

  test "cannot create backup with priority_shipping less than 50 cents" do
    assert_no_difference('Backup.count') do
      post backups_url, params: { backup: { format: "USB", price: 2500, cad_price: 3500, shipping_price: 1000, priority_shipping: 49, intl_shipping: 1500,  } }
    end
  end

  test "cannot create backup with intl_shipping less than 50 cents" do
    assert_no_difference('Backup.count') do
      post backups_url, params: { backup: { format: "USB", price: 2500, cad_price: 3500, shipping_price: 1000, priority_shipping: 1500, intl_shipping: 49,  } }
    end
  end

  test "can view backup" do
    get backup_url(@backup)
    assert_response :success
  end

  test "can edit backup" do
    get edit_backup_url(@backup)
    assert_response :success
  end

  test "can update backup" do
    put backup_url(@backup), params: { backup: { price: 7500 } }
    assert_redirected_to backup_url(@backup)
    @backup.reload
    assert_equal 7500, @backup.price
  end

  test "can mark backup inactive" do
    put backup_url(@backup), params: { backup: { active: false } }
    assert_redirected_to backup_url(@backup)
    follow_redirect!
    assert_select 'dd', /No/
  end

  test "cannot update backup with invalid attributes" do
    put backup_url(@backup), params: { backup: { cad_price: "ABC"} }
    assert_response :success
    assert_select "h1", "Edit Backup"
  end

  test "can destroy backup" do
    assert_difference('Backup.count', -1) do
      delete backup_url(@backup)
    end
    assert_redirected_to backups_url
  end
end
