require 'test_helper'

class ShirtsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @shirt = shirts(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get shirts_url
    assert_response :success
  end

  test "should get new" do
    get new_shirt_url
    assert_response :success
  end

  test "regular users should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_shirt_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "anonymous users should not get new" do
    sign_out :user
    get new_shirt_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create shirt" do
    assert_difference('Shirt.count') do
      post shirts_url, params: { shirt: {
        name: @shirt.name,
        price: @shirt.price,
        cad_price: @shirt.cad_price,
        description: @shirt.description,
        image: cached_shirt_image.to_json,
        intl_shipping: @shirt.intl_shipping,
        keywords: @shirt.keywords,
        meta_description: @shirt.meta_description,
        priority_shipping: @shirt.priority_shipping,
        shipping_price: @shirt.shipping_price,
        weight: @shirt.weight } }
    end

    assert_redirected_to shirt_url(Shirt.last)
  end

  test "should not create shirt without name" do
    assert_no_difference('Shirt.count') do
      post shirts_url, params: { shirt: {
        name: "",
        price: @shirt.price,
        cad_price: @shirt.cad_price,
        description: @shirt.description,
        image: cached_shirt_image.to_json,
        intl_shipping: @shirt.intl_shipping,
        keywords: @shirt.keywords,
        meta_description: @shirt.meta_description,
        priority_shipping: @shirt.priority_shipping,
        shipping_price: @shirt.shipping_price,
        weight: @shirt.weight } }
    end
  end

  test "should not create shirt without description" do
    assert_no_difference('Shirt.count') do
      post shirts_url, params: { shirt: {
        name: @shirt.name,
        price: @shirt.price,
        cad_price: @shirt.cad_price,
        description: "",
        image: cached_shirt_image.to_json,
        intl_shipping: @shirt.intl_shipping,
        keywords: @shirt.keywords,
        meta_description: @shirt.meta_description,
        priority_shipping: @shirt.priority_shipping,
        shipping_price: @shirt.shipping_price,
        weight: @shirt.weight } }
    end
  end

  test "should not create shirt without image" do
    # assert_no_difference('Shirt.count') do
    assert_raises Shrine::Error do
      post shirts_url, params: { shirt: {
        name: @shirt.name,
        price: @shirt.price,
        cad_price: @shirt.cad_price,
        description: @shirt.description,
        image: "",
        intl_shipping: @shirt.intl_shipping,
        keywords: @shirt.keywords,
        meta_description: @shirt.meta_description,
        priority_shipping: @shirt.priority_shipping,
        shipping_price: @shirt.shipping_price,
        weight: @shirt.weight } }
    end
  end

  test "should not create shirt without keywords" do
    assert_no_difference('Shirt.count') do
      post shirts_url, params: { shirt: {
        name: @shirt.name,
        price: @shirt.price,
        cad_price: @shirt.cad_price,
        description: @shirt.description,
        image: cached_shirt_image.to_json,
        intl_shipping: @shirt.intl_shipping,
        keywords: "",
        meta_description: @shirt.meta_description,
        priority_shipping: @shirt.priority_shipping,
        shipping_price: @shirt.shipping_price,
        weight: @shirt.weight } }
    end
  end

  test "should not create shirt without meta_description" do
    assert_no_difference('Shirt.count') do
      post shirts_url, params: { shirt: {
        name: @shirt.name,
        price: @shirt.price,
        cad_price: @shirt.cad_price,
        description: @shirt.description,
        image: cached_shirt_image.to_json,
        intl_shipping: @shirt.intl_shipping,
        keywords: @shirt.keywords,
        meta_description: "",
        priority_shipping: @shirt.priority_shipping,
        shipping_price: @shirt.shipping_price,
        weight: @shirt.weight } }
    end
  end

  test "should show shirt" do
    get shirt_url(@shirt)
    assert_response :success
  end

  test "should get edit" do
    get edit_shirt_url(@shirt)
    assert_response :success
  end

  test "should update shirt" do
    patch shirt_url(@shirt), params: { shirt: {
      cad_price: @shirt.cad_price,
      description: @shirt.description,
      image: @shirt.image_data,
      intl_shipping: @shirt.intl_shipping,
      keywords: @shirt.keywords,
      meta_description: @shirt.meta_description,
      name: @shirt.name,
      price: @shirt.price,
      priority_shipping: @shirt.priority_shipping,
      shipping_price: @shirt.shipping_price,
      slug: @shirt.slug,
      weight: @shirt.weight } }
    assert_redirected_to shirt_url(@shirt)
  end

  test "should destroy shirt" do
    assert_difference('Shirt.count', -1) do
      delete shirt_url(@shirt)
    end

    assert_redirected_to shirts_url
  end

  test "can mark shirt inactive" do
    patch shirt_url(@shirt), params: { shirt: { active: false } }
    assert_redirected_to shirt_url(@shirt)
    follow_redirect!
    assert_select "h5", /This item is currently unavailable./
  end
end
