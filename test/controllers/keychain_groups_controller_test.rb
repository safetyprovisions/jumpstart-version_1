require 'test_helper'

class KeychainGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @keychain_group = keychain_groups(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get keychain_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_keychain_group_url
    assert_response :success
  end

  test "should not get index for regular users" do
    sign_out :user
    sign_in users(:regular)
    get keychain_groups_url
    assert_response :redirect
  end

  test "should not get new for regular users" do
    sign_out :user
    sign_in users(:regular)
    get new_keychain_group_url
    assert_response :redirect
  end

  test "should create keychain_group" do
    assert_difference('KeychainGroup.count') do
      post keychain_groups_url, params: { keychain_group: { description: @keychain_group.description,
        image: cached_keychain_group_image.to_json,
        # image_data: @keychain_group.image_data,
        name: @keychain_group.name } }
    end

    assert_redirected_to keychain_group_url(KeychainGroup.last)
  end

  test "should show keychain_group" do
    get keychain_group_url(@keychain_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_keychain_group_url(@keychain_group)
    assert_response :success
  end

  test "should update keychain_group" do
    patch keychain_group_url(@keychain_group), params: { keychain_group: { description: @keychain_group.description, image_data: @keychain_group.image_data, name: @keychain_group.name } }
    # assert_redirected_to keychain_group_url(@keychain_group)
    assert_redirected_to "http://localhost:3000/keychain_groups/#{ @keychain_group.slug }"
  end

  # test "should destroy keychain_group" do
  #   assert_difference('KeychainGroup.count', -1) do
  #     delete keychain_group_url(@keychain_group)
  #   end

  #   assert_redirected_to keychain_groups_url
  # end

  test "can mark keychain_group inactive" do
    patch keychain_group_url(@keychain_group), params: { keychain_group: { active: false } }
    # assert_redirected_to keychain_group_url(@keychain_group)
    assert_redirected_to "http://localhost:3000/keychain_groups/#{ @keychain_group.slug }"
    follow_redirect!
    assert_select 'h5', /This item is currently unavailable./
  end
end
