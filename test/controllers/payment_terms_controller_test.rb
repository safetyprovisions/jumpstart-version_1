require 'test_helper'

class PaymentTermsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @payment_term = payment_terms(:one)
    @company_id = companies(:one).id
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get payment_terms_url
    assert_response :redirect
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=payment&q%5B%5D=terms"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get payment_terms_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get payment_terms_url
    assert_response :success
  end

  test "can access new payment_term page" do
    get new_payment_term_url
    assert_response :success
  end

  test "can create a new payment_term" do
    assert_difference('PaymentTerm.count') do
      post payment_terms_url, params: { payment_term: { company_id: @company_id, term: 45 } }
    end

    assert_redirected_to payment_term_url(PaymentTerm.last)
  end

  test "cannot create two identical payment_terms" do
    assert_difference('PaymentTerm.count', 1) do
      post payment_terms_url, params: { payment_term: { company_id: @company_id, term: 45 } }
      post payment_terms_url, params: { payment_term: { company_id: @company_id, term: 45 } }
    end
  end

  test "cannot create payment_term without company_id" do
    assert_no_difference('PaymentTerm.count') do
      post payment_terms_url, params: { payment_term: { company_id: '', term: 45 } }
    end
  end

  test "cannot create payment_term without term" do
    assert_no_difference('PaymentTerm.count') do
      post payment_terms_url, params: { payment_term: { company_id: @company_id, term: '' } }
    end
  end

  test "can view payment_term" do
    get payment_term_url(@payment_term)
    assert_response :success
  end

  test "can edit payment_term" do
    get edit_payment_term_url(@payment_term)
    assert_response :success
  end

  test "can update payment_term" do
    put payment_term_url(@payment_term), params: { payment_term: { term: 45 } }
    assert_redirected_to payment_term_url(@payment_term)
    @payment_term.reload
    assert_equal 45, @payment_term.term
  end

  test "cannot update payment_term with invalid attributes" do
    put payment_term_url(@payment_term), params: { payment_term: { term: "abc"} }
    assert_response :success
    assert_select "h1", "Edit Payment Term"
  end

  test "can destroy payment_term" do
    assert_difference('PaymentTerm.count', -1) do
      delete payment_term_url(@payment_term)
    end
    assert_redirected_to payment_terms_url
  end
end
