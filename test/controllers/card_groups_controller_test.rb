require 'test_helper'

class CardGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @card_group = card_groups(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get card_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_card_group_url
    assert_response :success
  end

  test "regular users should not get index" do
    sign_out :user
    sign_in users(:regular)
    get card_groups_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=card&q%5B%5D=groups"
  end

  test "regular users should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_card_group_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "anonymous users should not get index" do
    sign_out :user
    get card_groups_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=card&q%5B%5D=groups"
  end

  test "anonymous users should not get new" do
    sign_out :user
    get new_card_group_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create card_group" do
    assert_difference('CardGroup.count') do
      post card_groups_url, params: { card_group: {
        description: @card_group.description,
        image_data: @card_group.image_data,
        keywords: @card_group.keywords,
        meta_description: @card_group.meta_description,
        name: @card_group.name + "2" } }
    end

    assert_redirected_to card_group_url(CardGroup.last)
  end

  test "should show card_group" do
    get card_group_url(@card_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_card_group_url(@card_group)
    assert_response :success
  end

  test "should update card_group" do
    patch card_group_url(@card_group), params: { card_group: {
      description: @card_group.description,
      image_data: @card_group.image_data,
      keywords: @card_group.keywords,
      meta_description: @card_group.meta_description,
      name: @card_group.name,
      slug: @card_group.slug } }
    assert_redirected_to card_group_url(@card_group)
  end

  test "can mark card_group inactive" do
    patch card_group_url(@card_group), params: { card_group: { active: false } }
    assert_redirected_to card_group_url(@card_group)
    follow_redirect!
    assert_select 'h5', /This item is currently unavailable./
  end

  # test "should destroy card_group" do
  #   assert_difference('CardGroup.count', -1) do
  #     delete card_group_url(@card_group)
  #   end

  #   assert_redirected_to card_groups_url
  # end
end
