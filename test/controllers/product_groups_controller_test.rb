require 'test_helper'

class ProductGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @product_group = product_groups(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get product_groups_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=product&q%5B%5D=groups"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get product_groups_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get product_groups_url
    assert_response :success
  end

  test "can access new product_group page" do
    get new_product_group_url
    assert_response :success
  end

  test "can create a new product_group" do
    assert_difference('ProductGroup.count') do
      post product_groups_url, params: { product_group: {
        name: "Test Product Group",
        image_data: @product_group.image_data,
        video: 'MjA0NjIw',
        overview: "Text",
        training_standards: "Text",
        facts: "Text",
        fails: "Text",
        slug: "test-product-group"
       } }
    end

    assert_redirected_to "http://localhost:3000/product_groups/test-product-group/edit#product-group-categories"
  end

  test "cannot create product_group without name" do
    assert_no_difference('ProductGroup.count') do
      post product_groups_url, params: { product_group: {
        name: "",
        image_data: @product_group.image_data,
        video: 'MjA0NjIw',
        overview: "Text",
        training_standards: "Text",
        facts: "Text",
        fails: "Text",
        slug: "test-product-group"
       } }
    end
  end

  test "cannot create product_group without image" do
    assert_raises Shrine::Error do
    # assert_no_difference('ProductGroup.count') do
      post product_groups_url, params: { product_group: {
        name: "Test Product Group",
        image: '',
        video: 'MjA0NjIw',
        overview: "Text",
        training_standards: "Text",
        facts: "Text",
        fails: "Text",
        slug: "test-product-group"
       } }
    end
  end

  test "cannot create product_group without video" do
    assert_no_difference('ProductGroup.count') do
      post product_groups_url, params: { product_group: {
        name: "Test Product Group",
        image_data: @product_group.image_data,
        video: '',
        overview: "Text",
        training_standards: "Text",
        facts: "Text",
        fails: "Text",
        slug: "test-product-group"
       } }
    end
  end

  test "cannot create product_group without overview" do
    assert_no_difference('ProductGroup.count') do
      post product_groups_url, params: { product_group: {
        name: "Test Product Group",
        image_data: @product_group.image_data,
        video: 'MjA0NjIw',
        overview: "",
        training_standards: "Text",
        facts: "Text",
        fails: "Text",
        slug: "test-product-group"
       } }
    end
  end

  test "cannot create product_group without training_standards" do
    assert_no_difference('ProductGroup.count') do
      post product_groups_url, params: { product_group: {
        name: "Test Product Group",
        image_data: @product_group.image_data,
        video: 'MjA0NjIw',
        overview: "Text",
        training_standards: "",
        facts: "Text",
        fails: "Text",
        slug: "test-product-group"
       } }
    end
  end

  test "cannot create product_group without facts" do
    assert_no_difference('ProductGroup.count') do
      post product_groups_url, params: { product_group: {
        name: "Test Product Group",
        image_data: @product_group.image_data,
        video: 'MjA0NjIw',
        overview: "Text",
        training_standards: "Text",
        facts: "",
        fails: "Text",
        slug: "test-product-group"
       } }
    end
  end

  test "cannot create product_group without fails" do
    assert_no_difference('ProductGroup.count') do
      post product_groups_url, params: { product_group: {
        name: "Test Product Group",
        image_data: @product_group.image_data,
        video: 'MjA0NjIw',
        overview: "Text",
        training_standards: "Text",
        facts: "Text",
        fails: "",
        slug: "test-product-group"
       } }
    end
  end

  test "can add global tag" do
    post add_tag_global_url, params: { tag_to_add: 'icanhascheezburger' }
    assert_redirected_to user_path(users(:superadmin))
    assert_equal ActsAsTaggableOn::Tag.last.name, "ICANHASCHEEZBURGER"
  end

  test "can remove global tag" do
    post add_tag_global_url, params: { tag_to_add: 'icanhascheezburger' }
    post remove_tag_global_url, params: { tag_id: ActsAsTaggableOn::Tag.last.id, id: users(:superadmin).id }
    assert_redirected_to user_path(users(:superadmin))
    assert_equal ActsAsTaggableOn::Tag.count, 0
  end

  test "can add a tag to an existing product_group" do
    get add_tag_url, params: { id: @product_group.id, tag: "Updated Text" }
    assert_redirected_to edit_product_group_url(@product_group)
    follow_redirect!
    assert_select "div.container", "×
      Tag was successfully added."
  end

  test "can view product_group" do
    # NOTE Create bulk prices so it can display
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 9500, tier: 1 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 50, price: 7500, cad_price: 9500, tier: 1 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 100, price: 7500, cad_price: 9500, tier: 1 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 1, price: 7500, cad_price: 9500, tier: 2 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 9500, tier: 2 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 50, price: 7500, cad_price: 9500, tier: 2 } }
    post bulk_price_levels_url, params: { bulk_price_level: { qty: 100, price: 7500, cad_price: 9500, tier: 2 } }
    get product_group_url(@product_group)
    assert_response :success
  end

  test "can edit product_group" do
    get edit_product_group_url(@product_group)
    assert_response :success
  end

  test "can update product_group" do
    put product_group_url(@product_group), params: { product_group: { overview: "Updated Text" } }
    assert_redirected_to edit_product_group_url(@product_group)
    follow_redirect!
    assert_select "div.container", "×
      You must add at least one Category!"
  end

  test "cannot update product_group with invalid attributes" do
    put product_group_url(@product_group), params: { product_group: { name: ""} }
    assert_redirected_to edit_product_group_url(@product_group)
    follow_redirect!
    assert_select "h1", "Edit Product Group"
  end

  # TODO Do we need to soft delete these, or what? Or can we just use the method below to make them inactive, and call it good?
  # test "can destroy product_group" do
  #   assert_difference('ProductGroup.count', -1) do
  #     delete product_group_url(@product_group)
  #   end
  #   assert_redirected_to product_groups_url
  # end

  test "can mark product_group inactive" do
    put product_group_url(@product_group), params: { product_group: { active: false } }
    assert_redirected_to edit_product_group_url(@product_group)
    follow_redirect!
    assert_select "body", /You must add at least one Category!/
  end
end
