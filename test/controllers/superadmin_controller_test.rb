require 'test_helper'

class SuperadminControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @company_id = companies(:one).id
    @course_id  = online_courses(:one).talent_course_id
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "can create a new non_catalog_course_access" do
    assert_difference('NonCatalogCourseAccess.count') do
      # params[:course] is Talent Course ID
      post assign_non_catalog_course_url, params: { company: "#{ @company_id }", course: "#{ @course_id }"}
    end

    assert_redirected_to user_url(users(:superadmin))
  end

  test "can reset download window" do
    @order = orders(:two)
    post reset_download_window_url, params: { order: @order.id }
    assert_redirected_to user_url(users(:superadmin))
    follow_redirect!
    assert_select 'body', /Download window has been reset!/
  end
end
