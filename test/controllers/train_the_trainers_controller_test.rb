require 'test_helper'

class TrainTheTrainersControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @train_the_trainer = train_the_trainers(:one)
    @product_group_id = product_groups(:one).id
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get train_the_trainers_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=train&q%5B%5D=the&q%5B%5D=trainers"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get train_the_trainers_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get train_the_trainers_url
    assert_response :success
  end

  test "can access new train_the_trainer page" do
    get new_train_the_trainer_url
    assert_response :success
  end

  test "can create a new train_the_trainer" do
    assert_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: 65000,
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: 85000,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end

    assert_redirected_to train_the_trainer_url(TrainTheTrainer.last)
  end

  test "cannot create two identical train_the_trainers" do
    assert_difference('TrainTheTrainer.count', 1) do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Train The Trainer Example",
        price: 70000,
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Train The Trainer Example",
        price: 70000,
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without title" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "",
        price: 70000,
        sale_price: 65000,
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: 85000,
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without price" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: '',
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without cad_price" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: '',
        cad_sale_price: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without language" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: '',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without standards" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: '',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without product_group_id" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: '',
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without talent_course_id" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: '',
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without practical_evaluation" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: '',
        practical_evaluation: '',
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without exam" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: '',
        answer_key: cached_answer_key.to_json
       } }
    end
  end

  test "cannot create train_the_trainer without answer_key" do
    assert_no_difference('TrainTheTrainer.count') do
      post train_the_trainers_url, params: { train_the_trainer: {
        title: "New Test Train The Trainer",
        price: 70000,
        sale_price: '',
        sale_start: '',
        sale_expiry: '',
        language: 'english',
        standards: 'osha',
        product_group_id: product_groups(:one).id,
        talent_course_id: 124,
        kit_id: kits(:one).id,
        cad_price: 90000,
        cad_sale_price: '',
        practical_evaluation: cached_practical_evaluation.to_json,
        exam: cached_exam.to_json,
        answer_key: ''
       } }
    end
  end

  test "can view train_the_trainer" do
    get train_the_trainer_url(@train_the_trainer)
    assert_response :success
  end

  test "can edit train_the_trainer" do
    get edit_train_the_trainer_url(@train_the_trainer)
    assert_response :success
  end

  test "can update train_the_trainer" do
    patch train_the_trainer_url(@train_the_trainer), params: { train_the_trainer: { title: "New Name", product_group_id: @product_group_id } }
    assert_redirected_to train_the_trainer_url(@train_the_trainer)
    @train_the_trainer.reload
    assert_equal "New Name", @train_the_trainer.title
  end

  test "cannot update train_the_trainer with invalid attributes" do
    patch train_the_trainer_url(@train_the_trainer), params: { train_the_trainer: { title: ""} }
    assert_response :success
    assert_select "h1", "Edit Train The Trainer"
  end

  # TODO How do we delete these? Or, is it enough to use the method below to simply make them inactive?
  # test "can destroy train_the_trainer" do
  #   assert_difference('TrainTheTrainer.count', -1) do
  #     delete train_the_trainer_url(@train_the_trainer)
  #   end
  #   assert_redirected_to train_the_trainers_url
  # end

  test "can mark train_the_trainer inactive" do
    assert_difference(@active_train_the_trainer_count, -1) do
      patch train_the_trainer_url(@train_the_trainer), params: { train_the_trainer: {
          active: false
        }
      }
    end
  end
end
