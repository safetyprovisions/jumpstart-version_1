require 'test_helper'

class PostersControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @poster = posters(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get posters_url
    assert_response :success
  end

  test "regular user should not get index" do
    sign_out :user
    sign_in users(:regular)
    get posters_url
    assert_redirected_to "http://localhost:3000/search?q=posters"
  end

  test "should get new" do
    get new_poster_url
    assert_response :success
  end

  test "regular user should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_poster_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create poster" do
    assert_difference('Poster.count') do
      post posters_url, params: { poster: {
        name: "Newly Created Poster",
        description: @poster.description,
        poster_group_id: @poster.poster_group_id,
        price: @poster.price,
        cad_price: @poster.cad_price,
        display_format: @poster.display_format,
        download_data: @poster.download_data,
        image_data: @poster.image_data,
        intl_shipping: @poster.intl_shipping,
        priority_shipping: @poster.priority_shipping,
        shipping_price: @poster.shipping_price,
        active: true
        }
      }
    end

    assert_redirected_to poster_url(Poster.last)
  end

  test "should show poster" do
    get poster_url(@poster)
    assert_response :success
    assert_select "h1", @poster.name
  end

  test "should get edit" do
    get edit_poster_url(@poster)
    assert_response :success
  end

  test "should update poster" do
    patch poster_url(@poster), params: { poster: {
        name: @poster.name,
        description: @poster.description,
        poster_group_id: @poster.poster_group_id,
        price: @poster.price + 100,
        cad_price: @poster.cad_price,
        display_format: @poster.display_format,
        download_data: @poster.download_data,
        image_data: @poster.image_data,
        intl_shipping: @poster.intl_shipping,
        priority_shipping: @poster.priority_shipping,
        shipping_price: @poster.shipping_price
      }
    }
    assert_redirected_to "http://localhost:3000/posters/poster1"
    @poster.reload
    assert_equal 101, @poster.price
  end

  test "can mark poster inactive" do
    patch poster_url(@poster), params: { poster: { active: false } }
    assert_redirected_to "http://localhost:3000/posters/poster1"
    follow_redirect!
    assert_select "h5", /This item is currently unavailable./
  end

  test "should destroy poster" do
    assert_difference('Poster.count', -1) do
      delete poster_url(@poster)
    end

    assert_redirected_to posters_url
  end
end
