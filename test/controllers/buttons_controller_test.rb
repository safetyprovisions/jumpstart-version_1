require 'test_helper'

class ButtonsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @button = buttons(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "superadmin should get index" do
    get buttons_url
    assert_response :success
  end

  test "regular user should not get index" do
    sign_out :user
    sign_in users(:regular)
    get buttons_url
    assert_redirected_to "http://localhost:3000/search?q=buttons"
  end

  test "superadmin should get new" do
    get new_button_url
    assert_response :success
  end

  test "regular user should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_button_url
    assert_redirected_to "http://localhost:3000/search?q=new"
  end

  test "should create button" do
    assert_difference('Button.count') do
      post buttons_url, params: { button: { active: @button.active, button_group_id: @button.button_group_id, cad_price: @button.cad_price, intl_shipping: @button.intl_shipping, name: @button.name, price: @button.price, priority_shipping: @button.priority_shipping, shipping_price: @button.shipping_price, weight: @button.weight } }
    end

    assert_redirected_to button_url(Button.last)
  end

  test "should show button to superadmin" do
    get button_url(@button)
    assert_response :success
  end

  test "should not show button to regular user" do
    sign_out :user
    sign_in users(:regular)
    get button_url(@button)
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=98&q%5B%5D=19&q%5B%5D=96"
  end

  test "superadmin should get edit" do
    get edit_button_url(@button)
    assert_response :success
  end

  test "regular user should not get edit" do
    sign_out :user
    sign_in users(:regular)
    get edit_button_url(@button)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update button" do
    patch button_url(@button), params: { button: { active: @button.active, button_group_id: @button.button_group_id, cad_price: @button.cad_price, intl_shipping: @button.intl_shipping, name: @button.name, price: @button.price, priority_shipping: @button.priority_shipping, shipping_price: @button.shipping_price, weight: @button.weight } }
    assert_redirected_to button_url(@button)
  end

  test "can mark button inactive" do
    assert_difference('@button.button_group.active_button_count', -1) do
      put button_url(@button), params: { button: { active: false } }
      assert_redirected_to button_url(@button)
      follow_redirect!
      assert_select 'dd', /No/
    end
  end

  test "should destroy button" do
    assert_difference('Button.count', -1) do
      delete button_url(@button)
    end

    assert_redirected_to buttons_url
  end
end
