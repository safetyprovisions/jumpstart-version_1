require 'test_helper'

class CouponCodesControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @coupon_code = coupon_codes(:total)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get coupon_codes_url
    # assert_response :redirect
    # follow_redirect!
    # assert_select "div.container", "×
    #   You are not authorized." # Includes the x to close the div
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=coupon&q%5B%5D=codes"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get coupon_codes_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get coupon_codes_url
    assert_response :success
  end

  test "can access new coupon_code page" do
    get new_coupon_code_url
    assert_response :success
  end

  test "can create a new coupon_code" do
    assert_difference('CouponCode.count') do
      post coupon_codes_url, params: { coupon_code: { code: '25offtotal',
        valid_from: Time.now,
        valid_to: (Time.now + 3.months),
        flat_rate: '',
        percentage: 25,
        scope: 'order_total',
        min_purchase_amt: 50,
        times_used: 0 }
      }
    end

    assert_redirected_to coupon_code_url(CouponCode.last)
  end

  test "cannot create two identical coupon_codes" do
    assert_difference('CouponCode.count', 1) do
      post coupon_codes_url, params: { coupon_code: { code: '25offtotal',
        valid_from: Time.now,
        valid_to: (Time.now + 3.months),
        flat_rate: '',
        percentage: 25,
        scope: 'order_total',
        min_purchase_amt: 50,
        times_used: 0 }
      }
      post coupon_codes_url, params: { coupon_code: { code: '25offtotal',
        valid_from: Time.now,
        valid_to: (Time.now + 3.months),
        flat_rate: '',
        percentage: 25,
        scope: 'order_total',
        min_purchase_amt: 50,
        times_used: 0 }
      }
    end
  end

  test "cannot create coupon_code without code" do
    assert_no_difference('CouponCode.count') do
      post coupon_codes_url, params: { coupon_code: { code: '',
        valid_from: Time.now,
        valid_to: (Time.now + 3.months),
        flat_rate: '',
        percentage: 25,
        scope: 'order_total',
        min_purchase_amt: 50,
        times_used: 0 }
      }
    end
  end

  test "cannot create coupon_code without valid_from" do
    assert_no_difference('CouponCode.count') do
      post coupon_codes_url, params: { coupon_code: { code: '25offtotal',
        valid_from: '',
        valid_to: (Time.now + 3.months),
        flat_rate: '',
        percentage: 25,
        scope: 'order_total',
        min_purchase_amt: 50,
        times_used: 0 }
      }
    end
  end

  test "cannot create coupon_code without valid_to" do
    assert_no_difference('CouponCode.count') do
      post coupon_codes_url, params: { coupon_code: { code: '25offtotal',
        valid_from: Time.now,
        valid_to: '',
        flat_rate: '',
        percentage: 25,
        scope: 'order_total',
        min_purchase_amt: 50,
        times_used: 0 }
      }
    end
  end

  test "can view coupon_code" do
    get coupon_code_url(@coupon_code)
    assert_response :success
  end

  test "can edit coupon_code" do
    get edit_coupon_code_url(@coupon_code)
    assert_response :success
  end

  test "can update coupon_code" do
    put coupon_code_url(@coupon_code), params: { coupon_code: { code: '26offtotal',
        valid_from: Time.now,
        valid_to: (Time.now + 3.months),
        flat_rate: '',
        percentage: 26,
        scope: 'order_total',
        min_purchase_amt: 50,
        times_used: 0 }
      }
    assert_redirected_to coupon_code_url(@coupon_code)
    @coupon_code.reload
    assert_equal "26OFFTOTAL", @coupon_code.code
  end

  test "cannot update coupon_code with invalid attributes" do
    put coupon_code_url(@coupon_code), params: { coupon_code: {
        code: '26offtotal',
        valid_from: Time.now,
        valid_to: '',
        flat_rate: '',
        percentage: 26,
        scope: 'order_total',
        min_purchase_amt: 0,
        times_used: 0 }
      }
    assert_response :success
    assert_select "h1", "Edit Coupon Code"
  end

  test "can destroy coupon_code" do
    assert_difference('CouponCode.count', -1) do
      delete coupon_code_url(@coupon_code)
    end
    assert_redirected_to coupon_codes_url
  end
end
