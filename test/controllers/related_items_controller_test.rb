require 'test_helper'

class RelatedItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @related_item = related_items(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get related_items_url
    assert_response :success
  end

  test "non-admin should not get index" do
    sign_out :user
    get related_items_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=related&q%5B%5D=items"
  end

  test "should get new" do
    get new_related_item_url
    assert_select 'body', /Please use the link found on the Show page for anything that can be related to a Product Group, for example a specific Keychain Group./
  end

  test "should create related_item" do
    @keychain = keychains(:one)
    assert_difference('RelatedItem.count') do
      post related_items_url, params: { related_item: {
        product_group_id: product_groups(:two).id,
        relatable_type: "Keychain",
        relatable_id: @keychain.id }
      }
    end

    assert_redirected_to related_item_url(RelatedItem.last)
  end

  test "should show related_item" do
    get related_item_url(@related_item)
    assert_response :success
  end

  test "should not show related_item to non-admin" do
    sign_out :user
    get related_item_url(@related_item)
    follow_redirect!
    assert_select 'body', /Search Results/
  end

  test "should get edit" do
    get edit_related_item_url(@related_item)
    assert_response :success
  end

  test "should update related_item" do
    patch related_item_url(@related_item), params: { related_item: { product_group_id: @related_item.product_group_id, relatable_id: @related_item.relatable_id } }
    assert_redirected_to related_item_url(@related_item)
  end

  test "should destroy related_item" do
    assert_difference('RelatedItem.count', -1) do
      delete related_item_url(@related_item)
    end

    assert_redirected_to related_items_url
  end

  # TODO Can we add a test to make sure that inactive ProductGroups/items are not included, ie, that they are not displayed on the Cart page?
end
