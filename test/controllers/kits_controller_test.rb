require 'test_helper'

class KitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @kit = kits(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get kits_url
    assert_redirected_to "http://localhost:3000/search?q=kits"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get kits_url
    assert_redirected_to "http://localhost:3000/search?q=kits"
  end

  test "can access if superadmin" do
    get kits_url
    assert_response :success
  end

  test "can access new kit page" do
    get new_kit_url
    assert_response :success
  end

  test "can create a new kit" do
    assert_difference('Kit.count') do
      post kits_url, params: { kit: { title: "New Kit", price: 39900, language: "english", standards: "osha", product_group_id: product_groups(:one).id, sample: cached_sample_file.to_json, market_type: "usa", cad_price: 45000, download: "url", additional_download: "url" } }
    end

    assert_redirected_to kit_url(Kit.last)
  end

  test "cannot create kit without title" do
    assert_no_difference('Kit.count') do
      post kits_url, params: { kit: { title: "", price: 39900, language: "english", standards: "osha", product_group_id: product_groups(:one).id, sample: cached_sample_file.to_json, market_type: "usa", cad_price: 45000, download: "url", additional_download: "url" } }
    end
  end

  test "cannot create kit without price" do
    assert_no_difference('Kit.count') do
      post kits_url, params: { kit: { title: "New Kit", price: '', language: "english", standards: "osha", product_group_id: product_groups(:one).id, sample: cached_sample_file.to_json, market_type: "usa", cad_price: 45000, download: "url", additional_download: "url" } }
    end
  end

  test "cannot create kit without language" do
    assert_no_difference('Kit.count') do
      post kits_url, params: { kit: { title: "New Kit", price: 39900, language: "", standards: "osha", product_group_id: product_groups(:one).id, sample: cached_sample_file.to_json, market_type: "usa", cad_price: 45000, download: "url", additional_download: "url" } }
    end
  end

  test "cannot create kit without standards" do
    assert_no_difference('Kit.count') do
      post kits_url, params: { kit: { title: "New Kit", price: 39900, language: "english", standards: "", product_group_id: product_groups(:one).id, sample: cached_sample_file.to_json, market_type: "usa", cad_price: 45000, download: "url", additional_download: "url" } }
    end
  end

  test "cannot create kit without product_group_id" do
    assert_no_difference('Kit.count') do
      post kits_url, params: { kit: { title: "New Kit", price: 39900, language: "english", standards: "osha", product_group_id: '', sample: cached_sample_file.to_json, market_type: "usa", cad_price: 45000, download: "url", additional_download: "url" } }
    end
  end

  test "cannot create kit without sample" do
    assert_no_difference('Kit.count') do
      post kits_url, params: { kit: { title: "New Kit", price: 39900, language: "english", standards: "osha", product_group_id: product_groups(:one).id, sample: '', market_type: "usa", cad_price: 45000, download: "url", additional_download: "url" } }
    end
  end

  test "cannot create kit without cad_price" do
    assert_no_difference('Kit.count') do
      post kits_url, params: { kit: { title: "New Kit", price: 39900, language: "english", standards: "osha", product_group_id: product_groups(:one).id, sample: cached_sample_file.to_json, market_type: "usa", cad_price: '', download: "url", additional_download: "url" } }
    end
  end

  test "cannot create kit without download" do
    assert_no_difference('Kit.count') do
      post kits_url, params: { kit: { title: "New Kit", price: 39900, language: "english", standards: "osha", product_group_id: product_groups(:one).id, sample: cached_sample_file.to_json, market_type: "usa", cad_price: 45000, download: "", additional_download: "url" } }
    end
  end

  test "can view kit" do
    get kit_url(@kit)
    assert_response :success
  end

  test "can edit kit" do
    get edit_kit_url(@kit)
    assert_response :success
  end

  test "can update kit" do
    put kit_url(@kit), params: { kit: { title: "New Kit Title" } }
    assert_redirected_to kit_url(@kit)
    @kit.reload
    assert_equal "New Kit Title", @kit.title
  end

  test "cannot update kit with invalid attributes" do
    put kit_url(@kit), params: { kit: { title: ""} }
    assert_response :success
    assert_select "h1", "Edit Kit"
  end

  # test "can destroy kit" do
  #   assert_difference('Kit.count', -1) do
  #     delete kit_url(@kit)
  #   end
  #   assert_redirected_to kits_url
  # end

  test "can mark kit inactive" do
    put kit_url(@kit), params: { kit: { active: false } }
    assert_redirected_to kit_url(@kit)
    follow_redirect!
    assert_select 'dd', /No/
  end
end
