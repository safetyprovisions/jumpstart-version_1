require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @user = users(:regular)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get user_url(@user)
    assert_response :redirect
    follow_redirect!
    assert_select "div.container", "×
      You are not authorized." # Includes the x to close the div
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in @user
    get user_url(users(:superadmin))
    assert_response :redirect
  end

  test "can view user if user" do
    sign_out :user
    sign_in @user
    get user_url(@user)
    assert_response :success
    assert_select 'h1', "#{ @user.name.full }"
  end

  test "regular user cannot view Superadmin functions" do
    sign_out :user
    sign_in @user
    get user_url(@user)
    assert_select "h2", {count: 0, text: "SUPERADMIN"}, "This page must contain no H2 headers that say SUPERADMIN."
  end

  test "can view user if superadmin" do
    get user_url(@user)
    assert_response :success
    assert_select 'h2', /Superadmin/
  end

  test "regular user can edit user" do
    sign_out :user
    sign_in @user
    get 'http://localhost:3000/users/edit'
    assert_select 'body', /Edit Account/
  end

  test "admin user can access add_employee page" do
    sign_out :user
    sign_in users(:admin)
    get add_employee_url
    assert_response :success
  end

  test "admin user can add individual employee" do
    sign_out :user
    sign_in users(:admin)
    CompanyAdmin.create(user_id: users(:admin).id, company_id: companies(:one).id)
    companies(:one).employees << users(:admin)
    post new_user_url, params: { user: { first_name: "", last_name: '', email: @user.email } }
    assert_redirected_to add_employee_url
    follow_redirect!
    assert_select "div.container", "×
      #{ @user.name.full } has been added as an employee of #{ users(:admin).companies.last.name}."
  end

  # TODO This requires soft delete!
  # test "can destroy user" do
  #   assert_difference('User.count', -1) do
  #     delete user_url(@user)
  #   end
  #   assert_redirected_to users_url
  # end
end
