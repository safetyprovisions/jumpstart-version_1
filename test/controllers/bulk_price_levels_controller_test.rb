require 'test_helper'

class BulkPriceLevelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @bulk_price_level = bulk_price_levels(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get bulk_price_levels_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=bulk&q%5B%5D=price&q%5B%5D=levels"
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get bulk_price_levels_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get bulk_price_levels_url
    assert_response :success
  end

  test "can access new bulk_price_level page" do
    get new_bulk_price_level_url
    assert_response :success
  end

  test "can create a new bulk_price_level" do
    assert_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 9500, tier: 1 } }
    end

    assert_redirected_to bulk_price_level_url(BulkPriceLevel.last)
  end

  test "cannot create two identical bulk_price_levels" do
    assert_difference('BulkPriceLevel.count', 1) do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 9500, tier: 1 } }
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 9500, tier: 1 } }
    end
  end

  test "cannot create more than four bulk_price_levels in a tier" do
    assert_difference('BulkPriceLevel.count', 3) do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 9500, tier: 1 } }
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 50, price: 7500, cad_price: 9500, tier: 1 } }
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 100, price: 7500, cad_price: 9500, tier: 1 } }
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 150, price: 7500, cad_price: 9500, tier: 1 } }
    end
  end

  test "cannot create bulk_price_level without qty" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: '', price: 7500, cad_price: 9500, tier: 1 } }
    end
  end

  test "cannot create bulk_price_level without price" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: '', cad_price: 9500, tier: 1 } }
    end
  end

  test "cannot create bulk_price_level without cad_price" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: '', tier: 1 } }
    end
  end

  test "cannot create bulk_price_level with non-numeric qty" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 'abc', price: 7500, cad_price: 9500, tier: 1 } }
    end
  end

  test "cannot create bulk_price_level with non-numeric price" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 'abc', cad_price: 9500, tier: 1 } }
    end
  end

  test "cannot create bulk_price_level with non-numeric cad_price" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 'abc', tier: 1 } }
    end
  end

  test "cannot create bulk_price_level without tier" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 25, price: 7500, cad_price: 9500, tier: '' } }
    end
  end

  test "cannot create bulk_price_level with qty 0" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 0, price: 7500, cad_price: 9500, tier: 1 } }
    end
  end

  test "cannot create bulk_price_level with price < 50 cents" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 1, price: 49, cad_price: 9500, tier: 1 } }
    end
  end

  test "cannot create bulk_price_level with cad_price < 50 cents" do
    assert_no_difference('BulkPriceLevel.count') do
      post bulk_price_levels_url, params: { bulk_price_level: { qty: 1, price: 7500, cad_price: 49, tier: 1 } }
    end
  end

  test "can view bulk_price_level" do
    get bulk_price_level_url(@bulk_price_level)
    assert_response :success
  end

  test "can edit bulk_price_level" do
    get edit_bulk_price_level_url(@bulk_price_level)
    assert_response :success
  end

  test "can update bulk_price_level" do
    put bulk_price_level_url(@bulk_price_level), params: { bulk_price_level: { qty: 50 } }
    assert_redirected_to bulk_price_level_url(@bulk_price_level)
    @bulk_price_level.reload
    assert_equal 50, @bulk_price_level.qty
  end

  test "cannot update bulk_price_level with invalid attributes" do
    put bulk_price_level_url(@bulk_price_level), params: { bulk_price_level: { qty: ''} }
    assert_response :success
    assert_select "h1", "Edit Bulk Price Level"
  end

  test "can destroy bulk_price_level" do
    assert_difference('BulkPriceLevel.count', -1) do
      delete bulk_price_level_url(@bulk_price_level)
    end
    assert_redirected_to bulk_price_levels_url
  end
end
