require 'test_helper'

class TagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @new_name = "SOMETHING"
    @new_tag  = ActsAsTaggableOn::Tag.create(name: @new_name, taggings_count: 0)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "can edit an existing tag" do
    post edit_tag_url, params: { id: @new_tag.id, name: @new_name }
    assert_response :success
    assert_select 'body', /Edit Tag/
    assert_select 'body', /SOMETHING/
  end

  test "can update an existing tag" do
    post update_tag_url, params: { id: @new_tag.id, name: 'LOSTSKELETON' }
    assert_redirected_to user_path(users(:superadmin))
    follow_redirect!
    assert_select 'body', /Tag was successfully updated./
    assert_select 'body', /LOSTSKELETON/
  end
end
