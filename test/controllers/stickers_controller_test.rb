require 'test_helper'

class StickersControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @sticker = stickers(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "superadmin should get index" do
    get stickers_url
    assert_response :success
  end

  test "superadmin should get new" do
    get new_sticker_url
    assert_response :success
  end

  test "regular user should not get new" do
    sign_out :user
    get new_sticker_url
    assert_redirected_to "http://localhost:3000/"
  end

  test "should create sticker" do
    assert_difference('Sticker.count') do
      post stickers_url, params: { sticker: {
        active: @sticker.active,
        cad_price: @sticker.cad_price,
        description: @sticker.description,
        image_data: cached_shirt_image.to_json,
        intl_shipping: @sticker.intl_shipping,
        keywords: @sticker.keywords,
        meta_description: @sticker.meta_description,
        name: @sticker.name,
        price: @sticker.price,
        priority_shipping: @sticker.priority_shipping,
        shipping_price: @sticker.shipping_price,
        weight: @sticker.weight
        }
      }
    end

    assert_redirected_to sticker_url(Sticker.last)
  end

  test "should show sticker to superadmin" do
    get sticker_url(@sticker)
    assert_response :success
  end

  test "superadmin should get edit" do
    get edit_sticker_url(@sticker)
    assert_response :success
  end

  test "regular user should not get edit" do
    sign_out :user
    get edit_sticker_url(@sticker)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update sticker" do
    patch sticker_url(@sticker), params: { sticker: {
      active: @sticker.active,
      cad_price: @sticker.cad_price,
      description: @sticker.description,
      # image_data: @sticker.image_data,
      intl_shipping: @sticker.intl_shipping,
      keywords: @sticker.keywords,
      meta_description: @sticker.meta_description,
      name: @sticker.name,
      price: @sticker.price,
      priority_shipping: @sticker.priority_shipping,
      shipping_price: @sticker.shipping_price,
      slug: @sticker.slug,
      weight: @sticker.weight
      }
    }
    assert_redirected_to sticker_url(@sticker)
  end

  test "should destroy sticker" do
    assert_difference('Sticker.count', -1) do
      delete sticker_url(@sticker)
    end

    assert_redirected_to stickers_url
  end
end
