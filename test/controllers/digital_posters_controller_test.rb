require 'test_helper'

class DigitalPostersControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @digital_poster = digital_posters(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "should get index" do
    get digital_posters_url
    assert_response :success
  end

  test "should get new" do
    get new_digital_poster_url
    assert_response :success
  end

  test "regular user should not get index" do
    sign_out :user
    sign_in users(:regular)
    get digital_posters_url
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=digital&q%5B%5D=posters"
  end

  test "regular user should not get new" do
    sign_out :user
    sign_in users(:regular)
    get new_digital_poster_url
    assert_redirected_to "http://localhost:3000/search?q=new"
  end

  test "should create digital_poster" do
    assert_difference('DigitalPoster.count') do
      post digital_posters_url, params: { digital_poster: {
        cad_price: @digital_poster.cad_price,
        poster_id: @digital_poster.poster_id,
        price: @digital_poster.price } }
    end

    assert_redirected_to digital_poster_url(DigitalPoster.last)
  end

  test "should show digital_poster" do
    get digital_poster_url(@digital_poster)
    assert_response :success
  end

  test "should get edit" do
    get edit_digital_poster_url(@digital_poster)
    assert_response :success
  end

  test "regular user should not show digital_poster" do
    sign_out :user
    sign_in users(:regular)
    get digital_poster_url(@digital_poster)
    assert_redirected_to "http://localhost:3000/search?q%5B%5D=98&q%5B%5D=19&q%5B%5D=96"
  end

  test "regular user should not get edit" do
    sign_out :user
    sign_in users(:regular)
    get edit_digital_poster_url(@digital_poster)
    assert_redirected_to "http://localhost:3000/search?q=edit"
  end

  test "should update digital_poster" do
    patch digital_poster_url(@digital_poster), params: { digital_poster: {
        cad_price: @digital_poster.cad_price,
        poster_id: @digital_poster.poster_id,
        price: @digital_poster.price }
      }
    assert_redirected_to digital_poster_url(@digital_poster)
  end

  test "should destroy digital_poster" do
    assert_difference('DigitalPoster.count', -1) do
      delete digital_poster_url(@digital_poster)
    end

    assert_redirected_to digital_posters_url
  end
end
