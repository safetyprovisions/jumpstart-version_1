require 'test_helper'

class CompanyControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:superadmin)
    @company = companies(:one)
    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  test "redirected if not logged in" do
    sign_out :user
    get companies_url
    assert_response :redirect
    follow_redirect!
    assert_select "div.container", "×
      You are not authorized." # Includes the x to close the div
  end

  test "redirected if not superadmin" do
    sign_out :user
    sign_in users(:regular)
    get companies_url
    assert_response :redirect
  end

  test "can access if superadmin" do
    get companies_url
    assert_response :success
  end

  test "can access new company page" do
    get new_company_url
    assert_response :success
  end

  test "can create a new company" do
    assert_difference('Company.count') do
      post companies_url, params: { company: { name: "Company Name", address: "218 Dividend Dr, suite 4", city: "Rexburg", state: 'ID', zip: 83440, email: "companyname@testdb.com", password: "password", password_confirmation: "password", credits: 0, preferred_currency: "usd", cad_credits: 0 } }
    end

    assert_redirected_to companies_homepage_url
  end

  test "cannot create company without address" do
    assert_no_difference('Company.count') do
      post companies_url, params: { company: { name: "Company Name", address: "", city: "Rexburg", state: 'ID', zip: 83440, email: "companyname@testdb.com", password: "password", password_confirmation: "password", credits: 0, preferred_currency: "usd", cad_credits: 0 } }
    end
  end

  test "cannot create company without city" do
    assert_no_difference('Company.count') do
      post companies_url, params: { company: { name: "Company Name", address: "218 Dividend Dr, suite 4", city: "", state: 'ID', zip: 83440, email: "companyname@testdb.com", password: "password", password_confirmation: "password", credits: 0, preferred_currency: "usd", cad_credits: 0 } }
    end
  end

  test "cannot create company without state" do
    assert_no_difference('Company.count') do
      post companies_url, params: { company: { name: "Company Name", address: "218 Dividend Dr, suite 4", city: "Rexburg", state: '', zip: 83440, email: "companyname@testdb.com", password: "password", password_confirmation: "password", credits: 0, preferred_currency: "usd", cad_credits: 0 } }
    end
  end

  test "cannot create company without zip" do
    assert_no_difference('Company.count') do
      post companies_url, params: { company: { name: "Company Name", address: "218 Dividend Dr, suite 4", city: "Rexburg", state: 'ID', zip: '', email: "companyname@testdb.com", password: "password", password_confirmation: "password", credits: 0, preferred_currency: "usd", cad_credits: 0 } }
    end
  end

  test "can view company" do
    get company_url(@company)
    assert_response :success
  end

  test "can edit company" do
    get edit_company_url(@company)
    assert_response :success
  end

  test "can update company" do
    put company_url(@company), params: { company: { address: "218 Dividend Dr, suite 1" } }
    assert_redirected_to companies_homepage_url
    @company.reload
    assert_equal "218 Dividend Dr, suite 1", @company.address
  end

  test "cannot update company with invalid attributes" do
    put company_url(@company), params: { company: { address: "" } }
    assert_response :success
    assert_select "h1", "Edit Company"
  end

  test "admin can view branch_view" do
    sign_out :user
    sign_in users(:admin)
    companies(:one).employees << users(:admin)
    get companies_branch_view_url
    assert_response :success
    assert_select "h1", @company.name
  end

  test "admin can view add_employee" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    get companies_add_employee_url
    assert_response :success
    assert_select "h1", "Add Employees"
  end

  test "admin can add individual employee" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    get companies_add_employee_url
    assert_difference('companies(:one).employees.count') do
      post new_user_url, params: { user: { first_name: "First", last_name: "Last", email: "firstlast@test.com", password: "password", password_confirmation: "password", add_individual: true } }
    end
    assert_redirected_to add_employee_url
  end

  test "admin can view manage_employees" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    get companies_manage_employees_url
    assert_response :success
    assert_select "h1", "Manage Employees"
  end

  test "admin can remove an employee" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    post new_user_url, params: { user: { first_name: "First", last_name: "Last", email: "firstlast@test.com", password: "password", password_confirmation: "password", add_individual: true } }
    assert_difference('companies(:one).employees.count', -1) do
      post companies_remove_employee_url, params: { emp: companies(:one).employees.last.id }
    end
  end

  test "admin can toggle admin status" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    assert_difference('companies(:one).admins.count', -1) do
      post companies_toggle_admin_url, params: { employee: companies(:one).employees.last.id }
    end
  end

  test "admin can toggle employee status" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    get companies_manage_employees_url
    assert_select "input.btn-success", 1
    post companies_toggle_employee_status_url, params: { employee: companies(:one).employees.last.id }
    assert_select "input.btn-success", 0
  end

  test "admin can assign_identifier to company" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    get assign_identifier_url
    assert_not companies(:one).identifier.nil?
  end

  test "admin can assign_parent_company" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    post assign_parent_company_url, params: { identifier: "EEEEEEEE" }
    assert_equal companies(:one).parent, companies(:parent)
  end

  test "admin can remove_parent_company" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    post assign_parent_company_url, params: { identifier: "EEEEEEEE" }
    assert_equal companies(:one).parent, companies(:parent)
    get remove_parent_company_url
    assert_not (companies(:one).parent_id == "EEEEEEEE")
  end

  test "admin can view assign_course page" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    get assign_course_url
    assert_response :success
    assert_select "body", /Course Assignment/
  end

  test "admin can view company orders" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    get companies_orders_url
    assert_response :success
    assert_select "h1", /Orders for/
  end

  test "admin can view company reports" do
    sign_out :user
    companies(:one).employees << users(:admin)
    sign_in users(:admin)
    sign_in companies(:one)
    get companies_reports_url
    assert_response :success
    assert_select "h1", /Reports/
  end
end

# TODO NOT TESTED:
# import_employees
#   - Not sure how to deal with CSV uploading, etc.
#   - Testing creates actual accounts in Talent
#   - I think the methods involved get tested elsewhere, anyways.

# We don't want to destroy a Company because of all the connecting records. We will "shadow delete" them by removing anything personally identifiable.
# test "can destroy company" do
#   assert_difference('Company.count', -1) do
#     delete company_url(@company)
#   end
#   assert_redirected_to companies_url
# end

# test "superadmin can view branch_view" do
#   get companies_branch_view_url
#   assert_response :success
# end

  # TODO THIS ONE IS REALLY IMPORTANT, BUT I DON'T KNOW WHY IT WON'T WORK!
  # test "admin can assign_multiple_courses to selected employees" do
  #   sign_out :user
  #   companies(:one).employees << users(:admin)
  #   sign_in users(:admin)
  #   sign_in companies(:one)
  #   get assign_course_url
  #   post companies_assign_multiple_courses_url, params: { users: ["20508"], course_id: online_courses(:one).id, talent_course_id: "342", lookup_class: "OnlineCourse" }
  #   assert_redirected_to assign_course_url
  #   assert_select "body", /The specified employees have successfully been assigned to this course!/
  # end
