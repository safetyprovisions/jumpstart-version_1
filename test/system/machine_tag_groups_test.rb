require "application_system_test_case"

class MachineTagGroupsTest < ApplicationSystemTestCase
  setup do
    @machine_tag_group = machine_tag_groups(:one)
  end

  test "visiting the index" do
    visit machine_tag_groups_url
    assert_selector "h1", text: "Machine Tag Groups"
  end

  test "creating a Machine tag group" do
    visit machine_tag_groups_url
    click_on "New Machine Tag Group"

    fill_in "Description", with: @machine_tag_group.description
    fill_in "Image data", with: @machine_tag_group.image_data
    fill_in "Keywords", with: @machine_tag_group.keywords
    fill_in "Meta description", with: @machine_tag_group.meta_description
    fill_in "Name", with: @machine_tag_group.name
    fill_in "Slug", with: @machine_tag_group.slug
    click_on "Create Machine tag group"

    assert_text "Machine tag group was successfully created"
    click_on "Back"
  end

  test "updating a Machine tag group" do
    visit machine_tag_groups_url
    click_on "Edit", match: :first

    fill_in "Description", with: @machine_tag_group.description
    fill_in "Image data", with: @machine_tag_group.image_data
    fill_in "Keywords", with: @machine_tag_group.keywords
    fill_in "Meta description", with: @machine_tag_group.meta_description
    fill_in "Name", with: @machine_tag_group.name
    fill_in "Slug", with: @machine_tag_group.slug
    click_on "Update Machine tag group"

    assert_text "Machine tag group was successfully updated"
    click_on "Back"
  end

  test "destroying a Machine tag group" do
    visit machine_tag_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Machine tag group was successfully destroyed"
  end
end
