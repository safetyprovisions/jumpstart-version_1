require "application_system_test_case"

class HatsTest < ApplicationSystemTestCase
  setup do
    @hat = hats(:one)
  end

  test "visiting the index" do
    visit hats_url
    assert_selector "h1", text: "Hats"
  end

  test "creating a Hat" do
    visit hats_url
    click_on "New Hat"

    fill_in "Cad price", with: @hat.cad_price
    fill_in "Description", with: @hat.description
    fill_in "Fastener", with: @hat.fastener
    fill_in "Image data", with: @hat.image_data
    fill_in "Intl shipping", with: @hat.intl_shipping
    fill_in "Keywords", with: @hat.keywords
    fill_in "Meta description", with: @hat.meta_description
    fill_in "Name", with: @hat.name
    fill_in "Price", with: @hat.price
    fill_in "Priority shipping", with: @hat.priority_shipping
    fill_in "Shipping price", with: @hat.shipping_price
    fill_in "Size", with: @hat.size
    fill_in "Slug", with: @hat.slug
    fill_in "Structure", with: @hat.structure
    fill_in "Style", with: @hat.style
    fill_in "Weight", with: @hat.weight
    click_on "Create Hat"

    assert_text "Hat was successfully created"
    click_on "Back"
  end

  test "updating a Hat" do
    visit hats_url
    click_on "Edit", match: :first

    fill_in "Cad price", with: @hat.cad_price
    fill_in "Description", with: @hat.description
    fill_in "Fastener", with: @hat.fastener
    fill_in "Image data", with: @hat.image_data
    fill_in "Intl shipping", with: @hat.intl_shipping
    fill_in "Keywords", with: @hat.keywords
    fill_in "Meta description", with: @hat.meta_description
    fill_in "Name", with: @hat.name
    fill_in "Price", with: @hat.price
    fill_in "Priority shipping", with: @hat.priority_shipping
    fill_in "Shipping price", with: @hat.shipping_price
    fill_in "Size", with: @hat.size
    fill_in "Slug", with: @hat.slug
    fill_in "Structure", with: @hat.structure
    fill_in "Style", with: @hat.style
    fill_in "Weight", with: @hat.weight
    click_on "Update Hat"

    assert_text "Hat was successfully updated"
    click_on "Back"
  end

  test "destroying a Hat" do
    visit hats_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Hat was successfully destroyed"
  end
end
