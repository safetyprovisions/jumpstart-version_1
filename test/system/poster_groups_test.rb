require "application_system_test_case"

class PosterGroupsTest < ApplicationSystemTestCase
  setup do
    @poster_group = poster_groups(:one)
  end

  test "visiting the index" do
    visit poster_groups_url
    assert_selector "h1", text: "Poster Groups"
  end

  test "creating a Poster group" do
    visit poster_groups_url
    click_on "New Poster Group"

    fill_in "Description", with: @poster_group.description
    fill_in "Keywords", with: @poster_group.keywords
    fill_in "Meta description", with: @poster_group.meta_description
    fill_in "Name", with: @poster_group.name
    fill_in "Slug", with: @poster_group.slug
    click_on "Create Poster group"

    assert_text "Poster group was successfully created"
    click_on "Back"
  end

  test "updating a Poster group" do
    visit poster_groups_url
    click_on "Edit", match: :first

    fill_in "Description", with: @poster_group.description
    fill_in "Keywords", with: @poster_group.keywords
    fill_in "Meta description", with: @poster_group.meta_description
    fill_in "Name", with: @poster_group.name
    fill_in "Slug", with: @poster_group.slug
    click_on "Update Poster group"

    assert_text "Poster group was successfully updated"
    click_on "Back"
  end

  test "destroying a Poster group" do
    visit poster_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Poster group was successfully destroyed"
  end
end
