require "application_system_test_case"

class ButtonsTest < ApplicationSystemTestCase
  setup do
    @button = buttons(:one)
  end

  test "visiting the index" do
    visit buttons_url
    assert_selector "h1", text: "Buttons"
  end

  test "creating a Button" do
    visit buttons_url
    click_on "New Button"

    check "Active" if @button.active
    fill_in "Button group", with: @button.button_group_id
    fill_in "Cad price", with: @button.cad_price
    fill_in "Intl shipping", with: @button.intl_shipping
    fill_in "Name", with: @button.name
    fill_in "Price", with: @button.price
    fill_in "Priority shipping", with: @button.priority_shipping
    fill_in "Shipping price", with: @button.shipping_price
    fill_in "Weight", with: @button.weight
    click_on "Create Button"

    assert_text "Button was successfully created"
    click_on "Back"
  end

  test "updating a Button" do
    visit buttons_url
    click_on "Edit", match: :first

    check "Active" if @button.active
    fill_in "Button group", with: @button.button_group_id
    fill_in "Cad price", with: @button.cad_price
    fill_in "Intl shipping", with: @button.intl_shipping
    fill_in "Name", with: @button.name
    fill_in "Price", with: @button.price
    fill_in "Priority shipping", with: @button.priority_shipping
    fill_in "Shipping price", with: @button.shipping_price
    fill_in "Weight", with: @button.weight
    click_on "Update Button"

    assert_text "Button was successfully updated"
    click_on "Back"
  end

  test "destroying a Button" do
    visit buttons_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Button was successfully destroyed"
  end
end
