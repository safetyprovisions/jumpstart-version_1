require "application_system_test_case"

class MachineTagsTest < ApplicationSystemTestCase
  setup do
    @machine_tag = machine_tags(:one)
  end

  test "visiting the index" do
    visit machine_tags_url
    assert_selector "h1", text: "Machine Tags"
  end

  test "creating a Machine tag" do
    visit machine_tags_url
    click_on "New Machine Tag"

    fill_in "Cad price", with: @machine_tag.cad_price
    fill_in "Intl shipping", with: @machine_tag.intl_shipping
    fill_in "Machine tag group", with: @machine_tag.machine_tag_group_id
    fill_in "Name", with: @machine_tag.name
    fill_in "Price", with: @machine_tag.price
    fill_in "Priority shipping", with: @machine_tag.priority_shipping
    fill_in "Shipping price", with: @machine_tag.shipping_price
    fill_in "Weight", with: @machine_tag.weight
    click_on "Create Machine tag"

    assert_text "Machine tag was successfully created"
    click_on "Back"
  end

  test "updating a Machine tag" do
    visit machine_tags_url
    click_on "Edit", match: :first

    fill_in "Cad price", with: @machine_tag.cad_price
    fill_in "Intl shipping", with: @machine_tag.intl_shipping
    fill_in "Machine tag group", with: @machine_tag.machine_tag_group_id
    fill_in "Name", with: @machine_tag.name
    fill_in "Price", with: @machine_tag.price
    fill_in "Priority shipping", with: @machine_tag.priority_shipping
    fill_in "Shipping price", with: @machine_tag.shipping_price
    fill_in "Weight", with: @machine_tag.weight
    click_on "Update Machine tag"

    assert_text "Machine tag was successfully updated"
    click_on "Back"
  end

  test "destroying a Machine tag" do
    visit machine_tags_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Machine tag was successfully destroyed"
  end
end
