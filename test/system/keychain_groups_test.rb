require "application_system_test_case"

class KeychainGroupsTest < ApplicationSystemTestCase
  setup do
    @keychain_group = keychain_groups(:one)
  end

  test "visiting the index" do
    visit keychain_groups_url
    assert_selector "h1", text: "Keychain Groups"
  end

  test "creating a Keychain group" do
    visit keychain_groups_url
    click_on "New Keychain Group"

    fill_in "Description", with: @keychain_group.description
    fill_in "Image data", with: @keychain_group.image_data
    fill_in "Name", with: @keychain_group.name
    click_on "Create Keychain group"

    assert_text "Keychain group was successfully created"
    click_on "Back"
  end

  test "updating a Keychain group" do
    visit keychain_groups_url
    click_on "Edit", match: :first

    fill_in "Description", with: @keychain_group.description
    fill_in "Image data", with: @keychain_group.image_data
    fill_in "Name", with: @keychain_group.name
    click_on "Update Keychain group"

    assert_text "Keychain group was successfully updated"
    click_on "Back"
  end

  test "destroying a Keychain group" do
    visit keychain_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Keychain group was successfully destroyed"
  end
end
