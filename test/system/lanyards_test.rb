require "application_system_test_case"

class LanyardsTest < ApplicationSystemTestCase
  setup do
    @lanyard = lanyards(:one)
  end

  test "visiting the index" do
    visit lanyards_url
    assert_selector "h1", text: "Lanyards"
  end

  test "creating a Lanyard" do
    visit lanyards_url
    click_on "New Lanyard"

    check "Active" if @lanyard.active
    fill_in "Cad price", with: @lanyard.cad_price
    fill_in "Intl shipping", with: @lanyard.intl_shipping
    fill_in "Lanyard group", with: @lanyard.lanyard_group_id
    fill_in "Name", with: @lanyard.name
    fill_in "Price", with: @lanyard.price
    fill_in "Priority shipping", with: @lanyard.priority_shipping
    fill_in "Shipping price", with: @lanyard.shipping_price
    fill_in "Weight", with: @lanyard.weight
    click_on "Create Lanyard"

    assert_text "Lanyard was successfully created"
    click_on "Back"
  end

  test "updating a Lanyard" do
    visit lanyards_url
    click_on "Edit", match: :first

    check "Active" if @lanyard.active
    fill_in "Cad price", with: @lanyard.cad_price
    fill_in "Intl shipping", with: @lanyard.intl_shipping
    fill_in "Lanyard group", with: @lanyard.lanyard_group_id
    fill_in "Name", with: @lanyard.name
    fill_in "Price", with: @lanyard.price
    fill_in "Priority shipping", with: @lanyard.priority_shipping
    fill_in "Shipping price", with: @lanyard.shipping_price
    fill_in "Weight", with: @lanyard.weight
    click_on "Update Lanyard"

    assert_text "Lanyard was successfully updated"
    click_on "Back"
  end

  test "destroying a Lanyard" do
    visit lanyards_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Lanyard was successfully destroyed"
  end
end
