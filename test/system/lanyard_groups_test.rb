require "application_system_test_case"

class LanyardGroupsTest < ApplicationSystemTestCase
  setup do
    @lanyard_group = lanyard_groups(:one)
  end

  test "visiting the index" do
    visit lanyard_groups_url
    assert_selector "h1", text: "Lanyard Groups"
  end

  test "creating a Lanyard group" do
    visit lanyard_groups_url
    click_on "New Lanyard Group"

    check "Active" if @lanyard_group.active
    fill_in "Description", with: @lanyard_group.description
    fill_in "Image data", with: @lanyard_group.image_data
    fill_in "Keywords", with: @lanyard_group.keywords
    fill_in "Meta description", with: @lanyard_group.meta_description
    fill_in "Name", with: @lanyard_group.name
    fill_in "Slug", with: @lanyard_group.slug
    click_on "Create Lanyard group"

    assert_text "Lanyard group was successfully created"
    click_on "Back"
  end

  test "updating a Lanyard group" do
    visit lanyard_groups_url
    click_on "Edit", match: :first

    check "Active" if @lanyard_group.active
    fill_in "Description", with: @lanyard_group.description
    fill_in "Image data", with: @lanyard_group.image_data
    fill_in "Keywords", with: @lanyard_group.keywords
    fill_in "Meta description", with: @lanyard_group.meta_description
    fill_in "Name", with: @lanyard_group.name
    fill_in "Slug", with: @lanyard_group.slug
    click_on "Update Lanyard group"

    assert_text "Lanyard group was successfully updated"
    click_on "Back"
  end

  test "destroying a Lanyard group" do
    visit lanyard_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Lanyard group was successfully destroyed"
  end
end
