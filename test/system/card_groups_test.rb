require "application_system_test_case"

class CardGroupsTest < ApplicationSystemTestCase
  setup do
    @card_group = card_groups(:one)
  end

  test "visiting the index" do
    visit card_groups_url
    assert_selector "h1", text: "Card Groups"
  end

  test "creating a Card group" do
    visit card_groups_url
    click_on "New Card Group"

    fill_in "Description", with: @card_group.description
    fill_in "Image data", with: @card_group.image_data
    fill_in "Keywords", with: @card_group.keywords
    fill_in "Meta description", with: @card_group.meta_description
    fill_in "Name", with: @card_group.name
    fill_in "Slug", with: @card_group.slug
    click_on "Create Card group"

    assert_text "Card group was successfully created"
    click_on "Back"
  end

  test "updating a Card group" do
    visit card_groups_url
    click_on "Edit", match: :first

    fill_in "Description", with: @card_group.description
    fill_in "Image data", with: @card_group.image_data
    fill_in "Keywords", with: @card_group.keywords
    fill_in "Meta description", with: @card_group.meta_description
    fill_in "Name", with: @card_group.name
    fill_in "Slug", with: @card_group.slug
    click_on "Update Card group"

    assert_text "Card group was successfully updated"
    click_on "Back"
  end

  test "destroying a Card group" do
    visit card_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Card group was successfully destroyed"
  end
end
