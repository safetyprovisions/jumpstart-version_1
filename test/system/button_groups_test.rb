require "application_system_test_case"

class ButtonGroupsTest < ApplicationSystemTestCase
  setup do
    @button_group = button_groups(:one)
  end

  test "visiting the index" do
    visit button_groups_url
    assert_selector "h1", text: "Button Groups"
  end

  test "creating a Button group" do
    visit button_groups_url
    click_on "New Button Group"

    check "Active" if @button_group.active
    fill_in "Description", with: @button_group.description
    fill_in "Image data", with: @button_group.image_data
    fill_in "Keywords", with: @button_group.keywords
    fill_in "Meta description", with: @button_group.meta_description
    fill_in "Name", with: @button_group.name
    fill_in "Slug", with: @button_group.slug
    click_on "Create Button group"

    assert_text "Button group was successfully created"
    click_on "Back"
  end

  test "updating a Button group" do
    visit button_groups_url
    click_on "Edit", match: :first

    check "Active" if @button_group.active
    fill_in "Description", with: @button_group.description
    fill_in "Image data", with: @button_group.image_data
    fill_in "Keywords", with: @button_group.keywords
    fill_in "Meta description", with: @button_group.meta_description
    fill_in "Name", with: @button_group.name
    fill_in "Slug", with: @button_group.slug
    click_on "Update Button group"

    assert_text "Button group was successfully updated"
    click_on "Back"
  end

  test "destroying a Button group" do
    visit button_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Button group was successfully destroyed"
  end
end
