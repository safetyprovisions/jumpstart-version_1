require "application_system_test_case"

class RelatedItemsTest < ApplicationSystemTestCase
  setup do
    @related_item = related_items(:one)
  end

  test "visiting the index" do
    visit related_items_url
    assert_selector "h1", text: "Related Items"
  end

  test "creating a Related item" do
    visit related_items_url
    click_on "New Related Item"

    fill_in "Product group", with: @related_item.product_group_id
    fill_in "Relatable", with: @related_item.relatable_id
    click_on "Create Related item"

    assert_text "Related item was successfully created"
    click_on "Back"
  end

  test "updating a Related item" do
    visit related_items_url
    click_on "Edit", match: :first

    fill_in "Product group", with: @related_item.product_group_id
    fill_in "Relatable", with: @related_item.relatable_id
    click_on "Update Related item"

    assert_text "Related item was successfully updated"
    click_on "Back"
  end

  test "destroying a Related item" do
    visit related_items_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Related item was successfully destroyed"
  end
end
