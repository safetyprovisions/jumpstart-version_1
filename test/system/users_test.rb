# NOTE: This test is really flaky! Sometimes it passes, and other times there is a Net::ReadTimeout error for no apparent reason. It seems to (maybe) help to restart the server before running this test. Not sure why or what else might help.

# Tried having it sleep(5) after clicking on a link, to give things time to load, but it didn't seem to help.

require "application_system_test_case"

class UsersTest < ApplicationSystemTestCase
  setup do
    @product_group = ProductGroup.create!(
        name: "Test Product Group",
        image: cached_image.to_json,
        video: 'MjA0NjIw',
        overview: "Text",
        training_standards: "Text",
        facts: "Text",
        fails: "Text",
        slug: "test-product-group"
      )

    @product_group.tag_list.add("SOMETHING")
    @product_group.save

    @online_course = OnlineCourse.create!(
        title: "Online Course Test",
        price: 7900,
        cad_price: 9900,
        sale_start: Time.now,
        sale_expiry: (Time.now + 1.year),
        language: "english",
        standards: "osha",
        product_group_id: @product_group.id,
        talent_course_id: 342,
        practical_evaluation: cached_practical_evaluation.to_json,
        ttt: false,
        exam: cached_exam.to_json,
        answer_key: cached_answer_key.to_json,
        market_type: "usa",
        public: true
      )

    # 3 more Tier 1's
    BulkPriceLevel.create!(qty: 25, price: 7500, cad_price: 9500, tier: 1 )
    BulkPriceLevel.create!(qty: 50, price: 7500, cad_price: 9500, tier: 1 )
    BulkPriceLevel.create!(qty: 100, price: 7500, cad_price: 9500, tier: 1 )
    # All Tier 2's
    BulkPriceLevel.create!(qty: 1, price: 2900, cad_price: 3900, tier: 2 )
    BulkPriceLevel.create!(qty: 25, price: 2500, cad_price: 3500, tier: 2 )
    BulkPriceLevel.create!(qty: 50, price: 2000, cad_price: 3000, tier: 2 )
    BulkPriceLevel.create!(qty: 100, price: 1500, cad_price: 2500, tier: 2 )

    VCR.insert_cassette(name)
  end

  teardown do
    VCR.eject_cassette
  end

  # New User signs up and puts OnlineCourse in Cart
  test "adding online course to cart" do
    # visit 'http://127.0.0.1/users/sign_up'
    visit "http://localhost:3000/users/sign_up"
    fill_in('Full Name', with: 'Pammy Yaeber')
    fill_in('Email Address', with: 'pammyyaeber@test.com')
    fill_in('Password', with: 'password')
    fill_in('Confirm Password', with: 'password')
    click_on "Sign Me Up!"
    # assert_selector "h1", text: 'Pammy Yaeber'
    page.has_content?('Pammy Yaeber')
    click_on "Topics"
    # assert_selector "h1", text: "All Training Topics"
    page.has_content?('All Training Topics')
    # assert_selector "h4", text: "SOMETHING"
    page.has_content?('SOMETHING')
    click_on "Test Product Group"
    # assert_selector "p.product-title", text: "TEST PRODUCT GROUP"
    page.has_content?('TEST PRODUCT GROUP')
    find("#test-product-group-onlinecourse-usa").find("input[type=submit]").click
    # assert_selector "h1", text: "Shopping Cart"
    page.has_content?('Shopping Cart')
    # assert_selector "a", text: "Online Course Test"
    page.has_content?('Online Course Test')
  end

  test "creating company and adding an individual employee" do
    # visit 'http://127.0.0.1/users/sign_up'
    visit "http://localhost:3000/users/sign_up"
    page.has_content?('Pammy Yaeber')
    fill_in('Full Name', with: 'Pammy Yaeber')
    fill_in('Email Address', with: 'pammyyaeber@test.com')
    fill_in('Password', with: 'password')
    fill_in('Confirm Password', with: 'password')
    click_on "Sign Me Up!"
    # assert_selector "h1", text: 'Pammy Yaeber'
    page.has_content?('Pammy Yaeber')
    # assert_selector "div.container", text: "×\nWelcome! You have signed up successfully."
    page.has_content?('Welcome! You have signed up successfully.')
    click_on "Create a Company"
    # assert_selector "h1", text: "New Company Account"
    page.has_content?('New Company Account')
    fill_in("Company/Branch Name", with: "Pammy's Company")
    fill_in("Address", with: "123 Sesame Street")
    fill_in("City", with: "New York")
    fill_in("Zip", with: "10108")
    select "New York", from: "company_state"
    click_on "Sign up"
    # assert_selector "h1", text: "Pammy's Company"
    page.has_content?("Pammy's Company")
  end
end

# TODO Tests to create:

# Making a purchase using a CouponCode
