require "application_system_test_case"

class CertificatesTest < ApplicationSystemTestCase
  setup do
    @certificate = certificates(:one)
  end

  test "visiting the index" do
    visit certificates_url
    assert_selector "h1", text: "Certificates"
  end

  test "creating a Certificate" do
    visit certificates_url
    click_on "New Certificate"

    fill_in "Cad price", with: @certificate.cad_price
    fill_in "Description", with: @certificate.description
    fill_in "Image data", with: @certificate.image_data
    fill_in "Intl shipping", with: @certificate.intl_shipping
    fill_in "Keywords", with: @certificate.keywords
    fill_in "Meta description", with: @certificate.meta_description
    fill_in "Name", with: @certificate.name
    fill_in "Price", with: @certificate.price
    fill_in "Priority shipping", with: @certificate.priority_shipping
    fill_in "Shipping price", with: @certificate.shipping_price
    fill_in "Slug", with: @certificate.slug
    click_on "Create Certificate"

    assert_text "Certificate was successfully created"
    click_on "Back"
  end

  test "updating a Certificate" do
    visit certificates_url
    click_on "Edit", match: :first

    fill_in "Cad price", with: @certificate.cad_price
    fill_in "Description", with: @certificate.description
    fill_in "Image data", with: @certificate.image_data
    fill_in "Intl shipping", with: @certificate.intl_shipping
    fill_in "Keywords", with: @certificate.keywords
    fill_in "Meta description", with: @certificate.meta_description
    fill_in "Name", with: @certificate.name
    fill_in "Price", with: @certificate.price
    fill_in "Priority shipping", with: @certificate.priority_shipping
    fill_in "Shipping price", with: @certificate.shipping_price
    fill_in "Slug", with: @certificate.slug
    click_on "Update Certificate"

    assert_text "Certificate was successfully updated"
    click_on "Back"
  end

  test "destroying a Certificate" do
    visit certificates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Certificate was successfully destroyed"
  end
end
