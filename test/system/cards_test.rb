require "application_system_test_case"

class CardsTest < ApplicationSystemTestCase
  setup do
    @card = cards(:one)
  end

  test "visiting the index" do
    visit cards_url
    assert_selector "h1", text: "Cards"
  end

  test "creating a Card" do
    visit cards_url
    click_on "New Card"

    fill_in "Cad price", with: @card.cad_price
    fill_in "Card group", with: @card.card_group_id
    fill_in "Intl shipping", with: @card.intl_shipping
    fill_in "Name", with: @card.name
    fill_in "Price", with: @card.price
    fill_in "Priority shipping", with: @card.priority_shipping
    fill_in "Shipping price", with: @card.shipping_price
    fill_in "Weight", with: @card.weight
    click_on "Create Card"

    assert_text "Card was successfully created"
    click_on "Back"
  end

  test "updating a Card" do
    visit cards_url
    click_on "Edit", match: :first

    fill_in "Cad price", with: @card.cad_price
    fill_in "Card group", with: @card.card_group_id
    fill_in "Intl shipping", with: @card.intl_shipping
    fill_in "Name", with: @card.name
    fill_in "Price", with: @card.price
    fill_in "Priority shipping", with: @card.priority_shipping
    fill_in "Shipping price", with: @card.shipping_price
    fill_in "Weight", with: @card.weight
    click_on "Update Card"

    assert_text "Card was successfully updated"
    click_on "Back"
  end

  test "destroying a Card" do
    visit cards_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Card was successfully destroyed"
  end
end
