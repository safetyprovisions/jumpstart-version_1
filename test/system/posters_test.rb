require "application_system_test_case"

class PostersTest < ApplicationSystemTestCase
  setup do
    @poster = posters(:one)
  end

  test "visiting the index" do
    visit posters_url
    assert_selector "h1", text: "Posters"
  end

  test "creating a Poster" do
    visit posters_url
    click_on "New Poster"

    fill_in "Cad price", with: @poster.cad_price
    fill_in "Description", with: @poster.description
    fill_in "Display format", with: @poster.display_format
    fill_in "Download data", with: @poster.download_data
    fill_in "Format", with: @poster.format
    fill_in "Image data", with: @poster.image_data
    fill_in "Intl shipping", with: @poster.intl_shipping
    fill_in "Name", with: @poster.name
    fill_in "Poster group", with: @poster.poster_group_id
    fill_in "Price", with: @poster.price
    fill_in "Priority shipping", with: @poster.priority_shipping
    fill_in "Shipping price", with: @poster.shipping_price
    fill_in "Weight", with: @poster.weight
    click_on "Create Poster"

    assert_text "Poster was successfully created"
    click_on "Back"
  end

  test "updating a Poster" do
    visit posters_url
    click_on "Edit", match: :first

    fill_in "Cad price", with: @poster.cad_price
    fill_in "Description", with: @poster.description
    fill_in "Display format", with: @poster.display_format
    fill_in "Download data", with: @poster.download_data
    fill_in "Format", with: @poster.format
    fill_in "Image data", with: @poster.image_data
    fill_in "Intl shipping", with: @poster.intl_shipping
    fill_in "Name", with: @poster.name
    fill_in "Poster group", with: @poster.poster_group_id
    fill_in "Price", with: @poster.price
    fill_in "Priority shipping", with: @poster.priority_shipping
    fill_in "Shipping price", with: @poster.shipping_price
    fill_in "Weight", with: @poster.weight
    click_on "Update Poster"

    assert_text "Poster was successfully updated"
    click_on "Back"
  end

  test "destroying a Poster" do
    visit posters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Poster was successfully destroyed"
  end
end
