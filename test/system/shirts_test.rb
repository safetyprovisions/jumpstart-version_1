require "application_system_test_case"

class ShirtsTest < ApplicationSystemTestCase
  setup do
    @shirt = shirts(:one)
  end

  test "visiting the index" do
    visit shirts_url
    assert_selector "h1", text: "Shirts"
  end

  test "creating a Shirt" do
    visit shirts_url
    click_on "New Shirt"

    fill_in "Cad price", with: @shirt.cad_price
    fill_in "Description", with: @shirt.description
    fill_in "Image data", with: @shirt.image_data
    fill_in "Intl shipping", with: @shirt.intl_shipping
    fill_in "Keywords", with: @shirt.keywords
    fill_in "Meta description", with: @shirt.meta_description
    fill_in "Name", with: @shirt.name
    fill_in "Price", with: @shirt.price
    fill_in "Priority shipping", with: @shirt.priority_shipping
    fill_in "Shipping price", with: @shirt.shipping_price
    fill_in "Slug", with: @shirt.slug
    fill_in "Weight", with: @shirt.weight
    click_on "Create Shirt"

    assert_text "Shirt was successfully created"
    click_on "Back"
  end

  test "updating a Shirt" do
    visit shirts_url
    click_on "Edit", match: :first

    fill_in "Cad price", with: @shirt.cad_price
    fill_in "Description", with: @shirt.description
    fill_in "Image data", with: @shirt.image_data
    fill_in "Intl shipping", with: @shirt.intl_shipping
    fill_in "Keywords", with: @shirt.keywords
    fill_in "Meta description", with: @shirt.meta_description
    fill_in "Name", with: @shirt.name
    fill_in "Price", with: @shirt.price
    fill_in "Priority shipping", with: @shirt.priority_shipping
    fill_in "Shipping price", with: @shirt.shipping_price
    fill_in "Slug", with: @shirt.slug
    fill_in "Weight", with: @shirt.weight
    click_on "Update Shirt"

    assert_text "Shirt was successfully updated"
    click_on "Back"
  end

  test "destroying a Shirt" do
    visit shirts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Shirt was successfully destroyed"
  end
end
