require "application_system_test_case"

class DigitalPostersTest < ApplicationSystemTestCase
  setup do
    @digital_poster = digital_posters(:one)
  end

  test "visiting the index" do
    visit digital_posters_url
    assert_selector "h1", text: "Digital Posters"
  end

  test "creating a Digital poster" do
    visit digital_posters_url
    click_on "New Digital Poster"

    fill_in "Cad price", with: @digital_poster.cad_price
    fill_in "Poster", with: @digital_poster.poster_id
    fill_in "Price", with: @digital_poster.price
    click_on "Create Digital poster"

    assert_text "Digital poster was successfully created"
    click_on "Back"
  end

  test "updating a Digital poster" do
    visit digital_posters_url
    click_on "Edit", match: :first

    fill_in "Cad price", with: @digital_poster.cad_price
    fill_in "Poster", with: @digital_poster.poster_id
    fill_in "Price", with: @digital_poster.price
    click_on "Update Digital poster"

    assert_text "Digital poster was successfully updated"
    click_on "Back"
  end

  test "destroying a Digital poster" do
    visit digital_posters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Digital poster was successfully destroyed"
  end
end
