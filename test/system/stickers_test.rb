require "application_system_test_case"

class StickersTest < ApplicationSystemTestCase
  setup do
    @sticker = stickers(:one)
  end

  test "visiting the index" do
    visit stickers_url
    assert_selector "h1", text: "Stickers"
  end

  test "creating a Sticker" do
    visit stickers_url
    click_on "New Sticker"

    check "Active" if @sticker.active
    fill_in "Cad price", with: @sticker.cad_price
    fill_in "Description", with: @sticker.description
    fill_in "Image data", with: @sticker.image_data
    fill_in "Intl shipping", with: @sticker.intl_shipping
    fill_in "Keywords", with: @sticker.keywords
    fill_in "Meta description", with: @sticker.meta_description
    fill_in "Name", with: @sticker.name
    fill_in "Price", with: @sticker.price
    fill_in "Priority shipping", with: @sticker.priority_shipping
    fill_in "Shipping price", with: @sticker.shipping_price
    fill_in "Slug", with: @sticker.slug
    fill_in "Weight", with: @sticker.weight
    click_on "Create Sticker"

    assert_text "Sticker was successfully created"
    click_on "Back"
  end

  test "updating a Sticker" do
    visit stickers_url
    click_on "Edit", match: :first

    check "Active" if @sticker.active
    fill_in "Cad price", with: @sticker.cad_price
    fill_in "Description", with: @sticker.description
    fill_in "Image data", with: @sticker.image_data
    fill_in "Intl shipping", with: @sticker.intl_shipping
    fill_in "Keywords", with: @sticker.keywords
    fill_in "Meta description", with: @sticker.meta_description
    fill_in "Name", with: @sticker.name
    fill_in "Price", with: @sticker.price
    fill_in "Priority shipping", with: @sticker.priority_shipping
    fill_in "Shipping price", with: @sticker.shipping_price
    fill_in "Slug", with: @sticker.slug
    fill_in "Weight", with: @sticker.weight
    click_on "Update Sticker"

    assert_text "Sticker was successfully updated"
    click_on "Back"
  end

  test "destroying a Sticker" do
    visit stickers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sticker was successfully destroyed"
  end
end
