require 'test_helper'

class ProductGroupTest < ActiveSupport::TestCase

  test "should save product_group with all attributes" do
    product_group = ProductGroup.new(
      name: "Test Product Group",
      image: cached_product_group_image,
      video: "MjA0NjIw",
      overview: "Lorem ipsum",
      training_standards: "Lorem ipsum",
      facts: "Lorem ipsum",
      fails: "Lorem ipsum"
      )

    assert product_group.save, "Did not save the product_group"
  end

  test "should not save product_group without name" do
    product_group = ProductGroup.new(
      # name: "Test Product Group",
      image: cached_product_group_image,
      video: "MjA0NjIw",
      overview: "Lorem ipsum",
      training_standards: "Lorem ipsum",
      facts: "Lorem ipsum",
      fails: "Lorem ipsum"
      )
    assert_not product_group.save, "Saved product_group without name"
  end

  test "should not save product_group without image" do
    product_group = ProductGroup.new(
      name: "Test Product Group",
      # image: cached_product_group_image,
      video: "MjA0NjIw",
      overview: "Lorem ipsum",
      training_standards: "Lorem ipsum",
      facts: "Lorem ipsum",
      fails: "Lorem ipsum"
      )
    assert_not product_group.save, "Saved product_group without image"
  end

  test "should not save product_group without video" do
    product_group = ProductGroup.new(
      name: "Test Product Group",
      image: cached_product_group_image,
      # video: "MjA0NjIw",
      overview: "Lorem ipsum",
      training_standards: "Lorem ipsum",
      facts: "Lorem ipsum",
      fails: "Lorem ipsum"
      )
    assert_not product_group.save, "Saved product_group without video"
  end

  test "should not save product_group without overview" do
    product_group = ProductGroup.new(
      name: "Test Product Group",
      image: cached_product_group_image,
      video: "MjA0NjIw",
      # overview: "Lorem ipsum",
      training_standards: "Lorem ipsum",
      facts: "Lorem ipsum",
      fails: "Lorem ipsum"
      )
    assert_not product_group.save, "Saved product_group without overview"
  end

  test "should not save product_group without training_standards" do
    product_group = ProductGroup.new(
      name: "Test Product Group",
      image: cached_product_group_image,
      video: "MjA0NjIw",
      overview: "Lorem ipsum",
      # training_standards: "Lorem ipsum",
      facts: "Lorem ipsum",
      fails: "Lorem ipsum"
      )
    assert_not product_group.save, "Saved product_group without training_standards"
  end

  test "should not save product_group without facts" do
    product_group = ProductGroup.new(
      name: "Test Product Group",
      image: cached_product_group_image,
      video: "MjA0NjIw",
      overview: "Lorem ipsum",
      training_standards: "Lorem ipsum",
      # facts: "Lorem ipsum",
      fails: "Lorem ipsum"
      )
    assert_not product_group.save, "Saved product_group without facts"
  end

  test "should not save product_group without fails" do
    product_group = ProductGroup.new(
      name: "Test Product Group",
      image: cached_product_group_image,
      video: "MjA0NjIw",
      overview: "Lorem ipsum",
      training_standards: "Lorem ipsum",
      facts: "Lorem ipsum",
      # fails: "Lorem ipsum"
      )
    assert_not product_group.save, "Saved product_group without fails"
  end
end

    # t.string "name", null: false
    # t.text "image_data", null: false
    # t.datetime "created_at", null: false
    # t.datetime "updated_at", null: false
    # t.string "video", null: false
    # t.text "overview", null: false
    # t.text "training_standards", null: false
    # t.text "facts", null: false
    # t.text "fails", null: false
    # t.string "slug"
    # t.text "keywords"
    # t.text "meta_description"
