require 'test_helper'

class TttActivationTest < ActiveSupport::TestCase

  test "should save ttt_activation with all attributes" do
    ttt_activation = TttActivation.new(
      user: users(:regular),
      order: orders(:one),
      train_the_trainer: train_the_trainers(:one),
      )
    assert ttt_activation.save, "Did not save the ttt_activation"
  end

  test "should not save ttt_activation without user" do
    ttt_activation = TttActivation.new(
      # user: users(:regular),
      order: orders(:one),
      train_the_trainer: train_the_trainers(:one),
      )
    assert_not ttt_activation.save, "Saved ttt_activation without user"
  end

  test "should not save ttt_activation without order" do
    ttt_activation = TttActivation.new(
      user: users(:regular),
      # order: orders(:one),
      train_the_trainer: train_the_trainers(:one),
      )
    assert_not ttt_activation.save, "Saved ttt_activation without order"
  end

  test "should not save ttt_activation without train_the_trainer" do
    ttt_activation = TttActivation.new(
      user: users(:regular),
      order: orders(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not ttt_activation.save, "Saved ttt_activation without train_the_trainer"
  end
end

    # t.bigint "user_id"
    # t.bigint "order_id"
    # t.bigint "train_the_trainer_id"
