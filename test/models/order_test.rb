# TODO NEED TO BUILD SOME SORT OF VALIDATIONS TO VERIFY I AM GETTING WHAT IS REQUIRED ALONG THE WAY!

require 'test_helper'

class OrderTest < ActiveSupport::TestCase

  test "should save order with all attributes" do
    order = Order.new(
      user: users(:regular),
      status: "draft",
      stripe_id: 'stripe_id',
      card_brand: 'Visa',
      card_last4: '4242',
      card_exp_month: '04',
      card_exp_year: '24',
      paid_at: Time.now,
      # shipment_id: ,
      # to_name: ,
      # to_street1: ,
      # to_street2: ,
      # to_city: ,
      # to_state: ,
      # to_zip: ,
      # to_country: ,
      # to_email: ,
      download_start_time: Time.now,
      # additional_info: ,
      shipping_method: "shipping_price",
      order_placed_at: Time.now,
      # group_training_date: ,
      # label_created: ,
      # label_url: ,
      # tracking_code: ,
      idaho_resident: 'false',
      currency: 'usd',
      # ahoy_visit_id: ,
      card_zip_code: '90210',
      final_total: '7900'
      )
    assert order.save, "Did not save the order"
  end
end

    # t.bigint "user_id"
    # t.string "status", default: "draft", null: false
    # t.string "stripe_id"
    # t.string "card_brand"
    # t.string "card_last4"
    # t.string "card_exp_month"
    # t.string "card_exp_year"
    # t.datetime "created_at", null: false
    # t.datetime "updated_at", null: false
    # t.datetime "paid_at"
    # t.string "shipment_id"
    # t.string "to_name"
    # t.string "to_street1"
    # t.string "to_city"
    # t.string "to_state"
    # t.string "to_zip"
    # t.string "to_country"
    # t.string "to_email"
    # t.datetime "download_start_time", default: -> { "CURRENT_TIMESTAMP" }, null: false
    # t.text "additional_info"
    # t.text "shipping_method", default: "shipping_price", null: false
    # t.datetime "order_placed_at"
    # t.date "group_training_date"
    # t.datetime "label_created"
    # t.string "label_url"
    # t.string "tracking_code"
    # t.boolean "idaho_resident", default: false
    # t.string "currency"
    # t.bigint "ahoy_visit_id"
    # t.string "to_street2"
    # t.string "card_zip_code"
    # t.integer "final_total"
