require 'test_helper'

class BackupTest < ActiveSupport::TestCase

  test "should save backup with all attributes" do
    backup = Backup.new(
      format: 'CD',
      price: 2500,
      shipping_price: 1000,
      priority_shipping: 1500,
      intl_shipping: 1500,
      cad_price: 3500)
    assert backup.save, "Did not save the backup"
  end

  test "should not save backup without format" do
    backup = Backup.new(
      # format: 'usb',
      price: 2500,
      shipping_price: 1000,
      priority_shipping: 1500,
      intl_shipping: 1500,
      cad_price: 3500)
    assert_not backup.save, "Saved the backup without format"
  end

  test "should not save backup without price" do
    backup = Backup.new(
      format: 'usb',
      # price: 2500,
      shipping_price: 1000,
      priority_shipping: 1500,
      intl_shipping: 1500,
      cad_price: 3500)
    assert_not backup.save, "Saved the backup without price"
  end

  test "should not save backup without shipping_price" do
    backup = Backup.new(
      format: 'usb',
      price: 2500,
      # shipping_price: 1000,
      priority_shipping: 1500,
      intl_shipping: 1500,
      cad_price: 3500)
    assert_not backup.save, "Saved the backup without shipping_price"
  end

  test "should not save backup without priority_shipping" do
    backup = Backup.new(
      format: 'usb',
      price: 2500,
      shipping_price: 1000,
      # priority_shipping: 1500,
      intl_shipping: 1500,
      cad_price: 3500)
    assert_not backup.save, "Saved the backup without priority_shipping"
  end

  test "should not save backup without intl_shipping" do
    backup = Backup.new(
      format: 'usb',
      price: 2500,
      shipping_price: 1000,
      priority_shipping: 1500,
      # intl_shipping: 1500,
      cad_price: 3500)
    assert_not backup.save, "Saved the backup without intl_shipping"
  end

  test "should not save backup without cad_price" do
    backup = Backup.new(
      format: 'usb',
      price: 2500,
      shipping_price: 1000,
      priority_shipping: 1500,
      intl_shipping: 1500,
      # cad_price: 3500
      )
    assert_not backup.save, "Saved the backup without cad_price"
  end

  test "should not save backup price less than 50 cents" do
    backup = Backup.new(
      format: 'usb',
      price: 0,
      shipping_price: 1000,
      priority_shipping: 1500,
      intl_shipping: 1500,
      cad_price: 3500
      )
    assert_not backup.save, "Saved the backup with price less than 50 cents"
  end

end
