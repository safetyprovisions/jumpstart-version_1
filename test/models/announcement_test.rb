require 'test_helper'

class AnnouncementTest < ActiveSupport::TestCase

  test "should save article with all attributes" do
    announcement = Announcement.new(
      announcement_type: 'update',
      description: 'Text',
      published_at: Time.now,
      name: "Announcement Name")
    assert announcement.save, "Did not save the announcement"
  end

  test "should not save article without name" do
    announcement = Announcement.new(
      announcement_type: 'update',
      description: 'Text',
      published_at: Time.now)
    assert_not announcement.save, "Saved the announcement without a name"
  end

  test "should not save article without a description" do
    announcement = Announcement.new(
      announcement_type: 'update',
      published_at: Time.now,
      name: "Announcement Name")
    assert_not announcement.save, "Saved the announcement without an description"
  end

  test "should save article without announcement_type because it has a default" do
    announcement = Announcement.new(
      description: 'Text',
      published_at: Time.now,
      name: "Announcement Name")
    assert announcement.save, "Did not save the announcement with a default announcement_type"
  end

  test "should save article without published_at date because it has a default" do
    announcement = Announcement.new(
      announcement_type: 'update',
      description: 'Text',
      name: "Announcement Name")
    assert announcement.save, "Did not save the announcement with a default published_at date"
  end
end
