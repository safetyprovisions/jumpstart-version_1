require 'test_helper'

class BulkPriceLevelTest < ActiveSupport::TestCase

  test "should save bulk_price_level with all attributes" do
    bulk_price_level = BulkPriceLevel.new(
      qty: 25,
      price: 187500,
      cad_price: 237500,
      tier: '1',
      )
    assert bulk_price_level.save, "Did not save the bulk_price_level"
  end

  test "should not save bulk_price_level without qty" do
    bulk_price_level = BulkPriceLevel.new(
      # qty: 25,
      price: 187500,
      cad_price: 237500,
      tier: '1',
      )
    assert_not bulk_price_level.save, "Saved bulk_price_level without qty"
  end

  test "should not save bulk_price_level without price" do
    bulk_price_level = BulkPriceLevel.new(
      qty: 25,
      # price: 187500,
      cad_price: 237500,
      tier: '1',
      )
    assert_not bulk_price_level.save, "Saved bulk_price_level without price"
  end

  test "should not save bulk_price_level without cad_price" do
    bulk_price_level = BulkPriceLevel.new(
      qty: 25,
      price: 187500,
      # cad_price: 237500,
      tier: '1',
      )
    assert_not bulk_price_level.save, "Saved bulk_price_level without cad_price"
  end

  test "should not save bulk_price_level without tier" do
    bulk_price_level = BulkPriceLevel.new(
      qty: 25,
      price: 187500,
      cad_price: 237500,
      # tier: '1',
      )
    assert_not bulk_price_level.save, "Saved bulk_price_level without tier"
  end

  test "should not save bulk_price_level with price < 500" do
    bulk_price_level = BulkPriceLevel.new(
      qty: 25,
      price: 0,
      cad_price: 237500,
      tier: '1'
      )
    assert_not bulk_price_level.save, "Saved bulk_price_level with price < 500"
  end

end
