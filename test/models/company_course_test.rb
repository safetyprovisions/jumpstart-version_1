require 'test_helper'

class CompanyCourseTest < ActiveSupport::TestCase

  test "should save company_course with online_course and no train_the_trainer" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      online_course: online_courses(:one),
      order: orders(:one),
      quantity: 1,
      currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert company_course.save, "Did not save the company_course with online_course and no train_the_trainer"
  end

  test "should save company_course with train_the_trainer and no online_course" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      # online_course: online_courses(:one),
      order: orders(:one),
      quantity: 1,
      currency: 'usd',
      company: companies(:one),
      train_the_trainer: train_the_trainers(:one),
      )
    assert company_course.save, "Did not save the company_course with train_the_trainer and no online_course"
  end

  test "should not save company_course with online_course and train_the_trainer" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      online_course: online_courses(:one),
      order: orders(:one),
      quantity: 1,
      currency: 'usd',
      company: companies(:one),
      train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved company_course with online_course and train_the_trainer"
  end

  test "should not save company_course without user" do
    company_course = CompanyCourse.new(
      # user: users(:regular),
      online_course: online_courses(:one),
      order: orders(:one),
      quantity: 1,
      currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course without user"
  end

  test "should not save company_course without online_course" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      # online_course: online_courses(:one),
      order: orders(:one),
      quantity: 1,
      currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course without online_course"
  end

  test "should not save company_course without order" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      online_course: online_courses(:one),
      # order: orders(:one),
      quantity: 1,
      currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course without order"
  end

  test "should not save company_course without quantity" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      online_course: online_courses(:one),
      order: orders(:one),
      # quantity: 1,
      currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course without quantity"
  end

  test "should not save company_course without currency" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      online_course: online_courses(:one),
      order: orders(:one),
      quantity: 1,
      # currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course without currency"
  end

  test "should not save company_course without company" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      online_course: online_courses(:one),
      order: orders(:one),
      quantity: 1,
      currency: 'usd',
      # company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course without company"
  end

  test "should not save company_course without online_course or train_the_trainer" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      # online_course: online_courses(:one),
      order: orders(:one),
      quantity: 1,
      currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course without online_course or train_the_trainer"
  end

  test "should not save company_course with quantity < 0" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      online_course: online_courses(:one),
      order: orders(:one),
      quantity: -1,
      currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course with quantity < 0"
  end

  test "should not save company_course with non-numeric quantity" do
    company_course = CompanyCourse.new(
      user: users(:regular),
      online_course: online_courses(:one),
      order: orders(:one),
      quantity: 'abc',
      currency: 'usd',
      company: companies(:one),
      # train_the_trainer: train_the_trainers(:one),
      )
    assert_not company_course.save, "Saved the company_course with non-numeric quantity"
  end

end
