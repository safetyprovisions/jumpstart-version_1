require 'test_helper'

class NonCatalogCourseAccessTest < ActiveSupport::TestCase

  test "should save non_catalog_course_access with all attributes" do
    non_catalog_course_access = NonCatalogCourseAccess.new(
      online_course: online_courses(:one),
      company: companies(:one)
      )
    assert non_catalog_course_access.save, "Did not save the non_catalog_course_access"
  end

  test "should not save non_catalog_course_access without online_course" do
    non_catalog_course_access = NonCatalogCourseAccess.new(
      # online_course: online_courses(:one),
      company: companies(:one)
      )
    assert_not non_catalog_course_access.save, "Saved non_catalog_course_access without online_course"
  end

  test "should not save non_catalog_course_access without company" do
    non_catalog_course_access = NonCatalogCourseAccess.new(
      online_course: online_courses(:one),
      # company: companies(:one)
      )
    assert_not non_catalog_course_access.save, "Saved non_catalog_course_access without company"
  end

  test "should not save identical non_catalog_course_access" do
    non_catalog_course_access = NonCatalogCourseAccess.new(
      online_course: online_courses(:one),
      company: companies(:one)
      )
    non_catalog_course_access.save
    non_catalog_course_access_2 = NonCatalogCourseAccess.new(
      online_course: online_courses(:one),
      company: companies(:one)
      )
    assert_not non_catalog_course_access_2.save, "Saved identical non_catalog_course_access"
  end
end

    # t.bigint "online_course_id"
    # t.bigint "company_id"
