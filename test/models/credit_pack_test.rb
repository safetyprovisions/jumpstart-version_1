require 'test_helper'

class CreditPackTest < ActiveSupport::TestCase

  test "should save credit_pack with all USD attributes" do
    credit_pack = CreditPack.new(
      name: 'Credit Pack One',
      price: 7777,
      cad_price: 0,
      credits: 7777,
      cad_credits: 0
      )
    assert credit_pack.save, "Did not save the USD credit_pack"
  end

  test "should save credit_pack with all CAD attributes" do
    credit_pack = CreditPack.new(
      name: 'Credit Pack Two',
      price: 0,
      cad_price: 7777,
      credits: 0,
      cad_credits: 7777
      )
    assert credit_pack.save, "Did not save the CAD credit_pack"
  end

  test "should not save credit_pack with USD and CAD attributes" do
    credit_pack = CreditPack.new(
      name: 'Credit Pack Two',
      price: 7777,
      cad_price: 7777,
      credits: 7777,
      cad_credits: 7777
      )
    assert_not credit_pack.save, "Saved the credit_pack with USD and CAD attributes"
  end

  test "should not save credit_pack with USD and CAD price" do
    credit_pack = CreditPack.new(
      name: 'Credit Pack Two',
      price: 7777,
      cad_price: 7777,
      credits: 7777,
      cad_credits: 0
      )
    assert_not credit_pack.save, "Saved the credit_pack with USD and CAD price"
  end

  test "should not save credit_pack with USD and CAD credits" do
    credit_pack = CreditPack.new(
      name: 'Credit Pack Two',
      price: 7777,
      cad_price: 0,
      credits: 7777,
      cad_credits: 7777
      )
    assert_not credit_pack.save, "Saved the credit_pack with USD and CAD credits"
  end

  test "should not save credit_pack with USD price and CAD credits" do
    credit_pack = CreditPack.new(
      name: 'Credit Pack Two',
      price: 7777,
      cad_price: 0,
      credits: 0,
      cad_credits: 7777
      )
    assert_not credit_pack.save, "Saved the credit_pack with USD price and CAD credits"
  end
end

    # t.string "name", null: false
    # t.integer "price", default: 0, null: false
    # t.integer "credits", default: 0, null: false
    # t.integer "cad_price", default: 0, null: false
    # t.integer "cad_credits", default: 0, null: false
