require 'test_helper'

class CouponCodeTest < ActiveSupport::TestCase

  test "should save coupon_code with all attributes" do
    coupon_code = CouponCode.new(
      code: 'testflatratecode',
      scope: 'order_total',
      flat_rate: 500,
      percentage: '',
      min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    assert coupon_code.save, "Did not save the coupon_code"
  end

  test "should not save duplicate coupon_code" do
    coupon_code = CouponCode.new(
      code: 'testflatratecode',
      scope: 'order_total',
      flat_rate: 500,
      percentage: '',
      min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    coupon_code.save
    coupon_code_2 = CouponCode.new(
      code: 'testflatratecode',
      scope: 'order_total',
      flat_rate: 500,
      percentage: '',
      min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    assert_not coupon_code_2.save, "Saved duplicate coupon_code"
  end

  test "should not save coupon_code without code" do
    coupon_code = CouponCode.new(
      # code: 'testflatratecode',
      scope: 'order_total',
      flat_rate: 500,
      percentage: '',
      min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    assert_not coupon_code.save, "Saved coupon_code without code"
  end

  test "should not save coupon_code without scope" do
    coupon_code = CouponCode.new(
      code: 'testflatratecode',
      # scope: 'order_total',
      flat_rate: 500,
      percentage: '',
      min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    assert_not coupon_code.save, "Saved coupon_code without scope"
  end

  test "should not save coupon_code without flat_rate or percentage" do
    coupon_code = CouponCode.new(
      code: 'testflatratecode',
      scope: 'order_total',
      # flat_rate: 500,
      # percentage: '',
      min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    assert_not coupon_code.save, "Saved coupon_code without flat_rate or percentage"
  end

  test "should not save coupon_code with both flat_rate and percentage" do
    coupon_code = CouponCode.new(
      code: 'testflatratecode',
      scope: 'order_total',
      flat_rate: 500,
      percentage: 25,
      min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    assert_not coupon_code.save, "Saved coupon_code with both flat_rate and percentage"
  end

  test "should not save order_total coupon_code without min_purchase_amt" do
    coupon_code = CouponCode.new(
      code: 'testflatratecode',
      scope: 'order_total',
      flat_rate: 500,
      percentage: '',
      # min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    assert_not coupon_code.save, "Saved order_total coupon_code without min_purchase_amt"
  end

  test "should not save order_total coupon_code with flat_rate < 100" do
    coupon_code = CouponCode.new(
      code: 'testflatratecode',
      scope: 'order_total',
      flat_rate: 50,
      percentage: '',
      min_purchase_amt: 5000,
      valid_from: Time.now,
      valid_to: 1.year.from_now
      )
    assert_not coupon_code.save, "Saved order_total coupon_code with flat_rate < 100"
  end
end

    # t.string "code", null: false
    # t.text "scope", null: false
    # t.integer "flat_rate"
    # t.integer "percentage"
    # t.integer "min_purchase_amt"
    # t.date "valid_from", null: false
    # t.date "valid_to", null: false
