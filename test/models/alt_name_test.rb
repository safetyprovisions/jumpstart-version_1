require 'test_helper'

class AltNameTest < ActiveSupport::TestCase

  test "should save alt_name with all attributes" do
    alt_name = AltName.new(
      product_group: product_groups(:one),
      name: "Alt Name")
    assert alt_name.save, "Did not save the alt_name"
  end

  test "should not save alt_name without product_group" do
    alt_name = AltName.new(
      name: "Alt Name")
    assert_not alt_name.save, "Saved the alt_name without product_group"
  end

  test "should not save alt_name without name" do
    alt_name = AltName.new(
      product_group: product_groups(:one))
    assert_not alt_name.save, "Saved the alt_name without name"
  end

  test "should not save identical alt_name" do
    alt_name = AltName.new(
      product_group: product_groups(:one),
      name: "Alt Name")
    alt_name.save
    alt_name_two = AltName.new(
      product_group: product_groups(:one),
      name: "Alt Name")
    assert_not alt_name_two.save, "Saved identical alt_name"
  end
end
