require 'test_helper'

class OnlineCourseActivationTest < ActiveSupport::TestCase

  test "should save online_course_activation with all attributes" do
    online_course_activation = OnlineCourseActivation.new(
      user: users(:regular),
      order: orders(:one),
      online_course: online_courses(:one)
      )
    assert online_course_activation.save, "Did not save the online_course_activation"
  end

  test "should not save online_course_activation without user" do
    online_course_activation = OnlineCourseActivation.new(
      # user: users(:regular),
      order: orders(:one),
      online_course: online_courses(:one)
      )
    assert_not online_course_activation.save, "Saved online_course_activation without user"
  end

  test "should not save online_course_activation without order" do
    online_course_activation = OnlineCourseActivation.new(
      user: users(:regular),
      # order: orders(:one),
      online_course: online_courses(:one)
      )
    assert_not online_course_activation.save, "Saved online_course_activation without order"
  end

  test "should not save online_course_activation without online_course" do
    online_course_activation = OnlineCourseActivation.new(
      user: users(:regular),
      order: orders(:one),
      # online_course: online_courses(:one)
      )
    assert_not online_course_activation.save, "Saved online_course_activation without online_course"
  end
end

    # t.bigint "user_id"
    # t.bigint "order_id"
    # t.bigint "online_course_id"
