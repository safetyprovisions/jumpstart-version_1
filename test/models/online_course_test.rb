require 'test_helper'

class OnlineCourseTest < ActiveSupport::TestCase

  test "should save online_course with all attributes" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert online_course.save, "Did not save the online_course"
  end

  test "should not save online_course without title" do
    online_course = OnlineCourse.new(
      # title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without title"
  end

  test "should not save online_course without price" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      # price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without price"
  end

  test "should not save online_course without price > 0" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 0,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without price > 0"
  end

  test "should not save online_course without cad_price" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      # cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without cad_price"
  end

  test "should not save online_course without cad_price > 0" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 0,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without cad_price > 0"
  end

  test "should not save online_course without language" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      # language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without language"
  end

  test "should not save online_course without standards" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      # standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without standards"
  end

  test "should not save online_course without product_group" do
    assert_raise ActiveRecord::RecordNotFound do
      online_course = OnlineCourse.new(
        title: 'Test Online Course',
        price: 7900,
        # sale_price: ,
        cad_price: 9900,
        # cad_sale_price: ,
        sale_start: Time.now,
        sale_expiry: Time.now,
        language: 'english',
        standards: 'osha',
        product_group_id: '',
        talent_course_id: '234',
        practical_evaluation: practical_evaluation,
        exam: exam,
        answer_key: answer_key,
        market_type: 'usa',
        public: 'true'
        )
      online_course.save
    end
  end

  test "should not save online_course without talent_course_id" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      # talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without talent_course_id"
  end

  test "should not save online_course without practical_evaluation" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      # practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without practical_evaluation"
  end

  test "should not save online_course without exam" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      # exam: exam,
      answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without exam"
  end

  test "should not save online_course without answer_key" do
    online_course = OnlineCourse.new(
      title: 'Test Online Course',
      price: 7900,
      # sale_price: ,
      cad_price: 9900,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: '234',
      practical_evaluation: practical_evaluation,
      exam: exam,
      # answer_key: answer_key,
      market_type: 'usa',
      public: 'true'
      )
    assert_not online_course.save, "Saved online_course without answer_key"
  end
end
