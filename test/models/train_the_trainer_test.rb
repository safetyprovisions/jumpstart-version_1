require 'test_helper'

class TrainTheTrainerTest < ActiveSupport::TestCase

  test "should save train_the_trainer with all attributes" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert train_the_trainer.save, "Did not save the train_the_trainer"
  end

  test "should not save train_the_trainer without title" do
    train_the_trainer = TrainTheTrainer.new(
      # title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without title"
  end

  test "should not save train_the_trainer without price" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      # price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without price"
  end

  test "should not save train_the_trainer without price > 0" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 0,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without price > 0"
  end

  test "should not save train_the_trainer without cad_price" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      # cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without cad_price"
  end

  test "should not save train_the_trainer without cad_price > 0" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 0,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without cad_price > 0"
  end

  test "should not save train_the_trainer without language" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      # language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without language"
  end

  test "should not save train_the_trainer without standards" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      # standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without standards"
  end

  test "should not save train_the_trainer without product_group" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      # product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without product_group"
  end

  test "should not save train_the_trainer without talent_course_id" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      # talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without talent_course_id"
  end

  test "should not save train_the_trainer without kit" do
    assert_raise NoMethodError do
      train_the_trainer = TrainTheTrainer.new(
        title: 'Test Train The Trainer',
        price: 70000,
        # sale_price: ,
        cad_price: 90000,
        # cad_sale_price: ,
        sale_start: Time.now,
        sale_expiry: Time.now,
        language: 'english',
        standards: 'osha',
        product_group: product_groups(:one),
        talent_course_id: 345,
        # kit: kits(:one),
        practical_evaluation: practical_evaluation,
        exam: exam,
        answer_key:  answer_key
        )
      assert_not train_the_trainer.save
    end
  end

  test "should not save train_the_trainer without practical_evaluation" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      # practical_evaluation: practical_evaluation,
      exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without practical_evaluation"
  end

  test "should not save train_the_trainer without exam" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      # exam: exam,
      answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without exam"
  end

  test "should not save train_the_trainer without answer_key" do
    train_the_trainer = TrainTheTrainer.new(
      title: 'Test Train The Trainer',
      price: 70000,
      # sale_price: ,
      cad_price: 90000,
      # cad_sale_price: ,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      talent_course_id: 345,
      kit: kits(:one),
      practical_evaluation: practical_evaluation,
      exam: exam,
      # answer_key:  answer_key
      )
    assert_not train_the_trainer.save, "Saved train_the_trainer without answer_key"
  end
end

    # t.string "title", null: false
    # t.integer "price", null: false
    # t.integer "sale_price"
    # t.date "sale_start", default: -> { "CURRENT_TIMESTAMP" }, null: false
    # t.date "sale_expiry", default: -> { "CURRENT_TIMESTAMP" }, null: false
    # t.integer "language", null: false
    # t.integer "standards", null: false
    # t.integer "product_group_id", null: false
    # t.integer "talent_course_id", null: false
    # t.bigint "kit_id", null: false
    # t.datetime "created_at", null: false
    # t.datetime "updated_at", null: false
    # t.integer "cad_price", null: false
    # t.integer "cad_sale_price"
    # t.text "practical_evaluation_data", null: false
    # t.text "exam_data", null: false
    # t.text "answer_key_data", null: false
