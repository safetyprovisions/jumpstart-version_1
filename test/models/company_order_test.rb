require 'test_helper'

class CompanyOrderTest < ActiveSupport::TestCase

  test "should save company_order with all attributes" do
    company_order = CompanyOrder.new(
      user: users(:regular),
      order: orders(:one),
      company: companies(:one)
      )
    assert company_order.save, "Did not save the company_order with all attributes"
  end

  test "should not save company_order without user" do
    company_order = CompanyOrder.new(
      # user: users(:regular),
      order: orders(:one),
      company: companies(:one)
      )
    assert_not company_order.save, "Saved the company_order without user"
  end

  test "should not save company_order without order" do
    company_order = CompanyOrder.new(
      user: users(:regular),
      # order: orders(:one),
      company: companies(:one)
      )
    assert_not company_order.save, "Saved the company_order without order"
  end

  test "should not save company_order without company" do
    company_order = CompanyOrder.new(
      user: users(:regular),
      order: orders(:one),
      # company: companies(:one)
      )
    assert_not company_order.save, "Saved the company_order without company"
  end
end
