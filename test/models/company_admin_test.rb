require 'test_helper'

class CompanyAdminTest < ActiveSupport::TestCase

  test "should save company_admin with all attributes" do
    company_admin = CompanyAdmin.new(
      user: users(:regular),
      company: companies(:one)
      )
    assert company_admin.save, "Did not save the company_admin"
  end

  test "should not save company_admin without user" do
    company_admin = CompanyAdmin.new(
      # user: users(:regular),
      company: companies(:one)
      )
    assert_not company_admin.save, "Saved the company_admin without user"
  end

  test "should not save company_admin without company" do
    company_admin = CompanyAdmin.new(
      user: users(:regular),
      # company: companies(:one)
      )
    assert_not company_admin.save, "Saved the company_admin without company"
  end

  test "should not save the same company_admin twice" do
    company_admin = CompanyAdmin.new(
      user: users(:regular),
      company: companies(:one)
      )
    company_admin.save
    company_admin_two = CompanyAdmin.new(
      user: users(:regular),
      company: companies(:one)
      )
    assert_not company_admin_two.save, "Saved the same company_admin twice"
  end

end
