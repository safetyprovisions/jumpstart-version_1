require 'test_helper'

class GroupTrainingTest < ActiveSupport::TestCase

  test "should save group_training with all attributes" do
    group_training = GroupTraining.new(
      online_course: online_courses(:two)
      )
    assert group_training.save, "Did not save the group_training"
  end

  test "should not save group_training without all attributes" do
    group_training = GroupTraining.new(
      # online_course: online_courses(:one)
      )
    assert_not group_training.save, "Saved the group_training without all attributes"
  end
end

# t.bigint "online_course_id", null: false
