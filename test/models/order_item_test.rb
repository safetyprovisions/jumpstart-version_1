# TODO NEED TO BUILD VALIDATIONS FOR THESE TO MAKE SURE WE ARE GETTING WHAT IS REQUIRED
# Should product_id be removed?

require 'test_helper'

class OrderItemTest < ActiveSupport::TestCase

  test "should save order_item with all attributes" do
    order_item = OrderItem.new(
      order: orders(:one),
      # product: ,
      quantity: 1,
      order_item_type: "",
      orderable_type: 'Backup',
      orderable: backups(:one),
      shipping_method: "none"
      # backup_for:
      )
    assert order_item.save, "Did not save the order_item"
  end

  test "should not save order_item without order" do
    order_item = OrderItem.new(
      # order: orders(:one),
      # product: ,
      quantity: 1,
      order_item_type: "",
      orderable_type: 'Backup',
      orderable: backups(:one),
      shipping_method: "none"
      # backup_for:
      )
    assert_not order_item.save, "Saved the order_item without order"
  end

  test "should not save order_item without quantity" do
    order_item = OrderItem.new(
      order: orders(:one),
      # product: ,
      # quantity: 1,
      order_item_type: "",
      orderable_type: 'Backup',
      orderable: backups(:one),
      shipping_method: "none"
      # backup_for:
      )
    assert_not order_item.save, "Saved the order_item without quantity"
  end
end

    # t.bigint "order_id"
    # t.bigint "product_id"
    # t.integer "quantity", default: 0, null: false
    # t.datetime "created_at", null: false
    # t.datetime "updated_at", null: false
    # t.string "order_item_type", default: "", null: false
    # t.string "orderable_type"
    # t.bigint "orderable_id"
    # t.text "shipping_method", default: "none", null: false
    # t.string "backup_for"
