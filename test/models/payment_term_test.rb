require 'test_helper'

class PaymentTermTest < ActiveSupport::TestCase

  test "should save payment_term with all attributes" do
    payment_term = PaymentTerm.new(
      company: companies(:one),
      term: 30
      )
    assert payment_term.save, "Did not save the payment_term"
  end

  test "should not save payment_term without company" do
    payment_term = PaymentTerm.new(
      # company: companies(:one),
      term: 30
      )
    assert_not payment_term.save, "Saved the payment_term without company"
  end

  test "should not save payment_term without term" do
    payment_term = PaymentTerm.new(
      company: companies(:one),
      # term: 30
      )
    assert_not payment_term.save, "Did not save the payment_term without term"
  end
end

    # t.bigint "company_id", null: false
    # t.integer "term", null: false
