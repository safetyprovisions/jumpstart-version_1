require 'test_helper'

class KitTest < ActiveSupport::TestCase

  test "should save kit with all attributes" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert kit.save, "Did not save the kit"
  end

  test "should not save kit without title" do
    kit = Kit.new(
      # title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without title"
  end

  test "should not save kit without price" do
    kit = Kit.new(
      title: 'Test Kit',
      # price: 7900,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without price"
  end

  test "should not save kit without price > 0" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 0,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without price > 0"
  end

  test "should not save kit without cad_price > 0" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      cad_price: 0,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without cad_price > 0"
  end

  test "should not save kit without cad_price" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      # cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without cad_price"
  end

  test "should not save kit without language" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      # language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without language"
  end

  test "should not save kit without standards" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      # standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without standards"
  end

  test "should not save kit without product_group" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      # product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without product_group"
  end

  test "should not save kit without sample" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      # sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without sample"
  end

  test "should not save kit without download" do
    kit = Kit.new(
      title: 'Test Kit',
      price: 7900,
      # sale_price: 7900,
      cad_price: 7900,
      # cad_sale_price: 7900,
      sale_start: Time.now,
      sale_expiry: Time.now,
      language: 'english',
      standards: 'osha',
      product_group: product_groups(:one),
      sample: sample_file,
      market_type: 'usa',
      # s3_url: 'url',
      # download: 'Kit-Name',
      # additional_download: 'url'
      )
    assert_not kit.save, "Saved kit without download"
  end

  # test "can attach sample" do
  #   kit = Kit.create(sample: sample_file)
  #   assert_equal :store, kit.sample.storage_key
  # end

end

    # validates_presence_of :market_type, :title, :download, :language, :standards, :product_group_id, :sample

    # t.string "title", null: false
    # t.integer "price", null: false
    # t.integer "sale_price"
    # t.date "sale_expiry"
    # t.integer "language", null: false
    # t.integer "standards", null: false
    # t.integer "product_group_id", null: false
    # t.text "sample_data"
    # t.datetime "created_at", null: false
    # t.datetime "updated_at", null: false
    # t.date "sale_start"
    # t.string "market_type", default: "usa", null: false
    # t.integer "cad_price", null: false
    # t.integer "cad_sale_price"
    # t.text "s3_url"
    # t.text "download", null: false
    # t.text "additional_download"
