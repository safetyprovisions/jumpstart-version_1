require 'test_helper'

class CompanyTest < ActiveSupport::TestCase

  charset = Array('A'..'Z') + Array('a'..'z')

  test "should save company with all attributes" do
    company = Company.new(
      name: 'Company',
      address: '218 Dividend Dr, suite 4',
      city: 'Rexburg',
      state: 'ID',
      zip: '83440',
      email: "#{ Array.new(30) { charset.sample }.join }@test.com",
      password: 'password',
      password_confirmation: 'password',
      credits: 0,
      cad_credits: 0,
      preferred_currency: 'usd'
      )
    assert company.save, "Did not save the company"
  end

  test "should not save company without a name" do
    company = Company.new(
      # name: 'Company',
      address: '218 Dividend Dr, suite 4',
      city: 'Rexburg',
      state: 'ID',
      zip: '83440',
      email: "#{ Array.new(30) { charset.sample }.join }@test.com",
      password: 'password',
      password_confirmation: 'password'
      )
    assert_not company.save, "Saved the company without a name"
  end

  test "should not save company without an address" do
    company = Company.new(
      name: 'Company',
      # address: '218 Dividend Dr, suite 4',
      city: 'Rexburg',
      state: 'ID',
      zip: '83440',
      email: "#{ Array.new(30) { charset.sample }.join }@test.com",
      password: 'password',
      password_confirmation: 'password'
      )
    assert_not company.save, "Saved the company without an address"
  end

  test "should not save company without a city" do
    company = Company.new(
      name: 'Company',
      address: '218 Dividend Dr, suite 4',
      # city: 'Rexburg',
      state: 'ID',
      zip: '83440',
      email: "#{ Array.new(30) { charset.sample }.join }@test.com",
      password: 'password',
      password_confirmation: 'password'
      )
    assert_not company.save, "Saved the company without a city"
  end

  test "should not save company without a state" do
    company = Company.new(
      name: 'Company',
      address: '218 Dividend Dr, suite 4',
      city: 'Rexburg',
      # state: 'ID',
      zip: '83440',
      email: "#{ Array.new(30) { charset.sample }.join }@test.com",
      password: 'password',
      password_confirmation: 'password'
      )
    assert_not company.save, "Saved the company without a state"
  end

  test "should not save company without a zip" do
    company = Company.new(
      name: 'Company',
      address: '218 Dividend Dr, suite 4',
      city: 'Rexburg',
      state: 'ID',
      # zip: '83440',
      email: "#{ Array.new(30) { charset.sample }.join }@test.com",
      password: 'password',
      password_confirmation: 'password'
      )
    assert_not company.save, "Saved the company without a zip"
  end

  # NOTE This test should be obsolete because current_user.email is automatically set on Company creation.
  # test "should not save company with invalid email" do
  #   company = Company.new(
  #     name: 'Company',
  #     address: '218 Dividend Dr, suite 4',
  #     city: 'Rexburg',
  #     state: 'ID',
  #     zip: '83440',
  #     email: 'fakecompany@testdbcom',
  #     password: 'password',
  #     password_confirmation: 'password'
  #     )
  #   assert_not company.save, "Saved the company with invalid email"
  # end

  test "should not save company with invalid state" do
    company = Company.new(
      name: 'Company',
      address: '218 Dividend Dr, suite 4',
      city: 'Rexburg',
      state: 'IDA',
      zip: '83440',
      email: "#{ Array.new(30) { charset.sample }.join }@test.com",
      password: 'password',
      password_confirmation: 'password'
      )
    assert_not company.save, "Saved the company with invalid state"
  end

  test "should not save company with invalid name" do
    company = Company.new(
      name: 'Co',
      address: '218 Dividend Dr, suite 4',
      city: 'Rexburg',
      state: 'ID',
      zip: '83440',
      email: "#{ Array.new(30) { charset.sample }.join }@test.com",
      password: 'password',
      password_confirmation: 'password'
      )
    assert_not company.save, "Saved the company with invalid name"
  end

end
