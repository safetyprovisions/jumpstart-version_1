class UserMailerPreview < ActionMailer::Preview
  def order_success_email
    UserMailer.with(user: User.first).order_success_email
  end
end
