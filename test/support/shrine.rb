module TestData
  module_function

  # This image is 400x400
  def image_data
    attacher = Shrine::Attacher.new
    attacher.set(uploaded_400x400_image)

    # if you're processing derivatives
    attacher.set_derivatives(
      large: uploaded_400x400_image,
      # medium: uploaded_400x400_image,
      small: uploaded_400x400_image,
    )

    attacher.column_data # or attacher.data in case of postgres jsonb column
  end
  def uploaded_400x400_image
    file = File.open("test/fixtures/files/example.png", binmode: true)

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => File.size(file.path),
      "mime_type" => "image/png",
      "filename"  => "example.png",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end

  # This is the cached version
  def cached_image_data
    attacher = Shrine::Attacher.new
    attacher.set(cached_400x400_image)

    # if you're processing derivatives
    attacher.set_derivatives(
      large: cached_400x400_image,
      # medium: cached_400x400_image,
      small: cached_400x400_image,
    )

    attacher.column_data # or attacher.data in case of postgres jsonb column
  end
  def cached_400x400_image
    file = File.open("test/fixtures/files/example.png", binmode: true)

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => File.size(file.path),
      "mime_type" => "image/png",
      "filename"  => "example.png",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end

  #####################

  # This image is 1198x530
  def product_group_image_data
    attacher = Shrine::Attacher.new
    attacher.set(uploaded_1198x530_image)

    # if you're processing derivatives
    attacher.set_derivatives(
      large: uploaded_1198x530_image,
      # medium: uploaded_400x400_image,
      small: uploaded_1198x530_image,
    )

    attacher.column_data # or attacher.data in case of postgres jsonb column
  end
  def uploaded_1198x530_image
    file = File.open("test/fixtures/files/example3.png", binmode: true)

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => File.size(file.path),
      "mime_type" => "image/png",
      "filename"  => "example3.png",
      "width"     => 1198,
      "height"    => 530
    )

    uploaded_file
  end

  #####################

  # This image is 300x375
  def poster_image_data
    attacher = Shrine::Attacher.new
    attacher.set(uploaded_poster_image)

    # if you're processing derivatives
    attacher.set_derivatives(
      large: uploaded_poster_image,
      # medium: uploaded_400x400_image,
      small: uploaded_poster_image,
    )

    attacher.column_data # or attacher.data in case of postgres jsonb column
  end
  def uploaded_poster_image
    file = File.open("test/fixtures/files/example2.png", binmode: true)

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => File.size(file.path),
      "mime_type" => "image/png",
      "filename"  => "poster.png",
      "width"     => 300,
      "height"    => 375
    )

    uploaded_file
  end

  # This is the cached version
  def cached_poster_image_data
    attacher = Shrine::Attacher.new
    attacher.set(cached_poster_image)

    # if you're processing derivatives
    attacher.set_derivatives(
      large: cached_poster_image,
      # medium: cached_poster_image,
      small: cached_poster_image,
    )

    attacher.column_data # or attacher.data in case of postgres jsonb column
  end
  def cached_poster_image
    file = File.open("test/fixtures/files/example2.png", binmode: true)

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => File.size(file.path),
      "mime_type" => "image/png",
      "filename"  => "poster.png",
      "width"     => 300,
      "height"    => 375
    )

    uploaded_file
  end

  #####################

  def pdf_download_data
    attacher = Shrine::Attacher.new
    attacher.set(pdf_download)

    attacher.column_data # or attacher.data in case of postgres jsonb column
  end
  def pdf_download
    file = File.open("test/fixtures/files/sample.pdf", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "sample.pdf"
    )

    uploaded_file
  end
end
