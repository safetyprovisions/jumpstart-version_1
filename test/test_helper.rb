ENV['RAILS_ENV'] ||= 'test'
require_relative './support/shrine'
require_relative '../config/environment'
require 'rails/test_help'
require "minitest/spec"
require "database_cleaner"
require 'webmock/minitest'

# # This should be unnecessary, per https://anti-pattern.com/transactional-fixtures-in-rails
# class Minitest::Spec
#   before :each do
#     DatabaseCleaner.start
#   end

#   after :each do
#     DatabaseCleaner.clean
#   end
# end

VCR.configure do |config|
  # ignore_request { |req| ... } will ignore any request for which the given block returns true.

  # ignore_hosts 'foo.com', 'bar.com' allows you to specify particular hosts to ignore.
  config.ignore_hosts 'chromedriver.storage.googleapis.com'

  # ignore_localhost = true is equivalent to ignore_hosts 'localhost', '127.0.0.1', '0.0.0.0'. It is particularly useful for when you use VCR with a javascript-enabled capybara driver, since capybara boots your rack app and makes localhost requests to it to check that it has booted.
  config.ignore_localhost = true

  # unignore_hosts 'foo.com', 'bar.com' makes VCR stop ignoring particular hosts.

  # Ignored requests are not recorded and are always allowed, regardless of the record mode, and even outside of a VCR.use_cassette block. For more info, see: https://relishapp.com/vcr/vcr/v/5-0-0/docs/configuration/ignore-request

  config.cassette_library_dir = 'test/vcr_cassettes'
  # config.stub_with :webmock # Deprecated. Replaced by:
  config.hook_into :webmock
end

class ActiveSupport::TestCase

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  include Devise::Test::IntegrationHelpers

  ### BEGIN Shrine 3 test data
  def sample_file(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end
  def cached_sample_file(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      # "storage"  => "cache",
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end

  def poster_download(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end
  def cached_poster_download(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      # "storage"  => "cache",
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end

  def practical_evaluation(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end
  def cached_practical_evaluation(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end

  def exam(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end
  def cached_exam(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end

  def answer_key(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end
  def cached_answer_key(name="test-sample.pdf")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "application/pdf",
      "filename"  => "#{name}",
    )

    uploaded_file
  end

  def image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 800,
      "height"    => 530

    )

    uploaded_file
  end
  def cached_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 800,
      "height"    => 530

    )

    uploaded_file
  end

  def shirt_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400

    )

    uploaded_file
  end
  def cached_shirt_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400

    )

    uploaded_file
  end

  def button_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400

    )

    uploaded_file
  end
  def cached_button_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400

    )

    uploaded_file
  end

  def hat_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400

    )

    uploaded_file
  end
  def cached_hat_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400

    )

    uploaded_file
  end

  def poster_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 800,
      "height"    => 530

    )

    uploaded_file
  end
  def cached_poster_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 800,
      "height"    => 530

    )

    uploaded_file
  end

  def poster_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 800,
      "height"    => 530

    )

    uploaded_file
  end
  def cached_poster_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 800,
      "height"    => 530

    )

    uploaded_file
  end

  def keychain_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end
  def cached_keychain_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end

  def lanyard_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end
  def cached_lanyard_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end

  def certificate_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end
  def cached_certificate_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end

  def card_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end
  def cached_card_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 400,
      "height"    => 400
    )

    uploaded_file
  end

  def machine_tag_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 300,
      "height"    => 400
    )

    uploaded_file
  end
  def cached_machine_tag_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 300,
      "height"    => 400
    )

    uploaded_file
  end

  def product_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 1198,
      "height"    => 530

    )

    uploaded_file
  end
  def cached_product_group_image(name="test-image.gif")
    file = File.open("test/files/#{name}", binmode: true)
    size = file.size

    # for performance we skip metadata extraction and assign test metadata
    uploaded_file = Shrine.upload(file, :cache, metadata: false)
    uploaded_file.metadata.merge!(
      "size"      => size,
      "mime_type" => "image/gif",
      "filename"  => "#{name}",
      "width"     => 1198,
      "height"    => 530

    )

    uploaded_file
  end
  ### END Shrine 3 test data

  # Minitest.after_run {
  #   puts "~" * 50
  #   puts "Cleaning up after tests..."
  #   rake db:test:prepare
  #   puts "~" * 50
  # }

end
