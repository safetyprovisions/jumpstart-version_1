require 'rails_helper'

RSpec.describe "coupon_codes/edit", type: :view do
  before(:each) do
    @coupon_code = assign(:coupon_code, CouponCode.create!(
      :code => "MyString",
      :flat_rate => 1,
      :percentage => 1
    ))
  end

  it "renders the edit coupon_code form" do
    render

    assert_select "form[action=?][method=?]", coupon_code_path(@coupon_code), "post" do

      assert_select "input[name=?]", "coupon_code[code]"

      assert_select "input[name=?]", "coupon_code[flat_rate]"

      assert_select "input[name=?]", "coupon_code[percentage]"
    end
  end
end
