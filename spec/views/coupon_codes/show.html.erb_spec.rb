require 'rails_helper'

RSpec.describe "coupon_codes/show", type: :view do
  before(:each) do
    @coupon_code = assign(:coupon_code, CouponCode.create!(
      :code => "Code",
      :flat_rate => 2,
      :percentage => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Code/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
