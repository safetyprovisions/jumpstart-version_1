require 'rails_helper'

RSpec.describe "coupon_codes/index", type: :view do
  before(:each) do
    assign(:coupon_codes, [
      CouponCode.create!(
        :code => "Code",
        :flat_rate => 2,
        :percentage => 3
      ),
      CouponCode.create!(
        :code => "Code",
        :flat_rate => 2,
        :percentage => 3
      )
    ])
  end

  it "renders a list of coupon_codes" do
    render
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
