require 'rails_helper'

RSpec.describe "coupon_codes/new", type: :view do
  before(:each) do
    assign(:coupon_code, CouponCode.new(
      :code => "MyString",
      :flat_rate => 1,
      :percentage => 1
    ))
  end

  it "renders new coupon_code form" do
    render

    assert_select "form[action=?][method=?]", coupon_codes_path, "post" do

      assert_select "input[name=?]", "coupon_code[code]"

      assert_select "input[name=?]", "coupon_code[flat_rate]"

      assert_select "input[name=?]", "coupon_code[percentage]"
    end
  end
end
