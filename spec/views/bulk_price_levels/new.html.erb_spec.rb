require 'rails_helper'

RSpec.describe "bulk_price_levels/new", type: :view do
  before(:each) do
    assign(:bulk_price_level, BulkPriceLevel.new(
      :qty => 1,
      :price => 1,
      :cad_price => 1,
      :tier => "MyString"
    ))
  end

  it "renders new bulk_price_level form" do
    render

    assert_select "form[action=?][method=?]", bulk_price_levels_path, "post" do

      assert_select "input[name=?]", "bulk_price_level[qty]"

      assert_select "input[name=?]", "bulk_price_level[price]"

      assert_select "input[name=?]", "bulk_price_level[cad_price]"

      assert_select "input[name=?]", "bulk_price_level[tier]"
    end
  end
end
