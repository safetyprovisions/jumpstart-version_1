require 'rails_helper'

RSpec.describe "bulk_price_levels/edit", type: :view do
  before(:each) do
    @bulk_price_level = assign(:bulk_price_level, BulkPriceLevel.create!(
      :qty => 1,
      :price => 1,
      :cad_price => 1,
      :tier => "MyString"
    ))
  end

  it "renders the edit bulk_price_level form" do
    render

    assert_select "form[action=?][method=?]", bulk_price_level_path(@bulk_price_level), "post" do

      assert_select "input[name=?]", "bulk_price_level[qty]"

      assert_select "input[name=?]", "bulk_price_level[price]"

      assert_select "input[name=?]", "bulk_price_level[cad_price]"

      assert_select "input[name=?]", "bulk_price_level[tier]"
    end
  end
end
