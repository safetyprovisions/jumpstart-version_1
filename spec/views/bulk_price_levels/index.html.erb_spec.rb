require 'rails_helper'

RSpec.describe "bulk_price_levels/index", type: :view do
  before(:each) do
    assign(:bulk_price_levels, [
      BulkPriceLevel.create!(
        :qty => 2,
        :price => 3,
        :cad_price => 4,
        :tier => "Tier"
      ),
      BulkPriceLevel.create!(
        :qty => 2,
        :price => 3,
        :cad_price => 4,
        :tier => "Tier"
      )
    ])
  end

  it "renders a list of bulk_price_levels" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "Tier".to_s, :count => 2
  end
end
