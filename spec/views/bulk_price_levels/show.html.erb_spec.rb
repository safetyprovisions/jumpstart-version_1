require 'rails_helper'

RSpec.describe "bulk_price_levels/show", type: :view do
  before(:each) do
    @bulk_price_level = assign(:bulk_price_level, BulkPriceLevel.create!(
      :qty => 2,
      :price => 3,
      :cad_price => 4,
      :tier => "Tier"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/Tier/)
  end
end
