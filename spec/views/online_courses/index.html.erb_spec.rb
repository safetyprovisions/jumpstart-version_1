# require 'rails_helper'

# RSpec.describe "online_courses/index", type: :view do
#   before(:each) do
#     assign(:online_courses, [
#       OnlineCourse.create!(
#         :title => "Title",
#         :image_data => "MyText",
#         :description => "MyText",
#         :price => 2,
#         :sale_price => 3,
#         :language => 4,
#         :standards => 5,
#         :product_group_id => 6,
#         :video => "Video",
#         :talent_course_id => 7,
#         :practical_evaluation_data => "MyText"
#       ),
#       OnlineCourse.create!(
#         :title => "Title",
#         :image_data => "MyText",
#         :description => "MyText",
#         :price => 2,
#         :sale_price => 3,
#         :language => 4,
#         :standards => 5,
#         :product_group_id => 6,
#         :video => "Video",
#         :talent_course_id => 7,
#         :practical_evaluation_data => "MyText"
#       )
#     ])
#   end

#   xit "renders a list of online_courses" do
#     render
#     assert_select "tr>td", :text => "Title".to_s, :count => 2
#     assert_select "tr>td", :text => "MyText".to_s, :count => 2
#     assert_select "tr>td", :text => "MyText".to_s, :count => 2
#     assert_select "tr>td", :text => 2.to_s, :count => 2
#     assert_select "tr>td", :text => 3.to_s, :count => 2
#     assert_select "tr>td", :text => 4.to_s, :count => 2
#     assert_select "tr>td", :text => 5.to_s, :count => 2
#     assert_select "tr>td", :text => 6.to_s, :count => 2
#     assert_select "tr>td", :text => "Video".to_s, :count => 2
#     assert_select "tr>td", :text => 7.to_s, :count => 2
#     assert_select "tr>td", :text => "MyText".to_s, :count => 2
#   end
# end
