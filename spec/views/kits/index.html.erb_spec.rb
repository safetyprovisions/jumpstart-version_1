# require 'rails_helper'

# RSpec.describe "kits/index", type: :view do
#   before(:each) do
#     assign(:kits, [
#       Kit.create!(
#         :type => "Type",
#         :title => "Title",
#         :image_data => "MyText",
#         :download_data => "MyText",
#         :description => "MyText",
#         :price => 2,
#         :sale_price => 3,
#         :language => 4,
#         :standards => 5,
#         :product_group_id => 6,
#         :sample_data => "MyText"
#       ),
#       Kit.create!(
#         :type => "Type",
#         :title => "Title",
#         :image_data => "MyText",
#         :download_data => "MyText",
#         :description => "MyText",
#         :price => 2,
#         :sale_price => 3,
#         :language => 4,
#         :standards => 5,
#         :product_group_id => 6,
#         :sample_data => "MyText"
#       )
#     ])
#   end

#   xit "renders a list of kits" do
#     render
#     assert_select "tr>td", :text => "Type".to_s, :count => 2
#     assert_select "tr>td", :text => "Title".to_s, :count => 2
#     assert_select "tr>td", :text => "MyText".to_s, :count => 2
#     assert_select "tr>td", :text => "MyText".to_s, :count => 2
#     assert_select "tr>td", :text => "MyText".to_s, :count => 2
#     assert_select "tr>td", :text => 2.to_s, :count => 2
#     assert_select "tr>td", :text => 3.to_s, :count => 2
#     assert_select "tr>td", :text => 4.to_s, :count => 2
#     assert_select "tr>td", :text => 5.to_s, :count => 2
#     assert_select "tr>td", :text => 6.to_s, :count => 2
#     assert_select "tr>td", :text => "MyText".to_s, :count => 2
#   end
# end
