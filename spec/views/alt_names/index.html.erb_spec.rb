require 'rails_helper'

RSpec.describe "alt_names/index", type: :view do
  before(:each) do
    assign(:alt_names, [
      AltName.create!(
        :name => "MyText",
        :product_group => nil
      ),
      AltName.create!(
        :name => "MyText",
        :product_group => nil
      )
    ])
  end

  it "renders a list of alt_names" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
