require 'rails_helper'

RSpec.describe "alt_names/show", type: :view do
  before(:each) do
    @alt_name = assign(:alt_name, AltName.create!(
      :name => "MyText",
      :product_group => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
  end
end
