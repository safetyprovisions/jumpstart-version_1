require 'rails_helper'

RSpec.describe "alt_names/edit", type: :view do
  before(:each) do
    @alt_name = assign(:alt_name, AltName.create!(
      :name => "MyText",
      :product_group => nil
    ))
  end

  it "renders the edit alt_name form" do
    render

    assert_select "form[action=?][method=?]", alt_name_path(@alt_name), "post" do

      assert_select "textarea[name=?]", "alt_name[name]"

      assert_select "input[name=?]", "alt_name[product_group_id]"
    end
  end
end
