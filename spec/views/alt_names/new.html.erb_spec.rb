require 'rails_helper'

RSpec.describe "alt_names/new", type: :view do
  before(:each) do
    assign(:alt_name, AltName.new(
      :name => "MyText",
      :product_group => nil
    ))
  end

  it "renders new alt_name form" do
    render

    assert_select "form[action=?][method=?]", alt_names_path, "post" do

      assert_select "textarea[name=?]", "alt_name[name]"

      assert_select "input[name=?]", "alt_name[product_group_id]"
    end
  end
end
