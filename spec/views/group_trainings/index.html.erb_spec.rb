require 'rails_helper'

RSpec.describe "group_trainings/index", type: :view do
  before(:each) do
    assign(:group_trainings, [
      GroupTraining.create!(
        :online_course => nil
      ),
      GroupTraining.create!(
        :online_course => nil
      )
    ])
  end

  it "renders a list of group_trainings" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
