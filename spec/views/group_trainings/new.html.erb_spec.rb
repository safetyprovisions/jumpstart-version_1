require 'rails_helper'

RSpec.describe "group_trainings/new", type: :view do
  before(:each) do
    assign(:group_training, GroupTraining.new(
      :online_course => nil
    ))
  end

  it "renders new group_training form" do
    render

    assert_select "form[action=?][method=?]", group_trainings_path, "post" do

      assert_select "input[name=?]", "group_training[online_course_id]"
    end
  end
end
