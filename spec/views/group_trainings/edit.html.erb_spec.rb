require 'rails_helper'

RSpec.describe "group_trainings/edit", type: :view do
  before(:each) do
    @group_training = assign(:group_training, GroupTraining.create!(
      :online_course => nil
    ))
  end

  it "renders the edit group_training form" do
    render

    assert_select "form[action=?][method=?]", group_training_path(@group_training), "post" do

      assert_select "input[name=?]", "group_training[online_course_id]"
    end
  end
end
