require 'rails_helper'

RSpec.describe "payment_terms/edit", type: :view do
  before(:each) do
    @payment_term = assign(:payment_term, PaymentTerm.create!(
      :company => nil,
      :term => 1
    ))
  end

  it "renders the edit payment_term form" do
    render

    assert_select "form[action=?][method=?]", payment_term_path(@payment_term), "post" do

      assert_select "input[name=?]", "payment_term[company_id]"

      assert_select "input[name=?]", "payment_term[term]"
    end
  end
end
