require 'rails_helper'

RSpec.describe "payment_terms/show", type: :view do
  before(:each) do
    @payment_term = assign(:payment_term, PaymentTerm.create!(
      :company => nil,
      :term => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
  end
end
