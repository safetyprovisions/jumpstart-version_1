require 'rails_helper'

RSpec.describe "payment_terms/new", type: :view do
  before(:each) do
    assign(:payment_term, PaymentTerm.new(
      :company => nil,
      :term => 1
    ))
  end

  it "renders new payment_term form" do
    render

    assert_select "form[action=?][method=?]", payment_terms_path, "post" do

      assert_select "input[name=?]", "payment_term[company_id]"

      assert_select "input[name=?]", "payment_term[term]"
    end
  end
end
