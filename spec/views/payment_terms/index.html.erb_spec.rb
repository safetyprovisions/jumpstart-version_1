require 'rails_helper'

RSpec.describe "payment_terms/index", type: :view do
  before(:each) do
    assign(:payment_terms, [
      PaymentTerm.create!(
        :company => nil,
        :term => 2
      ),
      PaymentTerm.create!(
        :company => nil,
        :term => 2
      )
    ])
  end

  it "renders a list of payment_terms" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
