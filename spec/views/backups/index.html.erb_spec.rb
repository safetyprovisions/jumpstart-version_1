require 'rails_helper'

RSpec.describe "backups/index", type: :view do
  before(:each) do
    assign(:backups, [
      Backup.create!(
        :format => "MyText",
        :price => 2,
        :shipping_price => 3,
        :priority_shipping => 4,
        :intl_shipping => 5
      ),
      Backup.create!(
        :format => "MyText",
        :price => 2,
        :shipping_price => 3,
        :priority_shipping => 4,
        :intl_shipping => 5
      )
    ])
  end

  it "renders a list of backups" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
  end
end
