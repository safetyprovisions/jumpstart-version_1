require 'rails_helper'

RSpec.describe "backups/edit", type: :view do
  before(:each) do
    @backup = assign(:backup, Backup.create!(
      :format => "MyText",
      :price => 1,
      :shipping_price => 1,
      :priority_shipping => 1,
      :intl_shipping => 1
    ))
  end

  it "renders the edit backup form" do
    render

    assert_select "form[action=?][method=?]", backup_path(@backup), "post" do

      assert_select "textarea[name=?]", "backup[format]"

      assert_select "input[name=?]", "backup[price]"

      assert_select "input[name=?]", "backup[shipping_price]"

      assert_select "input[name=?]", "backup[priority_shipping]"

      assert_select "input[name=?]", "backup[intl_shipping]"
    end
  end
end
