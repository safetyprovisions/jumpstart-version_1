require 'rails_helper'

RSpec.describe "backups/show", type: :view do
  before(:each) do
    @backup = assign(:backup, Backup.create!(
      :format => "MyText",
      :price => 2,
      :shipping_price => 3,
      :priority_shipping => 4,
      :intl_shipping => 5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
  end
end
