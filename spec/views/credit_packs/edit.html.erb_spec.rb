require 'rails_helper'

RSpec.describe "credit_packs/edit", type: :view do
  before(:each) do
    @credit_pack = assign(:credit_pack, CreditPack.create!(
      :name => "MyString",
      :price => 1,
      :credits => 1
    ))
  end

  it "renders the edit credit_pack form" do
    render

    assert_select "form[action=?][method=?]", credit_pack_path(@credit_pack), "post" do

      assert_select "input[name=?]", "credit_pack[name]"

      assert_select "input[name=?]", "credit_pack[price]"

      assert_select "input[name=?]", "credit_pack[credits]"
    end
  end
end
