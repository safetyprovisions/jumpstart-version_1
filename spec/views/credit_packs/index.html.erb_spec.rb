require 'rails_helper'

RSpec.describe "credit_packs/index", type: :view do
  before(:each) do
    assign(:credit_packs, [
      CreditPack.create!(
        :name => "Name",
        :price => 2,
        :credits => 3
      ),
      CreditPack.create!(
        :name => "Name",
        :price => 2,
        :credits => 3
      )
    ])
  end

  it "renders a list of credit_packs" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
