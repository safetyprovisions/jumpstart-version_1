require 'rails_helper'

RSpec.describe "credit_packs/new", type: :view do
  before(:each) do
    assign(:credit_pack, CreditPack.new(
      :name => "MyString",
      :price => 1,
      :credits => 1
    ))
  end

  it "renders new credit_pack form" do
    render

    assert_select "form[action=?][method=?]", credit_packs_path, "post" do

      assert_select "input[name=?]", "credit_pack[name]"

      assert_select "input[name=?]", "credit_pack[price]"

      assert_select "input[name=?]", "credit_pack[credits]"
    end
  end
end
