require 'rails_helper'

RSpec.describe "credit_packs/show", type: :view do
  before(:each) do
    @credit_pack = assign(:credit_pack, CreditPack.create!(
      :name => "Name",
      :price => 2,
      :credits => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
