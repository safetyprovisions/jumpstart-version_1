require 'rails_helper'

RSpec.describe "train_the_trainers/show", type: :view do
  before(:each) do
    @train_the_trainer = assign(:train_the_trainer, TrainTheTrainer.create!(
      :title => "Title",
      :price => 2,
      :sale_price => 3,
      :language => 4,
      :standards => 5,
      :product_group_id => 6,
      :talent_course_id => 7,
      :practical_evaluation_data => "MyText",
      :exam_data => "MyText",
      :answer_key_data => "MyText",
      :market_type => "Market Type",
      :kit => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/6/)
    expect(rendered).to match(/7/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Market Type/)
    expect(rendered).to match(//)
  end
end
