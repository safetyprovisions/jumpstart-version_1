require 'rails_helper'

RSpec.describe "train_the_trainers/index", type: :view do
  before(:each) do
    assign(:train_the_trainers, [
      TrainTheTrainer.create!(
        :title => "Title",
        :price => 2,
        :sale_price => 3,
        :language => 4,
        :standards => 5,
        :product_group_id => 6,
        :talent_course_id => 7,
        :practical_evaluation_data => "MyText",
        :exam_data => "MyText",
        :answer_key_data => "MyText",
        :market_type => "Market Type",
        :kit => nil
      ),
      TrainTheTrainer.create!(
        :title => "Title",
        :price => 2,
        :sale_price => 3,
        :language => 4,
        :standards => 5,
        :product_group_id => 6,
        :talent_course_id => 7,
        :practical_evaluation_data => "MyText",
        :exam_data => "MyText",
        :answer_key_data => "MyText",
        :market_type => "Market Type",
        :kit => nil
      )
    ])
  end

  it "renders a list of train_the_trainers" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Market Type".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
