require 'rails_helper'

RSpec.describe "train_the_trainers/new", type: :view do
  before(:each) do
    assign(:train_the_trainer, TrainTheTrainer.new(
      :title => "MyString",
      :price => 1,
      :sale_price => 1,
      :language => 1,
      :standards => 1,
      :product_group_id => 1,
      :talent_course_id => 1,
      :practical_evaluation_data => "MyText",
      :exam_data => "MyText",
      :answer_key_data => "MyText",
      :market_type => "MyString",
      :kit => nil
    ))
  end

  it "renders new train_the_trainer form" do
    render

    assert_select "form[action=?][method=?]", train_the_trainers_path, "post" do

      assert_select "input[name=?]", "train_the_trainer[title]"

      assert_select "input[name=?]", "train_the_trainer[price]"

      assert_select "input[name=?]", "train_the_trainer[sale_price]"

      assert_select "input[name=?]", "train_the_trainer[language]"

      assert_select "input[name=?]", "train_the_trainer[standards]"

      assert_select "input[name=?]", "train_the_trainer[product_group_id]"

      assert_select "input[name=?]", "train_the_trainer[talent_course_id]"

      assert_select "textarea[name=?]", "train_the_trainer[practical_evaluation_data]"

      assert_select "textarea[name=?]", "train_the_trainer[exam_data]"

      assert_select "textarea[name=?]", "train_the_trainer[answer_key_data]"

      assert_select "input[name=?]", "train_the_trainer[market_type]"

      assert_select "input[name=?]", "train_the_trainer[kit_id]"
    end
  end
end
