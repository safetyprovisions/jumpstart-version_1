require "rails_helper"

RSpec.describe GroupTrainingsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/group_trainings").to route_to("group_trainings#index")
    end

    it "routes to #new" do
      expect(:get => "/group_trainings/new").to route_to("group_trainings#new")
    end

    it "routes to #show" do
      expect(:get => "/group_trainings/1").to route_to("group_trainings#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/group_trainings/1/edit").to route_to("group_trainings#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/group_trainings").to route_to("group_trainings#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/group_trainings/1").to route_to("group_trainings#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/group_trainings/1").to route_to("group_trainings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/group_trainings/1").to route_to("group_trainings#destroy", :id => "1")
    end
  end
end
