require "rails_helper"

RSpec.describe BackupsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/backups").to route_to("backups#index")
    end

    it "routes to #new" do
      expect(:get => "/backups/new").to route_to("backups#new")
    end

    it "routes to #show" do
      expect(:get => "/backups/1").to route_to("backups#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/backups/1/edit").to route_to("backups#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/backups").to route_to("backups#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/backups/1").to route_to("backups#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/backups/1").to route_to("backups#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/backups/1").to route_to("backups#destroy", :id => "1")
    end
  end
end
