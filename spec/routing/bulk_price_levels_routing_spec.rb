require "rails_helper"

RSpec.describe BulkPriceLevelsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/bulk_price_levels").to route_to("bulk_price_levels#index")
    end

    it "routes to #new" do
      expect(:get => "/bulk_price_levels/new").to route_to("bulk_price_levels#new")
    end

    it "routes to #show" do
      expect(:get => "/bulk_price_levels/1").to route_to("bulk_price_levels#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/bulk_price_levels/1/edit").to route_to("bulk_price_levels#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/bulk_price_levels").to route_to("bulk_price_levels#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/bulk_price_levels/1").to route_to("bulk_price_levels#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/bulk_price_levels/1").to route_to("bulk_price_levels#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/bulk_price_levels/1").to route_to("bulk_price_levels#destroy", :id => "1")
    end
  end
end
