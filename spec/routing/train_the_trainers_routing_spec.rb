require "rails_helper"

RSpec.describe TrainTheTrainersController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/train_the_trainers").to route_to("train_the_trainers#index")
    end

    it "routes to #new" do
      expect(:get => "/train_the_trainers/new").to route_to("train_the_trainers#new")
    end

    it "routes to #show" do
      expect(:get => "/train_the_trainers/1").to route_to("train_the_trainers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/train_the_trainers/1/edit").to route_to("train_the_trainers#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/train_the_trainers").to route_to("train_the_trainers#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/train_the_trainers/1").to route_to("train_the_trainers#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/train_the_trainers/1").to route_to("train_the_trainers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/train_the_trainers/1").to route_to("train_the_trainers#destroy", :id => "1")
    end
  end
end
