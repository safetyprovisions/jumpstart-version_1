require "rails_helper"

RSpec.describe AltNamesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/alt_names").to route_to("alt_names#index")
    end

    it "routes to #new" do
      expect(:get => "/alt_names/new").to route_to("alt_names#new")
    end

    it "routes to #show" do
      expect(:get => "/alt_names/1").to route_to("alt_names#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/alt_names/1/edit").to route_to("alt_names#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/alt_names").to route_to("alt_names#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/alt_names/1").to route_to("alt_names#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/alt_names/1").to route_to("alt_names#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/alt_names/1").to route_to("alt_names#destroy", :id => "1")
    end
  end
end
