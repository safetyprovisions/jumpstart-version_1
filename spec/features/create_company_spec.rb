require 'rails_helper'

RSpec.describe 'Creating a company', type: :feature do
  before { visit new_company_registration_path }

  scenario 'all required fields are provided' do
    fill_in 'Name', with: 'Carl Poppa'
    fill_in 'Address', with: '123 Street Street'
    fill_in 'City', with: 'MyText'
    fill_in 'State', with: 'ID'
    fill_in 'Zip', with: '12345'
    # fill_in 'Parent company', with: '1'
    fill_in 'Email', with: 'carlpoppa@test.com'
    fill_in 'Password', with: '123asd'
    fill_in 'Password confirmation', with: '123asd'
    click_on 'Sign up'
    expect(page).to have_content('Welcome! You have signed up successfully.')
  end

  scenario 'missing name' do
    fill_in 'Name', with: ''
    fill_in 'Address', with: '123 Street Street'
    fill_in 'City', with: 'MyText'
    fill_in 'State', with: 'ID'
    fill_in 'Zip', with: '12345'
    # fill_in 'Parent company', with: '1'
    fill_in 'Email', with: 'carlpoppa@test.com'
    fill_in 'Password', with: '123asd'
    fill_in 'Password confirmation', with: '123asd'
    click_on 'Sign up'
    expect(page).to have_content("Name can't be blank")
  end

  scenario 'state is too long' do
    fill_in 'Name', with: 'Carl Poppa'
    fill_in 'Address', with: '123 Street Street'
    fill_in 'City', with: 'MyText'
    fill_in 'State', with: 'asdfg'
    fill_in 'Zip', with: '12345'
    # fill_in 'Parent company', with: '1'
    fill_in 'Email', with: 'carlpoppa@test.com'
    fill_in 'Password', with: '123asd'
    fill_in 'Password confirmation', with: '123asd'
    click_on 'Sign up'
    expect(page).to have_content("State must be two letters")
  end

  scenario 'state contains invalid characters' do
    fill_in 'Name', with: 'Carl Poppa'
    fill_in 'Address', with: '123 Street Street'
    fill_in 'City', with: 'MyText'
    fill_in 'State', with: 'a1'
    fill_in 'Zip', with: '12345'
    # fill_in 'Parent company', with: '1'
    fill_in 'Email', with: 'carlpoppa@test.com'
    fill_in 'Password', with: '123asd'
    fill_in 'Password confirmation', with: '123asd'
    click_on 'Sign up'
    expect(page).to have_content("State must be two letters")
  end

end
