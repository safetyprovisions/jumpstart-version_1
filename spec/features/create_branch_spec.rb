require 'rails_helper'

RSpec.describe 'Creating a company', type: :feature do
  before { visit new_company_registration_path }

  xscenario 'with a parent company' do
    fill_in 'Name', with: 'Carl Poppa'
    fill_in 'Address', with: '123 Street Street'
    fill_in 'City', with: 'MyText'
    fill_in 'State', with: 'ID'
    fill_in 'Zip', with: '12345'
    # fill_in 'Parent company', with: '1'
    fill_in 'Email', with: 'carlpoppa@test.com'
    fill_in 'Password', with: '123asd'
    fill_in 'Password confirmation', with: '123asd'
    click_on 'Sign up'
    expect(page).to have_content('Welcome! You have signed up successfully.')
  end
end
