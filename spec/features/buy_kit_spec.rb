require 'rails_helper'
include Warden::Test::Helpers

RSpec.describe 'Buying a Kit', type: :feature do

  # NOTE To do:
  # Login as User (Create User first?)
  # Visit kits_path
  # Go to a Kit page
  # Add it to Cart
  # Checkout
  # Make sure download is available on Order page

  # After Credits purchasing is in place, we will need to verify that the correct amount was deducted on successful checkout.

  # NOTE
  # The code below does NOT work.
  # Chris@GoRails is going to do a tutorial on how to properly test Stripe. As of 2019-04-10, he plans to do it a few weeks.


  before do
    # Login user
    user = FactoryBot.create(:individual_user)
    login_as(user, :scope => :user)

    # Create Kit
    kit = FactoryBot.build(:kit)
    kit.update_attributes(id: '1')
    kit.update_attributes(market_type: 'usa')
    kit.update_attributes(weight: 5)
    kit.update_attributes(price: 10000)
    kit.update_attributes(shipping_price: 500)
    kit.update_attributes(image: File.open("test/files/test-image.gif"))
    kit.update_attributes(download: File.open("test/files/test-kit.zip"))
    kit.update_attributes(sample: File.open("test/files/test-sample.pdf"))
    kit.save
  end

  xscenario 'as an employee' do
    visit kit_path(1)
    click_on 'Add to Cart'
    fill_in 'to_name', with: "Carl's Momma"
    fill_in 'to_street1', with: '123 Pothole Street'
    fill_in 'to_city', with: 'MyText'
    fill_in 'to_state', with: 'ID'
    fill_in 'to_zip', with: '12345'
    fill_in 'to_country', with: 'MyText'
    fill_in 'to_email', with: 'carlsmomma@test.com'
    click_on 'Proceed to Checkout'
    # Fill in Stripe info
    click_on 'Pay $111.30'
    expect(page).to have_content('Welcome! You have signed up successfully.')
  end

  xscenario 'as an admin' do

  end
end
