require 'rails_helper'
include Warden::Test::Helpers

RSpec.describe 'Tagging Product Groups', type: :feature do

  before do
    # Login user
    user = FactoryBot.create(:admin)
    login_as(user, :scope => :user)

    # Create ProductGroup
    product_group = FactoryBot.build(:product_group)
    product_group.update_attributes(image: File.open("test/files/test-image.gif"))
    product_group.save
  end

  scenario 'adding and removing a tag' do
    visit edit_product_group_path(1)
    expect(page).to have_content('Which Categories Does This Product Group Belong To?')

    click_link("Add tag", :match => :first)
    expect(page).to have_content('Tag was successfully added.')
    expect(page).to have_css(".btn-danger")
  # end

  # scenario 'removing a tag' do
  #   visit edit_product_group_path(1)
  #   expect(page).to have_content('Which Categories Does This Product Group Belong To?')

  #   click_link("Add tag", :match => :first)
  #   expect(page).to have_content('Tag was successfully added.')
  #   expect(page).to have_css(".btn-danger")

    click_link("Remove tag", :match => :first)
    expect(page).to have_content('Tag was successfully removed.')
    expect(page).not_to have_css(".btn-danger")
  end

  xscenario 'as a non-admin' do
    #  Should not be able to access the Edit page
    #  Perhaps this test should go somewhere else?
  end

end
