require "rails_helper"

RSpec.describe Users, type: :system do
  # context 'a non-logged in user', js: true do
  context 'a non-logged in user' do
    before(:each) do
      @user1 = create(:user)
    end

    it 'cannot access the User Show page' do
      visit user_path(@user1)
      expect(page).to have_content('You are not authorized to view this record.')
    end
  end

  # context 'a logged in non-admin user', js: true do
  context 'a logged in non-admin user' do
    before(:each) do
      @user1 = create(:user)
      @user2 = create(:user)

      login_as @user1, scope: :user
    end

    it 'can access the User Show page' do
      visit user_path(@user1)
      expect(page).to have_content(@user1.name.full)
    end

    it 'cannot access other User\'s Show page' do
      visit user_path(@user2)
      expect redirect_to(user_path(@user1))
    end
  end

  context 'a logged in admin user' do
    before do
      @superadmin = create(:superadmin)
      @admin = create(:admin)
      @user1 = create(:user)
      @user2 = create(:user)
      @company_one = create(:company_one)
      @company_two = create(:company_two)
      @company_one.employees << @admin
      @company_one.employees << @user1
      @company_two.employees << @user2

      login_as @admin
    end

    it 'can access employee Show page' do
      visit user_path(@user1)
      expect(page).to have_content(@user1.name.full)
    end

    it 'cannot access nonemployee Show page' do
      visit user_path(@user2)
      expect redirect_to(user_path(@admin))
    end

    it 'cannot access superadmin Show page' do
      visit user_path(@superadmin)
      expect redirect_to(user_path(@admin))
      expect(page).to have_content('You are not authorized to view this record.')
    end
  end



  context 'a logged in superadmin user' do
    before do
      @admin = create(:admin)
      @user = create(:user)
      @superadmin = create(:superadmin)

      login_as @superadmin
    end

    it 'can access other User\'s Show page' do
      visit user_path(@user)
      expect(page).to have_content(@user.name.full)
    end
  end

end
