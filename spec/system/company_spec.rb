require "rails_helper"

RSpec.describe Company do
  # context 'a visitor', js: true do # In-browser
  context 'a visitor' do
    before(:each) do
      @company = create(:company_one)
    end

    it 'cannot access the Company homepage' do
      visit companies_homepage_path
      expect(page).to have_content('You need to sign in or sign up before continuing.')
    end
  end

  context 'a user not logged in to a company account' do
    before(:each) do
      @company = create(:company_one)
      @user1 = create(:user)

      login_as @user1
    end

    it 'cannot access the Company homepage' do
      visit companies_homepage_path
      expect(page).to have_content('You need to sign in or sign up before continuing.')
    end
  end

  context 'a logged in company' do
    before(:each) do
      @company = create(:company_one)
      visit 'companies/sign_in'
      fill_in 'Email', with: @company.email
      fill_in 'Password', with: @company.password
      click_button 'Log in'
      # login_as @company
    end

    it 'can access dashboard' do
      visit companies_homepage_path
      # byebug
      # post companies_homepage_url # id: @company.id
      # login_as @company
      # expect(page).to have_content('Signed in successfully.')

      # visit companies_homepage_url(method: :post, id: @company.id)
      expect(page).to have_content(@company.name)
    end
  end

end
