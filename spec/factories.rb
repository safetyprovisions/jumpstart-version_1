FactoryBot.define do
  factory :alt_name do
    name { "MyText" }
    product_group { nil }
  end
  factory :company_admin do
    user { nil }
    company { nil }
  end
  factory :bulk_price_level do
    qty { 1 }
    price { 1 }
    cad_price { 1 }
    tier { "MyString" }
  end
  factory :non_catalog_course_access do
    online_course { nil }
    company { nil }
  end
  factory :ttt_activation do
    user { nil }
    order { nil }
    train_the_trainer { nil }
  end
  factory :online_course_activation do
    user { nil }
    order { nil }
    online_course { nil }
  end
  factory :company_course do
    user { nil }
    online_course { nil }
    order { nil }
    quantity { 1 }
  end
  factory :payment_term do
    company { nil }
    term { 1 }
  end
  factory :company_order do
    user { nil }
    company { nil }
    order { nil }
  end
  factory :credit_pack do
    name { "MyString" }
    price { 1 }
    credits { 1 }
  end
  factory :train_the_trainer do
    title { "MyString" }
    price { 1 }
    sale_price { 1 }
    sale_start { "2019-06-27" }
    sale_expiry { "2019-06-27" }
    language { 1 }
    standards { 1 }
    product_group_id { 1 }
    talent_course_id { 1 }
    practical_evaluation_data { "MyText" }
    exam_data { "MyText" }
    answer_key_data { "MyText" }
    market_type { "MyString" }
    kit { nil }
  end
  factory :backup do
    format { "MyText" }
    price { 1 }
    shipping_price { 1 }
    priority_shipping { 1 }
    intl_shipping { 1 }
  end
  factory :group_training do
    online_course { nil }
  end

  factory :order_item do
    order
    # product
    product_id { 1 }
    quantity { 1 }
    kit_id { 1 }
    order_item_type { "" }
    orderable_type { "" }
    orderable_id { 1 }
  end

  factory :order do
    user
    # user_id { 1 }
    status { "draft" }
    stripe_id { "MyString" }
    card_brand { "MyString" }
    card_last4 { "MyString" }
    card_exp_month { "MyString" }
    card_exp_year { "MyString" }
    paid_at { Time.now }
    shipment_id { "MyString" }
    to_name { "MyString" }
    to_street1 { "MyString" }
    to_city { "MyString" }
    to_state { "MyString" }
    to_zip { "MyString" }
    to_country { "MyString" }
    to_email { "MyString" }
    download_start_time { Time.now }
  end

  factory :online_course do
    title { "MyString" }
    image_data { }
    description { "MyText" }
    price { 1 }
    sale_price { 1 }
    sale_start { "2019-03-14" }
    sale_expiry { "2019-03-14" }
    language { 1 }
    standards { 1 }
    product_group_id { 1 }
    video { "MyString" }
    talent_course_id { 1 }
    practical_evaluation_data { }
  end

  factory :kit do
    # id { 1 }
    # type { "" }
    market_type { "" }
    title { "MyString" }
    image_data { }
    download_data { }
    description { "MyText" }
    price { 1 }
    sale_price { 1 }
    sale_expiry { "2019-02-20" }
    language { 1 }
    standards { 1 }
    product_group_id { 1 }
    sample_data { }
  end

  factory :product_group do
    name { "MyString" }
    image_data { }
  end

  factory :company_one, class: Company, aliases: [:parent] do
    # branch
    # employee
    name                  { 'CompanyOne' }
    address               { 'StreetAddress' }
    city                  { 'CityName' }
    state                 { 'CA' }
    zip                   { '90210' }
    sequence(:email)      { |n| "company#{n}@example.com" }
    password              { '123456' }
    password_confirmation { '123456' }
  end

  factory :company_two, class: Company, aliases: [:branch] do
    # parent
    # employee
    name                  { 'CompanyTwo' }
    address               { 'StreetAddress' }
    city                  { 'CityName' }
    state                 { 'CA' }
    zip                   { '90210' }
    sequence(:email)      { |n| "branch#{n}@example.com" }
    password              { '123456' }
    password_confirmation { '123456' }
  end

  factory :superadmin, class: User do
    sequence(:email)      { |n| "superadmin#{n}@example.com" }
    first_name            { 'Test' }
    last_name             { 'Name' }
    password              { '123456' }
    password_confirmation { '123456' }
    superadmin            { true }
  end

  factory :admin, class: User do
    sequence(:email)      { |n| "user#{n}@example.com" }
    first_name            { 'Test' }
    last_name             { 'Name' }
    password              { '123456' }
    password_confirmation { '123456' }
    admin                 { true }
  end

  factory :user, aliases: [:employee] do
    sequence(:email)      { |n| "user#{n}@example.com" }
    first_name            { 'Test' }
    last_name             { 'Name' }
    password              { '123456' }
    password_confirmation { '123456' }
    admin                 { false }
  end

  factory :individual_user, class: User do
    email                 { "individual_user@example.com" }
    first_name            { 'Test' }
    last_name             { 'User' }
    password              { '123456' }
    password_confirmation { '123456' }
    admin                 { false }
  end

  factory :product do
    name                  { 'Product' }
    price                 { 1995 }
  end
end
