require 'rails_helper'

RSpec.describe Order, type: :model do
  subject { FactoryBot.build(:order) }
  let(:order_item) { FactoryBot.build(:order_item) }
  let(:product) { FactoryBot.build(:product) }
  let(:kit) { FactoryBot.build(:kit) }

  context "when all required attributes are present" do
    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context "adding an order_item" do
    before do
      order_item.update_attributes(orderable_type: 'Product')
      order_item.update_attributes(product_id: product.id)
      order_item.update_attributes(order_id: subject.id)
      subject.order_items << order_item
    end

    it "is valid" do
      expect(subject.order_items.first.new_record?).to be true
    end
  end

  context "calculates taxes correctly" do

    subject { FactoryBot.create(:order) }
    let(:product) { FactoryBot.create(:product) }
    # let(:uploaded_image) { File.open("test/files/test-image.gif") }
    # let(:uploaded_kit) { File.open("test/files/test-kit.zip") }
    # let(:uploaded_sample) { File.open("test/files/test-sample.pdf") }

    before do
      # kit.update_attributes(market_type: 'usa')
      # kit.update_attributes(image: uploaded_image)
      # kit.update_attributes(download: uploaded_kit)
      # kit.update_attributes(sample: uploaded_sample)
      # kit.update_attributes(price: 100)
      # kit.update_attributes(shipping_price: 0)
      product.save
      order_item.update_attributes(orderable_type: 'Product')
      # order_item.update_attributes(product_id: product.id)
      # order_item.update_attributes(kit_id: 0)
      order_item.update_attributes(order_id: subject.id)
      subject.update_attributes(to_state: 'ID')
      subject.order_items << order_item
    end

    xit "is valid" do
      expect(subject.calculate_taxes).to eq(119.7)
    end
  end

  context "calculates taxes for Idaho" do

    xit "is valid" do
      expect(subject.calculate_taxes).to eq(119.7)
    end
  end

  context "calculates taxes for non-Idaho states" do

    xit "is not valid" do
      expect(subject.calculate_taxes).to eq(119.7)
    end
  end

end


# Issue:
# Whenever I set

# order_item.update_attributes(orderable_type: 'Product')

# the error msg says, "DETAIL:  Key (kit_id)=(1) is not present in table "kits".

# and when I set

# order_item.update_attributes(orderable_type: 'Kit')

# it says, "DETAIL:  Key (product_id)=(1) is not present in table "products"."