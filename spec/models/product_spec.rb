require "rails_helper"

RSpec.describe Product, type: :model do
  subject { FactoryBot.build(:product) }

  context "all required attributes are present" do
    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context "name is missing" do
    before do
      subject.update_attributes(name: nil)
    end

    it "is not valid" do
      expect(subject.errors[:name]).to include("can't be blank")
    end
  end

  context "price is missing" do
    before do
      subject.update_attributes(price: nil)
    end

    it "is not valid" do
      expect(subject.errors[:price]).to include("can't be blank")
    end
  end

  context "price is not a number" do
    before do
      subject.update_attributes(price: 'abc')
    end

    it "is not valid" do
      expect(subject.errors[:price]).to include("is not a number")
    end
  end
end
