require "rails_helper"

RSpec.describe Company, type: :model do
  subject { FactoryBot.build(:company_one) }

  context "all required attributes are present" do
    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context 'name is not present' do
    before { subject.update_attributes(name: nil) }

    it 'is not valid' do
      # expect { subject.update_attributes(name: nil) }.to raise_error(ActiveRecord::NotNullViolation)

      expect(subject.errors.messages[:name]).to include("can't be blank")
    end
  end

  context 'invalid email' do
    before { subject.update_attributes(email: 'nil@test') }

    it 'is not valid' do
      expect(subject.errors.messages[:email]).to include("must be a valid email format")
    end
  end

end
