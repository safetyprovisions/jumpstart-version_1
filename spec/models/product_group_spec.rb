require 'rails_helper'

RSpec.describe ProductGroup, type: :model do
  subject { FactoryBot.build(:product_group) }
  let(:uploaded_image) { File.open("test/files/test-image.gif") }

  context "all required attributes are present" do
    before do
      subject.update_attributes(image: uploaded_image)
    end

    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context "image is missing" do
    before do
      subject.update_attributes(image: nil)
    end

    it "is not valid" do
      expect(subject.errors[:image]).to include("can't be blank")
    end
  end

  context "name is missing" do
    before do
      subject.update_attributes(name: nil)
    end

    it "is not valid" do
      expect(subject.errors[:name]).to include("can't be blank")
    end
  end
end
