require 'rails_helper'

RSpec.describe Kit, type: :model do
  subject { FactoryBot.build(:kit) }
  let(:uploaded_image) { File.open("test/files/test-image.gif") }
  let(:uploaded_kit) { File.open("test/files/test-kit.zip") }
  let(:uploaded_sample) { File.open("test/files/test-sample.pdf") }

  context "all required attributes are present" do
    before do
      subject.update_attributes(market_type: 'usa')
      subject.update_attributes(image: uploaded_image)
      subject.update_attributes(download: uploaded_kit)
      subject.update_attributes(sample: uploaded_sample)
    end

    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context "image is missing" do
    before do
      subject.update_attributes(market_type: 'usa')
      subject.update_attributes(image: nil)
      subject.update_attributes(download: uploaded_kit)
      subject.update_attributes(sample: uploaded_sample)
    end

    it "is not valid" do
      expect(subject.errors[:image]).to include("can't be blank")
    end
  end

  context "download is missing" do
    before do
      subject.update_attributes(market_type: 'usa')
      subject.update_attributes(image: uploaded_image)
      subject.update_attributes(download: nil)
      subject.update_attributes(sample: uploaded_sample)
    end

    it "is not valid" do
      expect(subject.errors[:download]).to include("can't be blank")
    end
  end

  context "sample is missing" do
    before do
      subject.update_attributes(market_type: 'usa')
      subject.update_attributes(image: uploaded_image)
      subject.update_attributes(download: uploaded_kit)
      subject.update_attributes(sample: nil)
    end

    it "is not valid" do
      expect(subject.errors[:sample]).to include("can't be blank")
    end
  end
end
