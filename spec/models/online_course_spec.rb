require 'rails_helper'

RSpec.describe OnlineCourse, type: :model do
  subject { FactoryBot.build(:online_course) }
  let(:uploaded_image) { File.open("test/files/test-image.gif") }
  let(:practical_evaluation) { File.open("test/files/test-evaluation.docx") }

  context "all required attributes are present" do
    before do
      subject.update_attributes(image: uploaded_image)
      subject.update_attributes(practical_evaluation: practical_evaluation)
    end

    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context "image is missing" do
    before do
      subject.update_attributes(image: nil)
    end

    it "is not valid" do
      expect(subject.errors[:image]).to include("can't be blank")
    end
  end

  context "practical evaluation is missing" do
    before do
      subject.update_attributes(practical_evaluation: nil)
    end

    it "is not valid" do
      expect(subject.errors[:practical_evaluation]).to include("can't be blank")
    end
  end
end
