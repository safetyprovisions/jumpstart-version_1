require "rails_helper"

RSpec.describe User, type: :model do
  subject { FactoryBot.build(:user) }

  context "all required attributes are present" do
    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context "first name is missing" do
    before do
      subject.update_attributes(first_name: nil)
    end

    it "is not valid" do
      expect(subject.errors[:first_name]).to include("can't be blank")
    end
  end

  context "last name is missing" do
    before do
      subject.update_attributes(last_name: nil)
    end

    it "is not valid" do
      expect(subject.errors[:last_name]).to include("can't be blank")
    end
  end
end
