# talentlms gem code:
TalentLMS.init({
  api_key: (Rails.env.production? ? ENV['TALENT_LIVE_KEY'] :  Rails.application.credentials.talent_live_key),
  sub_domain: 'safetyclasses'
  # :api_key => '03a82de6de2d939564aa607b0e24a030b5047c54ed87c77fea',
  # :sub_domain => 'example'
})




# My original code:
# if Rails.env.production?
#   TalentLMS::setApiKey(ENV['TALENT_LIVE_KEY']);
# else
#   TalentLMS::setApiKey(Rails.application.credentials.talent_live_key);
# end

# TalentLMS::setDomain('https://safetyclasses.talentlms.com');
