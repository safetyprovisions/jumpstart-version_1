if Rails.env.test?
  require 'webmock'
  # WebMock.disable_net_connect!(allow_localhost: true)
  WebMock.disable_net_connect!(
    allow_localhost: true,
    allow: ['http://127.0.0.1', 'http://127.0.0.1/users/sign_up', "127.0.0.1:9515", "127.0.0.1:9516"]
  )
end