if Rails.env.test? || Rails.env.development?
  Lockbox.master_key = Rails.application.credentials.lockbox_development_key
  BlindIndex.master_key = Lockbox.master_key
end

if Rails.env.production?
  Lockbox.master_key = ENV["LOCKBOX_MASTER_KEY"]
  BlindIndex.master_key = Lockbox.master_key
end
