class Rack::Attack
  unless Rails.env.test? || Rails.env.development?
    ### Configure Cache ###

    # If you don't want to use Rails.cache (Rack::Attack's default), then
    # configure it here.
    #
    # Note: The store is only used for throttling (not blocklisting and
    # safelisting). It must implement .increment and .write like
    # ActiveSupport::Cache::Store

    # Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new

    ### Throttle Spammy Clients ###

    # If any single client IP is making tons of requests, then they're
    # probably malicious or a poorly-configured scraper. Either way, they
    # don't deserve to hog all of the app server's CPU. Cut them off!
    #
    # Note: If you're serving assets through rack, those requests may be
    # counted by rack-attack and this throttle may be activated too
    # quickly. If so, enable the condition to exclude them from tracking.

    # Throttle all requests by IP (60rpm)
    #
    # Key: "rack::attack:#{Time.now.to_i/:period}:req/ip:#{req.ip}"
    throttle('req/ip', limit: 300, period: 5.minutes) do |req|
      req.ip # unless req.path.start_with?('/assets')
    end

    ### Prevent Brute-Force Login Attacks ###

    # The most common brute-force login attack is a brute-force password
    # attack where an attacker simply tries a large number of emails and
    # passwords to see if any credentials match.
    #
    # Another common method of attack is to use a swarm of computers with
    # different IPs to try brute-forcing a password for a specific account.

    # Throttle POST requests to /login by IP address
    #
    # Key: "rack::attack:#{Time.now.to_i/:period}:logins/ip:#{req.ip}"
    throttle('logins/ip', limit: 5, period: 20.seconds) do |req|
      if req.path == '/login' && req.post?
        req.ip
      end
    end

    # Throttle POST requests to /login by email param
    #
    # Key: "rack::attack:#{Time.now.to_i/:period}:logins/email:#{req.email}"
    #
    # Note: This creates a problem where a malicious user could intentionally
    # throttle logins for another user and force their login requests to be
    # denied, but that's not very common and shouldn't happen to you. (Knock
    # on wood!)
    throttle("logins/email", limit: 5, period: 20.seconds) do |req|
      if req.path == '/login' && req.post?
        # return the email if present, nil otherwise
        req.params['email'].presence
      end
    end

    ### Custom Throttle Response ###

    # By default, Rack::Attack returns an HTTP 429 for throttled responses,
    # which is just fine.
    #
    # If you want to return 503 so that the attacker might be fooled into
    # believing that they've successfully broken your app (or you just want to
    # customize the response), then uncomment these lines.
    # self.throttled_response = lambda do |env|
    #  [ 503,  # status
    #    {},   # headers
    #    ['']] # body
    # end

    ### My Custom Blocklists ###

    # Block DotBot crawlers
    # IP list & more info: https://www.distilnetworks.com/bot-directory/bot/dotbot1-1/
    Rack::Attack.blocklist('block DotBot visits') do |req|
      req.user_agent == ('Mozilla/5.0 (compatible; DotBot/1.1; http://www.opensiteexplorer.org/dotbot, help@moz.com)' || 'Mozilla/5.0 (compatible; DotBot/1.1; http://www.dotnetdotcom.org/, crawler@dotnetdotcom.org)')
    end

    # Block SEMRush User Agents
    AGENTS = %w[ SemrushBot SemrushBot-SA SemrushBot-BA SemrushBot-SI SemrushBot-SWA SemrushBot-CT SemrushBot-BM SemrushBot\ 6.0 ]
    Rack::Attack.blocklist('block_specific_addresses') do|req|
      AGENTS.include?(req.user_agent)
    end

    # Block SEMRush IPs
    SEMRUSH_IPS = ["107.20.126.228", "107.21.139.47", "107.21.149.52", "107.21.155.119", "107.21.183.210", "107.22.128.82", "107.22.128.82", "107.22.134.187", "107.22.68.116", "174.129.166.77", "184.72.140.220", "184.72.163.70", "184.72.168.246", "184.72.181.183", "184.72.187.47", "184.72.94.71", "184.73.13.192", "184.73.142.88", "184.73.143.31", "184.73.58.164", "192.243.136", "192.243.53.51", "192.243.55.129", "192.243.55.129", "192.243.55.130", "192.243.55.132", "192.243.55.133", "192.243.55.133", "192.243.55.134", "192.243.55.134", "192.243.55.135", "192.243.55.135", "192.243.55.136", "192.243.55.137", "192.243.55.137", "192.243.55.138", "192.243.55.138", "192.243.55.138", "192.243.55.51", "204.236.243.108", "204.236.244.200", "213.174.146.163", "213.174.147.83", "23.20.2.192", "23.20.75.166", "46.229.161.132", "46.229.161.136", "46.229.161.138", "46.229.162.101", "46.229.164.100", "46.229.164.101", "46.229.164.101", "46.229.164.102", "46.229.164.102", "46.229.164.103", "46.229.164.113", "46.229.164.114", "46.229.164.97", "46.229.164.98", "46.229.164.99", "46.299.164.97", "46.229.168.140", "46.229.168.147", "46.229.168.149", "50.16.11.183", "50.16.154.80", "50.16.154.80", "50.16.162.168", "50.16.179.70", "50.16.94.41", "50.17.105.131", "50.17.107.112", "50.17.117.164", "50.17.33.197", "50.19.162.168", "50.19.162.61", "50.19.179.186", "50.19.36.141", "75.101.205.112", "75.101.210.171", "75.101.242.89"]
    Rack::Attack.blocklist('block_semrushbot_addresses') do|req|
      SEMRUSH_IPS.include?(req.ip.to_i)
    end

    # Block Chinese IPs
    CHINA_1 = (IPAddr.new('113.64.0.0').to_i..IPAddr.new('113.95.255.255').to_i)
    CHINA_2 = (IPAddr.new('223.64.0.0').to_i..IPAddr.new('223.117.255.255').to_i)
    Rack::Attack.blocklist('block_chinese_addresses') do|req|
      CHINA_1.include?(IPAddr.new(req.ip).to_i)
      CHINA_2.include?(IPAddr.new(req.ip).to_i)
    end

    # Block Specific IPs
    SPECIFICS = %w[ 216.244.66.246 ]
    Rack::Attack.blocklist('block_specific_addresses') do|req|
      SPECIFICS.include?(req.ip.to_i)
    end

    # Block improper request formats
    FORMATS = %w[ aspx php ]
    Rack::Attack.blocklist('block_improper_request_formats') do|req|
      if File.extname(req.path).present?
        FORMATS.include?(File.extname(req.path))
      end
    end
  end
end
