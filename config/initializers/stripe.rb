if Rails.env.production?
  Stripe.api_key = ENV['STRIPE_PRIVATE_KEY']
else
  Stripe.api_key = Rails.application.credentials.stripe_private_test_key
end