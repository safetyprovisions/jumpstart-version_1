require 'shrine'

if Rails.env.development? # || Rails.env.test?
  require "shrine/storage/file_system"
  Shrine.storages = {
    cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"),
    store: Shrine::Storage::FileSystem.new("public", prefix: "uploads/store")
  }
elsif Rails.env.test?
# if Rails.env.test? || Rails.env.development?
  # Shrine 3
  require 'shrine/storage/memory'

  Shrine.storages = {
    cache: Shrine::Storage::Memory.new,
    store: Shrine::Storage::Memory.new,
  }
else
  require "shrine/storage/s3"
  s3_options = {
    access_key_id:     ENV['AWS_ACCESS_KEY_ID'],
    secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    bucket:            ENV['S3_BUCKET_NAME'],
    region:            ENV['S3_REGION'], # Rails.application.credentials[:aws_region],
  }
  Shrine.storages = {
    cache: Shrine::Storage::S3.new(prefix: "cache", **s3_options),
    store: Shrine::Storage::S3.new(prefix: "store", **s3_options)
  }
end
Shrine.plugin :derivatives
Shrine.plugin :activerecord
Shrine.plugin :cached_attachment_data
Shrine.plugin :restore_cached_data

# # For Shrine 3.0
Shrine.plugin :url_options, store: {
# Shrine.plugin :default_url_options, store: {
                                      expires_in: 24*60*60,
                                      public: false,
                                      response_content_disposition: "attachment",
                                      }

# In the docs, they create a Shrine::Storage::S3, name it s3, and then do s3.url( expires_in: 60 )
# I couldn't get this to work.
