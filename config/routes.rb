require 'sidekiq/web'

Rails.application.routes.draw do

  get '/sitemap.xml' => 'sitemaps#index', defaults: { format: 'xml' }
  post "/xmlrpc.php", to: "application#error_render_method"

  match '(*any)', to: redirect(subdomain: ''), via: :all, constraints: {subdomain: 'www'}

  root to: 'home#index'
  # Switch to this for maintenance mode
  # root to: 'home#maintenance'

  devise_for :users, controllers: {
    omniauth_callbacks: "users/omniauth_callbacks",
    sessions: 'users/sessions',
    registrations:  'users/registrations'
  }

  # devise_scope :user do
  #   authenticated :user do
  #     root :to => 'pages#other', as: :authenticated_root
  #   end
  #   unauthenticated :user do
  #     root :to => 'sessions#new', as: :unauthenticated_root
  #   end
  # end

  devise_for :companies, controllers: {
    sessions:       'companies/sessions',
    registrations:  'companies/registrations'
  }


  # Admins only *** NOT Superadmins! ***
  authenticated :user, lambda { |u| u.admin? } do
  # devise_scope :companies do
    root to: "companies#main"

    post '/users/new', to: 'users#existence_check'

    get  'companies/branch_view',         to: "companies#branch_view"
    # post 'companies/branch_view',         to: "companies#branch_view"

    get  'companies/add_employee',        to: "companies#add_employee"
    # post 'companies/add_employee',        to: "companies#add_employee"

    post 'companies/toggle_admin',        to: "companies#toggle_admin"

    post 'companies/toggle_employee_status',        to: "application#toggle_employee_status"

    get  'companies/manage_employees',    to: "companies#manage_employees"
    # post 'companies/manage_employees',    to: "companies#manage_employees"

    get  'companies/homepage',            to: "companies#main"
    # post 'companies/homepage',            to: "companies#main"

    post 'companies/remove_employee', to: "application#remove_employee"

    # post '/download_validator',           to: "orders#download_validator"

    get '/assign_identifier',             to: "companies#assign_identifier"

    post '/assign_parent_company',        to: "companies#assign_parent_company"

    post '/remove_parent_company',        to: "companies#remove_parent_company"

    match '/assign_course', to: "companies#assign_course", via: [:get, :post]

    post 'companies/assign_multiple_courses',    to: "companies#assign_multiple_courses"

    get  'companies/orders',  to: "companies#company_orders"
    get  'companies/reports', to: "companies#company_reports"
    post '/get_talent_user_id', to: "talent#get_talent_user_id"

    # TODO Some of these should be moved to Superadmin!
    # get  'unpaid_orders',     to: "orders#unpaid_orders"
    # post 'invoiced',          to: "orders#invoiced"
    # post 'labeled',           to: "orders#labeled"
    # post 'order_paid',        to: "orders#order_paid"
    get  'mark_all_as_read',  to: "application#mark_all_as_read"
    post 'import_employees',  to: "companies#import_employees"

    resources :users # , only: [:show]
  # end

  # devise_scope :admin do
    get '/add_employee',            to: "users#add_employee"
    get '/manage_employees',        to: "users#manage_employees"
    post '/remove_employee',        to: "application#remove_employee"
    post '/toggle_employee_status', to: "application#toggle_employee_status"
    post '/make_assignable',        to: 'application#make_assignable'
  end

  # Superadmins only
  authenticated :user, lambda { |u| u.superadmin? } do
    mount Sidekiq::Web => '/sidekiq'

    resources :alt_names
    resources :backups
    resources :bulk_price_levels
    resources :button_groups
    resources :buttons
    resources :card_groups
    resources :cards
    resources :certificates
    resources :companies
    resources :coupon_codes
    resources :credit_packs
    resources :digital_posters
    resources :group_trainings
    resources :hats
    resources :keychain_groups
    resources :keychains
    resources :kits
    resources :lanyard_groups
    resources :lanyards
    resources :machine_tag_groups
    resources :machine_tags
    resources :online_courses
    resources :payment_terms
    resources :poster_groups
    resources :posters
    resources :product_groups
    resources :related_items
    resources :shirts
    resources :stickers
    resources :train_the_trainers
    resources :users
    get  '/analytics',        to: 'reports#analytics'
    post '/assign_non_catalog_course', to: 'superadmin#assign_non_catalog_course'
    post 'invoiced',          to: "orders#invoiced"
    post 'labeled',           to: "orders#labeled"
    get  '/labels_required',  to: "users#labels_required"
    post 'order_paid',        to: "orders#order_paid"
    post 'related_items/new', to: "related_items#new"
    post '/reset_download_window',     to: 'superadmin#reset_download_window'
    get  'unpaid_orders',     to: "orders#unpaid_orders"
  end

  # These are required for things to show up in Administrate.
  namespace :admin do
    resources :alt_names
    resources :announcements
    resources :backups
    resources :bulk_price_levels
    resources :buttons
    resources :button_groups
    resources :card_groups
    resources :cards
    resources :certificates
    resources :companies
    resources :coupon_codes
    resources :credit_packs
    resources :digital_posters
    resources :group_trainings
    resources :hats
    resources :keychains
    resources :keychain_groups
    resources :kits
    resources :lanyard_groups
    resources :lanyards
    resources :machine_tags
    resources :machine_tag_groups
    resources :non_catalog_course_accesses
    resources :notifications
    resources :orders do
      member do
        put :refund
      end
    end
    resources :online_courses
    resources :order_items #, only: [:create, :update]
    resources :payment_terms
    resources :poster_groups
    resources :posters
    resources :products
    resources :product_groups
    resources :services
    resources :shirts
    resources :stickers
    resources :train_the_trainers
    resources :users

    root to: "users#index"
  end

  # PUBLIC ROUTES
  get  'add_tag',               to: "product_groups#add_tag"
  get '/all_group_trainings',   to: 'home#all_group_trainings'
  get '/all_online_courses',    to: 'home#all_online_courses'
  post '/sales',                to: 'reports#sales'
  get '/all_train_the_trainer_courses',    to: 'home#all_train_the_trainer_courses'
  get  'remove_tag',            to: "product_groups#remove_tag"
  post  'add_tag_global',       to: "product_groups#add_tag_global"
  post  'remove_tag_global',    to: "product_groups#remove_tag_global"

  post '/add_to_course',        to: 'talent#online_course_validator'
  post '/add_backup',           to: 'order_items#add_backup'
  post '/add_shipping_address', to: 'checkouts#add_shipping_address'
  get '/about',                 to: 'home#about'
  get '/canadian_group_trainings',to: 'home#canadian_group_trainings'
  get '/canadian_online_courses',to: 'home#canadian_online_courses'
  get '/canadian_train_the_trainer_courses',to: 'home#canadian_train_the_trainer_courses'
  get '/canadian_training_kits',to: 'home#canadian_training_kits'
  get '/change_shipping',       to: 'orders#change_shipping'
  get '/contact_us',            to: 'home#contact'
  # This allows users to get their downloads from Orders!
  post '/download_validator',   to: "orders#download_validator"
  get '/diy_training_kits',     to: 'home#diy_training_kits'
  post '/edit_tag',             to: "tags#edit"
  get '/faq',                   to: 'home#faq'
  get '/course',                to: 'product_groups#course'
  post '/get_certificate',      to: 'talent#get_cert'
  get '/osha_group_trainings',  to: 'home#osha_group_trainings'
  get '/osha_online_courses',   to: 'home#osha_online_courses'
  get '/osha_train_the_trainer_courses',    to: 'home#osha_train_the_trainer_courses'
  get '/osha_training_kits',    to: 'home#osha_training_kits'
  get '/spanish_group_trainings',to: 'home#spanish_group_trainings'
  get '/spanish_online_courses',to: 'home#spanish_online_courses'
  get '/spanish_train_the_trainer_courses', to: 'home#spanish_train_the_trainer_courses'
  get '/spanish_training_kits', to: 'home#spanish_training_kits'
  get '/tagalog_group_trainings',to: 'home#tagalog_group_trainings'
  get '/tagalog_online_courses',to: 'home#tagalog_online_courses'
  get '/tagalog_train_the_trainer_courses', to: 'home#tagalog_train_the_trainer_courses'
  get '/tagalog_training_kits', to: 'home#tagalog_training_kits'
  post '/talent_login',         to: 'talent#log_into_talent'
  get '/training_topics',       to: 'home#topics'
  post '/update_tag',           to: "tags#update"
  post '/reset_password',       to: "application#reset_password"

  match '/onsite_request',      to: "home#onsite_training", via: [:get, :post]

  get '/remove_coupon_code',  to: "orders#remove_coupon_code"
  get '/search',              to: 'home#search'
  post '/send_contact_email', to: 'home#send_contact_email'
  get '/privacy',             to: 'home#privacy'
  get '/terms',               to: 'home#terms'
  get '/training_types',      to: 'home#training_types'
  post '/update_preferred_currency',      to: 'home#update_preferred_currency'
  get '/verify_code',         to: "orders#verify_code"
  get '/documentation',       to: 'documents#index'
  get 'employee_list',        to: 'reports#employee_list'
  get 'reports',              to: 'reports#index'

  resources :announcements, only: [:index]
  resources :button_groups, only: [:show, :index]
  resources :card_groups, only: [:show]
  resource :cart do
    resource :checkout
  end
  resources :certificates, only: [:show]
  resources :companies
  resources :credit_packs, only: [:index]
  resources :hats, only: [:show, :index]
  resources :keychain_groups, only: [:show]
  # TODO Should these be here? They are purchased through their ProductGroup
  # resources :kits, except: [:show]           # <-- ???
  resources :lanyard_groups, only: [:show]
  resources :machine_tag_groups, only: [:show]
  resources :notifications, only: [:index]
  # TODO Should these be here? They are purchased through ProductGroup
  # resources :online_courses, except: [:show] # <-- ???
  resources :order_items, only: [:create, :update]
  resources :orders
  resources :poster_groups, only: [:index, :show]
  resources :posters, only: [:show]
  resources :product_groups, only: [:show]
  # Uncomment if this gets used
  # resources :products, only: [:show]
  resources :shirts, only: [:show, :index]
  resources :stickers, only: [:show, :index]
  resources :users, only: [:show, :edit]

  # authenticate :user, lambda { |u| u.superadmin? } do
  #   mount Sidekiq::Web => '/sidekiq'
  #   get  '/analytics', to: 'reports#analytics'
  #   post '/assign_non_catalog_course', to: 'superadmin#assign_non_catalog_course'
  #   post '/reset_download_window', to: 'superadmin#reset_download_window'
  # end

  # # Test Routes. Change as needed.
  # post '/edituser', to: 'users#talent_user_edit'
  # # post '/talent_user_delete', to: 'users#talent_user_delete'


  # match "/404", to: "errors#not_found", via: :all
  match "/404", to: "application#error_render_method", via: :all
  match "/422", to: "errors#unacceptable", via: :all
  match "/500", to: "errors#internal_server_error", via: :all

  # RoutingError
  # match "*path", to: "home#catch_404", via: :all
  match "*path", to: "application#error_render_method", via: :all

end
