# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


####### BEFORE RUNNING THIS FILE! #######

# You must comment out the generate_location method in the uploaders!


####### AFTER RUNNING THIS FILE! #######

# Need to manually create the companies below and add employees.

# You must uncomment the generate_location method in the uploaders!

# ####### CREATE ADMINS #######
# NOTE I started implementing this (Admins) and Companies, below, but never finished. The code below it is untested but should work.
# User.create(
#     email: "admin1@test.com",
#     password: "password",
#     password_confirmation: "password",
#     first_name: "Super",
#     last_name: "Admin",
#     superadmin: false,
#     active: true,
#     preferred_currency: "usd"
# )

# # ####### CREATE COMPANIES   #######
# Company.create!(
#   name: "Testing Company 1",
#   address: "123 street",
#   city: "Cityville",
#   state: "ID",
#   zip: "83440",
#   parent_id: nil,
#   identifier: "R65F3R2V",
#   # email: "testingcompany1@test.com",
#   # talent_name: "testingcompany1",
#   # talent_branch_id: "2162",
#   # credits: 0,
#   # preferred_currency: "usd",
#   # cad_credits: 9900
#   )

# Company.create(
#   name: "Testing Company 2",
#   address: "456 street",
#   city: "Cityville",
#   state: "ID",
#   zip: "83440",
#   parent_id: 'R65F3R2V',
#   email: "testingcompany2@test.com",
#   talent_name: "testingcompany2",
#   # talent_branch_id: "2162",
#   # identifier: "R65F3R2V",
#   credits: 0,
#   preferred_currency: "cad",
#   cad_credits: 9900
#   )

User.create(
  email: "admin1@test.com",
  first_name: "Admin",
  last_name: "One",
  superadmin: false,
  active: true
  )

Company.create(
  name: "Company One",
  address: "111 First St",
  city: "Oneville",
  state: "CA",
  zip: "95670",
  email: "admin1@test.com",
  identifier: "UCUCOUID"
  )

User.create(
  email: "admin2@test.com",
  first_name: "Admin",
  last_name: "Two",
  superadmin: false,
  active: true
  )

Company.create(
  name: "Company Two",
  address: "222 Second St",
  city: "Twotown",
  state: "CA",
  zip: "95671",
  parent_id: Company.first.id,
  email: "admin2@test.com",
  password: "password",
  password_confirmation: "password"
  )

####### CREATE EMPLOYEES #######
require 'faker'

employees = []

25.times do
  employee = User.create(
  # User.create(
    email: Faker::Internet.unique.email,
    first_name: Faker::Name.unique.first_name,
    last_name: Faker::Name.unique.last_name,
    password: 'password',
    password_confirmation: 'password'
  )
  employees << employee
end

current_company = Company.first

employees.each do |user|
  current_company.employees << user
end

employees = [] # Reset for reuse below

25.times do
  employee = User.create(
  # User.create(
    email: Faker::Internet.unique.email,
    first_name: Faker::Name.unique.first_name,
    last_name: Faker::Name.unique.last_name,
    password: 'password',
    password_confirmation: 'password'
  )
  employees << employee
end

current_company = Company.last

employees.each do |user|
  current_company.employees << user
end

####### CREATE SUPERADMIN #######
User.create(
    email: "superadmin@test.com",
    password: "password",
    password_confirmation: "password",
    first_name: "Super",
    last_name: "Admin",
    superadmin: true,
    active: true,
    preferred_currency: "usd"
)

####### CREATE CATEGORIES #######
cats = ["MARITIME", "OSHA 10", "HEALTH/HUMAN RESOURCES", "AERIAL LIFTS/AT HEIGHTS", "INDUSTRIAL HYGIENE", "CRANES/RIGGING", "OTHER", "EARTH MOVERS", "ELECTRICAL", "FORKLIFTS", "OTHER MOBILE EQUIPMENT"]

cats.each do |cat|
  new_tag = ActsAsTaggableOn::Tag.new(name: cat, taggings_count: 0)
  new_tag.save
end

####### CREATE BULK PRICE LEVELS #######
BulkPriceLevel.create(
  tier: 1,
  qty: 1,
  price: 7900,
  cad_price: 9900
  )
BulkPriceLevel.create(
  tier: 1,
  qty: 25,
  price: 7500,
  cad_price: 9500
  )
BulkPriceLevel.create(
  tier: 1,
  qty: 50,
  price: 6500,
  cad_price: 8500
  )
BulkPriceLevel.create(
  tier: 1,
  qty: 100,
  price: 4500,
  cad_price: 6500
  )

BulkPriceLevel.create(
  tier: 2,
  qty: 1,
  price: 2900,
  cad_price: 3900
  )
BulkPriceLevel.create(
  tier: 2,
  qty: 25,
  price: 2500,
  cad_price: 3500
  )
BulkPriceLevel.create(
  tier: 2,
  qty: 50,
  price: 2000,
  cad_price: 3000
  )
BulkPriceLevel.create(
  tier: 2,
  qty: 100,
  price: 1500,
  cad_price: 2500
  )

####### CREATE PRODUCT GROUPS #######
uploader = ProductGroupImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/product_group_image.jpg'))
uploaded_product_group_image = uploader.upload(file)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json, # text
    video: "MjA0NjIw", # string
    name: "Sexual Harassment",
    overview: "<div>Our Sexual Harassment training course is built to EEOC and other federal regulations. This class discusses several topics such as different types of harassment (including verbal, visual, physical and quid pro quo), recourse, retaliation, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>OSHA doesn’t have a specific standard for sexual harassment training. However, under the General Duty Clause, Section 5(a)(1) of the Occupational Safety and Health Act of 1970, employers are required to provide a workplace that “is free from recognizable hazards that are causing or likely to cause death or serious harm to employees.”<br><br></div><div>The Canadian Human Rights Act and the Canada Labour Code address sexual harassment. They also&nbsp; establish criteria for determining when unwelcome conduct of a sexual nature constitutes sexual harassment, define the circumstances under which an employer may be held liable, and suggest affirmative steps employers should take to prevent sexual harassment.&nbsp;<br><br></div><div>Because of these requirements, employers have a legal and ethical obligation to develop and maintain a workplace that is free from hazards associated with sexual harassment. Employees have the right to work in an atmosphere that promotes the safety and well-being of all.</div>",
    facts: "<ul><li>One study showed that rural workplaces were more likely to have sexual harassment incidents than other urban or suburban areas. (Source: <a href=\"https://www.edisonresearch.com/sexualharassmentworkplace/\"><em>Edison Research</em></a>)</li><li>25% of women workers who experienced sexual harassment felt strongly that they could report the incident to an employer. (Source: <a href=\"https://www.edisonresearch.com/sexualharassmentworkplace/\"><em>Edison Research</em></a>)</li><li>As of 2019, it is believed that 87-94% of those sexually harassed do not file a complaint. (Source: <a href=\"https://i-sight.com/resources/2018-guide-to-workplace-sexual-harassment-infographic/\"><em>i-Sight</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.nytimes.com/2019/01/10/technology/google-rubin-shareholder-lawsuit.html?rref=collection%252Ftimestopic%252FSexual%2520Harassment&amp;action=click&amp;contentCollection=timestopics&amp;region=stream&amp;module=stream_unit&amp;version=latest&amp;contentPlacement=3&amp;pgtype=collection\">Board Sued Over Google’s Exit Package for Accused Executive<br></a><a href=\"https://www.businessinsider.com/nearly-all-sexual-harassment-in-the-workplace-goes-unreported-2018-12\">About 5 Million People Experience Sexual Harassment at Work Every Year</a> <br><a href=\"https://www.bbc.com/news/av/uk-42286562/young-women-on-sexual-harassment-in-the-workplace\">Young Women on Sexual Harassment in the Workplace (BBC)<br></a><a href=\"https://www.youtube.com/watch?v=NGc7KfQ3uWs\">Sexual Harassment in the Workplace<br></a><a href=\"https://hardhattraining.com/sexual-harassment-workplaceculture/\">Sexual Harassment: Workplace Culture<br></a><a href=\"https://hardhattraining.com/sexual-harassment-workplace-epidemic/\">Sexual Harassment: A Workplace Epidemic</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Bucket Truck",
    overview: "<div>Our Bucket Truck Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomy, rigging, stability, operations, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR, Subpart F – Powered Platforms, Manlifts, and Vehicle Mounted Work Platforms</li><li>29 CFR 1926, Subpart L – Scaffolds</li><li>ANSI A92.2-2001 – For Vehicle Mounted Elevating and Rotating Aerial Devices (Bucket Trucks)</li><li>ANSI A92.3-2006 – For Manually Propelled Elevating Aerial Platforms</li><li>ANSI A92.5-2006 – For Boom-Supported Elevating Aerial Platforms</li><li>ANSI A92.6-1999 – For Self-Propelled Elevating Work Platforms (Scissor Lifts)</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B354.1-04 – Portable Elevating Work Platforms</li><li>CAN/CSA-B354.2-01 – Self-Propelled Elevating Platforms</li><li>CAN/CSA-B354.4-02 – Self-Propelled Boom Supported</li><li>CAN/CSA Z271: Safety Code for Elevating Platforms</li><li>CAN/CSA-C225-10: Vehicle-Mounted Aerial Devices</li><li>CAN/CSA-B354.5-07 – Mast Climbing</li><li>CAN/CSA-Z259 &amp; Subsections – Fall Protection, Arrest</li></ul>",
    facts: "<ul><li>Bucket trucks are also referred to as boom-supported aerial platforms.</li><li>From 1992-1999, 44% of the construction deaths involving a boom-supported lift were caused by electrocution.</li><li>The next highest cause of death was falling, causing about 25% of the reported accidents. (Source: <a href=\"http://elcosh.org/document/1417/d000484/deaths-from-aerial-lifts.html\"><em>elcosh</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"http://www.enewscourier.com/news/local_news/sammet-ready-to-take-the-reins/article_7ebb6f40-c8f4-11e8-b5a3-e769a50bc2b3.html\">Sammet Ready to Take the Reins<br></a><a href=\"http://www.abc6.com/story/38089506/man-electrocuted-in-south-kingstown\">Police Identify Man Electrocuted in South Kingstown<br></a><a href=\"https://youtu.be/bIYE2yXNJaQ\">Tree Trimmer Dies in Bucket Truck Accident in North Versailles<br></a><a href=\"https://youtu.be/IgDn8f27xvQ\">Bucket Truck Overturns, Operator Life Flighted<br></a><a href=\"https://hardhattraining.com/fall-protection-bucket-trucks-aerial-lifts/\">Fall Protection, Bucket Trucks, Aerial Lifts<br></a><a href=\"https://hardhattraining.com/travel-safely-between-job-sites-in-a-bucket-truck/\">Travel Safely Between Job Sites in A Bucket Truck</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Pedestal Crane",
    overview: "<div>Our Pedestal Crane safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy, rigging, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for Pedestal Cranes:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>29 CFR 1915 – Shipyard, Subpart G: Gear and Equipment for Rigging and Materials Handling</li><li>29 CFR 1917 – Marine Terminals, Subpart C: Cargo Handling Gear and Equipment</li><li>29 CFR 918 – Safety and Health regulations for Longshoring, Subpart F: vessels Cargo Handling Gear</li><li>29 CCFR 1919 – Gear Certification</li></ul><div><strong>Canada Standards:<br></strong><br></div><ul><li>CAN/CSA- Z150-11 – Safety Code on Mobile Cranes</li><li>CAN/CSA-Z150.3-11 – Safety Code on Articulating Boom Cranes</li><li>CAN/CSA-C22.2 – Safety Code for Material Hoists</li><li>CGSB Standard65.11-M88: Personal Floatation Device</li></ul><div><strong>International Standards<br></strong><br></div><ul><li>ISO 16715:2014 – Hand Signals used with Cranes</li><li>ASME/ANSI B30.1-29 – Cranes, Slings, Below the Hook Lifting Devices</li><li>ASME/ANSI B30.5 – Mobile and Locomotive Cranes</li><li>ASME/ANSI B30.22 – Articulating Boom Cranes</li></ul>",
    facts: "<ul><li>Pedestal Cranes are also known as offshore cranes.</li><li>Just over half of fatal crane injuries since 2011 are from being struck by something. (<em>Source: </em><a href=\"https://www.bls.gov/iif/oshwc/cfoi/cranes_fact_sheet.htm\"><em>Bureau of Labor Statistics</em></a><em>)</em></li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.newshub.co.nz/nznews/lyttelton-port-suffers-another-workplace-accident-2014091717\">Workplace accident in Lyttelton Port<br></a><a href=\"http://fuelfix.com/blog/2011/08/18/worker-killed-in-offshore-crane-accident-near-galveston/\">Worker Killed in Offshore Crane Accident<br></a><a href=\"https://www.youtube.com/watch?v=149vkwgSlqQ\">Offshore Rig Crane Accident<br></a><a href=\"https://hardhattraining.com/the-crane-problem/\">The Crane Problem<br></a><a href=\"https://hardhattraining.com/man-loses-life-crane-accident/\">Man Loses Life in Crane Accident</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Ladder Safety",
    overview: "<div>Our Ladder Safety Training course is built to regulation standards. This class discusses topics including ladder styles, inspections, general safety, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for ladders:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.25 – Portable Wood Ladders</li><li>29 CFR 1910.26 – Portable Metal Ladders</li><li>29 CFR 1910.27 – Fixed Ladders</li><li>29 CFR 1910.29 – Manually Propelled Ladder Stands &amp; Scaffolds</li><li>29 CFR 1910.28, Subpart D – Scaffolding, Walking/Working Surfaces</li><li>1915.72 Subpart (.71-.77) – Scaffolds, Ladders and Other Working Surfaces for Shipyards</li><li>1917.118 &amp; 119 – Marine Terminals: Fixed &amp; Portable Ladders</li><li>29 CFR 1926.1053 – Ladders</li><li>29 CFR 1926.1053 Subpart X – Stairways &amp; Ladders</li><li>OSH Act of 1970</li><li>29 CFR 1926.21 – Training</li><li>ANSI ASC A14.1-2007 – Wood Ladders</li><li>ANSI ASC A14.2-2007 – Metal Ladders</li><li>ANSI ASC A14.5-2007 – Reinforced Plastic Ladders&nbsp;</li><li>ANSI ASC A14.7-2011 – Safety Requirements for Mobile Ladder Stands and Mobile Ladder Stand Platforms.&nbsp;</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA CAN3 Z11-12 – Portable Ladders</li><li>CSA Z259.12-11 – Fall Protection</li><li>B.C. – OHS Part 13, Ladders, Scaffolds, Temp Work Platforms</li><li>Alberta – OHS Code Part 8, Entrances, Walkways, Stairways, Ladders</li><li>Manitoba – Workplace Safety &amp; Health Regs, 217/2006 Part 13</li><li>Nova Scotia – Occupational Safety General Regs Part 13.147, Ladders</li><li>New Brunswick – OHS Act, Part 11, Temporary Structures</li></ul>",
    facts: "<ul><li>The step ladder was invented in 1862 in Dayton, Ohio. (Source: <a href=\"https://www.fixfastusa.com/news-blog/cool-things-may-not-known-ladders/\"><em>Fix Fast USA</em></a>)</li><li>Just under 45% of fatal falls involve a ladder. (Source: <a href=\"https://safety.blr.com/workplace-safety-news/employee-safety/ladder-safety/Researchers-reveal-deadly-facts-about-workplace-la/\"><em>Safety BLR</em></a>)</li><li>Most injuries involving ladders were preventable. (Source: <a href=\"https://safety.blr.com/workplace-safety-news/employee-safety/ladder-safety/Researchers-reveal-deadly-facts-about-workplace-la/\"><em>Safety BLR</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://abcnews.go.com/Health/Healthday/story?id=4507177&amp;page=1\">Ladder-Related Accidents Climb in U.S.: Safety<br></a><a href=\"https://www.safetyandhealthmagazine.com/articles/construction-worker-dies-after-fall-from-ladder-2\">Construction Worker Dies After Fall from Ladder</a> <br><a href=\"https://www.youtube.com/watch?v=H-2eEdyIGR0\">Ladder Safety: 4 Main Types of Ladder Accidents<br></a><a href=\"https://www.youtube.com/watch?v=aqL2PwcxijE\">Fall From Ladder<br></a><a href=\"https://hardhattraining.com/ladder-training-belt-buckle-rule/\">Ladder Training and the Belt Buckle Rule<br></a><a href=\"https://hardhattraining.com/falls-how-high-is-deadly/\">Falls: How High is Deadly?</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Aerial Lift/MEWP (Boom & Scissor)",
    overview: "<div>Our Aerial Lift Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on equipment and anatomy, maintenance and inspections, safe operations and stability, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.<br><br></div><div>During this training, we will be taking a look at the functionality and components of aerial lifts in relation to both scissor lifts and boom lifts. As part of this training, we’ll show you why it’s important to conduct a thorough pre-shift inspection each day before using the equipment. You will also learn about machine stability and the importance of knowing the aerial lift’s capacity. We will also emphasize the importance of planning each job and setting up the machine and site properly to avoid hazards and obstacles around the worksite. Finally, you will learn about some of the common hazards associated with aerial lifts so&nbsp; you know how to recognize, avoid, or minimize them.<br><br></div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for aerial lifts:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>ANSI A92.2 – Vehicle Mounted Elevating and Rotating Aerial Devices</li><li>ANSI A92.3 – Manually Propelled Elevating Aerial Platforms</li><li>A92.5 – Boom Supported Elevating Work Platforms</li><li>A92.6 – Self-Propelled Elevating Work Platforms</li><li>A92.8 – Vehicle Mounted Bridge Inspection and Maintenance Devices</li><li>A92.20 – Design, Calculations, Safety Requirements, Test Methods for MEWPs</li><li>A92.22 – Safe Use of MEWPs</li><li>A92.24 – Training Requirements for MEWP Operators</li><li>1910 Subpart F – Powered Platforms, Man lifts, Vehicle Mounted Work Platforms</li><li>1910.23 – Guarding Floor and Wall Openings and Holes</li><li>1910.28 – Safety Requirements for Scaffolding&nbsp;</li><li>1910.29 – Manually Propelled Mobile Ladder Stands and Scaffolds (Towers)</li><li>1910.67 – Vehicle Mounted elevating and rotating work platforms</li><li>1910.333 – Selection and Use of Work Practices Shipyards &nbsp;</li><li>1915.71 – Scaffolds or Staging Construction&nbsp;</li><li>1926, Subpart L – Scaffolds</li><li>1926.21 – Safety Training and Education&nbsp;</li><li>1926.451 – General Requirements</li><li>1926.452 – Additional Requirements to Specific Types of Scaffolds</li><li>1926.453 – Aerial Lifts</li><li>1926.454 – Training Requirements</li><li>1926.501, 502 – Duty to have Fall Protection</li><li>1926.556 – Aerial Lifts</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA B354.1 – (PORTABLE ELEVATING WORK PLATFORMS)</li><li>CAN/CSA B354.2 – (SELF-PROPELLED ELEVATING PLATFORMS)</li><li>CAN/CSA B354.4 – (SELF-PROPELLED BOOM SUPPORTED)</li><li>CAN/CSA B354.5 – (MAST CLIMBING)&nbsp;</li><li>CAN/CSA B354.6 – (DESIGN)</li><li>CAN/CSA B354.7 – (SAFE USE)</li><li>CAN/CSA B354.8 – (TRAINING)</li><li>CAN/CSA C225 – (VEHICLE MOUNTED AERIAL DEVICES)</li><li>CAN/CSA Z259 – AND SUBSECTIONS (FALL PROTECTION, ARREST)</li><li>CAN/CSA Z271 – (SAFETY CODE FOR ELEVATING PLATFORMS)</li></ul>",
    facts: "<ul><li>1,380 workers were injured operating an aerial or scissor lift from 2011-2014.</li><li>From 2011-2014, 87 workers died while operating a scissor lift. (Source:<em> </em><a href=\"https://www.cdc.gov/niosh/topics/falls/aeriallift.html\"><em>CDC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://13wham.com/news/local/teens-severely-injured-after-40-ft-fall-at-farm-in-yates-county\">Teens Severely Injured After 40-ft. Fall at Farm in New York<br></a><a href=\"http://www.enterprisepub.com/public/man-dies-after--foot-fall/article_799980dc-9f1a-11e8-aecc-0b70911196c2.html\">Man Dies After 45-Foot Fall<br></a><a href=\"https://youtu.be/JLHc2wsyke0\">Bemidji Man Killed in Boom Lift Accident - Lakeland News at Ten - May 17, 2012<br></a><a href=\"https://youtu.be/HOxcYzgIu3o\">OSHA: Deadly Taunton Lift Accident Could Have Been Prevented<br></a><a href=\"https://hardhattraining.com/hazard-spotting-boom-lift-fall-protection/\">Hazard Spotting: Boom Lift, Fall Protection<br></a><a href=\"https://hardhattraining.com/2-machine-accidents-in-the-news/\">2 Machine Accidents in the News</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Scaffolding",
    overview: "<div>Our Scaffolding safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on the anatomy, proper inspection techniques, stability, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for Scaffolding:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>1926.450 – Scope and definitions of scaffolding</li><li>1926.451 – General Requirements</li><li>1926.452 – Additional Requirements</li></ul><div><strong>Canada Standards<br></strong><br></div><ul><li>CAN/CSA-Z271-10 – Safety Code for Suspended Platforms</li><li>CAN/CSA-Z797-09 – Code of Practice for Access Scaffold</li><li>CAN/CSA-S269.2 – Access Scaffolding for Construction</li></ul><div><strong>Industrial Standards<br></strong><br></div><ul><li>ANSI A10.8 – Construction, Demolition Operation</li></ul>",
    facts: "<ul><li>The number one cause of scaffolding fatalities is improper assembly of the scaffolding.</li><li>Scaffolding is number three on OSHA’s top ten most cited standards of 2018 (<em>Source: </em><a href=\"https://www.osha.gov/Top_Ten_Standards.html\"><em>OSHA</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://miami.cbslocal.com/2014/02/10/scaffolding-collapse-traps-workers-in-ft-lauderdale/\">Scaffolding Collapse Sends Four Workers to Hospital<br></a><a href=\"https://www.abc.net.au/news/2014-02-25/scaffolding-collapses-at-mascot-in-sydney/5282930\">Workover Investigates Sydney Scaffolding Collapse<br></a><a href=\"https://www.youtube.com/watch?v=no-18qN907Q\">Scaffolding Collapse Leaves Worker Hanging<br></a><a href=\"https://www.youtube.com/watch?v=jLoci0npxbQ\">8 injured in Scaffolding Collapse<br></a><a href=\"https://hardhattraining.com/four-steps-to-stay-safe-while-working-around-scaffolding/\">Four Tips to Stay Safe While Working Around Scaffolding<br></a><a href=\"https://hardhattraining.com/scaffolding-hazards/\">Scary Scaffolding - Hazard Spotting</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Scissor Lift",
    overview: "<div>Our Scissor Lift Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomy, stability, operations, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for scissor lifts:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSH ACT 1970&nbsp;</li><li>1910 Subpart F – Powered Platforms, Man lifts, Vehicle Mounted Work Platforms</li><li>1910.29 – Manually Propelled Mobile Ladder Stands and Scaffolds (Towers)</li><li>1910.67 – Vehicle Mounted Elevating and Rotating Work Platforms</li><li>1910.333 – Selection and Use of Work Practices Shipyards &nbsp;</li><li>1926.21 – Safety Training and Education&nbsp;</li><li>1926.451 – General Requirements</li><li>1926.452 – Additional Requirements to Specific Types of Scaffolds</li><li>1926.453 – Scissor Lifts</li><li>1926.454 – Training Requirements</li><li>1926.501, 502 – Duty to have Fall Protection</li><li>1926.556 – Scissor Lifts</li></ul><div><br></div><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA B354.1 – (PORTABLE ELEVATING WORK PLATFORMS&nbsp;</li><li>CAN/CSA B354.2 – (SELF-PROPELLED ELEVATING PLATFORMS</li><li>CAN/CSA B354.4 – (SELF-PROPELLED BOOM SUPPORTED</li><li>CAN/CSA B354.5 – (MAST CLIMBING)&nbsp;</li><li>CAN/CSA B354.6 – (DESIGN)</li><li>CAN/CSA B354.7 – (SAFE USE)</li><li>CAN/CSA B354.8 – (TRAINING)</li><li>CAN/CSA C225 – (VEHICLE MOUNTED AERIAL DEVICES)</li><li>CAN/CSA Z259 AND SUBSECTIONS – (FALL PROTECTION, ARREST)</li><li>CAN/CSA Z271 – (SAFETY CODE FOR ELEVATING PLATFORMS</li></ul><div><strong>International<br></strong><br></div><ul><li>ANSI A92.2-2015 – Vehicle Mounted Elevating and Rotating Scissor Devices</li><li>ANSI A92.3-2006 – Manually Propelled Elevating Scissor Platforms</li><li>A92.6-2017 – Self-Propelled Elevating Work Platforms</li><li>A92.8-2012 – Vehicle Mounted Bridge Inspection and Maintenance Devices</li></ul>",
    facts: "<ul><li>Scissor lifts offer great views and angles and have been known to be used on film sets. (Source: <a href=\"https://www.slideshare.net/LifterzUK/scissor-lifts-cutting-edge-facts\"><em>slideshare.net</em></a>)</li><li>Aerial lifts can be both two-wheel drive or four-wheel drive. (Source: <a href=\"https://www.aerialliftcertification.com/blog/11-things-never-knew-aerial-lifts/\"><em>aerialliftcertification.com</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.usnews.com/news/best-states/massachusetts/articles/2018-09-19/officials-2-workers-injured-when-scissor-lift-breaks-falls\">Officials: 2 Workers Injured When Scissor Lift Breaks, Falls<br></a><a href=\"http://www.heavyliftnews.com/accidents/fatal-scissor-lift-crush\">Fatal Scissor Lift Crush<br></a><a href=\"https://www.youtube.com/watch?v=eY7_qyQ4Jys\">Hot News - Scissor Lift Falls to the Ground with Two Workers on Board<br></a><a href=\"https://www.youtube.com/watch?v=lsLz5HElArM\">Scissor Lift Falls Over at Construction Site<br></a><a href=\"https://hardhattraining.com/scissor-lifts-safety-tips/\">Scissor Lifts Safety Tips<br></a><a href=\"https://hardhattraining.com/scissor-lift-accidents/\">Scissor Lift Accidents</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Carry Deck Crane",
    overview: "<div>Our Carry Deck Crane safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy, stability, rigging, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for Carry Deck Cranes:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>29 CFR 1926.180 – Crawler, Locomotive and Truck Cranes</li><li>29 CFR 1926.1400 – Cranes and Derricks</li></ul><div><strong>Canada Standards:<br></strong><br></div><ul><li>CAN/CSA-Z150-11 – safety Code on Mobile Cranes</li><li>CAN/CSA-Z150.3-11 – Safety Code on Articulating Boom Cranes</li><li>CAN/CSA-C22.2 – Safety Code for Material Hoists</li></ul><div><strong>International Standards<br></strong><br></div><ul><li>ISO 16715:2014 – Hand Signals used with Cranes</li><li>ASME/ANSI B30.1-29 – Cranes, Slings, Below the Hook Lifting Devices</li><li>ASME/ANSI B30.5 – Mobile and Locomotive Cranes</li><li>ASME/ANSI B30.22 – Articulating Boom Cranes</li></ul>",
    facts: "<ul><li>Carry Deck Cranes are better for maneuvering in tight spaces than standard cranes.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.mcall.com/news/mc-xpm-2003-08-22-3487784-story.html\">Three Electrocuted When Crane Touches Power Line<br></a><a href=\"https://hardhattraining.com/the-crane-problem/\">The Crane Problem<br></a><a href=\"https://hardhattraining.com/man-loses-life-crane-accident/\">Man Loses Life in Crane Accident</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Digger Derrick",
    overview: "<div>Our Digger-Derrick safety training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on basic anatomy, vehicle stability, rigging considerations, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.180 – Crawler, Locomotive, and Truck Cranes</li><li>29 CFR 1926.1400 – Cranes and Derricks</li><li>ASME B30.5 – Mobile and Locomotive Cranes</li><li>ASME B30.22 – Articulated Boom Cranes</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-Z150-11 – Safety Code on Mobile Cranes</li><li>CAN/CSA – Z150.3-11 – Safety Code on Articulating Boom Cranes</li><li>CAN/CSA – C22.2: Safety code for Material Hoists</li><li>ISO 16715:2014 – Hand Signals used with Cranes</li><li>ASME B30.5 – Mobile and Locomotive Cranes</li><li>ASME B30.22 – Articulating Boom Cranes</li><li>ANSI/ASSE A10 – Digger derrick specific standards</li></ul>",
    facts: "<ul><li>The term “derrick” comes from the name Thomas Derrick, a hangman during the 1600s. (Source: <a href=\"https://en.oxforddictionaries.com/definition/derrick\"><em>Oxford Dictionary</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.reformer.com/stories/hit-and-run-driver-taken-into-custody-in-conn,542658\">Hit-and-Run Driver Taken into Custody in Conn.<br></a><a href=\"https://www.ecmag.com/section/safety/apprentice-lineman-electrocuted-while-setting-utility-pole\">Apprentice Lineman Electrocuted While Setting Utility Pole</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Boom Truck",
    overview: "<div>Our Boom Truck Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomy, stability, rigging, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for boom trucks:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.180 – Crawler, Locomotive, and Truck Cranes</li><li>29 CFR 1926.1400 – Cranes and Derricks</li><li>ASME B30.5 – Mobile and Locomotive Cranes</li><li>ASME B30.22 – Articulating Boom Cranes</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-Z150-11 – Safety Code on Mobile Cranes</li><li>CAN/CSA – Z150.3-11 – Safety Code on Articulating Boom Cranes</li><li>CAN/CSA – C22.2: Safety code for Material Hoists</li><li>ISO 16715:2014 – Hand Signals used with Cranes</li><li>ASME B30.5 – Mobile and Locomotive Cranes</li><li>ASME B30.22 – Articulating Boom Cranes</li></ul>",
    facts: "<ul><li>Germany has one of the most powerful mobile cranes ever built: the Liebherr LTM 11200-9.1.</li><li>The LTM can lift 1200 metric tons, which is equivalent to 700 automobiles.&nbsp;</li><li>The LTM is also the longest telescopic boom in the world at 100 meters (about 109 yards). (Source:<em> </em><a href=\"https://fieldlens.com/blog/building-better/biggest-cranes/\"><em>Fieldlens.com</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.cdc.gov/niosh/nioshtic-2/20026903.html\">Crane Operator Electrocuted by Overhead Line<br></a><a href=\"https://www.bostonglobe.com/metro/2014/04/13/electrical-linemen-mourned-after-bourne-accident/XFaqRYGe8EOJJwyVXI0LgK/story.html\">Officials Probe Cause of Fatal Bourne Crane Accident</a> <br><a href=\"https://youtu.be/kyByUnfpsaE?t=22s\">Boom Truck Crane Fails in Tree Moving Attempt Again<br></a><a href=\"https://youtu.be/EzZW-aCaxC4\">Powerline Boom Truck Accident<br></a><a href=\"https://hardhattraining.com/boom-electrocuted/\">Boom! Electrocuted.</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Overhead Crane",
    overview: "<div>Our Overhead Crane safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy, rigging, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for Overhead Cranes:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>29 CFR 1910.179 – Overhead Cranes and Gantries</li><li>29 CFR 1926.554 – Overhead Hoists</li></ul><div><strong>Canada Standards:<br></strong><br></div><ul><li>CAN/CSA-B167-08 – Overhead traveling Cranes</li><li>CAN/CSA-C22.2 – Safety Code for Material Hoists</li></ul><div><strong>International Standards<br></strong><br></div><ul><li>ISO 16715:2014 – Hand Signals used with Cranes</li><li>ASME/ANSI B30.1-29 – Cranes, Slings, Below the Hook Lifting Devices</li><li>ASME/ANSI B30.2 – Overhead and Gantry Cranes</li><li>ASME B30.11 – Monorails and Under-hung Cranes</li><li>ASME B30.16 – Overhead Hoists</li><li>SME B30.17 Overhead and Gantry Cranes</li></ul>",
    facts: "<ul><li>In 1876 Sampson Moore designed and supplied electric overhead cranes.</li><li>In the last 10 years, over 37% of crane accidents were caused by being crushed by the load.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.craneaccidents.com/2013/12/report/crane-fatality-in-boston/\">Crane Fatality in Boston<br></a><a href=\"http://www.craneaccidents.com/2011/06/report/man-22-killed-in-south-side-industrial-accident/\">Man Killed in South Side Industrial Accident</a><br><a href=\"https://www.youtube.com/watch?v=ebTbLM9nKBk\">Overhead Crane Knocks Over Molten Vat<br></a><a href=\"https://hardhattraining.com/the-crane-problem/\">The Crane Problem<br></a><a href=\"https://hardhattraining.com/man-loses-life-crane-accident/\">Man Loses Life in Crane Accident</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Knuckle Boom",
    overview: "<div>Our Knuckle Boom safety training course is OSHA compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy, stability, rigging, operations, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards for articulated booms:&nbsp;<br><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926.650-652 – Excavations</li><li>29 CFR 1926, Subpart P, App A – Soil Classification</li><li>29 CFR 1926, Subpart P, App F – Protective Systems</li></ul>",
    facts: "<ul><li>Walter E.Thornton-Trump invented the first boom lift in 1951. (Source: <a href=\"https://www.bigrentz.com/how-to-guides/boom-lifts-used-today\"><em>BigRentz</em></a>)&nbsp;</li><li>The tallest articulating booms stand about 300 feet tall.&nbsp;</li><li>The most common aerial lift accidents include electrocutions, falls, lift tip-overs, catching between the lift and another object, and being struck by the lift or an object. (Source: <a href=\"https://www.certifymeonline.net/blog/5-common-aerial-lift-accidents/\"><em>CertifyMeOnline</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.americancranesandtransport.com/american-cranes-and-transport/training-and-safety-services-in-high-demand/136478.article\">&nbsp;Training and Safety Services in High Demand<br></a><a href=\"https://magicvalley.com/community/mini-cassia/news/burley-man-killed-another-injured-in-tree-trimming-accident/article_fafabf62-37d9-5717-bb31-57cd7d7244e9.html\">Burley Man Killed, Another Injured in Tree Trimming Accident<br></a><a href=\"https://www.youtube.com/watch?v=JLHc2wsyke0\">Bemidji Man Killed in Boom Lift Accident<br></a><a href=\"https://www.youtube.com/watch?v=ks0pyEYWT6s\">Raw Video: Worker Killed in Crane Accident<br></a><a href=\"https://hardhattraining.com/hazard-spotting-boom-lift-fall-protection/\">Hazard Spotting: Boom Lift, Fall Protection<br></a><a href=\"https://hardhattraining.com/aerial-lifts-shift-inspections/\">Aerial Lifts: Shift Inspections</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Lattice Boom",
    overview: "<div>Our Lattice Boom truck safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy, stability, rigging, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for Lattice Boom trucks:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>29 CFR 1926.180 – Crawler, Locomotive and Truck Cranes</li><li>29 CFR 1926.1400 – Cranes and Derricks</li></ul><div><strong>Canada Standards:<br></strong><br></div><ul><li>CAN/CSA-Z150-11 – safety Code on Mobile Cranes</li><li>CAN/CSA-Z150.3-11 – Safety Code on Articulating Boom Cranes</li><li>CAN/CSA-C22.2 – Safety Code for Material Hoists</li></ul><div><strong>International Standards<br></strong><br></div><ul><li>ISO 16715:2014 – Hand Signals used with Cranes</li><li>ASME/ANSI B30.1-29 – Cranes, Slings, Below the Hook Lifting Devices</li><li>ASME/ANSI B30.5 – Mobile and Locomotive Cranes</li><li>ASME/ANSI B30.22 – Articulating Boom Cranes</li></ul>",
    facts: "<ul><li>Lattice Boom Trucks are a better choice when jobs might last several days.</li><li>20% of construction related fatalities are the result of crane accidents.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.mcall.com/news/mc-xpm-2003-08-22-3487784-story.html\">Three Electrocuted When Crane Touches Power Line<br></a><a href=\"https://www.youtube.com/watch?v=yisTx2YTNI4\">Crane Falls onto Orlando Home<br></a><a href=\"https://www.youtube.com/watch?v=VMUDu6hUIzY\">One Killed, Another Injured in Metromover Accident<br></a><a href=\"https://hardhattraining.com/the-crane-problem/\">The Crane Problem<br></a><a href=\"https://hardhattraining.com/man-loses-life-crane-accident/\">Man Loses Life in Crane Accident</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Rigger Signaler",
    overview: "<div>Our Rigger and Signaler safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on weight, angles and stress, center of gravity, sling hitches and types, and the hardware and lifting devices these workers can expect to work with.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for riggers and signalmen:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>29 CFR 1926.180 – Crawler Locomotive and Truck Cranes</li><li>29 CFR 1926.1400 – Cranes and Derricks</li><li>29 CFR 1910.179 – Overhead Cranes and Gantries</li><li>29CFR 1926.554 – Overhead Hoists</li><li>ASME B30.5 – Mobile and Locomotive Cranes</li><li>ASME B30.22 – Articulating Boom Cranes</li><li>ASME B30.2, 11, 16, 17 – Overhead and Gantry Cranes</li></ul><div><strong>Canada Standards<br></strong><br></div><ul><li>CAN/CSA-Z150-11 – Safety Code on Mobile Cranes</li><li>CAN/CSA-Z150.3-11 – Safety Code on Articulating Boom Cranes</li><li>CAN/CSA-C22.2 – Safety Code for Material Hoists</li><li>ISO 16715:2014 – Hand Signals with Cranes</li><li>ASME B30.5 – Mobile and Locomotive Cranes</li><li>ASME B30.22 – Articulating Boom Cranes</li><li>ASME B30.2, 11, 16, 17 – Overhead and Gantry Cranes</li></ul>",
    facts: "<ul><li>Sailors were the original riggers, where their skills with ropes and knots allowed them to quickly raise sails, and other loads.</li><li>Texas employs the most riggers in the United States.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.sacbee.com/news/local/article4073691.html\">State Issues Fine for Deadly Crane Accident<br></a><a href=\"https://www.bisnow.com/new-york/news/construction-development/city-shuts-down-crane-work-at-multiple-nyc-sites-over-safety-concerns-100337\">City Shuts Down Crane Work at Multiple NYC Sites over Safety Concerns<br></a><a href=\"https://www.youtube.com/watch?v=aURagk13f4E\">2 Workers Die in Crane Accident in Winters<br></a><a href=\"https://www.youtube.com/watch?v=1khxU_vRIl8\">Crane Cable Snaps, Dropping AC unit 28 Stories<br></a><a href=\"https://hardhattraining.com/rigger-mortis-crane-safety/\">Rigor Mortis- Crane Safety<br></a><a href=\"https://hardhattraining.com/common-rigger-hand-signals/\">7 Common Rigger Hand Signals</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Signal Person",
    overview: "<div>Our Rigger and Signalman safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on weight, angles and stress, center of gravity, sling hitches and types, and the hardware and lifting devices these workers can expect to work with.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for riggers and signalmen:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>29 CFR 1926.180 – Crawler Locomotive and Truck Cranes</li><li>29 CFR 1926.1400 – Cranes and Derricks</li><li>29 CFR 1910.179 – Overhead Cranes and Gantries</li><li>29CFR 1926.554 – Overhead Hoists</li><li>ASME B30.5 – Mobile and Locomotive Cranes</li><li>ASME B30.22 – Articulating Boom Cranes</li><li>ASME B30.2, 11, 16, 17 – Overhead and Gantry Cranes</li></ul><div><strong>Canada Standards<br></strong><br></div><ul><li>CAN/CSA-Z150-11 – Safety Code on Mobile Cranes</li><li>CAN/CSA-Z150.3-11 – Safety Code on Articulating Boom Cranes</li><li>CAN/CSA-C22.2 – Safety Code for Material Hoists</li><li>ISO 16715:2014 – Hand Signals with Cranes</li><li>ASME B30.5 – Mobile and Locomotive Cranes</li><li>ASME B30.22 – Articulating Boom Cranes</li><li>ASME B30.2, 11, 16, 17 – Overhead and Gantry Cranes</li></ul>",
    facts: "<ul><li>Sailors were the original riggers, where their skills with ropes and knots allowed them to quickly raise sails, and other loads.</li><li>Texas employs the most riggers in the United States.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.sacbee.com/news/local/article4073691.html\">State Issues Fine for Deadly Crane Accident<br></a><a href=\"https://www.bisnow.com/new-york/news/construction-development/city-shuts-down-crane-work-at-multiple-nyc-sites-over-safety-concerns-100337\">City Shuts Down Crane Work at Multiple NYC Sites over Safety Concerns<br></a><a href=\"https://www.youtube.com/watch?v=aURagk13f4E\">2 Workers Die in Crane Accident in Winters<br></a><a href=\"https://www.youtube.com/watch?v=1khxU_vRIl8\">Crane Cable Snaps, Dropping AC unit 28 Stories<br></a><a href=\"https://hardhattraining.com/rigger-mortis-crane-safety/\">Rigor Mortis- Crane Safety<br></a><a href=\"https://hardhattraining.com/common-rigger-hand-signals/\">7 Common Rigger Hand Signals</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Asphalt",
    overview: "<div>Our Asphalt Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on exposure, first aid, personal protection equipment (PPE), safe practices, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for asphalt safety:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>Section 5 (a)(1) OSH act of 1970 – General Duty Clause on Training</li><li>1910 Subpart I – Personal Protective Equipment</li><li>1910.132 – General Requirements</li><li>1910.134 – Respiratory Protection</li></ul><div><br></div><div><strong>Canada<br></strong><br></div><ul><li>OHS guidelines – Chemical agents and biological agents</li><li>Occupational Health and Safety Regulation – Part 08 Personal Protective equipment</li><li>Environmental Management Act – Asphalt Plant Regulations</li><li>Alberta Code of Practice for Paving Plants</li><li>The Canadian Environmental Protection Act (CEPA) Code of Practice – Reduction of VOC Emissions From Cutback And Emulsified Asphalt.&nbsp;</li><li>Alberta – Alberta Labour OHS Regulations</li></ul>",
    facts: "<ul><li>Recycling asphalt roads saves about $300 million each year. About 80% of asphalt that is removed is re-used. (Source: <a href=\"https://www.wolfpaving.com/blog/10-cool-facts-you-may-not-know-about-asphalt\"><em>Wolf Paving</em></a>)&nbsp;</li><li>Exposure to asphalt fumes affects over 500,000 people. (Source: <a href=\"https://www.osha.gov/archive/oshinfo/priorities/asphalt.html\"><em>OSHA</em></a>)</li><li>Asphalt fumes can cause headaches, skin rashes, fatigue, reduced appetite, throat or eye irritation, coughing, breathing problems, or stomach problems. (Source: <a href=\"https://www.osha.gov/archive/oshinfo/priorities/asphalt.html\"><em>OSHA</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.wftv.com/news/trending-now/florida-industrial-worker-dies-in-fall-into-asphalt-machine/839876736\">Florida Industrial Worker Dies in Fall into Asphalt Machine<br></a><a href=\"https://www.tulsaworld.com/news/local/year-old-man-killed-in-accident-involving-asphalt-paver/article_0f6e305d-4c93-5ff6-ae2a-f8a5793db484.html\">59-year-old Man Killed in Accident Involving Asphalt Paver<br></a><a href=\"https://www.youtube.com/watch?v=XLl9k70rpms\">Asphalt Fumes &amp; Occupational Exposure Concerns<br></a><a href=\"https://www.youtube.com/watch?v=9cJIF8Fevi0\">Man Safe After Being Trapped in Asphalt Silo<br></a><a href=\"https://hardhattraining.com/roadway-work-an-underappreciated-danger/\">Roadway Work: An Underappreciated Danger</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Backhoe Loader",
    overview: "<div>Our Backhoe safety training course is OSHA compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy, stability, operation, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards for backhoes:<br><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926.650-652 – Excavations</li><li>29 CFR 1926, Subpart P, App A – Soil Classification</li><li>29 CFR 1926, Subpart P, App F – Protective Systems</li></ul>",
    facts: "<ul><li>The backhoe was originally called a steam shovel, developed in 1835. (Source: <a href=\"http://info.texasfinaldrive.com/shop-talk-blog/interesting-facts-about-the-backhoe-loader\"><em>Texas Final Drive</em></a>)</li><li>The first backhoe loader was created by Whitlock Bros. in 1951. (Source: <a href=\"http://info.texasfinaldrive.com/shop-talk-blog/interesting-facts-about-the-backhoe-loader\"><em>Texas Final Drive</em></a>)</li><li>Backhoes typically travel faster than most equipment because it is often an attachment to a highly mobile piece of machinery.&nbsp;</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.nbc4i.com/news/u-s-world/toddler-killed-in-backhoe-accident/1672532038\">Toddler Killed in Backhoe Accident<br></a><a href=\"https://www.khon2.com/news/local-news/man-dies-after-getting-pinned-in-excavator-in-hauula/1662575888\">Medical Examiner Identifies Man Killed in Backhoe Accident in Hauula<br></a><a href=\"https://www.youtube.com/watch?v=XmNdXd0KkNg\">Man Killed in Backhoe Accident<br></a><a href=\"https://www.youtube.com/watch?v=2sG3VvPQGXE\">Largest Backhoe is Located in Crookston<br></a><a href=\"https://www.youtube.com/watch?v=ZhhBo8IoPyY\">Backhoe Accident<br></a><a href=\"https://hardhattraining.com/backhoe-accidents-statistics-tips/\">Backhoe Accidents, Statistics, and Tips</a> <br><a href=\"https://hardhattraining.com/backhoe-loader-accidents/\">Backhoe Loader Accidents</a>&nbsp;</div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Bulldozer",
    overview: "<div>Our Bulldozer Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomy, stability, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for bulldozers:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926 Subpart W – Rollover Protective Structures</li><li>29 CFR 1926.20 – General Safety and Health Provisions, training</li><li>29 CFR 1926.21 – Training and Education</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B352.0-09 – ROPS, FOPS (General Mobile Equipment)</li><li>CAN/CSA-M12117-05 – Earth-Moving Machinery (TOPS) for Compact Excavators</li><li>CAN/CSA-M3471-05 – Earth-Moving Machinery – ROPS, Laboratory Tests, Performance Requirements</li><li>CAN/CSA-M3450-03 – Earth-Moving Machinery – Braking Systems of Rubber-Tired Machines – Systems and Performance Requirements and Test Procedures</li></ul>",
    facts: "<ul><li>The first bulldozers were modified tractors. (Source: <a href=\"http://www.newworldencyclopedia.org/entry/Bulldozer\"><em>newworldencyclopedia.org</em></a>)</li><li>Smaller bulldozers made for more urban areas are known as calfdozers. (Source:&nbsp; <a href=\"https://www.collinsdictionary.com/us/dictionary/english/calfdozer\"><em>collinsdictionary.com</em></a>)</li><li>Bulldozers are used in some militaries to attack and demolish structures. (Source: <a href=\"https://someinterestingfacts.net/how-do-bulldozers-work/\"><em>someinterestingfacts.net</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.pressdemocrat.com/news/8793265-181/man-dies-in-bulldozer-accident?sba=AAS\">Man Dies in Bulldozer Accident in Bodega<br></a><a href=\"https://www.abc10.com/video/news/north-fire-bulldozer-operator-killed-in-crash-en-route-to-incident-command/103-8248694\">North Fire: Bulldozer Operator Killed in Crash en route to Incident Command<br></a><a href=\"https://youtu.be/V9DAaHeF74Y\">Bulldozer Accident<br></a><a href=\"https://youtu.be/j6oO3mFPE6Y\">Worker Killed by Bulldozer at St. Petersburg Construction Site</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Concrete Truck",
    overview: "<div>Our Concrete Mixer Truck Safety training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on basic anatomy, machine stability, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for concrete mixer trucks:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.21 – Training and Education</li><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.20 – General Safety and Health Provisions, training</li><li>29 CFR 1910.146 – Confined Space</li></ul><div><strong>Canada<br></strong><br></div><ul><li>Canadian Environmental Protection Act, 1999 – Framework for pollution prevention</li><li>Occupational Health and Safety Regulations, 1996</li><li>Highway Traffic Act</li></ul>",
    facts: "<ul><li>In 2000, the most common reported injury for concrete truck drivers was sprains and strains.<strong>&nbsp;</strong></li><li>Concrete truck drivers injure their backs the most, followed by their upper extremities.&nbsp;</li><li>In the concrete industry, OSHA issues the most citations for permit-required confined space violations. (Source: <a href=\"https://www.cpwr.com/sites/default/files/publications/krreadymixed_0.PDF\"><em>CPWR</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.straitstimes.com/singapore/cement-mixer-truck-involved-in-accident-in-aljunied-road\">Cement Mixer Truck Involved in Accident in Aljunied Road<br></a><a href=\"https://abc13.com/cement-truck-driver-killed-in-rollover-crash-near-fulshear/3371934/\">Cement Truck Driver Killed in Rollover Crash Near Fulshear<br></a><a href=\"https://youtu.be/M488jgR86Zg\">Dashcam Captures Incredible Cement Truck Crash<br></a><a href=\"https://youtu.be/KX7qm8dU1Gw\">Cement Truck Overturns on I-5 in La Jolla<br></a><a href=\"https://hardhattraining.com/concrete-mixer-truck-safety/\">Concrete Mixer Truck Safety</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Directional Drill",
    overview: "<div>Our Directional Drill Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on basic anatomy, safe operations, common hazards, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following OSHA standards:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926 Subpart P – Excavations</li><li>29 CFR 1926 Subpart W – Rollover Protective Structures</li><li>29 CFR 1926.20 – General Safety and Health Provisions, training</li><li>29 CFR 1926.21 – Training and Education</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B352.0-09 – ROPS, FOPS (General Mobile Equipment)</li><li>Oil and Gas Occupational Safety and Health Regulations – SOR/87-612</li><li>Occupational Health and Safety Regulations, 1996 – Part XXIX – Oil and Gas</li><li>Occupational Health and Safety Code 2009 – Part 37 – Oil and Gas</li></ul>",
    facts: "<ul><li>Almost 60 workers die each year in excavation accidents.<strong>&nbsp;</strong></li><li>68% of those fatalities occur in companies with fewer than 50 workers.<strong>&nbsp;</strong></li><li>48% occur in companies with 10 or fewer workers. (Source: <a href=\"https://www.cdc.gov/niosh/updates/93-110.html\"><em>CDC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.nbcchicago.com/news/local/Worker-Killed-by-Excavator-Bucket-When-Operators-Cloth-Snags-on-Lever-Police-454522193.html\">Worker Fatally Struck by Excavator Bucket When Operator’s Clothing Snags on Lever<br></a><a href=\"https://www.smh.com.au/national/nsw/missing-safety-pin-blamed-as-excavator-scoop-crushes-husband-waiting-for-news-of-new-baby-20120207-1r2eb.html\">Missing 'Safety Pin' Blamed as Excavator Scoop Crushes Husband Waiting for News of New Baby<br></a><a href=\"https://youtu.be/3YBF5QmM6wE\">Erin Kelly on Fatal Excavator Accident<br></a><a href=\"https://youtu.be/CoaHldaMPTk\">Building Collapse in Portugal<br></a><a href=\"https://hardhattraining.com/excavator-hits-power-line-causes-fire/\">Excavator Hits Power Line, Causes Fire<br></a><a href=\"https://hardhattraining.com/360-excavator-recognizing-top-6-hazards/\">360 Excavator: Recognizing the Top 6 Hazards</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Dump Truck",
    overview: "<div>Our Dump Truck Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on internal and external anatomy, principles to maintain stability, safe operation, hazards to avoid, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for dump trucks:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.601 – Motor Vehicles</li><li>29 CFR 1926.602 – Material Handling</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926.28 – PPE</li><li>29 CFR 1926.55 – Gases, Fumes, Dusts</li><li>29 CFR 1926 Subpart G – Signs, Signals, Barricades</li></ul><div><strong>Canada<br></strong><br></div><ul><li>Can/CSA-B3252.0-09 – ROPS, FOPS (General Mobile Equipment)</li><li>CSA Z1001 – Occupational Health &amp; Safety Training</li><li>CSA Z96 – High Visibility Apparel</li><li>CSA Z617-06 – PPE</li><li>Canada Labour Code Part II – Employer and Employee Duties</li></ul>",
    facts: "<ul><li>From 1992 to 2007, there were 829 construction worker fatalities that occurred because of dump truck-related incidents. (Source: <a href=\"https://www.ncbi.nlm.nih.gov/pubmed/22113947\"><em>Pubmed</em></a>)</li><li>The largest dump truck in the world can carry 500 metric tons of dirt. This is equivalent to about 1 million pounds. (Source:<em> </em><a href=\"https://www.smithsonianmag.com/smart-news/worlds-largest-dump-truck-and-its-hybrid-180953812/\"><em>Smithsonian.com</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.columbiadailyherald.com/news/20190131/blown-tire-leads-to-fatal-i-65-dump-truck-crash\">Blown Tire Leads to Fatal I-65 Dump Truck Crash<br></a><a href=\"https://www.wspa.com/news/dump-truck-flips-in-i-85-crash-near-boiling-springs/1736328504\">Dump Truck Flips on I-85 <br></a><a href=\"https://www.wkrn.com/news/local-news/lewisburg-community-mourns-man-killed-by-dump-truck-while-saving-2-people/1746498023\">Lewisburg Community Mourns Man Killed by Dump Truck While Saving 2 People<br></a><a href=\"https://losangeles.cbslocal.com/video/3904956-dump-truck-accidentally-pulls-down-15-power-poles/\">Dump Truck Accidentally Pulls Down 15 Power Poles<br></a><a href=\"https://hardhattraining.com/dump-truck-near-miss/\">Dump Truck: A Near Miss<br></a><a href=\"https://hardhattraining.com/dump-truck-accident/\">Dump Truck Accident</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Roller Compactor",
    overview: "<div>Our Roller-Compactor Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, principles of stability, safe operations, hazards to avoid, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for roller-compactors:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926 Subpart P – App A Soil Classification</li><li>29 CFR 1926 Subpart W – Rollover Protective Structures; Overhead Protection</li><li>29 CFR 1926.1000 – Rollover Protective Structures (ROPS) for Material Handling Equipment</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B352.0-09 – ROPS, FOPS (General Equipment)</li><li>CAN/CSA-M12117-05 – Earth-Moving Machinery (TOPS) for Compact Excavators</li><li>CAN/CSA-M3471-05 – Earth-Moving Machinery – ROPS, Laboratory Tests, Performance Requirements</li><li>Can.CSA-M3450-03 – Earth-Moving Machinery – braking Systems of Rubber-Tired Machines – Systems and Performance Requirements and Test Procedures</li></ul>",
    facts: "<ul><li>Out of the 282 road grading and surface machinery-related deaths recorded by NIOSH from 1992 to 2001, at least 70 of these deaths were associated with roller-compacters. (Source: <a href=\"http://elcosh.org/document/1837/d000678/niosh%253A-preventing-injuries-when-working-with-ride-on-roller-compactors.html\"><em>ELCOSH</em></a>)</li><li>The first mechanical roller consisted of a steel drum with a seat on top of the frame for the driver. The roller was usually pulled by livestock. (Source: <a href=\"http://www.hcea.net/page-1547738/9989382#photo\"><em>HCEA</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.nbcchicago.com/news/local/Chicago-Man-Dies-in-Army-Corps-Industrial-Accident-in-Alaska--430243763.html\">Chicago Man Dies in Army Corps Industrial Accident in Alaska<br></a><a href=\"https://bangaloremirror.indiatimes.com/bangalore/crime/road-compactor-crushes-11-yr-old-boy-to-death-in-bsk-vi/articleshow/64336360.cms\">Road Compactor Crushes 11-yr-old Boy to Death in BSK-VI<br></a><a href=\"https://www.youtube.com/watch?v=JYJbZlpZI40\">Operator Trying to Avoid Crash Gets Pinned Under Asphalt Roller in Monroeville<br></a><a href=\"https://www.youtube.com/watch?v=k66ufnF3nq4\">Paving Roller Runs Over, Kills Man on Route 66</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Front End Loader",
    overview: "<div>Our Front-End Loader safety training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, principles of stability, safe operation, hazards to avoid, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for front-end loaders:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926.650-652 – Excavations</li><li>29 CFR 1926, Subpart P, App A – Soil Classification</li><li>29 CFR 1926, Subpart P, App F – Protective Systems</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B352.0-09 – ROPS, FOPS (General Mobile Equipment)</li><li>CAN/CSA-M12117-05 – Earth-Moving Machinery (TOPS) for Compact Excavators</li><li>CAN/CSA-M3471-05 – Earth-Moving Machinery – ROPS, Laboratory Tests, Performance Requirements</li><li>CAN/CSA-M3450-03 – Earth-Moving Machinery – Braking Systems of Rubber-Tired Machines – Systems and Performance Requirements and Test Procedures</li></ul>",
    facts: "<div><em>Coming!</em></div>",
    fails: "<div><a href=\"https://koaa.com/news/covering-colorado/2019/01/29/front-end-loader-used-to-vandalize-wildlife-facility/\">Front End Loader Used to Vandalize Wildlife Facility<br></a><a href=\"https://citizen.co.za/news/south-africa/accidents/2062396/man-killed-two-others-injured-in-johannesburg-car-front-end-loader-crash/\">Man Killed, Two Others Injured in Johannesburg Car, Front-End Loader Crash<br></a><a href=\"https://www.9news.com/video/news/crime/criminal-drives-front-end-loader-into-atm/73-8274165\">Criminal Drives Front End Loader into ATM<br></a><a href=\"https://www.youtube.com/watch?v=SwUTF6CW6WU\">Man in Front-End Loader Destroys N.S. Home, Flees to Liquor Store<br></a><a href=\"https://hardhattraining.com/balancing-front-loaders/\">5 Balancing Front Loaders Tips<br></a><a href=\"https://hardhattraining.com/front-end-loader-crash/\">Front-End Loader Crash</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Grader",
    overview: "<div>Our Grader Safety Training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomical components, principles of stability, safe operations, hazards to avoid, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for graders:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926.650-652 – Excavations</li><li>29 CFR 1926, Subpart P, App A – Soil Classification</li><li>29 CFR 1926, Subpart P, App F – Protective Systems</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B352.0-09 – ROPS, FOPS (General Mobile Equipment)</li><li>CAN/CSA-M12117-05 – Earth-Moving Machinery (TOPS) for Compact Excavators</li><li>CAN/CSA-M3471-05 – Earth-Moving Machinery – ROPS, Laboratory Tests, Performance Requirements</li><li>CAN/CSA-M3450-03 – Earth-Moving Machinery – Braking Systems of Rubber-Tired Machines – Systems and Performance Requirements and Test Procedures</li></ul>",
    facts: "<div><em>Coming!</em></div>",
    fails: "<div><a href=\"https://www.starherald.com/news/local_news/one-injured-in-crash-with-road-grader/article_fd8b97d8-470c-11e8-ade9-8715291eee11.html\">One Injured in Crash with Road Grader<br></a><a href=\"https://www.mlive.com/news/grand-rapids/index.ssf/2018/09/driver_dies_in_fiery_jenison_c.html\">Driver Dies in Fiery Jenison Crash with Road Grader<br></a><a href=\"https://www.kcci.com/article/police-teen-killed-by-road-grader-appears-to-be-accidental/9115869\">Police: Teen Killed by Road Grader Appears to be Accidental<br></a><a href=\"http://newschannelnebraska.com/local-news/one-person-life-flighted-after-road-grader-accident/\">One Person Life-Flighted After Road Grader Accident</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Skid Steer",
    overview: "<div>Our Skid Steer Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, principles of stability, safe operations, common hazards to avoid, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following general standards for skid steers:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>SAE J1388-2013 – General Standards for Skid Steers, Personnel Protection</li><li>1926.20 – General Safety and Health Provisions, Training</li><li>29 CFR 1926.21 – Training and Education</li><li>OSHA 29 CFR 1926.602 – Earth Moving Equipment (Currently Excludes Skid Steers)</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B352.0-09 – ROPS, FOPS (General Mobile Equipment)</li><li>CAN/CSA-M12117-05 – Earth-Moving Machinery (TOPS) for Compact Excavators</li><li>CAN/CSA-M3471-05 – Earth-Moving Machinery – ROPS, Laboratory Tests, Performance Requirements</li><li>CAN/CSA-M3450-03 – Earth-Moving Machinery – Braking Systems of Rubber-Tired Machines – Systems and Performance Requirements and Test Procedures</li></ul>",
    facts: "<ul><li>The largest skid steer currently on the market, the Gehl V420, has an operating weight of over 11,000 pounds. (Source: <a href=\"https://www.heavyequipmentguide.ca/article/27249/gehl-introduces-worlds-largest-skid-steer-loader\"><em>heavyequipmentguide.ca</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"http://www.abc6.com/story/39809575/jamestown-man-killed-in-landscaping-accident\">Jamestown Man Killed in Landscaping Accident<br></a><a href=\"https://okcfox.com/news/local/toddler-struck-killed-by-skid-steer-in-accident-in-kingfisher-county\">Toddler Struck, Killed by Skid-Steer in Accident in Kingfisher County<br></a><a href=\"https://www.youtube.com/watch?v=kRy26yNGeoI\">One Injured in Skid Loader Accident in Upper Allen Township, Pa.<br></a><a href=\"https://www.youtube.com/watch?v=NPAu3ogcr4I\">Skid Steer Accident at Oakland Work Site Kills Man<br></a><a href=\"https://hardhattraining.com/skid-steer-safety-tips/\">Skid Steers: 9 Safety Tips<br></a><a href=\"https://hardhattraining.com/skid-steer-woods/\">Skid Steer in the Woods</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Trench Safety",
    overview: "<div>Our Trench Safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on competency, soil classification, protective systems, atmosphere, common hazards and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam, as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for Trench Safety:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>29 CFR 1926 Subpart P</li><li>29 CFR 1926.650-Scope, Application, and definitions</li><li>29 CFR 1926.651- Specific Excavation Requirements</li><li>29 CFR 1926.652- Requirements for protective systems</li><li>29 CFR 1926 Subpart P App A- Soil Classification</li><li>29 CFR 1926 Subpart P App B- Sloping and Benching</li><li>29 CFR 1926 Subpart P App C- Timber Shoring for Trenches</li><li>29 CFR 1926 Subpart P App D- Aluminum Hydraulic Shoring for Trenches</li><li>29 CFR 1926 Subpart P App E- Alternatives to Timber Shoring</li><li>29 CFR 1926 Subpart P App F- Selection of Protective Systems</li><li>29 CFR 1926.20, General Safety and Health Provisions, training</li><li>29 CFR 1926.21, Training and Education</li></ul>",
    facts: "<ul><li>A cubic yard of soil (27 cubic feet, or 1 cubic meter) weighs more than the average compact car.</li><li>From 2011 - 2016, there were over 130 fatalities related to trenching and excavation.</li><li>49% of the 130 fatalities occurred in 2015-2016.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://trib.com/news/state-and-regional/men-die-in-wyoming-construction-site-accident/article_e466e944-a15e-5a0f-b876-8fc63049b334.html\">Two Men die in Wyoming Construction Site Accident<br></a><a href=\"https://boston.cbslocal.com/2017/02/08/south-end-trench-collapse-atlantic-drain-services-manslaughter/\">Company Owner Charged with Manslaughter After Deadly South End Trench Collapse<br></a><a href=\"https://www.youtube.com/watch?v=gUd7o_XF-8U\">Man dies in Trench Collapse on Construction Site<br></a><a href=\"https://www.youtube.com/watch?v=zAG_ohhARVU\">Two Men Dead in Trench Collapse at Windsor Construction Site<br></a><a href=\"https://hardhattraining.com/trench-safety-5-common-hazards/\">Trench Safety: 5 Common Hazards<br></a><a href=\"https://hardhattraining.com/trench-collapse-dangers/\">Trench Collapse Dangers</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Trencher",
    overview: "<div>Our Trencher safety training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, safe operations, common hazards, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for trenchers:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926 Subpart P – Excavations</li><li>29 CFR 1926 Subpart W – Rollover Protective Structures</li><li>29 CFR 1926.20 – General Safety and Health Provisions, training</li><li>29 CFR 1926.21 – Training and Education</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B352.0-09 – ROPS, FOPS General Mobile Equipment</li><li>CAN/CSA-M3471-05 – Earth-Moving Machinery – ROPS, Laboratory Tests, Performance Requirements</li><li>CAN/CSA-M3450-03 – Earth-Moving Machinery – Braking Systems of Rubber-Tired Machines – System and Performance Requirements and Test Procedures</li></ul>",
    facts: "<ul><li>Trenching machines are also known as ditchers or digging wheels. (Source<em>: </em><a href=\"https://www.britannica.com/technology/trenching-machine\"><em>Encyclopedia Britannica</em></a>)</li><li>From 2000 to 2006, over 270 workers were killed in trenching or excavation cave-ins. (Source: <a href=\"https://www.cdc.gov/niosh/topics/trenching/default.html\"><em>CDC</em></a>)&nbsp;</li></ul><div><br></div>",
    fails: "<div><a href=\"https://valleycentral.com/news/local/police-cadet-nearly-sliced-in-half-in-freak-accident-survives-owes-500000-in-medical-bills\">Police Cadet Nearly Sliced in Half in Freak Accident Survives, Owes $500,000 in Medical Bills<br></a><a href=\"https://www.enr.com/articles/44899-text-field-amputation-unit-finds-regular-call-from-construction\">Texas Trauma Unit Fields Regular Calls from Construction<br></a><a href=\"https://www.nbcdfw.com/news/local/Construction-Worker-Trapped-Doctor-on-Way-Authorities-472155133.html\">Construction Worker Hospitalized After Entrapment in Frisco<br></a><a href=\"https://hardhattraining.com/trench-collapse-raises-concern-for-trench-safety/\">Trench Collapse Raises Concern for Trench Safety</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Arc Flash",
    overview: "<div>Our Arc Flash Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on controls, operations, common hazards, emergency response, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for arc flash reduction:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSHA 29 CFR 1910.269 Subpart R – Special Industries</li><li>OSHA 29 CFR 1910.269 Subpart S – Electrical, General Industry</li><li>OSHA 29 CFR 1926 Subpart V – Electric Power Transmission and Distribution, Construction</li><li>ANSI Z535 – Series of Standards for Safety Signs and Tags</li></ul><div><strong>Canada<br></strong><br></div><ul><li>NFPA 70E – Standard for Electrical Safety in the workplace, National Fire Protection Association</li><li>NEC Article 110.16, Arc Flash Hazard Warning</li><li>Article 240.87 Arc Energy Reduction, National Electric Code</li><li>CSA Z462 – Workplace Electrical Safety</li><li>CEC C22.1 – Canadian Electrical Code</li><li>Alberta – Alberta OHS Act, Regulation, and Code&nbsp;</li><li>B.C., Workers Compensation Act – Part 3 Occupational Health and Safety</li><li>Manitoba – Workplace Safety and Health Act and Regulation</li><li>Nova Scotia – OHS Act and Regulation</li><li>Saskatchewan – OHS Act and Regulation</li><li>Ontario – OHS Act and Regulation</li></ul><div><strong>International<br></strong><br></div><ul><li>NFPA 70E – Standard for Electrical Safety in the Workplace</li><li>NEC Article 110.16 – Arc Flash Hazard Warning</li><li>NEC Article 240.87 – Arc Energy Reduction, National Electric Code</li></ul>",
    facts: "<ul><li>From 1992-2013, nearly 6,000 workers died from electrical injuries in the U.S.</li><li>From 2003-2012, more than 24,100 workers were non-fatally injured by electricity.</li><li>About 40% of electrical incidents involved 250 volts or less. (Source: <a href=\"https://www.nfpa.org/-/media/Files/News-and-Research/Fire-statistics-and-reports/Electrical/RFArcFlashOccData.ashx?la=en\"><em>NFPA</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.ecmweb.com/arc-flash/two-workers-injured-arc-flash-air-force-base\">Two Workers Injured in Arc Flash at Air Force Base<br></a><a href=\"https://www.kxii.com/content/news/One-person-hospitalized-after-explosion-at-Panda-Power-in-Sherman-417302293.html\">One Person Hospitalized After Incident at Panda Power in Sherman<br></a><a href=\"https://www.youtube.com/watch?v=ePiTODvl_vk\">Arc Flash Accident<br></a><a href=\"https://youtu.be/E-tKlTp1UJw\">News Footage of Arc Flash</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Pallet Jack",
    overview: "<div>Our Class III Pallet Jack Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, principles of stability, safe operations, hazards to avoid, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for pallet jacks:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.178 – Powered Industrial Trucks, General Standards</li><li>29 CFR 1926.602 – Material Equipment and Handling, Requirements for Powered Industrial Trucks&nbsp;</li><li>29 CFR 1915.120 – Powered Industrial Truck Operator Training</li><li>29 CFR 1918.65 – Mechanically Powered Vehicles Used Aboard Vessels</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B335-04 – Safety Standards for Lift Trucks</li><li>CAN/CSA-B335-94 – Lift Truck Operator Training</li><li>Can/CSA-B352.0-09 – ROPS, FOPS</li><li>ANSI/ITSDF B56.1 – Safety Standard for Low Lift and High Lift Trucks</li><li>ANSI/ITSDF B56/1 – Safety Standard for Rough Terrain Forklift Trucks</li><li>ISO 5057:1993 – Inspection, repair of fork arms in service on fork-lift trucks</li></ul>",
    facts: "<ul><li>Pallet jacks have been around since 1918. (Source: <a href=\"https://books.google.com/books?id=EikDAAAAMBAJ&amp;pg=PA54#v=onepage&amp;q&amp;f=false\"><em>Popular Science Monthly</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.sun-sentinel.com/local/broward/deerfield-beach/fl-sb-deerfield-woman-trapped-beneath-pallets-20170802-story.html\">Supermarket Worker Hurt in Accident with Lifting Device at Deerfield Beach Store<br></a><a href=\"https://www.couriermail.com.au/news/queensland/worker-rides-pallet-jack-like-a-scooter-awarded-compo/news-story/deea89395198f3525b7ebcd444c0d49f\">Worker Rides Pallet Jack Like a Scooter, Awarded Compo</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Stand Up Forklift",
    overview: "<div>Our class I Stand-Up Forklift Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, principles of stability, safe operations, common hazards to avoid, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.<br><br></div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following general standards for stand-up forklifts:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSHA 29 CFR 1910.178 – Powered Industrial Trucks</li><li>OSHA 29 CFR 1910.178 APP A</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B335-04 – Safety Standard for Lift Trucks</li><li>CAN/CSA-B335-94 – Lift Truck Operator Training</li><li>CAN/CSA-B352.0-09 – ROPS, FOPS</li></ul><div><strong>International<br></strong><br></div><ul><li>ANSI/ITSDF B56.1 – Low Lift and High Lift Trucks</li><li>ANSI/ITSDF B56.6 – Rough Terrain Forklift Lift Trucks</li><li>ISO 5057:1993 – Inspection, repair of fork arms in service on fork-lift trucks</li></ul>",
    facts: "<ul><li>Forklifts became popular during World War 1 when manual labor was scarce. (Source: <a href=\"http://www.fork-lift-training.co.uk/history.html\"><em>History of the Forklift Truck</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.theledger.com/news/20151211/winter-haven-man-dies-after-forklift-accident-at-davenport-business\">Winter Haven Man Dies after Forklift Accident at Davenport Business<br></a><a href=\"https://www.omaha.com/news/metro/man-dies-in-valley-forklift-accident/article_1bc0f4c6-488f-11e4-948c-0017a43b2370.html\">Man Dies in Valley Forklift Accident<br></a><a href=\"https://hardhattraining.com/national-forklift-safety-day/\">National Forklift Safety Day</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Telehandler",
    overview: "<div>Our class VII Telehandler safety training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, principles of stability, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following general standards for telehandlers:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSHA 29 CFR 1910.178 – Powered Industrial Trucks</li><li>OSHA 29 CFR 1910.178 APP A</li><li>OSHA 29 CFR 1926.451, 452, 454 – Applicable Scaffolding Standard</li><li>OSHA 29 CFR 1926.602(c)(2)(v) – Scaffold Platforms</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B335-04 – Safety Standard for Lift Trucks</li><li>CAN/CSA-B335-94 – Lift Truck Operator Training</li><li>CAN/CSA-B352.0-09 – ROPS, FOPS</li></ul><div><strong>International<br></strong><br></div><ul><li>ANSI/ITSDF B56.1 – Low Lift and High Lift Trucks</li><li>ANSI/ITSDF B56.6 – Rough Terrain Forklift Lift Trucks</li><li>ANSI/ITSDF B56.6-2005 (5.15, 5.16, 8.24)</li><li>ANSI A92.5-2006 – Boom Supported Elevating Work Platforms</li></ul>",
    facts: "<div><em>Coming!</em></div>",
    fails: "<div><a href=\"https://www.somersetcountygazette.co.uk/news/17373869.two-man-arrested-after-forklift-incident-in-kingsbury-episcopi/\">Two Men Arrested After Forklift Incident in Kingsbury Episcopi<br></a><a href=\"http://press.hse.gov.uk/2014/costain-sentenced-for-parkway-telehandler-death/\">Costain Sentenced for Employee Death in Crane Overturn<br></a><a href=\"https://www.youtube.com/watch?v=AG4KVmLyu7I\">Tip Over Fatality<br></a><a href=\"https://www.youtube.com/watch?v=gM7EjhCHmRE\">Forklift Operator Killed by Steel Beam in Freak Accident<br></a><a href=\"https://hardhattraining.com/telehandler-hazards-top-6-hazards/\">Telehandler Hazards: Top 6 Hazards<br></a><a href=\"https://hardhattraining.com/telehandler-and-scaffolding-hazardspotting/\">Telehandler and Scaffolding – #HazardSpotting</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Sit Down Forklift",
    overview: "<div>Our classes I, IV &amp; V Sit-Down Forklift Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, principles of stability, safe operations, hazards to avoid, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following general standards for sit-down forklifts:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSHA 29 CFR 1910.178 – Powered Industrial Trucks</li><li>OSHA 29 CFR 1910.178 APP A</li><li>OSHA 29 CFR 1926.451, 452, 454 – Applicable Scaffolding Standard</li><li>OSHA 29 CFR 1926.602(c)(2)(v) – Scaffold Platforms</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B335-04 – Safety Standard for Lift Trucks</li><li>CAN/CSA-B335-94 – Lift Truck Operator Training</li><li>CAN/CSA-B352.0-09 – ROPS, FOPS</li></ul><div><strong>International<br></strong><br></div><ul><li>ANSI/ITSDF B56.1 – Low Lift and High Lift Trucks</li><li>ANSI/ITSDF B56.6 – Rough Terrain Forklift Lift Trucks</li><li>ANSI/ITSDF B56.6 -2005 (5.15, 5.16, 8.24)/ANSI A92.5-2006 – Boom Supported Elevating Work Platforms</li></ul>",
    facts: "<ul><li>Each year in the U.S., almost 20,000 workers are seriously injured in accidents associated with forklifts. (Source: <a href=\"https://www.cdc.gov/niosh/docs/2001-109/\"><em>CDC</em></a>)</li><li>Forklift overturns are the most common cause of deaths related to forklifts. (Source: <a href=\"https://www.cdc.gov/niosh/docs/2001-109/\"><em>CDC</em></a>)</li><li>Before modern forklifts were invented, hoists were utilized to lift heavy materials in the late 1800s. (Source: <a href=\"https://web.archive.org/web/20130901014848/http:/www.themhedajournal.org/content/3q04/lifttrucks.php\"><em>Archive.org</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.avonandsomerset.police.uk/news/2019/01/two-arrested-after-man-injured-in-forklift-incident/\">Two Arrested after Man Injured in Forklift Incident<br></a><a href=\"https://www.whio.com/news/local/forklift-injury-reported-moraine-business/XRqbH558Qcqv3snoGZTNIJ/\">Forklift Falls on Man at Moraine Business, Police Say<br></a><a href=\"https://www.buzz.ie/video/warehouse-307643\">Forklift Driver Brings Down Entire Warehouse After Crash<br></a><a href=\"https://www.pahomepage.com/news/man-injured-in-fork-lift-accident/128898347\">Man Injured in Fork Lift Accident<br></a><a href=\"https://hardhattraining.com/forklift-safety-essentials/\">Forklift Safety: 2 Essential Functions<br></a><a href=\"https://hardhattraining.com/forklift-stability-avoid-being-crushed-to-death/\">Forklift Stability: Avoid Being Crushed to Death</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Garbage Truck",
    overview: "<div>Our Garbage Truck Safety Training course is compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomy, stability, safe operating procedures, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for garbage trucks:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSHA 1910.178 – Powered Industrial Trucks&nbsp;</li><li>OSHA 1910.212 – General Requirements for All Machines</li><li>ANSI Z245.1-2017 – Mobile Wastes and Recyclable Materials Collection, Transportation, and Compaction Equipment</li><li>(DOT) CFR 383.91 – Operators of Vehicles Over 26,000 LBS Must Possess a CDL</li><li>(DOT) CFR 396.11 – CDL Operators Must Conduct a Pre-shift Inspection</li><li>CFR 395.8 – CDL Operators Must Keep a Log Book</li><li>CFR 390.21 – CMVs Must Have an Official DOT Number</li></ul><div><strong>Canada<br></strong><br></div><ul><li>Canadian Environmental Protection Act, 1999 – <em>Framework for pollution prevention</em></li><li>Canada Labour Code, Part II – <em>Occupational Health and Safety. Sec. 124-125. “Duties of Employers.”</em></li><li>Canada Labour Code, Part II – <em>Occupational Health and Safety. Sec. 126. “Duties of Employees.”</em></li><li>CSA Standard Z94.2.02 – <em>Hearing Protection Devices (Performance Selection, Care and Use)</em></li><li>Transport Canada – <em>Motor Vehicle Safety Regulations</em></li></ul>",
    facts: "<ul><li>The first self-propelled garbage trucks were originally called steam motor tip-cars. (Source: <a href=\"https://archive.org/stream/TheAutomotorJournal1896ToSeptember1898/automotorjournal1896_djvu.txt\"><em>Archive.org</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"http://kiwaradio.com/lightssirens/sibley-man-taken-to-hospital-after-hitting-garbage-truck/\">Sibley Man Taken to Hospital After Hitting Garbage Truck<br></a><a href=\"https://www.pnj.com/picture-gallery/news/2019/02/02/firefighters-put-out-blaze-garbage-truck-pace-blvd-saturday/2755201002/\">Firefighters Put out Blaze in Garbage Truck on Pace Blvd.<br></a><a href=\"https://www.youtube.com/watch?v=bOnIlBuES2c\">Driver Critically Injured After Garbage Truck Crashes into Prairie Village Home<br></a><a href=\"https://www.youtube.com/watch?v=YMI9DvXpa5I\">FedEx Driver Injured During Rear-End Crash with Garbage Truck<br></a><a href=\"https://hardhattraining.com/garbage-truck-safety/\">Garbage Truck Safety</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Vacuum Truck",
    overview: "<div>Our Vacuum Truck Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on equipment, anatomy, maintenance, inspections, safe operations, stability, common hazards and more.&nbsp;<br><br></div><div>This presentation includes occasional practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, a checklist is also included for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for vacuum trucks:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.1200 – Hazard Communication</li><li>29 CFR 1910.106 – Flammable and Combustible Liquids</li><li>29 CFR 1910.120 – Hazardous Waste Operations and Emergency Response</li><li>29 CFR 1910.307 – Hazardous (classified) Locations</li><li>29 CFR 1910.1000 – Air Contaminants</li><li>29 CFR 1910.146 – Confined Spaces</li><li>CFR 383.91 – Operators of Vehicles Over 26,000 LBS Must Poses a CDL</li><li>CFR 396.11 – Pre-Shift Inspections</li><li>CFR 395.8 – Log Books</li><li>CFR 390.21 – CMVs Must Have an Official DOT Number</li><li>49 CFR 171, 172, 173, 178, 179, 382, 383 and 390-397 – Motor carrier safety requirements for proper hazard classification and manifesting of flammable liquids, approved container design, and periodic testing.&nbsp;</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA Standard Z94.2.02 – Hearing Protection Devices (Performance Selection, Care and Use)&nbsp;</li><li>Canadian Environmental Protection Act, 1999 – Framework for pollution prevention</li><li>OHS: 23.22,16.4, 16.5, 17.2.2 – Must be trained and certified as driver if GODI or oilfield hauler</li><li>WC Act: 115 (2)(e) – Maintain Logbooks</li><li>OHS: 16.34(1-3), 4.9 (1-3) – Must conduct pre-shift inspection</li><li>OHS: 8.2, 8.4, 5.48 PPE – Requirements</li><li>ETS: 4.81, 17.11 (3) – No Smoking in vehicle</li><li>OHS: 4.3(2), 4,5 – Operators manual must be available</li><li>OHS: 3.23 – Driver orientation by employer and worksite orientation</li><li>OSH: 16.33(1) – Seatbelts must be available and worn</li></ul>",
    facts: "<ul><li>Vacuum trucks can displace material at a rate of over 6000 cubic feet per minute. (Source: <a href=\"https://vac-con.com/industrial-vacuum-trucks/\"><em>Vac-Con</em></a>)</li><li>When properly calibrated, vacuum trucks can be used to locate underground utilities. (Source: <a href=\"https://www.osha.gov/laws-regs/standardinterpretations/2003-10-23\"><em>OSHA</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.indianagazette.com/news/clymer-firefighter-commended-for-saving-man-s-life/article_6874a0d2-cc94-11e8-af5d-47c9a41b0505.html\">Vacuum Truck Used to Save Man<br></a><a href=\"https://abc7ny.com/body-of-worker-recovered-after-brooklyn-wall-collapse/4224181/\">Accident Victim’s Body Recovered Using Vacuum Truck<br></a><a href=\"https://youtu.be/73h_vhXqOSE\">Vacuum Truck Explodes</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Ice Resurfacer",
    overview: "<div>Our Ice Resurfacer safety training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomical components, safe operations, common hazards to avoid, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for ice resurfacers:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910 Subpart Z – Air Contaminants</li><li>29 CFR 1910.38 – Emergency Action Plans</li><li>29 CFR 1910.94 – Ventilation</li><li>29 CFR 1910.95 – Occupational Noise Exposure</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA Standard z94.2.02 - Hearing Protection Devices</li><li>The Occupational Health and Safety Regulations, 1996</li></ul>",
    facts: "<ul><li>Frank J. Zamboni invented the first ice resurfacer. (Source: <a href=\"https://web.archive.org/web/20120604234216/https:/www.niaf.org/milestones/year_1920.asp\"><em>Archive.org</em></a>)</li><li>The average ice resurfacer covers nearly 2,000 miles each year. (Source: <a href=\"https://zamboni.com/about/fun-facts/\"><em>Zamboni.com</em></a>)</li><li>Between 12,000 and 15,000 gallons are used to create the typical hockey ice rink. (Source: <a href=\"https://entertainment.howstuffworks.com/ice-rink4.htm\"><em>Howstuffworks.com</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://ftw.usatoday.com/2014/12/81-people-hospitalized-after-leaks-carbon-monoxide-in-hockey-rink\">81 People Hospitalized After Ice Resurfacer Leaks Carbon Monoxide in Hockey Rink<br></a><a href=\"https://theislandnow.com/news-98/ice-resurfacer-catches-fire-at-port-washington-skating-center/\">Ice Resurfacer Catches Fire at Port Washington Skating Center<br></a><a href=\"https://www.youtube.com/watch?v=ANvI_2hCrwY\">Zamboni Fire on the Ice at Skating Rink, Burns Then Explodes</a>&nbsp;</div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Snowcat",
    overview: "<div>Our Snowcat safety training course is OSHA compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomical components, principles of stability, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training will encompass OSHA standards for snowcats.</div>",
    facts: "<div><em>Coming!</em></div>",
    fails: "<div><a href=\"https://www.sierrasun.com/news/opinion/law-review-snowboarder-runs-into-snowcat/\">Law Review: Snowboarder Runs into Snowcat<br></a><a href=\"https://www.idahostatesman.com/news/local/article202744704.html\">2 people Injured at Brundage Mountain in Snowcat Trip Incident<br></a><a href=\"https://hardhattraining.com/avalanche-safety/\">Avalanche Safety<br></a><a href=\"https://hardhattraining.com/winter-preparedness/\">Winter Preparedness</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Landscaping",
    overview: "<div>Our Landscaping safety training course is OSHA compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy of tools, ensuring stability, operations, common hazards, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards for landscaping:<br><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.136 – Foot Protection</li><li>29 CFR 1926.138 – Hand Protection</li><li>29 CFR 1926 Subpart W – Rollover Protective Structures</li><li>29 CFR 1926.20 – General Safety and Health Provisions, training</li><li>29 CFR 1926.21 – Training and Education</li><li>OSHA Act of 1970, 5(a)(1) – “Each employer shall furnish to each of his employees…a place of employment which is free from recognized hazards that are causing or are likely to cause death or serious physical harm to his employees.”</li></ul>",
    facts: "<ul><li>A fifth of an acre is the average lawn size for residential areas. (Source: <a href=\"https://www.tractorsupply.com/know-how_outdoors_outdoor-living_fun-facts-about-lawn-care\"><em>Tractor Supply Company</em></a>)</li><li>Lawns produce three times the oxygen that trees produce. (Source: <a href=\"https://www.tractorsupply.com/know-how_outdoors_outdoor-living_fun-facts-about-lawn-care\"><em>Tractor Supply Company</em></a>)</li><li>The landscaping industry was the highest killer in 2014. (Source: <a href=\"https://bes-corp.com/osha-landscaping-industry-was-the-1-killer-in-2014/\"><em>Bes-Corp</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.capecodtoday.com/article/2018/11/16/243349-Yarmouth-Police-Man-Seriously-Injured-Landscaping-Accident\">Yarmouth Police: Man Seriously Injured in Landscaping Accident<br></a><a href=\"https://magicvalley.com/community/mini-cassia/news/burley-man-killed-another-injured-in-tree-trimming-accident/article_fafabf62-37d9-5717-bb31-57cd7d7244e9.html\">Burley Man Killed, Another Injured in Tree Trimming Accident<br></a><a href=\"https://www.youtube.com/watch?v=hSEpfJwIDIE\">Landscaping Truck Accident<br></a><a href=\"https://www.youtube.com/watch?v=7TEJDr9KW0A\">Landscaping Accident Victim Identified<br></a><a href=\"https://hardhattraining.com/tree-trimming-accidents/\">Tree Trimming Accidents</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Drug & Alcohol",
    overview: "<div>Our Drug and Alcohol Awareness training course is built to regulation standards. This class discusses topics including general knowledge, prevention, signs and warnings, treatment, recourse, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for Drug and Alcohol Awareness:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSHA Act of 1970, 5(a)(1) – General Duty Clause&nbsp;</li><li>Drug Free Workplace Act of 1988</li></ul><div><strong>Canada<br></strong><br></div><ul><li>Canada Labour Code, Part II, IPG-080 – Substance use in the work place</li><li>Part II of the Canada Labour Code – Canada Labour Code</li></ul>",
    facts: "<ul><li>Accidental overdoses from drugs and alcohol at work increased 25% between 2016 and 2017. (Source: <a href=\"https://www.bls.gov/news.release/pdf/cfoi.pdf\"><em>Bureau of Labor Statistics</em></a>)</li><li>Mining industry workers have the highest rates of heavy alcohol use, with construction workers having the second highest rates. (Source: <a href=\"https://www.samhsa.gov/data/sites/default/files/report_1959/ShortReport-1959.html\"><em>SAMHSA</em></a>)</li><li>Over $740 billion are lost annually from the abuse of tobacco, alcohol, and illegal drugs in the form of health care costs, reduce work productivity, and crime. (Source: <a href=\"https://www.drugabuse.gov/related-topics/trends-statistics\"><em>Drugabuse.gov</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.syracuse.com/news/2019/01/upstate-medical-in-syracuse-developing-heroin-vaccine.html\">Heroin Vaccine Undergoing Testing in Syracuse<br></a><a href=\"https://abc3340.com/news/local/drug-overdose-death-rates-increased-drastically-a-local-expert-say-its-alarming\">Drug Overdose Rates Among Women are Steadily Increasing<br></a><a href=\"https://www.cnbc.com/2015/12/15/80-percent-of-workplaces-face-this-drug-scourge.html\">Opioid Epidemic Affects America's Workforce<br></a><a href=\"https://www.orlandosentinel.com/business/consumer/os-bz-alcoholism-drug-abuse-restaurant-industry-20180926-story.html\">Alcohol and Drug Abuse Prevalent Among Restaurant Workers<br></a><a href=\"https://hardhattraining.com/drug-alcohol-free-workplace/\">Drug and Alcohol Free-Workplace</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Street Sweeper",
    overview: "<div>Our Street Sweeper Safety Training course is regulations compliant, and our online version fulfills classroom training requirement. Each class contains sections on anatomical components, principles of stability, safe operations, common hazards, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for street sweepers:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>CFR 383.91 – Operators of Vehicles Over 26,000 LBS Must Possess a CDL</li><li>CFR 396.11 – CDL Operators Must Conduct a Pre-Shift Inspection</li><li>CFR 395.8 – CDL Operators Must Keep a Log Book</li><li>CFR 390.21 – CMVs Must Have an Official DOT Number&nbsp; &nbsp;</li></ul><div><strong>Canada<br></strong><br></div><ul><li>Canadian Environmental Protection Act, 1999 – Framework for pollution prevention</li><li>CSA Standard Z94.2.02 – Hearing Protection Devices (Performance Selection, Care and Use) Slow moving vehicle regulations (SMV)</li></ul>",
    facts: "<ul><li>Joseph Whitworth invented the first street cleaner in 1843. (Source: <a href=\"http://www.vacuumcleanerhistory.com/vacuum-cleaner-development/history-of-street-sweeper/\"><em>Vacuumcleanerhistory.com</em></a>)</li><li>Some hybrid-electric street sweepers have been designed to use natural gas, which is more fuel efficient than regular street sweepers. (Source: <a href=\"https://www.sciencedaily.com/releases/2015/06/150605081607.htm\"><em>ScienceDaily</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://losangeles.cbslocal.com/2018/12/13/la-habra-street-sweeper-fatality/\">1 Killed in Collision with Street Sweeper In La Habra<br></a><a href=\"https://fox13now.com/2018/08/16/man-killed-in-single-vehicle-crash-on-north-powder-mountain-road/\">Police Identify Man Killed in Street Sweeper Crash in Weber County<br></a><a href=\"https://www.cbsnews.com/news/street-sweeper-killed-tragic-accident-jacksonville-florida/\">\"It's Horrific\": Street Sweeper Killed in Tragic Freak Accident in Florida<br></a><a href=\"https://www.youtube.com/watch?v=7iEm0GcTxQA\">Street Sweeper Crashes into Traffic Pole in Glendale<br></a><a href=\"https://hardhattraining.com/top-5-causes-street-sweeper-accidents/\">Street Sweeper Accidents: Top 5 Causes</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Bloodborne Pathogens",
    overview: "<div>Our Bloodborne Pathogens Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on transmission, precautions, post-exposure, case studies, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for bloodborne pathogens:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.1030 – Bloodborne Pathogens</li><li>29 CFR 1910.151 – Medical Services and First Aid</li><li>29 CFR 1915.1030 – Toxic, Hazardous Substances, Exposure</li><li>29 CFR 1926.25 – Disposal of Sharps, Hazardous Waste</li><li>29 CFR 1926.20 – General Safety and Health Provisions, Training</li><li>29 CFR 1926.21(b)2 – Training, Education, Hazard Recognition</li><li>OSH Act of 1970, 5(a)(1) – General Clause&nbsp;</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CGSB/CSA-Z1610-11 – Protection of first responders from CBRN events (chemical, biological, radiological, nuclear)</li><li>B.C. - OHS Regulation Part 5, Chemical &amp; Biological Agents; &amp; Part 6, Substance Specific Agents (G6.34-1 – G6.10; Biological Agents</li><li>Ontario – OHS Act, Needle Safety</li><li>Alberta – OHS Code Part 4 , Chemical &amp; Biological Hazards, and Harmful Substances, General Requirements, &amp; Part 11 – First Aid</li><li>Manitoba –Safety &amp; Health Act, Needles in Medical Workplaces</li><li>Nova Scotia – Safe Needles in Healthcare Workplaces Act</li><li>Saskatchewan – OHS Act, Needle Safe Devices &amp; Improved Exposure Control Plans</li></ul>",
    facts: "<ul><li>About 99.7% of exposures to HIV-infected blood from needlesticks or cuts do not lead to infection.</li><li>The risk of infection from a needle or cut exposure to hepatitis B infected blood is 6% - 30% (depending on the antigen status of the infection source).</li><li>The chances of getting infected when exposed to hepatitis C infected blood is about 1.8%. (Source: <a href=\"https://www.cdc.gov/oralhealth/infectioncontrol/bloodborne_exposures.htm\"><em>CDC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"http://www.baltimoresun.com/business/bs-bz-osha-fines-post-office-20161201-story.html\">Blood Incidents Prompt OSHA to Fine Brooklyn Postal Service Location $342,000<br></a><a href=\"http://www.courant.com/news/connecticut/hc-health-clinic-infection-violations-0723-20160722-story.html\">OSHA Fines Hartford Medical Clinic $24,750 For Mishandling Bloodborne Pathogens<br></a><a href=\"https://youtu.be/LTvYAsAms1w\">Scientist Gets Zika After Accidentally Pricking Herself with Infected Needle<br></a><a href=\"https://youtu.be/5mMY0TSizac\">Hepatitis C Now Kills More Americans Than Any Other Infectious Disease<br></a><a href=\"https://hardhattraining.com/needlestick-danger/\">Needlestick Danger<br></a><a href=\"https://hardhattraining.com/bloodborne-pathogens-locked/\">Bloodborne Pathogens Locked Up</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Heat & Cold Stress",
    overview: "<div>Our Heat and Cold Stress training course is regulation compliant. This class discusses topics including Heat Illness, Cold Illness, Case Studies and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local Section 5(a)(1) General Duty Clause, this training encompasses the following standards:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>29 CFR 1910.132(d): PPE</li><li>29 CFR 1915.152(b): Hazard assessment and equipment</li><li>29 CFR 1917.95(a): Protective clothing</li><li>29 CFR 1910.151: Medical and First Aid</li></ul><div><strong>Canada Standards<br></strong><br></div><div>(No Canada standards at this time)</div>",
    facts: "<ul><li>There is risk of dehydration in both warm and cold weather.</li><li>Alcohol and caffeine can negatively change how you react to both heat and cold.</li><li>Hypothermia occurs most often during spring and fall.</li></ul><div><br></div>",
    fails: "<div><a href=\"http://www.newstribune.com/news/local/story/2016/nov/10/jefferson-city-roofing-company-cited-heat-death-worke/648478/\">Jefferson City Roofing Cited for Employee Death<br></a><a href=\"https://www.foxnews.com/food-drink/new-jersey-ice-cream-worker-sues-after-contracting-frost-bite\">New Jersey Ice Cream Worker Sues After Contracting Frostbite<br></a><a href=\"https://www.youtube.com/watch?v=Ki_cfPJFUSk\">Firefighter Collapses in Norfolk<br></a><a href=\"https://www.youtube.com/watch?v=N39fNxwyr1Y\">Man Dies from Hypothermia<br></a><a href=\"https://hardhattraining.com/hot-and-cold-stressors/\">Heat and Cold Stressors<br></a><a href=\"https://hardhattraining.com/heat-injury/\">Heat Injury</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Ergonomics",
    overview: "<div>Our Ergonomic training course is built to regulation standards. This class discusses topics including common ergonomic definitions and methods to protect the head and neck, shoulders and arms, back, legs and feet, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div><strong>U.S.<br></strong><br></div><div>OSHA no longer has a specific standard for ergonomics training. However, under the General Duty Clause, Section 5(a)(1) of the Occupational Safety and Health Act of 1970, employers are required to provide a workplace that “is free from recognizable hazards that are causing or likely to cause death or serious harm to employees.”<br><br></div><div>Because of this requirement, <strong>e</strong>mployers have a legal and ethical obligation to develop and maintain workplace procedures and duties that are ergonomically safe for workers to perform. Employees have the right to work in an atmosphere that promotes the safety and well-being of all.<br><br></div><div><strong>Canada<br></strong><br></div><div>Unlike the U.S., Canada still has specific standards for ergonomic safety within the workplace. Part II of the Canada Labour Code specifically requires employers to ensure “that the workplace, workspaces, and procedures meet prescribed ergonomic standards.”&nbsp;<br><br></div><div>The code goes on to include “employers are also responsible to ensure that machinery, equipment and tools used by workers in the course of their employment meet prescribed health, safety and ergonomic standards.”<br><br></div><div>Because of these requirements, employers must ensure that employees know and follow any safe working practices that are intended to keep them ergonomically safe.</div>",
    facts: "<ul><li>Tendon disorders such as carpal tunnel syndrome and tendinitis are some of the most common ergonomic-related injuries that occur in the workplace.&nbsp;</li><li>Repeatedly working with your hands above your head can cause rotator cuff tendinitis.&nbsp;</li><li>Vibration that affects the whole body, such as when driving trucks or buses, can lead to low back pain, shooting pain or numbness in the upper legs, and even back disability. (Source: <a href=\"https://www.osha.gov/Publications/osha3125.pdf\"><em>OSHA</em></a><em>)</em></li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.seattletimes.com/explore/careers/yep-typing-can-hurt-you-why-you-shouldnt-ignore-the-discomfort/\">Typing Can Hurt You. Why You Shouldn’t Ignore the Discomfort<br></a><a href=\"https://www.nytimes.com/2019/01/30/smarter-living/how-to-office-more-ergonomic-tips.html\">How to Make Your Office More Ergonomically Correct<br></a><a href=\"https://hardhattraining.com/17163-2/\">Potential Ergonomics Hazards<br></a><a href=\"https://hardhattraining.com/ergonomics-workplace/\">Ergonomics in the Workplace</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Opioid Awareness",
    overview: "<div>Our opioid awareness safety training course is perfect for those who wish to understand the dangers of Opioids, and how to overcome addiction, or avoid it entirely. This course covers some general knowledge, how to understand and recognize addictions, how to address it in the workplace, and ways that you can treat and prevent opioid addiction.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for a final written exam included with the course. In addition to the written exam, this course also includes a practical exam to review basic near awareness knowledge.</div>",
    training_standards: "<ul><li>No standards at this time.</li></ul><div><br></div>",
    facts: "<ul><li>In 2017 alone, more than 47,000 Americans died from opioid overdose, accounting for 68% of all overdose-caused deaths that year.</li><li>Workplace overdose deaths have been increasing by 25% or more each year.</li><li>About 80% of heroin users got started on prescription opioids.</li></ul>",
    fails: "<div><a href=\"https://khn.org/news/workers-overdose-on-the-job-and-employers-struggle-to-respond/\">Workers Overdose on Jobsites and Employers Struggle to Respond<br></a><a href=\"https://www.washingtonpost.com/local/virginia-news/alexandria-warns-of-opioid-danger-after-four-overdose-and-two-die/2019/01/21/b2589e62-1db9-11e9-9145-3f74070bbdb9_story.html?noredirect=on\">Alexandria Warns of Opioid Danger After Four Die<br></a><a href=\"https://www.youtube.com/watch?v=jOeKuoyRs3s\">Ottawa Family Shares Story of Teen's Overdose Death<br></a><a href=\"https://www.youtube.com/watch?v=WwKSXx5eEmU\">Recovering Addict's Son Dies of Heroin Overdose</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Mental Health",
    overview: "<div>Our Mental Health training course is built to regulation standards. This class discusses topics including stressors, risk factors, signs and symptoms, suicide, myths, mental wellness, treatment, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>OSHA doesn’t have a specific standard for mental health training. However, under the General Duty Clause, Section 5(a)(1) of the Occupational Safety and Health Act of 1970, employers are required to provide a workplace that “is free from recognizable hazards that are causing or likely to cause death or serious harm to employees.”<br><br></div><div>Canada also does not have a specific standards regarding mental health. However, the Employment Equity Act and the Canadian Human Rights Act allow employees to not be discriminated against and have an equal chance to work, no matter their race, gender, age, or disability.<br><br></div><div>Because of these requirement, employers have a legal and ethical obligation to develop and maintain a workplace that is free from hazards associated with mental health. Employees have the right to work in an atmosphere that promotes the safety and well-being of all.</div>",
    facts: "<ul><li>About a quarter of homeless people in a shelter lives with a serious mental health illness.&nbsp;</li><li>African and Hispanic Americans use only half of what Caucasian Americans use for mental health services.</li><li>2.4 million people in the U.S. live with schizophrenia. (Source: <a href=\"https://www.nami.org/nami/media/nami-media/infographics/generalmhfacts.pdf\"><em>NAMI</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.wired.com/story/screens-might-be-as-bad-for-mental-health-as-potatoes/\">Screens Might Be as Bad for Mental Health as…Potatoes<br></a><a href=\"https://www.whig.com/20190114/jail-law-enforcement-seek-ways-to-address-mental-health\">Law Enforcement Seeks Ways to Address Mental Health<br></a><a href=\"https://www.youtube.com/watch?v=wJjwAMr_BEE\">David Cameron: We Need to End Stigma of Mental Health<br></a><a href=\"https://www.youtube.com/watch?v=eefxE-O99bI\">20% of America’s Youth Suffer from a Mental, Emotional, or Behavioral Condition</a> <br><a href=\"https://hardhattraining.com/17598-2/\">Ways to Improve Mental Health<br></a><a href=\"https://hardhattraining.com/18218-2/\">Managing Mental and Emotional Distress Through Postvention Methods</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Workplace Civility",
    overview: "<div>Our Civility in the Workplace training course discusses topics including respect, harassment, bullying, workplace culture, recourse, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following laws and regulations:<br><br></div><div>U.S.A.:<br><br></div><ul><li>Civil Rights Act, Title VII SEC. 2000e-2.703&nbsp;</li><li>Section 5(a)(1) General Duty Clause</li><li>Americans with Disabilities Act&nbsp;</li><li>OSHA Act of 1970 Section 11(c)&nbsp;</li><li>Title 29 Parts 1604-1606&nbsp;</li><li>The Age Discrimination in Employment Act of 1967&nbsp;</li></ul><div>CANADA<br><br></div><ul><li>Canadian Human Rights Act Part 3(1)</li><li>Canadian Human Rights Act Part 14</li><li>Canadian Multiculturalism Act</li></ul>",
    facts: "<ul><li>45% of all harassment complaints to the EEOC allege harassment on the basis of gender.</li><li>The EEOC recovers an estimated $164.5 million each year for harassment charges.</li><li>About 300 workplace suicides occur every year. (<em>Source</em><a href=\"https://www.bls.gov/opub/mlr/2016/article/pdf/suicide-in-the-workplace.pdf\"><em>: Bureau of Labor and Statistics</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"http://www.sandiegouniontribune.com/news/columnists/diane-bell/sd-me-bell-20181006-story.html\">Column: Civility has been thrown under the bus<br></a><a href=\"https://youtu.be/P6PsgAqES2s\">Workplace Bullying: The Silent Epidemic<br></a><a href=\"https://youtu.be/VaLmrtCb3yM\">Workplace Bullying<br></a><a href=\"https://hardhattraining.com/sexual-harassment-workplaceculture/\">The Importance of Workplace Culture</a>&nbsp;</div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Workplace Violence",
    overview: "<div>Our Violence in the Workplace training course is built to regulation standards. This class discusses topics including predicting violence, prevention, response, forms of violence, active shooter scenarios, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>OSHA doesn’t have a specific standard for violence in the workplace. However, under the General Duty Clause, Section 5(a)(1) of the Occupational Safety and Health Act of 1970, employers are required to provide a workplace that “is free from recognizable hazards that are causing or likely to cause death or serious harm to employees.”&nbsp;<br><br></div><div>Likewise, Part XX of the Canada Occupational Health and Safety Regulations – Violence Prevention in the Work Place gives steps employers must implement in order to protect their employees against violence in the workplace, and ensure they have recourse if they are subjected to violence. Employers who do not take steps to prevent or abate a recognized violence hazard in the workplace can be cited.<br><br></div><div>Because of these requirements, employers have a legal and ethical obligation to develop and maintain a workplace that is free from hazards associated with violence in the workplace. Employees have the right to work in an atmosphere that promotes the safety and well-being of all.</div>",
    facts: "<ul><li>In 2014, over 400 people were fatally injured because of work violence.&nbsp;</li><li>Workplace violence comes in many forms, including criminals, customers, co-workers, and personal relationships.</li><li>Not surprisingly, the deadliest form of workplace violence comes in the form of active shooters. (Source: <a href=\"https://www.nsc.org/work-safety/safety-topics/workplace-violence\"><em>NSC</em></a>)&nbsp;</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.safetyandhealthmagazine.com/articles/17778-lawmaker-seeks-osha-standard-on-preventing-workplace-violence-in-health-care-social-services\">Lawmaker Seeks OSHA Standard on Preventing Workplace Violence in Health Care<br></a><a href=\"https://www.ehstoday.com/safety/chef-massachusetts-latest-victim-workplace-violence\">Chef in Massachusetts Latest Victim of Workplace Violence<br></a><a href=\"https://www.youtube.com/watch?v=cNNUMuxAV1A\">Workplace Violence: Coworker Confrontations</a> <br><a href=\"https://www.youtube.com/watch?v=WtmpuTZ3MKs\">Workplace Violence<br></a><a href=\"https://hardhattraining.com/violence-in-the-workplace/\">Violence in the Workplace</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Workplace Stress",
    overview: "<div>Our Stress Management training course is built to OSHA standards. This class discusses topics including common causes, signs and symptoms, stress management methods, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course.</div>",
    training_standards: "<div>OSHA doesn’t have a specific standard for stress management training. However, under the General Duty Clause, Section 5(a)(1) of the Occupational Safety and Health Act of 1970, employers are required to provide a workplace that “is free from recognizable hazards that are causing or likely to cause death or serious harm to employees.”<br><br></div><div>Because of this requirement, employers have a legal and ethical obligation to develop and maintain a workplace that is free from hazards associated with excessive amounts of stress. Employees have the right to work in an atmosphere that promotes the safety and well-being of all.</div>",
    facts: "<ul><li>Chronic stress can affect the function of the immune, digestive, sleep, and reproductive systems. (Source: <a href=\"https://www.nimh.nih.gov/health/publications/stress/index.shtml\"><em>NIH</em></a>)</li><li>People who experience chronic stress are more likely to have viral infections on a regular basis, such as the common cold or the flu. (Source: <a href=\"https://www.nimh.nih.gov/health/publications/stress/index.shtml\"><em>NIH</em></a>)</li><li>Nigeria has been ranked the most stressed-out country in the world. (Source: <a href=\"https://www.bloomberg.com/graphics/best-and-worst/#most-stressed-out-countries\"><em>Bloomberg</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.usnews.com/news/health-news/articles/2019-02-11/how-color-can-help-you-de-stress\">How Color Can Help You De-Stress<br></a><a href=\"https://nypost.com/2019/01/30/work-stress-is-costing-small-business-owners-thousands/\">Work Stress is Costing Small Business Owners Thousands<br></a><a href=\"https://www.cnbc.com/2018/09/10/3-strategies-to-cope-with-the-work-stress-thats-wearing-you-down.html\">3 Strategies to Cope with the Work Stress that's Wearing You Down<br></a><a href=\"https://globalnews.ca/video/4085254/workplace-stress-is-everywhere-how-do-you-recognize-it-and-how-do-you-deal-with-workplace-stress\">Workplace Stress is Everywhere. <br></a><a href=\"https://hardhattraining.com/school-stress-management/\">Stress Management for School</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Carbon Monoxide",
    overview: "<div>Our Carbon Monoxide Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on exposure, monitoring, safe practices, PPE, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for working with carbon monoxide:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>1910 Subpart Z – Toxic and Hazardous Substances</li><li>1910.1000 TABLE Z-1 – TABLE Z-1</li><li>1910.134 – Respiratory Protection</li><li>1910.146 – Permit-required confined spaces</li><li>1910.146 App B – Procedures for Atmospheric Testing</li></ul><div><strong>Canada<br></strong><br></div><ul><li>B.C. – Workplace B.C.&nbsp;</li><li>Manitoba – Safe Work Manitoba</li><li>New Brunswick – Occupational Heath and Safety Act &nbsp;</li><li>Ontario – Ministry of Labor Ontario&nbsp;</li><li>Quebec – Legis Quebec Ch. 2.1, r. 13</li><li>Alberta – Alberta Labour OHS Regulations</li></ul>",
    facts: "<ul><li>From 1999-2010, nearly 5,000 people died in the United States from carbon monoxide poisoning.&nbsp;</li><li>More males than females die each year from CO poisoning.</li><li>The death rates for carbon monoxide poisoning are highest from ages 25-64. (Source<em>: </em><a href=\"https://www.cdc.gov/mmwr/preview/mmwrhtml/mm6303a6.htm\"><em>CDC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://minnesota.cbslocal.com/2018/07/10/minneapolis-building-evacuated-carbon-monoxide-leak/\">Minneapolis Building Evacuated Due to Carbon Monoxide Leak<br></a><a href=\"https://globenewswire.com/news-release/2018/07/11/1536100/0/en/ADT-Helps-Save-Missouri-Family-From-Potentially-Deadly-Levels-of-Carbon-Monoxide-in-Their-Home.html\">ADT Helps Save Missouri Family from Potentially Deadly Levels of Carbon Monoxide in Their Home<br></a><a href=\"https://youtu.be/iXNWGIzCMQI\">2 Dead in Apparent Carbon Monoxide Poisoning in Newark<br></a><a href=\"https://youtu.be/c9DyhIRNlSg\">Attempt to Keep Family Warm Led to carbon Monoxide Deaths<br></a><a href=\"https://hardhattraining.com/carbon-monoxide-silent-killer/\">Carbon Monoxide: The Silent Killer<br></a><a href=\"https://hardhattraining.com/safety-precautions-regarding-carbon-monoxide-poisoning/\">Safety Precautions Regarding Carbon Monoxide Poisoning</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Hazard Communications (HazCom)",
    overview: "<div>Our Hazard Communication safety training course is standards compliant, and our online version fulfills classroom training requirement. Each class contains sections on safety data sheets, labels, operations, case studies, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for Hazard Communication:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.1200, Toxic and Hazardous Substances, HazCom for General Industry, Agriculture</li><li>29 CFR 1926.59, HazCom for Construction</li><li>29 CFR 1915.1200, HazCom for Shipyards</li><li>29 CFR 1917.1(a)(2)(vi), for Marine Terminals</li><li>29 CFR 1918.1(b)(4), for Longshoring</li><li>29 CFR 1926.20-21, on General Training, Education</li></ul><div><strong>Canada<br></strong><br></div><ul><li>WHIMIS 2.0 (2015)</li><li>B.C. - Workplace Hazardous Materials Information System (WHMIS)</li><li>Ontario – WHMIS Regulation</li><li>Alberta – Alberta OHS Regulation&nbsp;</li><li>Manitoba –The Workplace Safety and Health Regulation</li><li>Nova Scotia – Safe Needles in Healthcare Workplaces Act</li><li>Saskatchewan – The Occupational Health and Safety (Workplace Hazardous Materials Information System) Regulations</li><li>Canada- Hazardous Materials Information Review Regulations</li></ul>",
    facts: "<div>There are over 650,000 hazardous chemical products coming from over 3 million different jobs. (<em>source:</em> <a href=\"https://www.osha.gov/dsg/hazcom/finalmsdsreport.html\">OSHA</a>)</div>",
    fails: "<div><a href=\"https://www.oregonlive.com/kiddo/2009/03/kids_accidentally_drink_windsh.html\">Kids accidentally drink windshield wiper fluid at Arkansas daycare<br></a><a href=\"https://www.cbsnews.com/news/arsenic-death-a-tragic-accident/\">Arsenic Death A 'Tragic Accident'<br></a><a href=\"https://www.youtube.com/watch?v=h049Hgfk-BI\">Karen Wetterhahn's Story - Accidental Poisoning at Dartmouth<br></a><a href=\"https://hardhattraining.com/hazardous-gases-and-using-hazcom-methods-to-protect-its-users/\">Hazardous Gases and Using HazCom Methods to Protect its Users<br></a><a href=\"https://hardhattraining.com/osha-hazcom-chemicals/\">HazCom Violation: Painted Lungs!</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Bear Awareness",
    overview: "<div>Our Bear Awareness Safety Training course is perfect for those who work, live, or explore areas where bears live. This course covers bear behavior, what to do in a confrontation, basic defense techniques, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for a final written exam included with the course. In addition to the written exam, this course also includes a practical exam to review basic near awareness knowledge.<br><br>Bear populations are rebounding because of various conservation efforts. As a result, bear territories are expanding, and the chances of encountering a bear in the wild are increasing. The chances of a bear killing you are low. However, the chances of the bear being euthanized after too many human encounters is much higher. As most bear attacks are preventable, it is important to know what steps you should take to avoid an encounter, as well as what you should do to increase the likeliness of both you and the bear surviving.</div>",
    training_standards: "<ul><li>None.</li></ul>",
    facts: "<ul><li>North America is home to three species of bears: the black bear, the brown bear, and the polar bear.</li><li>Bear spray is 92% effective in preventing a bear attack.</li><li>In 2017, about 168 bears were euthanized and another 107 were relocated in Colorado. (Source: <a href=\"https://news.nationalgeographic.com/2015/09/150916-bears-attacks-animals-science-north-america-grizzlies/\"><em>National Geographic</em></a>; <a href=\"https://www.westword.com/news/168-bears-killed-in-colorado-in-2017-9675289\"><em>Westword</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.westword.com/news/168-bears-killed-in-colorado-in-2017-9675289\">Why at Least 168 Bears Have Been Euthanized in Colorado This Year<br></a><a href=\"https://www.newsweek.com/alaska-grizzly-bear-attack-mine-cubs-killed-1149214\">Alaska Grizzly Bear Attack: Troopers Identify victim Killed Near Mine<br></a><a href=\"https://youtu.be/E1zUzID12ME\">Man Thankful to Be Alive After Grizzly Bear Attack in B.C.<br></a><a href=\"https://youtu.be/JiKr3rT0Jww\">Brutal Bear Attack in Florida<br></a><a href=\"https://hardhattraining.com/bear-encounter-prevention/\">Bear Encounter in the Wild: Safety and Prevention<br></a><a href=\"https://hardhattraining.com/be-bear-aware/\">Be Aware of the Bear</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Mold",
    overview: "<div>Our Mold training course is perfect for people who work in conditions that may produce mold. This class discusses topics including exposure &amp; disease, prevention, remediation, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>OSHA doesn’t have a specific standard for mold training. However, under the General Duty Clause, Section 5(a)(1) of the Occupational Safety and Health Act of 1970, employers are required to provide a workplace that “is free from recognizable hazards that are causing or likely to cause death or serious harm to employees.”<br><br></div><div>Canada’s government also does not have any federal standards or recommendations for working with mold. However, each province and territory has their own occupational health and safety acts that require safe work places for employees.<br><br></div><div>Because of this requirement, employers have a legal and ethical obligation to develop and maintain a workplace that is free from hazards associated with mold. Employees have the right to work in an atmosphere that promotes the safety and well-being of all.</div>",
    facts: "<ul><li>Of the 21.8 million cases of asthma reported in the U.S. in 2007, about 4.6 million were attributed to mold exposure within the home. (Source: <a href=\"https://www.truthaboutmold.info/statistics\">truthaboutmold.info</a>)</li><li>King Tutankhamen’s tomb killed some thirty-seven archeologists within a fifty-year period. Because so many people died, many assumed that King Tut’s tomb was cursed. It was later revealed that archeologists who wore respirators survived and were thus not exposed to the mold within. (Source: <a href=\"https://www.nationalgeographic.com/science/2005/05/news-tutankhamun-carnarvon-mold-bacteria-toxins/\">Nationalgeographic.com</a>)</li><li>Mold is made up of over 10,000 species and can exist nearly everywhere. (Source: <a href=\"https://www.osha.gov/dts/shib/shib101003.html\">osha.gov</a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.wspa.com/news/mold-clean-up-to-start-at-upstate-jail/1695067457\">Mold Clean-up to Start at Cherokee Co. Jail<br></a><a href=\"http://www.wuwm.com/post/break-mold-gives-5-tips-combatting-mold-related-illness#stream/0\">“Break the Mold” Gives 5 Tips On Combatting Mold-Related Illness<br></a><a href=\"https://www.youtube.com/watch?v=WGYDx8LwpRg\">What Happens If You Eat Mold?<br></a><a href=\"https://www.youtube.com/watch?v=tHqUfwJQiNs\">Signs and Symptoms of Mold Exposure<br></a><a href=\"https://hardhattraining.com/black-mold-a-fickle-fungus/\">Black Mold: A Fickle Fungus</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Lab Safety",
    overview: "<div>Our Lab Safety training course is built to OSHA’s standards. This class discusses topics including hazard identification, monitoring, hygiene plan, knowing your lab, safe practices, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards for lab safety:<br><br></div><ul><li>29 CFR 1910.1450 – Exposure to Hazardous Chemicals in Labs</li><li>29 CFR 1910.1200 – Hazcom</li><li>29 CFR 1910.1030 – Bloodborne Pathogen</li><li>29 CFR 1910.132 – PPE</li><li>29 CFR 1910.133 – Eye and Face Protection</li><li>29 CFR 1910.134 – Respiratory Protection</li><li>29 CFR 1910.147 – Control of Hazardous Energy</li><li>29 CFR 1910.7 – Definition and Requirements of Nationally Recognized Lab</li><li>29 CFR 1910.151 (c) – Eye Wash Stations</li><li>OSH Act of 1970</li><li>29 CFR 1926.21 – Training</li></ul>",
    facts: "<ul><li>Water expands nearly 10% more than its volume when frozen. (Source: <a href=\"https://www.thoughtco.com/fun-and-interesting-chemistry-facts-604321\"><em>Thought Co.</em></a>)</li><li>Bee stings are acidic while wasp stings are alkaline. (Source: <a href=\"https://www.thoughtco.com/fun-and-interesting-chemistry-facts-604321\"><em>Thought Co.</em></a>)</li><li>Over half a million people are employed in laboratories. (Source: <a href=\"https://www.osha.gov/SLTC/laboratories/\"><em>OSHA</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.osha.gov/news/newsreleases/region1/09012016\">OSHA Cites Connecticut Diagnostic Laboratory for Inadequately Protecting its Employees<br></a><a href=\"https://www.the-scientist.com/news-opinion/gas-cylinder-explosion-kills-researcher-at-indian-laboratory-65190\">Gas Cylinder Explosion Kills Researcher at Indian Laboratory<br></a><a href=\"https://www.youtube.com/watch?v=ZZsuiaz0zkQ\">Biological Lab Safety<br></a><a href=\"https://www.youtube.com/watch?v=GjAD83B4JaY&amp;list=PL4qaj9envIYnBaQSPpcOMUqWiQUAgPoMq\">Proper Dress and PPE/Lab Safety Video Part 1</a> <br><a href=\"https://hardhattraining.com/personal-protective-equipment-gloves/\">Gloves and Their Uses for Safety</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Silica Awareness",
    overview: "<div>Our Silica training course is built to regulation standards. This class discusses topics including exposure and limits, controls, personal protective equipment (PPE), and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for silica:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>1926.1153 – Respirable Crystalline Silica Appendix A &amp; B</li><li>1910.1053 – Respirable Crystalline Silica Appendix A &amp; B</li><li>OSHA Act of 1970, 5(a)(1)&nbsp;</li></ul><div><br></div><div><strong>Canada<br></strong><br></div><ul><li>6.112.1 OHS Regulation – If employees are exposed or possibly exposed to silica dust and exposure control plan is required.</li><li>5.82 OHS Regulation – Employers responsibility to provide effective means of removing hazardous substances defined in section 5.1 from a worker's skin or clothing.</li><li>British Columbia – 0.025 mg/m3</li><li>Alberta – 0.025 mg/m3</li><li>Ontario – Meets ACGIH requirements, TWA does not exceed 0.005 mg/m3</li></ul>",
    facts: "<ul><li>The Earth’s crust is made up of 27% silicon. (Source: <a href=\"http://www.softschools.com/facts/periodic_table/silicon_facts/189/\"><em>SoftSchools</em></a>)</li><li>Silica exposure is a significant danger to 2 million U.S. workers. (Source: <a href=\"https://www.osha.gov/Publications/osha3177.html\"><em>OSHA</em></a>)</li><li>When silica dust enters the lungs, it creates scar tissue and makes it more difficult for the lungs to take in oxygen. (Source: <a href=\"https://www.osha.gov/Publications/osha3177.html\"><em>OSHA</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://ohsonline.com/articles/2018/10/18/niosh-studies-show-ways-to-reduce-silica.aspx\">NIOSH-Funded Studies Show Ways to Reduce Silica Exposure in Concrete Cutting</a> <br><a href=\"https://www.protoolreviews.com/news/understanding-silica-dust-and-oshas-50-microgram-pel/40752/\">Understanding Silica Dust and OSHA’s 50-microgram PEL<br></a><a href=\"https://www.youtube.com/watch?v=3X868gVvzH0\">Silica Dust Safety Training Video<br></a><a href=\"https://www.youtube.com/watch?v=J5RU8rS6spE\">The Danger of Silica Dust<br></a><a href=\"https://hardhattraining.com/respirable-crystalline-silica/\">OSHA to Enforce New Crystalline Silica Standards<br></a><a href=\"https://hardhattraining.com/18722-2/\">OSHA’s New Respirable Crystalline Silica Rule and How To Be Aware</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Marijuana in the Workplace",
    overview: "<div>Our Cannabis Awareness safety training course is perfect for educating people on the risks and dangers of cannabis use. This course covers general knowledge, the effects of cannabis both on the body and in the workplace, medicinal uses, addiction, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for a final written exam included with the course. In addition to the written exam, this course also includes a practical exam to review basic near awareness knowledge.<br><br>Cannabis is one of the most commonly used recreational drugs in North America. With a growing number of countries legalizing the use of cannabis for medicinal and recreational use, it is important that you are educated in how to use it properly, as well as how to prevent, identify, and respond to cannabis related issues.</div>",
    training_standards: "<div>None.</div>",
    facts: "<ul><li>Cannabis has direct impacts on the memory and decision-making portions of the brain.</li><li>Canada became the first county to offer medicinal cannabis to pain-suffering patients</li><li>Men are over twice as likely to use marijuana.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.nydailynews.com/news/national/ny-news-marijuana-heart-attack-70-hallucination-20190211-story.html\">Elderly Man has Heart Attack After Consuming Powerful Marijuana Lollipop<br></a><a href=\"https://www.mercurynews.com/2018/05/16/bay-areas-deadly-pot-related-crash-added-to-list-of-tragedies-in-other-cannabis-friendly-states/\">Bay Area's Deadly Pot Related Crash Added to List of Tragedies in Cannabis Friendly States<br></a><a href=\"https://www.youtube.com/watch?v=rSL6Fh6wJHY\">Driver Accused Of Marijuana DUI In Fatal 880 Crash<br></a><a href=\"https://www.youtube.com/watch?v=7Qvpyy_i7nw\">Fatal Crash- Driver Impairment with Marijuana</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Confined Space",
    overview: "<div>Our Confined Spaces Safety Training course is built to regulation guidelines. This class discusses topics including basic equipment, confined spaces testing, safe operations, common hazards, rescue techniques, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.146 – General Industry, Confined Spaces</li><li>29 CFR 1926 Subpart AA – Construction</li><li>29 CFR 1915 Subpart B – Shipyards</li><li>29 CFR 1910.134 – Respiratory Protection</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA Z1006-10 (R2015) – Management of work in confined spaces</li><li>CSA Z94.4-11 – Selection, use, and care of respirators</li><li>CSA Z180.1-13 – Compressed breathing air and systems</li><li>CSA Z259.12-11 – Fall Protection</li><li>CSA Z259.2.5-12 – Fall arresters and vertical lifelines</li><li>CSA Z259.100-12 – Full body harnesses</li><li>CSA Z259.2.2-14 – Self-retracting devices</li><li>CSA Z460-13 – Control of hazardous energy (lockout, and other methods)</li><li>CSA W117.2-94 – Safety in Welding, Cutting, Allied Processes</li></ul><div>In line with regulations, anyone who works with confined spaces must receive training prior to working on their own. Requirements for refresher training related to forklifts or other processes are very specific. Most other equipment doesn’t have such specific requirements, but it’s wise to follow the same guidelines.<br><br></div><div>When it comes to refresher training, the standards in some instances (like forklifts) are very specific: operators must be re-evaluated every three years to see if they are still competent to operate the equipment. Best practices say to apply this same rule to all types of equipment. A so-called “free-pass” cannot be awarded based on experience, age, or time on the job. The extent of the evaluation is to be determined by the employer but should include a written and practical examination that prove continued competency.&nbsp;</div>",
    facts: "<ul><li>From 1992 to 2005, there were 431 workers killed in confined space incidents.</li><li>One-fifth of confined space incidents result in multiple fatalities.</li><li>One-fourth of all confined spaces fatalities occur during regular repair, maintenance, cleaning, or inspection procedures. (Source: <a href=\"http://www.confinedspacecontrolcovers.com/osha-statistics/\"><em>CSCC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://safety.blr.com/workplace-safety-news/emergency-planning-and-response/confined-spaces/Oxygen-deficiency-causes-fatality-OSHA-citations-P/\">Oxygen Deficiency Causes Fatality<br></a><a href=\"https://www.cos-mag.com/personal-process-safety/31544-confined-and-confused/\">Confined and Confused<br></a><a href=\"https://youtu.be/vG6K-NulTok\">Men Working Underground in Key Largo Collapse and Die<br></a><a href=\"https://youtu.be/3oEy4Gz3Ag0\">Two Workers Killed in Deadly Sewage Accident<br></a><a href=\"https://hardhattraining.com/confined-spaces-employer-responsibilities/\">5 Confined Spaces Employer Responsibilities<br></a><a href=\"https://hardhattraining.com/permit-required-confined-space/\">Permit-Required Confined Space</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Emergency Response",
    overview: "<div>Our Emergency Response safety training course is perfect for those wish to be aware of and prepared for dangerous and potentially life-threatening emergencies. This course covers methods of emergency preparation, response, recovery, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for a final written exam included with the course. In addition to the written exam, this course also includes a practical exam to review basic near awareness knowledge.<br><br>From tornadoes to civil unrest, emergencies can occur no matter where you live. While many natural disasters and emergencies are not preventable, it is important to know what steps you should take to prepare for such events, as well as what you should do to increase the chances of your survival during and after the emergency. Being informed and having a plan to follow could possibly save your life.</div>",
    training_standards: "<div>None.</div>",
    facts: "<ul><li>Over the past 10 years, the countries that have been most frequently hit by natural disasters are China, the United States, the Philippines, Indonesia, and India. (Source: <a href=\"https://reliefweb.int/report/world/annual-disaster-statistical-review-2014-numbers-and-trends\"><em>Reliefweb</em></a>)</li><li>The Mount Saint Helens volcanic eruption in 1980 destroyed more than 220 square miles and razed millions of dollars worth of timber. (Source: <a href=\"https://www.ngdc.noaa.gov/hazard/stratoguide/helenfact.html\"><em>NOAA</em></a>)</li><li>The 2004 Indian Ocean Tsunami was triggered by a 9.1 earthquake that generated waves up to 30 feet tall and killed nearly 225,000 people. (Source: <a href=\"https://www.britannica.com/event/Indian-Ocean-tsunami-of-2004\"><em>Britannica</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.cbsnews.com/news/2017-and-2018-were-costliest-ever-years-for-natural-disasters/\">2017 and 2018 Were Costliest Ever Years for Natural Disasters<br></a><a href=\"https://www.nbcnews.com/news/us-news/chicago-residents-deal-polar-vortex-sealing-staying-n964676\">Polar Vortex Brings Chicago to a Standstill: 'It's Our Own Natural Disaster'<br></a><a href=\"https://www.nbcnews.com/news/weather/video/-strongest-ever--hurricane-makes-landfall-on-mexico-s-pacific-coast-550681667791\">'Strongest Ever' Hurricane Makes Landfall on Mexico's Pacific Coast<br></a><a href=\"https://www.youtube.com/watch?v=KeJw6U9_VeY\">Paris Braces for Riots as Protests Sweep France | TODAY<br></a><a href=\"https://hardhattraining.com/emergency-response-and-preparedness-plan/\">Emergency Response and Preparedness Plan<br></a><a href=\"https://hardhattraining.com/hurricane-safety-and-emergency-preparedness/\">Hurricane Safety and Emergency Preparedness</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Hand & Power Tools",
    overview: "<div>Our Hand &amp; Power Tools safety training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on the different types of guards, handles and switches, cutting ad fastening tools, and common hazards to watch for, and more.).&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for Hand &amp; Power Tools:<br><br></div><div><strong>U.S. Standards<br></strong><br></div><ul><li>1910 Subpart P, Hand and Portable Powered Tools and Other Hand-Held Equipment</li><li>1910.242 - Hand and portable powered tools and equipment, general</li><li>1910.243 - Guarding of portable powered tools</li><li>1926 Subpart I - Tools - Hand and Power</li><li>1926.300 - General requirements</li><li>1926.302 - Power-operated hand tools</li><li>1926.303 - Abrasive wheels and tools</li><li>1926.304 – Woodworking</li></ul><div><strong>Canada Standards<br></strong><br></div><ul><li>OHS Regulation: Part 12 Tools, Machinery and equipment</li><li>OHS Regulation: Part 07 Noise, vibration, radiation and temperature</li><li>Always read, refer to and follow all manufacturer guidelines as found in the operator manuals for your tool</li></ul>",
    facts: "<ul><li>A nail travels 1400 feet per second when it leaves a nail gun (<em>Source: </em><a href=\"https://kiloton.co.za/interesting-facts-on-the-history-of-power-tools/\">Kiloton</a>)</li><li>About 40% of electrical incidents involved 250 volts or less. (<em>Source: </em><a href=\"https://www.nfpa.org/-/media/Files/News-and-Research/Fire-statistics-and-reports/Electrical/RFArcFlashOccData.ashx?la=en\"><em>NFPA</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"http://content.safetyculture.com.au/news/index.php/02/man-suffers-serious-head-injuries-angle-grinder-blade-explodes-barrack-heights/\">Man Suffers Serious Head Injuries After Angle Grinder Blade Explodes<br></a><a href=\"https://www.totallandscapecare.com/landscaping/safety-watch-landscape-worker-dies-after-concrete-saw-kicks-back/\">Safety Watch: Landscape worker Dies After Concrete Saw Kicks Back<br></a><a href=\"https://www.youtube.com/watch?v=aI5M5bk9ASM\">Worker Killed When Saw Kicks Back</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Machine Guarding",
    overview: "<div>Our Machine Guarding Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on equipment, operations, hazards, case studies, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for machine guarding:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>1910 Subpart O – Machinery and Machine Guarding</li><li>1910.212 – General Requirements for all Machines</li><li>1910 Subpart P – Hand and Portable Powered Tools and Other Hand-Held Equipment</li><li>1917 Subpart G – Marine Terminals</li><li>1918 Subpart I – Longshoring</li><li>1926 Subpart I – Construction Industry</li><li>1928 Subpart D – Agriculture Industry</li><li>29 CFR 1910.212(a)(3)(ii)</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA Z432 – Safeguarding of Machinery</li><li>CSA Z142 – Power Press Operations</li><li>CSA C22.2 – Electric motor-operated hand-held tools</li><li>ANSI Z244.1 – Control of Hazardous Energy, LOTO and Alternative Methods</li><li>ANSI A10.44 – Control of Energy Sources (LOTO) for Construction and Demolition Operations</li><li>OSHA Sections 27 and 28</li><li>CSA Z1001 – Occupational Health &amp; Safety Training</li><li>Canada Labour Code Part II – Employer and Employee Duties</li><li>CSA Z617-06 - PPE</li></ul>",
    facts: "<ul><li>Machine guards protect workers from kickbacks, splashing liquids, and flying chips.&nbsp; (Source: <a href=\"https://www.safetyservicescompany.com/topic/osha/better-safety-with-machine-guards-basics-and-proper-use/\"><em>Safety Services Company</em></a>)&nbsp;</li><li>OSHA standards for machine guarding haven’t changed since 1975. (Source: <a href=\"https://www.mscdirect.com/betterMRO/metalworking/machine-guard-infographic-point-operation\"><em>MSC</em></a>)</li><li>Eyewear and other PPE can be considered a part of machine guarding in some standards. (Source: <a href=\"https://www.mscdirect.com/betterMRO/metalworking/machine-guard-infographic-point-operation\"><em>MSC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.powderbulksolids.com/article/Bumper-Guards-for-Orbital-Stretch-Wrapping-Machines-01-03-2019\">Bumper Guards for Orbital Stretch-Wrapping Machines<br></a><a href=\"https://www.youtube.com/watch?v=MS6215JTQI8\">Machines Operators and Guards Real Accidents Real Stories<br></a><a href=\"https://www.youtube.com/watch?v=2d1VzvPVAtY\">ABCs of Safety: M is for Machine Guarding<br></a><a href=\"https://hardhattraining.com/machine-guarding-not-suggestion/\">Machine Guarding: Not a Suggestion<br></a><a href=\"https://hardhattraining.com/machine-guarding-important/\">Machine Guarding - Is It Important?</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Record Keeping",
    overview: "<div>Our Record Keeping training course is built to OSHA standards. This class discusses topics including regulations, incident investigations, proper investigating, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards for record keeping:<br><br></div><ul><li>29 CFR 1904 – Recordkeeping&nbsp;</li><li>OSHA Act of 1970, 5(a)(1) – “Each employer shall furnish to each of his employees…a place of employment which is free from recognized hazards that are causing or are likely to cause death or serious physical harm to his employees.”</li></ul>",
    facts: "<ul><li>Before paper, people recorded on all sorts of things, including bamboo, birch bark, palm leaves, silk, and wax.</li><li>Archival science is a form of education and even occupational pursuit. It is the study and creation of archives, or in other words, record keeping.&nbsp;</li></ul><div><br></div>",
    fails: "<div><a href=\"https://buckrail.com/teton-county-explores-blockchain-for-record-keeping/\">Teton County Explores Blockchain for Record-keeping<br></a><a href=\"https://www.enterpriseinnovation.net/article/top-10-administrative-record-keeping-tips-healthcare-organizations-1285206881\"> Top 10 Administrative Record-keeping Tips for Healthcare Organizations<br></a><a href=\"https://www.youtube.com/watch?v=GbtStg1xd4w\">Introduction to Effective Record Keeping<br></a><a href=\"https://www.youtube.com/watch?v=29jMtgYbOIo\">VAT Record Keeping</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Self-Defense",
    overview: "<div>Our Self-Defense training course is built to OSHA standards. This class discusses topics including prevention, physical defense, legal defense, cyber defense, case studies, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>OSHA doesn’t have a specific standard for self-defense training. However, under the General Duty Clause, Section 5(a)(1) of the Occupational Safety and Health Act of 1970, employers are required to provide a workplace that “is free from recognizable hazards that are causing or likely to cause death or serious harm to employees.”<br><br></div><div>Because of this requirement, employers have a legal and ethical obligation to develop and maintain a workplace that is free from hazards associated with self-defense. Employees have the right to work in an atmosphere that promotes the safety and well-being of all.</div>",
    facts: "<ul><li>Guns are rarely used in self-defense by private citizens and are far more likely to be used in a civilian act of violence. (Source: <a href=\"http://vpc.org/wp-content/uploads/2016/04/Self-Defense-Fact-Sheet-April-2016-FINAL.pdf\"><em>Violence Policy Center</em></a>)</li><li>When someone claims self-defense in court, their claim must be justified. Also, the court must look into the type of force (such as non-deadly and deadly) that was used in self-defense. (Source: <a href=\"https://www.encyclopedia.com/social-sciences-and-law/law/law/self-defense\"><em>Encyclopedia.com</em></a>)</li><li>Self-defense isn’t designed to retaliate or fight, but to give the individual a response to enable an escape.</li></ul>",
    fails: "<ul><li><a href=\"https://www.thedailystar.com/news/local_news/free-self-defense-class-draws-crowd/article_2a162b95-15a1-51cc-9ea7-058a18d8e815.html\">Free Self-Defense Class Draws Crowd</a></li><li><a href=\"https://www.airforcetimes.com/news/your-air-force/2019/01/11/report-airman-on-trial-for-killing-roommate-claims-self-defense/\">Report: Airman on Trial for Killing Roommate; Claims Self-Defense</a></li><li><a href=\"https://www.youtube.com/watch?v=A-z5q9tYIVg\">Officers Say They Acted in Self-Defense During Shooting that Killed Autistic Boy</a></li><li><a href=\"https://www.youtube.com/watch?v=xxBdgSrkU7w\">Red Alert: Self Defense Techniques</a></li><li><a href=\"https://hardhattraining.com/a-brief-exposition-on-appropriate-self-defense/\">A Brief Exposition on Appropriate Self-Defense</a></li></ul>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "PPE",
    overview: "<div>Our Personal Protective Equipment (PPE) training course is built to regulation standards. This class discusses topics including establishing a program, classifications of PPE, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for PPE:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910 – General Industry, Subpart I, Personal Protective Equipment</li><li>29 CFR 1926 Subpart C – General Safety and Health Provisions</li><li>29 CFR 1926 Subpart E – Personal Protective and Life Saving Equipment</li><li>29 CFR 1926 Subpart M – Fall Protection</li><li>29 CFR 1926 Subpart P – Excavations</li><li>29 CFR 1915 – Maritime Industry, Subpart I, Personal Protective Equipment</li><li>OSHA Act of 1970, 5(a)(1)&nbsp;</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA Standard Z94.4-02 – <em>Selection, Care and Use of Respirators</em></li><li>CSA Standard Z94.3-07 – <em>Eye and Face Protectors</em></li><li>CSA Standard Z94.1 – <em>Protective Headwear</em></li><li>CSA Standard Z195-09 – <em>Protective Footwear</em></li><li>CSA Standard Z94.2.02 – <em>Hearing Protection Devices (Performance Selection, Care and Use</em></li><li>CSA-Z617-06 (R2011) – Personal Protective Equipment (PPE) for Blunt Trauma</li><li>OSH Act sections 25, 27 and 28</li><li><em>Canada Labour Code, </em>Part II Subsection 122.2, sections 125 and 126</li></ul><div><strong>International<br></strong><br></div><ul><li>ANSI Z87.1-1989 – Eye and face Protection&nbsp;</li><li>ANSI Z89.1-1986 – Head Protection&nbsp;</li><li>ANSI Z41.1-1991 – Foot Protection</li></ul>",
    facts: "<ul><li>PPE has different levels of defense to accommodate different needs.&nbsp;</li><li>Cleaning PPE affects the way PPE functions.</li><li>PPE saved the lives of two of the ten archeologists re-investigating King Tutankhamen’s tomb in the 1970s. The others died because, at the time, they weren’t required to wear it.&nbsp;</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.healio.com/infectious-disease/nosocomial-infections/news/in-the-journals/%257Bd06d815a-5243-4b38-8033-652cb3697c7d%257D/despite-guidelines-more-than-100-ways-ppe-doffing-process-can-fail\">Despite Guidelines, More Than 100 Ways PPE Doffing Process Can Fail<br></a><a href=\"https://www.infectioncontroltoday.com/personal-protective-equipment/self-contamination-during-ppe-doffing-and-ebola-transmission\">Self-Contamination During PPE Doffing and Ebola Transmission<br></a><a href=\"https://www.youtube.com/watch?v=4L7_RiDNySQ\">Personal Protective Equipment – Good + Bad<br></a><a href=\"https://www.youtube.com/watch?v=1-GEltJlgXw\">Safety Basics: Personal Protective Equipment<br></a><a href=\"https://hardhattraining.com/ppe-eye-protection/\">PPE: Eye Protection</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Walking & Working Surfaces",
    overview: "<div>Our Walking &amp; Working Surfaces Safety Training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. This class contains sections on outdoor and indoor surfaces, prevention, accident profiles, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.<br><br>Regulations make it very clear that any employees working in or around slip, trip, and fall hazards must be trained (no matter how long they’ve been on the job). The standards also state that the employer is responsible for overseeing employee safety training and evaluating the permit space program in order to confirm that the employees have the understanding, knowledge, and skills needed for safe confined space entry operations.&nbsp;<br><br></div><div>When it comes to refresher training, the standards in some instances (like forklifts) are very specific: operators must be re-evaluated every three years to see if they are still competent to operate the equipment. Best practices say to apply this same rule to all types of equipment. A so-called “free-pass” cannot be awarded based on experience, age, or time on the job. The extent of the evaluation is to be determined by the employer but should include a written and practical examination that prove continued competency.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSHA 29 CFR 1910.21 Subpart D – Walking-Working Surfaces</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CLC 2.14 (1) (2) (3)</li><li>CLC 3.14 &amp; 3.15</li><li>&nbsp;British Columbia, OHS 4.39-4.41</li><li>Saskatchewan, Part IX Section 124,189,195</li><li>Yukon Part 1.51 (1)(2)(3)</li></ul>",
    facts: "<ul><li>In 2013, nearly 600 workers died in falls from a higher level.&nbsp;</li><li>That same year, approximately 47,000 suffered time-lost injuries due to slip, trips, and falls.</li><li>More than 8.7 million people are treated in emergency rooms for fall-related injuries every year. (Source:<a href=\"https://www.nsc.org/work-safety/safety-topics/slips-trips-falls\"><em> National Safety Council</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.army.mil/article/206692/4_tips_to_avoid_slips_trips_falls\">4 Tips to Avoid Slips, Trips, &amp; Falls<br></a><a href=\"https://abc7.com/worker-falls-at-disneylands-small-world-ride-dangles-20-ft-off-ground/4421001/\">Worker Falls at Disneyland<br></a><a href=\"https://www.law.com/thelegalintelligencer/2018/05/17/suit-tripping-hazard-in-amazon-warehouse-violated-osha/?slreturn=20180910124151\">Tripping Hazard in Amazon Warehouse<br></a><a href=\"https://youtu.be/KA1a99KmPGI\">Worker Slips, Falls into Hot Oil<br></a><a href=\"https://youtu.be/utvcuignTO4\">Waitress Trips, Falls into Window<br></a><a href=\"https://hardhattraining.com/fall-protection-walking-working-surfaces/\">Fall Protection &amp; Walking-Working Surfaces<br></a><a href=\"https://hardhattraining.com/slips-trips-falls-silent-killers/\">Slips, Trips, Falls: Silent Killers</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Welding",
    overview: "<div>Our Welding Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on equipment, safe operations, hazards, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910 Subpart Q – General Industry, Welding, Cutting, and Brazing</li><li>29 CFR 1926 Subpart J – Construction</li><li>29 CFR 1915 Subpart D – Shipyards</li><li>29 CFR 1917.152 – Marine Terminals</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA Welding Standards</li><li>Canadian Welding Bureau Standards</li><li>ASME Section IX</li><li>B.C – 12.112-12.126</li><li>Alberta – Alberta OHS 171&nbsp;</li><li>Manitoba –Manitoba’s Act &amp; Regulations</li><li>Nova Scotia – Section 17A of the Apprenticeship and Trades Qualifications Act</li><li>Saskatchewan – TSASK Standards</li></ul>",
    facts: "<ul><li>Welders are exposed to dangerous fumes and gases, and are at risk of electric shock, fire, and explosions.&nbsp;</li><li>Welders can suffer eye damage (including vision loss), lung damage, brain damage, and severe burns.&nbsp;</li><li>Welders, cutter, solderers, and brazers hold just over 400,000 jobs nationwide.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://kutv.com/news/local/21-year-old-taken-to-hospital-after-welding-accident-in-west-valley-city\">21-year-old Taken to Hospital After Welding Accident<br></a><a href=\"https://www.usnews.com/news/best-states/rhode-island/articles/2018-06-29/welding-accident-sparks-massive-warehouse-fire\">Welding Accident Sparks Massive Warehouse Fire<br></a><a href=\"https://youtu.be/kKJEL4aS-Nk\">Welding Caused Fire Inside Amphitheater<br></a><a href=\"https://youtu.be/JeOfHlQCkx0\">Welding Accident</a> <br><a href=\"https://hardhattraining.com/welding-leads-shocking-symptoms/\">Welding Leads to Shocking Symptoms</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "360 Excavator",
    overview: "<div>Our 360 Excavator training course is OSHA compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on equipment and anatomy, maintenance and inspections, safe operations and stability, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards:<br><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926.650-652 – Excavations</li><li>29 CFR 1926, Subpart P, App A – Soil Classification</li><li>29 CFR 1926, Subpart P, App F – Protective Systems</li></ul>",
    facts: "<ul><li>Almost 60 workers die each year in excavation accidents.</li><li>68% of those fatalities occur in companies with fewer than 50 workers.</li><li>About half of these fatalities occur in companies with 10 or fewer workers. (Source:<em> </em><a href=\"https://www.cdc.gov/niosh/topics/trenching/default.html\"><em>CDC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.nbcchicago.com/news/local/Worker-Killed-by-Excavator-Bucket-When-Operators-Cloth-Snags-on-Lever-Police-454522193.html\">Worker Fatally Struck by Excavator Bucket When Operator’s Clothing Snags on Lever<br></a><a href=\"https://www.smh.com.au/national/nsw/missing-safety-pin-blamed-as-excavator-scoop-crushes-husband-waiting-for-news-of-new-baby-20120207-1r2eb.html\">Missing 'Safety Pin' Blamed as Excavator Scoop Crushes Husband Waiting for News of New Baby<br></a><a href=\"https://youtu.be/3YBF5QmM6wE\">Erin Kelly on Fatal Excavator Accident<br></a><a href=\"https://youtu.be/CoaHldaMPTk\">Building Collapse in Portugal<br></a><a href=\"https://hardhattraining.com/excavator-hits-power-line-causes-fire/\">Excavator Hits Power Line, Causes Fire<br></a><a href=\"https://hardhattraining.com/360-excavator-recognizing-top-6-hazards/\">360 Excavator: Recognizing the Top 6 Hazards</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "ATV/UTV",
    overview: "<div>Our ATV-UTV Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on equipment and anatomy, maintenance and inspections, safe operations and stability, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for ATVs and UTVs:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>OSH Act of 1970 – General Duty Clause&nbsp;</li><li>29 CFR 1926.21 – Training and Education</li><li>ANSI/SVIA 1-2017 – Consumer Product Safety Standard for Four Wheel All-Terrain Vehicle Equipment Configuration</li></ul><div><strong>Canada<br></strong><br></div><ul><li>Motor Vehicle Safety Regulations (C.R.C, c. 1083)</li><li>Alberta – Alberta&nbsp; Transportation</li><li>B.C. – Ministry of Transportation, Insurance Corporation of B.C.</li><li>Manitoba – Manitoba Infrastructure and Transportation</li><li>New Brunswick – Department of Transportation</li><li>Newfoundland and Labrador – Department of Transportation and Works</li><li>Northwest Territories – Department of Transportation</li><li>Nova Scotia – Transportation and Public Works</li><li>Nunavut – Department of Economic Development and Transportation</li><li>Ontario – Ministry of Transportation, Ontario’s Drive Clean</li><li>Prince Edward Island – Transportation and Public Works</li><li>Québec – Ministère des Transports, Société de l'assurance automobile du Québec – SAAQ</li><li>Saskatchewan – Saskatchewan Government Insurance, Highways and Transportation</li><li>Yukon – Department of Highways and Public Works-Transportation-Road Safety</li></ul>",
    facts: "<ul><li>From 2004 to 2013, there were 7,123 reported ATV-related deaths.</li><li>Of the 7,123 reported deaths, nearly 16% of them involved children under the age of 16.</li><li>From 1982 to 2013, Texas had the most reported ATV-related deaths at 735. (Source: <a href=\"https://onsafety.cpsc.gov/blog/2018/05/18/cpsc-infographic-atv-deaths-injuries/\"><em>CPSC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.washingtonpost.com/news/early-lead/wp/2017/10/15/former-white-sox-pitcher-daniel-webb-dies-in-atv-accident/?utm_term=.a9713694a283\">Former White Sox Pitcher Daniel Webb Dies in ATV Accident<br></a><a href=\"https://www.usnews.com/news/best-states/new-york/articles/2017-11-06/atv-accident-leaves-2-dead-in-upstate-new-york\">ATV Accident Leaves 2 Dead in Upstate New York<br></a><a href=\"https://youtu.be/DVd-RL2BLHo\">Four Bodies Recovered From ATV Accident North of Payson<br></a><a href=\"https://youtu.be/YWILk-FSN2w\">Several Children Seriously Hurt in Utah ATV Accident<br></a><a href=\"https://hardhattraining.com/atv-safety/\">ATV Safety<br></a><a href=\"https://hardhattraining.com/emerging-technology-atv-roll-bars/\">Emerging Technology: ATV Lifeguard Roll Bars</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Amphibious Excavator",
    overview: "<div>Our Amphibious Excavator Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomy, stability, operation, hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following the standards for amphibious excavators:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926.650-652 – Excavations</li><li>29 CFR 1926, Subpart P, App A – Soil Classification</li><li>29 CFR 1926, Subpart P, App F – Protective Systems</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-B352.0-09 – ROPS, FOPS (General Mobile Equipment)</li><li>CAN/CSA-M12117-05 – Earth-Moving Machinery (TOPS) for Compact Excavators</li><li>CAN/CSA-M3471-05 – Earth-Moving Machinery – ROPS, Laboratory Tests, Performance Requirements</li><li>CAN/CSA-M3450-03 – Earth-Moving Machinery – Braking Systems of Rubber-Tired Machines – Systems and Performance Requirements and Test Procedures</li></ul>",
    facts: "<ul><li>Amphibious excavators float because of sealed pontoons.&nbsp;</li><li>Excavators have been around for a long time, but amphibious excavators were not manufactured until 2005. (Source: <a href=\"https://www.oemoffhighway.com/drivetrains/article/10166439/amphibious-excavators\"><em>OEM</em></a>)</li><li>Amphibious excavators are also known as swamp buggy, marsh buggy, floating excavator, pontoon excavator, and swamp excavator.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.thefreepress.ca/news/contractor-missing-after-serious-incident-at-fording-river-mine/\">Worker Dies in Industrial Incident at Fording River Mine</a> <br><a href=\"https://canada.constructconnect.com/dcn/news/ohs/2010/09/clayton-construction-charged-in-death-of-oilsands-operator-dcn040788w\">Clayton Construction Charged in Death of Oilsands Operator<br></a><a href=\"https://www.youtube.com/watch?v=teWEQTFfLos\">Excavator Fail and a Lucky Escape<br></a><a href=\"https://www.youtube.com/watch?v=EbD7ryn4dI0\">Amphibious Excavator on Duty<br></a><a href=\"https://hardhattraining.com/excavator-hits-power-line-causes-fire/\">Excavator Hits Power Line, Causes Fire<br></a><a href=\"https://hardhattraining.com/360-excavator-recognizing-top-6-hazards/\">360 Excavator: Recognizing the Top 6 Hazards</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Asbestos",
    overview: "<div>Our Asbestos Awareness Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on exposure and diseases, identification, safe work practices, PPE, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.1001 – Asbestos in General industry</li><li>29 CFR 1926.1101 – Asbestos in Construction</li></ul><div><strong>Canada<br></strong><br></div><ul><li>B.C. – Workplace B.C. Safe Work Practices for Handling Asbestos</li><li>Manitoba – Safe Work Manitoba</li><li>New Brunswick – Occupational Heath and Safety Act &nbsp;</li><li>Ontario – Ministry of Labor Ontario&nbsp;</li><li>Quebec – Legis Quebec Ch. 2.1, r. 13</li><li>Alberta – Alberta Labour OHS Regulations</li></ul>",
    facts: "<ul><li>Malignant mesothelioma has been linked to inhalation exposure to asbestos.</li><li>The latency period for malignant mesothelioma is generally 20 to 40 years.</li><li>From 1999-2015, there have been 45,221 deaths in the United States linked to malignant mesothelioma. (Source: <a href=\"https://www.cdc.gov/mmwr/volumes/66/wr/mm6608a3.htm\"><em>CDC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.nbcnews.com/storyline/9-11-anniversary/9-11-first-responders-begin-feel-attack-s-long-term-n908306\">9/11 First Responders Begin to Feel Attack’s Long-Term Health Effects<br></a><a href=\"https://www.huffingtonpost.com/entry/libby-montana-affordable-care-act-provisions_us_58b582b3e4b060480e0be14f\">This Poisoned Montana Town Is Now at The Mercy Of The GOP Health Care Plan<br></a><a href=\"https://youtu.be/NRSxBiYFBuw\">Asbestos Scandal | 9 News Perth<br></a><a href=\"https://youtu.be/J_3nSHArEDI\">Secondhand Exposure to Asbestos Led to Six Surgeries<br></a><a href=\"https://hardhattraining.com/17308-2/\">Asbestos: A Peculiar Rock<br></a><a href=\"https://hardhattraining.com/health-effects-of-9-11-still-plague-new-york/\">Health Effects of 9/11 Still Plague New York</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Chainsaw",
    overview: "<div>Our Chainsaw Safety Training course is regulation compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy, PPE, safe operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for chainsaw operations:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.266(e)(2) – Chainsaws</li><li>29 CFR 1910.266(e)(1)(ii)(C) – Chains Adjusted Properly</li><li>29 CFR 1910.266(e)(1)(ii)(D) – Mufflers are Operational and in Place</li><li>29 CFR 1910.266(e)(1)(ii)(E) – Chain Brakes are in Place and Function Properly</li><li>29 CFR 1910.266(e)(1)(ii)(H) – All Other Safety Devices are in Place and Function Properly</li><li>ANSI B175.1 – Safety Requirements for Gasoline-Powered Chain Saws</li><li>241 FW 12 – Chain Saw Safety (non-fire)</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA Z62.1 “Chain Saws”</li><li>ANSI B175.1 “Gasoline Powered Chain Saws, Safety Requirements for”</li></ul><div>In line with regulations, anyone who works with chainsaws must receive training prior to working on their own. While requirements for refresher training related to forklifts or other processes are very specific, most other equipment doesn’t have such specific requirements, but it’s wise to follow the same guidelines.<br><br></div><div>When it comes to refresher training, the standards in some instances (like forklifts) are very specific: operators must be re-evaluated every three years to see if they are still competent to operate the equipment. Best practices say to apply this same rule to all types of equipment. A so-called “free-pass” cannot be awarded based on experience, age, or time on the job. The extent of the evaluation is to be determined by the employer but should include a written and practical examination that prove continued competency.&nbsp;</div>",
    facts: "<ul><li>From 2009-2013, there were about 116,000 emergency visits because of chainsaw accidents—that’s more than 60 accidents per day.&nbsp;</li><li>Roughly 95% of those injured were males. The main body parts injured were hands, fingers, and knees.&nbsp;</li><li>Most of the injuries occurred from September through November. (Source: <a href=\"https://www.hindawi.com/journals/aem/2015/459697/\"><em>Hindawi</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.thesun.co.uk/news/2871440/tree-surgeon-killed-horror-accident/\">Tree Surgeon Dad-of-One Killed in Horror Accident as ‘Chainsaw Carves Through His Neck Leaving Blood-Splattered Street’<br></a><a href=\"https://www.golf.com/tour-and-news/greg-norman-recovering-chainsaw-accident\">Greg Norman Nearly Lost His Left Hand in a Chainsaw Accident<br></a><a href=\"https://youtu.be/bhVZ4hdz8sw\">Chainsaw Fail During News Coverage<br></a><a href=\"https://youtu.be/Aw9_jP4G3Y0\">Woman Killed in Chainsaw Accident<br></a><a href=\"https://hardhattraining.com/chainsaw-safety/\">Chainsaw Safety<br></a><a href=\"https://hardhattraining.com/hand-tools-accidents/\">Hand Tools Accidents: Worse Than a Sore Thumb</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Defensive Driving",
    overview: "<div>Our Defensive Driving safety training course is regulation compliant. Each class contains sections on proper vehicle care, law and controls, weather conditions, preparedness, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.<br><br>With driving being such a commonplace activity, it is easy to become complacent and develop bad habits. Roughly 3,000 people die in road crashes every day—that’s about 1.25 million each year, according to the Association for Safe International Road Travel. While it’s impossible to know the exact conditions of every accident, many accidents could have been prevented had the drivers practiced safe, defensive driving techniques.&nbsp;<br><br></div><div>This training can help remind drivers of both commercial or passenger vehicles of the basics of driving on public roads, company property, or other locations where an accident is possible.</div>",
    training_standards: "<div>Though we include some basic traffic and driving laws, you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial and local laws and regulations.</div>",
    facts: "<ul><li>In 2016, transportation incidents caused more fatal work injuries than any other event.</li><li>Roughly 30% of work-related transportation accidents during 2016 occurred on a roadway.</li><li>About 3 in every 100 fatal transportation incidents involved multiple fatalities. (Source:<em> </em><a href=\"https://stats.bls.gov/iif/oshwc/cfoi/cfch0015.pdf\"><em>BLS</em></a><em>.</em>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.cnn.com/2018/10/10/us/new-york-limo-crash/index.html\">Limo Company Owner’s Son Arrested in Deadly New York Crash, Lawyer Says<br></a><a href=\"http://www.startribune.com/charges-possible-for-preventable-crash-that-killed-road-worker-on-i-94/495051691/\">‘Preventable’ I-94 Crash Results in Minnesota’s 13th Death in a Work Zone in ‘18<br></a><a href=\"https://youtu.be/rWPRzdG6CpQ\">Dashcam Video: Truck Driver Allegedly Looking at Phone at Time of Fatal Crash<br></a><a href=\"https://youtu.be/hKpQxegRKgA\">Video Shows Truck Driver Using Cell Phone Seconds Before Flipping Box Truck off I-75 Overpass<br></a><a href=\"https://hardhattraining.com/staying-safe-roads/\">Safe Driving Begins with You<br></a><a href=\"https://hardhattraining.com/pedestrian-vehicle-safety/\">Pedestrian-Vehicle Safety: Help Workers Return Home</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Fall Protection",
    overview: "<div>Our Fall Protection Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on equipment, inspections, operations, common hazards, rescue operations, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for fall protection:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>1926 Subpart M – Fall Protection</li><li>1926.501 – Duty to Have Fall Protection</li><li>1926.502 – Fall Protection Systems Criteria and Practices</li><li>1926.503 – Training Requirements</li><li>1910 Subpart D – Walking-Working Surfaces</li><li>1910.29 – Fall Protection Systems and Falling Object Protection-Criteria and Practices</li><li>1910.28 – Safety Requirements for Scaffolding</li><li>1910 Subpart F – Powered Platforms, Manlifts, and Vehicle-&nbsp;</li><li>1910 App C – Personal Fall Arrest System</li><li>1910.67 – Vehicle-Mounted Elevating and Rotating Work Platforms</li><li>1910 Subpart I – Personal Protective Equipment</li><li>1910.140 – Personal Fall Protection Systems</li><li>29 CFR 1917 – Marine Terminals</li><li>1917 Subpart F – Terminal Facilities</li><li>1917.112 – Guarding of Edges</li><li>1917.117 – Manlifts&nbsp;</li><li>1917.118 – Fixed Ladders&nbsp;</li><li>29 CFR 1915 – Shipyards&nbsp;</li><li>1915 Subpart E – Scaffolds, Ladders and Other Working Surfaces</li><li>1915.71 – Scaffolds or Staging</li><li>1915.72 – Ladders&nbsp;</li><li>1915.73 – Guarding of Deck Openings and Edges</li><li>1915 Subpart I – Personal Protective Equipment</li><li>1915.159 – Personal Fall Arrest Systems (PFAS)</li><li>29 CFR 1918 – Longshoring&nbsp; &nbsp;</li><li>1918 Subpart D – Working Surfaces</li><li>1918.32 – Stowed Cargo and Temporary Landing Surfaces</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA Z259-1-05 (R2015) - Body Belts and Saddles for Work Positioning and Travel Restraint</li><li>Z259.2.3:16 - Descent devices</li><li>Z259.10-12 (R2016) - Full body harnesses</li><li>Z259.12-16 - Connecting components for personal fall-arrest systems (PFAS)</li><li>Z259.13-16 - Manufactured horizontal lifeline systems</li><li>Z259.14-12 (R2016) - Fall restrict equipment for wood pole climbing</li><li>Z259.2.2-17 - Self-retracting devices</li><li>Z259.2.5-17 - Fall arresters and vertical lifelines</li><li>Z259.16-15 - Design of active fall-protection systems</li><li>Z259.2.4-15 - Fall arresters and vertical rigid rails</li><li>Z259.17-16 - Selection and use of active fall-protection equipment and systems</li><li>Z259.15-17 - Anchorage connectors</li><li>Alberta – Part 9 (Scaffolding, Work Platforms and Temporary Supporting Structures)</li><li>British Columbia – Part 11 (Fall Protection)</li><li>Manitoba – Part 14 (Fall Protection)</li><li>Newfoundland and Labrador – Part X (Fall Protection)</li><li>Northwest Territories - Section 118-122 (Fall Protection)</li><li>Nunavut – Section 118-122 (Fall Protection)</li><li>Ontario – Sections 52 &amp; 85 (Fall Protection)</li><li>Quebec – Divisions I,II,III,XXIII (Fall Protection)</li><li>Saskatchewan – Part IX (Safeguards, Storage, Warning Signs and Signals)</li><li>Yukon – Sections 1.37-1.43 (Protective Equipment and Clothing- Fall Arrest)</li><li>Federal Code –Parts I,II,III,X (Safety Materials, Equipment, Devices and Clothing)</li></ul>",
    facts: "<ul><li>39.2% of deaths within the construction industry in 2017 were caused by falls. (Source: <a href=\"https://www.osha.gov/oshstats/commonstats.html\"><em>OSHA</em></a>)</li><li>In 2016, there were 370 preventable fatalities from falls that occurred in the construction industry. (Source: <a href=\"https://www.osha.gov/stopfalls/\"><em>OSHA</em></a>)</li><li>In the United States, over $15 billion is spent on disability claims that occur because of falls. (Source: <a href=\"https://www.ehstoday.com/safety/workplace-falls-pose-danger-economy-well-workers\"><em>EHS Today</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://patch.com/texas/north-austin/2-escape-injury-when-bucket-truck-tips-over-north-austin\">2 Escape Injury When Bucket Truck Tips Over in North Austin<br></a><a href=\"https://patch.com/connecticut/tolland/man-hurt-fall-bucket-truck-tolland\">Man Hurt in Fall from Bucket Truck in Tolland<br></a><a href=\"https://www.youtube.com/watch?v=-2-hM-uPiP0\">Kansas City Painter Dies after Lift Falls Over<br></a><a href=\"https://www.stgeorgeutah.com/news/archive/2017/03/20/jcw-construction-worker-falls-from-very-serious-height/#.XGHeVFxKiUk\">Construction Worker Falls from ‘Very Serious’ Height<br></a><a href=\"https://hardhattraining.com/fall-protection-equipment-abcs/\">Fall Protection Equipment ABCs<br></a><a href=\"https://hardhattraining.com/fall-protection-save-life/\">Fall Protection Could Save Your Life</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "First Aid & CPR",
    overview: "<div>Our First Aid Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on CPR, AED, first aid procedures, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following general standards for first aid:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1910.151 – Medical Services and First Aid</li><li>29 CFR 1926.23 – First Aid and Medical Attention</li><li>29 CFR 1926.50 – Construction Medical Services and First Aid</li><li>29 CFR 1915.1030 – Shipyard Safety and Health</li><li>1926.20 – General Safety and Health Provisions, training</li><li>29 CFR 1926.21 – Training and Education</li><li>ANSI Z308.1 – First Aid Kits and Supplies</li></ul><div><strong>Canada<br></strong><br></div><ul><li>SOR/86-304, 16.1 – First Aid</li><li>SOR/86-304, 16.2 – General</li><li>SOR/86-304, 16.3 – First Aid Attendants</li><li>SOR/86-304, 16.5 – First Aid Stations</li><li>SOR/86-304, 16.6 – Communication of Information</li><li>SOR/86-304, 16.7 – First Aid Supplies and Equipment</li><li>SOR/86-304, 16.9 – First Aid Rooms</li><li>SOR/86-304, 16.11 – Transportation</li><li>SOR/86-304, 16.12 – Teaching First Aid</li><li>SOR/86-304, 16.13 – Records</li></ul>",
    facts: "<ul><li>Brain damage can occur within 4 to 6 minutes after a person starts choking. (Source: <a href=\"https://healthfinder.gov/HealthTopics/Category/everyday-healthy-living/safety/learn-first-aid\"><em>Healthfinder.gov</em></a>)</li><li>You should never utilize a tourniquet to control blood flow unless the victim has lost a limb or experienced a similar extreme emergency. (Source: <a href=\"http://nasdonline.org/301/d000105/basic-first-aid-script.html\"><em>NASD</em></a>)</li><li>In the last phases of hypothermia, the victim may feel as if they are too hot and start to undress themselves. This condition is called paradoxical undressing. (Source: <a href=\"https://www.nsc.org/home-safety/tools-resources/seasonal-safety/winter/frostbite\"><em>NSC</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.safetyandhealthmagazine.com/articles/18026-most-americans-not-sure-how-to-use-an-aed-survey-shows\">Most Americans Not Sure How to Use an AED, Survey Shows<br></a><a href=\"https://www.dailymail.co.uk/news/article-6692169/Doctor-saves-life-fellow-hockey-player-collapsed-cardiac-arrest-game.html\">Heroic Doctor Performs a 'Miracle' on Ice Saving the Life of His Fellow Hockey Player Who Went into Cardiac Arrest and Collapsed During a Game<br></a><a href=\"https://www.youtube.com/watch?v=CjBxk-3_XlA\">College Girl Performs CPR on Elderly Man at Train Station<br></a><a href=\"https://www.youtube.com/watch?v=NiS-43VZa3M\">Man Saved by AED (News Story)<br></a><a href=\"https://hardhattraining.com/bleeding-injury-first-aid/\">Emergency Preparedness: Bleeding Injury First Aid<br></a><a href=\"https://hardhattraining.com/injury-preparedness/\">Injury Preparedness</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "HAZWOPER 24",
    overview: "<div>Our HAZWOPER course meets the initial 24-hour safety training requirement. This training includes the&nbsp; 2013 updates of the Globally Harmonized System (GHS) for Hazard Communication, as well as information on hazard recognition; site characterization and monitoring; confined space work; site testing; medical surveillance; toxicology; material handling; excavation; safe work practices; PPE; decontamination; and emergency procedures.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes checklists for employers to use when administering practical exams as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards reduction:<br><br></div><ul><li>29 CFR 1910.120</li><li>GHS Hazard Communication Standards</li></ul><div>In accordance with OSHA’s HAZWOPER standard, employees who may be exposed to hazardous substances or situations must obtain training. These employees maybe be engaged in operations including clean-up, treatment, storage, and disposal of hazardous waste are required to take the initial HAZWOPER training which consists of a 24 or 40-hour course. Depending on your job type and experience, you may be able to take the 24-hour course to meet your HAZWOPER training requirement.&nbsp;<br><br></div><div>When it comes to refresher training for HAZWOPER, OSHA’s standard is very specific: workers must be re-evaluated every year to ensure they are still competent to work safely around hazardous materials. The extent of the evaluation is to be determined by the employer but should include a written and practical examination that prove continued competency.</div>",
    facts: "<div>OSHA has offered the following guidelines to clarify what they consider to be a hazardous substance or situation:&nbsp;<br><br></div><ul><li>High concentrations of toxic substances;</li><li>Situation that is life or injury threatening;</li><li>Imminent Danger to Life and Health (IDLH) environments;</li><li>Situation that presents an oxygen deficient atmosphere;</li><li>Condition that poses a fire or explosion hazard;</li><li>Situation that required an evacuation of the area;</li><li>A situation that requires immediate attention because of the danger posed to employees in the area.</li></ul>",
    fails: "<div><a href=\"https://www.natlawreview.com/article/august-21-effective-date-new-epa-standards-management-hazardous-waste\">New EPA Standards for Management for Hazardous Waste Pharmaceuticals<br></a><a href=\"https://www.fox13memphis.com/top-stories/4-employees-injured-after-fire-at-hazardous-waste-disposal-company-in-millington/974495222\">4 Employees Injured After Fire at Hazardous Waste Disposal Company<br></a><a href=\"https://youtu.be/b0FmNROj3aU\">Improper Waste Disposal Causes Toxic Chemical Spill<br></a><a href=\"https://youtu.be/JYqH-cxX-ug\">Train Derailment Spills Hazardous Waste<br></a><a href=\"https://hardhattraining.com/hazardous-gases-and-using-hazcom-methods-to-protect-its-users/\">Hazardous Gases &amp; Using HazCom Methods<br></a><a href=\"https://hardhattraining.com/reactive-chemical-explosions-die/\">Reactive Chemicals—Explosions to Die For</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "HAZWOPER 8",
    overview: "<div>This HAZWOPER course meets the 8-hour refresher safety training requirement. It includes the 2013 updates of the Globally Harmonized System (GHS) for Hazard Communication, as well as information on hazard recognition; site characterization and monitoring; confined space work; site testing; medical surveillance; toxicology; safe work practices; PPE; decontamination; and emergency procedures.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes checklists for employers to use when administering practical exams as required by OSHA.</div>",
    training_standards: "<div>Although you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards reduction:<br><br></div><ul><li>29 CFR 1910.120</li><li>GHS Hazard Communication Standards</li></ul><div>When it comes to refresher training for HAZWOPER, OSHA’s standard is very specific: workers must be re-evaluated every year to ensure they are still competent to work safely around hazardous materials.&nbsp;<br><br></div><div>The HAZWOPER standard applies to any employees who are exposed to hazardous substances and who are engaged in several operations including clean-up, treatment, storage, and disposal of hazardous waste. Furthermore, workers who may be involved in an emergency response operation dealing with hazardous waste should be fully trained in HAZWOPER.&nbsp;<br><br></div><div>Specifically, OSHA has offered these guidelines about what constitutes a hazardous substance or situation:<br><br></div><ul><li>High concentrations of toxic substances.</li><li>Situation that is life or injury threatening.</li><li>Imminent Danger to Life and Health (IDLH) environments.</li><li>Situation that presents an oxygen deficient atmosphere.</li><li>Condition that poses a fire or explosion hazard.</li><li>Situation that required an evacuation of the area.</li><li>A situation that requires immediate attention because of the danger posed to employees in the area.</li></ul>",
    facts: "<ul><li>Hazardous material removal is a growing industry; it is projected to grow 17% by 2016. (<a href=\"https://www.bls.gov/ooh/construction-and-extraction/hazardous-materials-removal-workers.htm\">Bureau of Labor Statistics</a>)</li><li>The Toxic Substances Control Act (TSCA) Chemical Substance Inventory contains all existing chemical substances manufactured, processed, or imported in the United States that do not qualify for an exemption or exclusion under TSCA. (<a href=\"https://www.epa.gov/tsca-inventory\">EPA</a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.natlawreview.com/article/august-21-effective-date-new-epa-standards-management-hazardous-waste\">New EPA Standards for Management for Hazardous Waste Pharmaceuticals<br></a><a href=\"https://www.fox13memphis.com/top-stories/4-employees-injured-after-fire-at-hazardous-waste-disposal-company-in-millington/974495222\">4 Employees Injured After Fire at Hazardous Waste Disposal Company<br></a><a href=\"https://youtu.be/b0FmNROj3aU\">Improper Waste Disposal Causes Toxic Chemical Spill<br></a><a href=\"https://youtu.be/JYqH-cxX-ug\">Train Derailment Spills Hazardous Waste<br></a><a href=\"https://hardhattraining.com/hazardous-gases-and-using-hazcom-methods-to-protect-its-users/\">Hazardous Gases &amp; Using HazCom Methods<br></a><a href=\"https://hardhattraining.com/reactive-chemical-explosions-die/\">Reactive Chemicals—Explosions to Die For</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "HIPAA",
    overview: "<div>Our Health Insurance Portability and Accountability Act (HIPAA) training course is built to OSHA standards. This class discusses topics including the portability, privacy rule, security rule, breach notification, enforcement, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following standards for HIPAA:<br><br></div><ul><li>45 C.F.R. Part 160 – Privacy Rule (General Administration Requirements)</li><li>45 C.F.R. Part 162 – Administration Requirements&nbsp;</li><li>45 C.F.R. Part 164 – Security and Privacy Rules</li><li>45 C.F.R. Part 164.308 (a)(5) &amp; 164.530 (b)(1) – Training</li></ul><div>OSHA doesn’t have a specific standard for HIPAA training. However, under the General Duty Clause, Section 5(a)(1) of the Occupational Safety and Health Act of 1970, employers are required to provide a workplace that “is free from recognizable hazards that are causing or likely to cause death or serious harm to employees.”<br><br></div><div>Because of this requirement, employers have a legal and ethical obligation to develop and maintain a workplace that is free from hazards associated with HIPAA. Employees have the right to work in an atmosphere that promotes the safety and well-being of all.</div>",
    facts: "<ul><li>HIPAA doesn’t allow medical or healthcare to share personal information to an employer without the employee’s written consent.&nbsp;</li><li>Under HIPAA, a family can act on a patient’s behalf in picking up medical supplies.&nbsp;</li><li>The transfer of medical records from doctor’s offices for treatment does not require patient consent.&nbsp;</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.healthcare-informatics.com/news-item/cybersecurity/ocr-fines-providers-hipaa-violations-failure-follow-basic-security\">Florida Provider Pays $500K to Settle Potential HIPAA Violations<br></a><a href=\"https://www.hipaajournal.com/massachusetts-attorney-general-issues-75000-hipaa-violation-fine-to-mclean-hospital/\">Massachusetts Attorney General Issues $75,000 HIPAA Violation Fine to McLean Hospital<br></a><a href=\"https://www.youtube.com/watch?v=UQV_fstve_o\">McAlester Hospital Sued for HIPAA Violations After Toddler's Death<br></a><a href=\"https://www.youtube.com/watch?v=qBGJCkaXKNQ\">State Dept. accused of HIPAA violations</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Lawnmower",
    overview: "<div>Our Lawnmower safety training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on anatomy, stability, operations, hazards, and more.<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for lawn mowers:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.136 – Foot Protection</li><li>29 CFR 1926.138 – Hand Protection</li><li>29 CFR 1926 Subpart W – Rollover Protective Structures</li><li>29 CFR 1926.20 – General Safety and Health Provisions, training</li><li>29 CFR 1926.21 – Training and Education</li><li>OSHA Act of 1970, 5(a)(1) – “Each employer shall furnish to each of his employees…a place of employment which is free from recognized hazards that are causing or are likely to cause death or serious physical harm to his employees.”</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CAN/CSA-M11684-97 – Tractors, Machinery for Agriculture and Forestry, Powered Lawn and Garden Equipment</li><li>CAN/CSA Z94.3-07 – Eye and Face Protectors</li><li>CAN/CSA Z94.1 – Protective Headwear.</li><li>CAN/CSA Z195-09 – <em>Protective Footwear</em></li><li>CAN/CSA Z94.2.02 – <em>Hearing Protection Devices (Performance Selection, Care and Use)</em></li><li>CAN/CSA-C22.2 NO. 62841-1:15 – Electric motor-operated hand-held tools, transportable tools and lawn and garden machinery</li><li>CAN/CSA-C22.2 NO. 62841-2-8:16 - Electric motor-operated hand-held tools, transportable tools and lawn and garden machinery - Safety - Part 2-8: Particular requirements for hand-held shears</li></ul>",
    facts: "<ul><li>The average American spends four hours per week working on lawn-related activities.</li><li>There is a British Lawnmower Museum.</li><li>Powered lawn mowers cause 68,000 injuries every year. (Source: <a href=\"https://blog.lawneq.com/10-fun-facts-about-lawns-and-lawn-mowers/\"><em>Lawn EQ</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.freep.com/story/news/local/michigan/2018/06/09/toddler-lawn-mower-accident-safety/687435002/\">Toddler Injured in Horrific Lawn Mower Accident Northeast of Metro Detroit<br></a><a href=\"https://www.ajc.com/news/national/year-old-loses-foot-lawnmower-accident/JHWQBQxVBo2k1U80ntjWSK/\">5-year-old Girl Loses Foot in Lawn Mower Accident<br></a><a href=\"https://www.youtube.com/watch?v=XXVdqzSrx6g\">Fatal Lawn Mower Accident (KMTV 3)<br></a><a href=\"https://www.youtube.com/watch?v=1Qk9Oy_AwS0\">News in 90: Lawn Mower Accident<br></a><a href=\"https://hardhattraining.com/lawn-mower-safety-tips/\">Lawnmower Safety Tips</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Lockout/Tagout",
    overview: "<div>Our Lock Out/Tag Out (LOTO) Safety Training course is regulation compliant, and our online version fulfills classroom training requirements. Each class contains sections on equipment, operations, hazards, case studies, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, provincial, territorial, and local standards, this training encompasses the following standards for LOTO:<br><br></div><div><strong>U.S.<br></strong><br></div><ul><li>1910.147 – The Control of Hazardous Energy (LOTO) &amp; App A</li><li>1910 Subpart R – Special Industries</li><li>1910.261 – Pulp, Paper, and Paperboard Mills&nbsp;</li><li>1910.269 – Electric Power Generation, Transmission, and Distribution&nbsp;</li><li>1910. Subpart S – Electrical</li><li>1910.306 – Specific Purpose Equipment and Installations</li><li>1910.333 – Selection and Use of Work Practices</li><li>ANSI Z244 – Control Hazardous Energy, Alternative LOTO procedures</li><li>1917 Subpart C – Cargo Handling Gear and Equipment</li><li>29 CFR 1918 – Longshoring&nbsp;</li><li>1918 Subpart G – Cargo Handling Gear and Equipment Other Than Ship’s Gear</li><li>1918.64 – Powered Conveyors</li><li>29 CFR 1926 – Construction Industry&nbsp;</li><li>1926 Subpart K – Electrical</li><li>1926.417 – Lockout and Tagging of Circuits</li><li>1926 Subpart Q – Concrete and Masonry Construction</li><li>1926.702 – Requirements for Equipment and Tools</li><li>29 CFR 1915 – Shipyards&nbsp;</li><li>1915 Subpart F – General Working Conditions</li></ul><div><strong>Canada<br></strong><br></div><ul><li>CSA Z460-05 Control of Hazardous Energy – Lockout &amp; Other Methods</li><li>B.C. – OHS Regulation Part 10, De-energization and lockout</li><li>Ontario – OHS Act, Section 6 – Lockout Procedures</li><li>Alberta – OHS Code Part 15, Managing the Control of Hazardous Energy</li><li>Manitoba – Workplace Safety &amp; Health Regulations, Part 16.14-16.18, Lockout&nbsp;</li><li>Nova Scotia – Safe Needles in Healthcare Workplaces Act</li><li>Saskatchewan – OHS Regulations section 139, Lock out</li></ul>",
    facts: "<ul><li>The oldest known lock is about 4,000 years old.&nbsp;</li><li>Harry Houdini was a locksmith’s apprentice as a young teen.&nbsp;</li><li>The HYT lock is supposedly unpickable due to its complex design. (Source: <a href=\"https://1800unlocks.com/40-fun-facts-about-locks-and-keys/\"><em>Locksmith Info</em></a>)</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.safetyandhealthmagazine.com/articles/17017-alternative-measures-for-lockouttagout\">Alternative Measures for Lockout/Tagout<br></a><a href=\"https://www.safetyandhealthmagazine.com/articles/16593-lockouttagout-inconsistencies\">Lockout/Tagout Inconsistencies<br></a><a href=\"https://www.youtube.com/watch?v=PKahjg3naxs\">What is Lockout Tagout?</a> <br><a href=\"https://www.youtube.com/watch?v=dr6yPLLp7cI\">Lockout Fatality-Nightline ABC News<br></a><a href=\"https://hardhattraining.com/hazardous-energy-future-lockouttagout/\">Hazardous Energy: The Future of Lockout/Tagout<br></a><a href=\"https://hardhattraining.com/lockouttagout-rule-change-game/\">Lockout/Tagout, and the Rule to Change the Game</a></div>"
)

ProductGroup.create(
    image_data: uploaded_product_group_image.to_json,
    video: "MjA0NjIw",
    name: "Mini Excavator",
    overview: "<div>Our Mini Excavator safety training course is OSHA compliant, and our online version fulfills OSHA’s classroom training requirement. Each class contains sections on anatomy, stability, operations, common hazards, and more.&nbsp;<br><br></div><div>This presentation includes intermittent practice quiz questions to prepare for the final written exam included with the course. In addition to the written exam, this course also includes a checklist for employers to use when administering a practical exam as required by OSHA.</div>",
    training_standards: "<div>Though you will still need to familiarize yourself with all other applicable federal, state, and local standards, this training encompasses the following OSHA standards for mini excavators:<br><br></div><ul><li>29 CFR 1926.600 – Equipment</li><li>29 CFR 1926.602 – Material Handling Equipment</li><li>29 CFR 1926.604 – Site Clearing</li><li>29 CFR 1926.650-652 – Excavations</li><li>29 CFR 1926, Subpart P, App A – Soil Classification</li><li>29 CFR 1926, Subpart P, App F – Protective Systems</li></ul>",
    facts: "<ul><li>A mini-excavator only differs from a regular excavator by its weight.&nbsp;</li><li>The only manufacturer of mini-excavators is in the United States under several company names.&nbsp;</li><li>The first excavator, called the steam shovel, was used in 1796.</li></ul><div><br></div>",
    fails: "<div><a href=\"https://www.parkrecord.com/news/tooele-man-pinned-under-excavator-in-eastern-summit-county-miraculously-escapes-injury/\">Tooele Man Pinned Under Excavator in Eastern Summit County ‘Miraculously’ Escapes Injury<br></a><a href=\"https://www.boston.com/news/local-news/2018/01/18/gloucester-construction-worker-killed\">Construction Worker Killed While Operating Mini-Excavator in Gloucester<br></a><a href=\"https://youtu.be/3YBF5QmM6wE\">Erin Kelly on Fatal Excavator Accident<br></a><a href=\"https://www.youtube.com/watch?v=TQ5gwuV7rDA\">So They Stuck The Mini Excavator<br></a><a href=\"https://hardhattraining.com/excavator-hits-power-line-causes-fire/\">Excavator Hits Power Line, Causes Fire<br></a><a href=\"https://hardhattraining.com/360-excavator-recognizing-top-6-hazards/\">360 Excavator: Recognizing the Top 6 Hazards</a></div>"
)

# Create image derivatives
ProductGroup.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE ONLINE COURSES #######
eval_uploader = PracticalEvaluationUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/blank.pdf'))
uploaded_practical_evaluation = eval_uploader.upload(file)

exam_uploader = ExamUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/blank.pdf'))
uploaded_exam = exam_uploader.upload(file)

answer_key_uploader = AnswerKeyUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/blank.pdf'))
uploaded_answer_key = answer_key_uploader.upload(file)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Aerial Lift/MEWP (Boom & Scissor)",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 82,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Scissor Lift",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 381,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Ladder Safety",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 162,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Scaffolding",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 97,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Ladder Safety",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 158,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Fall Protection",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 293,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Ladder Safety",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 159,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Aerial Lift/MEWP (Boom & Scissor)",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 132,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bucket Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 105,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Aerial Lift/MEWP (Boom & Scissor)",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 102,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Fall Protection",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 215,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bucket Truck",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 131,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Fall Protection",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 218,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bucket Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 96,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Scaffolding",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 112,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Scaffolding",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 138,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Rigger Signaler",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 147,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Boom Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 72,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Digger Derrick",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 238,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Scissor Lift",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 102,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Rigger Signaler",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 122,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Pedestal Crane",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 110,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Boom Truck",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 142,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Boom Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 104,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Knuckle Boom",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 144,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Digger Derrick",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 228,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Carry Deck Crane",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 369,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Knuckle Boom",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 107,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Knuckle Boom",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 73,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lattice Boom",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 91,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Overhead Crane",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 140,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lattice Boom",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 148,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Overhead Crane",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 74,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lattice Boom",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 325,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Pedestal Crane",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 133,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Overhead Crane",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 109,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Pedestal Crane",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 75,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Rigger Signaler",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 123,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Signal Person",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 193,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Backhoe",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 139,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Amphibious Excavator",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 353,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Asphalt",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 378,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Asphalt",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 360,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Amphibious Excavator",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 354,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Backhoe",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 99,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Backhoe",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 103,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Directional Drill",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 304,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bulldozer",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 197,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Concrete Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 365,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bulldozer",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 198,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bulldozer",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 202,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Directional Drill",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 300,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Dump Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 149,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Dump Truck",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 145,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "360 Excavator",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 136,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "360 Excavator",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 98,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Dump Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 155,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "360 Excavator",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 106,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Grader",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 240,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Front End Loader",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 124,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Front End Loader",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 125,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Front End Loader",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 135,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Mini Excavator",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 426,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Grader",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 289,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Grader",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 247,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Roller Compactor",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 364,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Skid Steer",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 100,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Roller Compactor",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 453,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Skid Steer",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 134,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Trench Safety",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 101,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Skid Steer",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 114,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Trench Safety",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 137,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Trench Safety",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 117,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Trencher",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 321,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Trencher",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 382,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Trencher",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 362,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Arc Flash",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 236,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Arc Flash",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 220,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Arc Flash",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 423,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lockout/Tagout",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 157,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Pallet Jack",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 203,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lockout/Tagout",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 152,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Pallet Jack",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 194,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lockout/Tagout",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 161,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Pallet Jack",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 195,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Sit Down Forklift",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 129,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Sit Down Forklift",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 93,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Sit Down Forklift",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 113,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Stand Up Forklift",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 94,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Stand Up Forklift",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 146,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Telehandler",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 95,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Stand Up Forklift",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 115,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Telehandler",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 130,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "ATV/UTV",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 315,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Telehandler",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 116,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Garbage Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 383,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "ATV/UTV",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 316,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Garbage Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 384,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Vacuum Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 344,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Ice Resurfacer",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 350,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Vacuum Truck",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 352,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Ice Resurfacer",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 351,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Snowcat",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 446,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Landscaping",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 248,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lawnmower",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 291,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lawnmower",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 249,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Landscaping",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 290,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Snowcat",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 380,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Street Sweeper",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 371,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Drug and Alcohol Free-Workplace",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 329,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Street Sweeper",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 372,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bloodborne Pathogens",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 153,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bloodborne Pathogens",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 154,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Street Sweeper",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 373,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bloodborne Pathogens",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 160,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Drug and Alcohol Free-Workplace",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 422,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "First Aid & CPR",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 234,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Ergonomics",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 301,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "First Aid & CPR",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 233,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Ergonomics",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 305,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Ergonomics",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 376,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "First Aid & CPR",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 317,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Heat & Cold Stress",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 385,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Mental Health",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 394,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Workplace Violence",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 307,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Sexual Harassment",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 243,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Mental Health",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 403,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Opioid Awareness",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 419,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Sexual Harassment",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 292,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Workplace Violence",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 309,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Workplace Civility",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 326,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Asbestos",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 401,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Workplace Stress",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 395,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Workplace Civility",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 399,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Workplace Civility",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 388,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Asbestos",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 298,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Asbestos",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 303,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "HazCom",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 204,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Carbon Monoxide",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 356,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Carbon Monoxide",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 357,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Carbon Monoxide",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 420,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "HazCom",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 205,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "HazCom",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 209,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "HAZWOPER 8",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 375,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Mold",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 424,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "HAZWOPER 24",
    price: 15800,
    cad_price: 21000,
    language: "english",
    standards: "osha",
    talent_course_id: 425,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Mold",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 366,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Lab Safety",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 565,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Mold",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 367,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Silica Awareness",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 299,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Silica Awareness",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 242,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Chainsaw",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 219,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bear Awareness",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 349,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Marijuana In The Workplace",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 451,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Marijuana In The Workplace",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 436,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Bear Awareness",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 391,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Chainsaw",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 216,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Chainsaw",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 217,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Defensive Driving",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 324,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Confined Space",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 126,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Defensive Driving",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 322,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Confined Space",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 127,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Confined Space",
    price: 7900,
    cad_price: 9900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 141,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Defensive Driving",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 604,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Emergency Response",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 346,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "HIPAA",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 421,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Emergency Response",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 392,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Hand & Power Tools",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 347,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Hand & Power Tools",
    price: 2900,
    cad_price: 3900,
    language: "spanish",
    standards: "osha",
    talent_course_id: 377,
    market_type: "spanish",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Hand & Power Tools",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 343,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "HIPAA",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 396,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Machine Guarding",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 229,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Machine Guarding",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 295,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "PPE",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 239,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "PPE",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 231,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Walking & Working Surfaces",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 288,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Record Keeping",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "osha",
    talent_course_id: 386,
    market_type: "usa",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Walking & Working Surfaces",
    price: 2900,
    cad_price: 3900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 294,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Welding",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "csa_ansi",
    talent_course_id: 237,
    market_type: "canada",
    public: true
)

OnlineCourse.create(
    product_group_id: ProductGroup.first.id,
    sale_start: Date.new(2019,12,23),
    sale_expiry: Date.new(2019,12,23),
    practical_evaluation_data: uploaded_practical_evaluation.to_json,
    exam_data: uploaded_exam.to_json,
    answer_key_data: uploaded_answer_key.to_json,
    title: "Welding",
    price: 7900,
    cad_price: 9900,
    language: "english",
    standards: "osha",
    talent_course_id: 232,
    market_type: "usa",
    public: true
)

####### CREATE KITS #######
sample_uploader = PracticalEvaluationUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/blank.zip'))
uploaded_sample = sample_uploader.upload(file)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Scaffolding",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Scaffolding.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Aerial Lift/MEWP (Boom & Scissor)",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Aerial-Lift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Aerial Lift/MEWP (Boom & Scissor)",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Aerial-Lift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bucket Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Bucket-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bucket Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Bucket-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Fall Protection",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Fall-Protection.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Fall Protection",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Fall-Protection.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Ladder Safety",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Ladder-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Ladder Safety",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Ladder-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Scaffolding",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Scaffolding.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Scissor Lift",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Scissor-Lift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Scissor Lift",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Scissor-Lift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Boom Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Boom-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Boom Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Boom-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Carry Deck Crane",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Carry-Deck-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Carry Deck Crane",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Carry-Deck-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Digger Derrick",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Digger-Derrick.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Digger Derrick",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Digger-Derrick.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Knuckle Boom",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Knuckle-Boom-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Knuckle Boom",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Knuckle-Boom.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lattice Boom",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Lattice-Boom-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Overhead Crane",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Overhead-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Overhead Crane",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Overhead-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Pedestal Crane",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Pedestal-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Pedestal Crane",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Pedestal-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Rigger Signaler",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Rigger-Signalman.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Rigger Signaler",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Rigger-Signalman.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Amphibious Excavator",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Amphibious-Excavator.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Amphibious Excavator",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Amphibious-Excavator.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Asphalt",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Asphalt-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Asphalt",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Asphalt-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Backhoe",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Loader-Backhoe.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bulldozer",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Bulldozer.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bulldozer",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Bulldozer.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Concrete Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Concrete-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Concrete Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Concrete-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Directional Drill",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Directional-Drill.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Directional Drill",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Directional-Drill.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Dump Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Dump-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Dump Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Dump-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "360 Excavator",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "360-Excavator.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "360 Excavator",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Excavator-Training.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lattice Boom",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Lattice-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Front End Loader",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Front-Loader.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Front End Loader",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Front-End-Loader.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Grader",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Grader.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Grader",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Grader.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Roller Compactor",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Roller-Compactor.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Skid Steer",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Skid-Steer.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Skid Steer",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Skid-Steer.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Trench",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Trench-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Trench",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Trench-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Trencher",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Trencher.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Trencher",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Trencher.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Arc Flash",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Arc-Flash.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Arc Flash",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Arc-Flash.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lockout/Tagout",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "LOTO.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lockout/Tagout",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "LOTO.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Pallet Jack",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Pallet-Jacks.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Pallet Jack",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Pallet-Jacks.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Sit Down Forklift",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Sit-Down-Forklift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Sit Down Forklift",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Sit-Down-Forklift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Stand Up Forklift",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Stand-Up-Forklift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Stand Up Forklift",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Stand-Up-Forklift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Telehandler",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Telehandler.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Telehandler",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Telescopic-Handler.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "ATV/UTV",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "ATV-UTV.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "ATV/UTV",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "ATV-UTV.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Garbage Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Garbage-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Garbage Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Garbage-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Vacuum Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Vacuum-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Vacuum Truck",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Vacuum-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Ice Resurfacer",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Ice-Resurfacer.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Ice Resurfacer",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Ice-Resurfacer.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Landscaping",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Landscaping-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lawnmower",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Lawn-Mower.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lawnmower",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Lawn-Mower.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Street Sweeper",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Street-Sweeper.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Street Sweeper",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Street-Sweeper.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bloodborne Pathogens",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "BBP.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bloodborne Pathogens",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "BBP.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Drug & Alcohol",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Drug-Alcohol.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Ergonomics",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Ergonomics.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Ergonomics",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Ergonomics.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "First Aid & CPR",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "First-Aid-CPR.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Landscaping",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Landscaping-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bucket Truck",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Bucket-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Opioid",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Opioid-Awareness.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Sexual Harassment",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Sexual-Harassment.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Sexual Harassment",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Sexual-Harassment.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Carbon Monoxide",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Carbon-Monoxide.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Workplace Violence",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Violence.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Workplace Violence",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Violence.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Workplace Civility",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Workplace-Civility.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Backhoe",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Loader-Backhoe.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Asbestos",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Asbestos.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Asbestos",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Asbestos.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Carbon Monoxide",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Carbon-Monoxide.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Hazard Communications (HazCom)",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "HazCom.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Hazard Communications (HazCom)",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "HazCom.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lab Safety",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Lab-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lab Safety",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Lab-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Mold",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Mold.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Mould",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Mould.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Silica",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Silica.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Silica",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Silica.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bear Awareness",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Bear-Awareness.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bear Awareness",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "canada",
    download: "Bear-Awareness.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "First Aid & CPR",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "First-Aid-CPR.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Marijuana in the Workplace",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Cannabis-Awareness.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Chainsaw",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Chainsaw.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Chainsaw",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Chainsaw.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Confined Space",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Confined-Space.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Confined Space",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Confined-Space.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Defensive Driving",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Defensive-Driving.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Defensive Driving",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Defensive-Driving.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Emergency Response",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Emergency-Response.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Hand & Power Tools",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Hand-And-Power-Tools.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Hand & Power Tools",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Hand-And-Power-Tools.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "HIPAA",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "HIPAA.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Machine Guarding",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Machine-Guarding.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Machine Guarding",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Machine-Guarding.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "PPE",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "PPE.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "PPE",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "PPE.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Record Keeping",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Record-Keeping.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Walking & Working Surfaces",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Walking-Working.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Walking & Working Surfaces",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Walking-Working.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Welding",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Welding.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Emergency Response",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Emergency-Response.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Marijuana in the Workplace",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "csa_ansi",
    market_type: "canada",
    download: "Cannabis-Awareness.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Workplace Civility",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Workplace-Civility.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Welding",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Welding.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Aerial Lift/MEWP (Boom & Scissor)",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Aerial-Lift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Backhoe",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Backhoe-Loader.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Fall Protection",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Fall-Protection.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Ladder Safety",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Ladder-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Scaffolding",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Scaffolding.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Boom Truck",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Boom-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Knuckle Boom",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Knuckle-Boom.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lattice Boom",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Lattice-Boom.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Overhead Crane",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Overhead-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Asphalt",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Asphalt-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Trench",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Trench-Safety.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bulldozer",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Bulldozer.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Dump Truck",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Dump-Truck.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "360 Excavator",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Excavator.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Front End Loader",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Front-Loader.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Grader",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Grader.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Arc Flash",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Arc-Flash.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Trencher",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Trencher.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Pedestal Crane",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Pedestal-Crane.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Mold",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Mold.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Drug & Alcohol",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Drug-Alcohol.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Ergonomics",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Ergonomics.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Bloodborne Pathogens",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "BBP.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Carbon Monoxide",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Carbon-Monoxide.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Hazard Communications (HazCom)",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "HazCom.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Pallet Jack",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Pallet-Jack.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Telehandler",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Telehandler.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Chainsaw",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Chainsaw.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Confined Space",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Confined-Space.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Defensive Driving",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Defensive-Driving.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Hand & Power Tools",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Hand-And-Power-Tools.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Lockout/Tagout",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "LOTO.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Rigger Signaler",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Rigger-Signaler.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Record Keeping",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Record-Keeping.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Silica",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Silica.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Asbestos",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Asbestos.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "HIPAA",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "HIPAA.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Mental Health",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Mental-Health.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Street Sweeper",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Street-Sweeper.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Mental Health",
    price: 39900,
    cad_price: 47500,
    language: "english",
    standards: "osha",
    market_type: "usa",
    download: "Mental-Health.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Workplace Civility",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Workplace-Civility.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Roller Compactor",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Roller-Compactor.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Sit Down Forklift",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Sit-Down-Forklift.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Skid Steer",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Skid-Steer.zip",
    sample_data: uploaded_sample.to_json
)

Kit.create(
    product_group_id: ProductGroup.first.id,
    title: "Stand Up Forklift",
    price: 39900,
    cad_price: 47500,
    language: "spanish",
    standards: "osha",
    market_type: "spanish",
    download: "Stand-Up-Forklift.zip",
    sample_data: uploaded_sample.to_json
)


####### ASSIGN CORRECT PRODUCT GROUPS TO ONLINE COURSES #######
online_courses = OnlineCourse.all
pgs_to_reset = []
online_courses.each do |oc|
  pg = ProductGroup.find_by(name: oc.title)
  oc.update(product_group: pg)
end

####### ASSIGN CORRECT PRODUCT GROUPS TO KITS #######
kits = Kit.all
pgs_to_reset = []
kits.each do |kit|
  pg = ProductGroup.find_by(name: kit.title)
  kit.update(product_group: pg)
end

####### ASSIGN CATEGORIES  TO PRODUCT GROUPS #######
# These are the 77 ProductGroups that are live as of 2019-12-24
product_group_array = [
["360 Excavator", "EARTH MOVERS"],

["Aerial Lift/MEWP (Boom & Scissor)", "AERIAL LIFTS/AT HEIGHTS"],
["Amphibious Excavator", "EARTH MOVERS"],
["Arc Flash", "ELECTRICAL"],
["Asbestos", "INDUSTRIAL HYGIENE"],
["Asphalt", "EARTH MOVERS"],
["ATV/UTV", "OTHER MOBILE EQUIPMENT"],

["Backhoe Loader", "EARTH MOVERS"],
["Bear Awareness", "OTHER"],
["Bloodborne Pathogens", "HEALTH/HUMAN RESOURCES"],
["Boom Truck", "CRANES/RIGGING"],
["Bucket Truck", "AERIAL LIFTS/AT HEIGHTS", "ELECTRICAL"],
["Bulldozer", "EARTH MOVERS"],

["Carbon Monoxide", "INDUSTRIAL HYGIENE"],
["Carry Deck Crane", "CRANES/RIGGING"],
["Chainsaw", "OTHER"],
["Concrete Truck", "EARTH MOVERS"],
["Confined Space", "OTHER"],

["Defensive Driving", "OTHER"],
["Digger Derrick", "CRANES/RIGGING"],
["Directional Drill", "EARTH MOVERS"],
["Dump Truck", "EARTH MOVERS"],
["Drug & Alcohol", "HEALTH/HUMAN RESOURCES"],

["Emergency Response", "OTHER"],
["Ergonomics", "HEALTH/HUMAN RESOURCES"],

["Fall Protection", "AERIAL LIFTS/AT HEIGHTS"],
["First Aid & CPR", "HEALTH/HUMAN RESOURCES"],
["Front End Loader", "EARTH MOVERS"],

["Garbage Truck", "OTHER MOBILE EQUIPMENT"],
["Grader", "EARTH MOVERS"],

["Hand & Power Tools", "OTHER"],
["Hazard Communications (HazCom)", "INDUSTRIAL HYGIENE"],
["HAZWOPER 24", "INDUSTRIAL HYGIENE"],
["HAZWOPER 8", "INDUSTRIAL HYGIENE"],
["Heat & Cold Stress", "HEALTH/HUMAN RESOURCES"],
["HIPAA", "OTHER"],

["Ice Resurfacer", "OTHER MOBILE EQUIPMENT"],

["Knuckle Boom", "CRANES/RIGGING"],

["Lab Safety", "INDUSTRIAL HYGIENE"],
["Ladder Safety", "AERIAL LIFTS/AT HEIGHTS"],
["Landscaping", "OTHER MOBILE EQUIPMENT"],
["Lattice Boom", "CRANES/RIGGING"],
["Lawnmower", "OTHER MOBILE EQUIPMENT"],
["Lockout/Tagout", "ELECTRICAL"],

["Machine Guarding", "OTHER"],
["Marijuana in the Workplace", "OTHER"],
["Mental Health", "HEALTH/HUMAN RESOURCES"],
["Mini Excavator", "EARTH MOVERS"],
["Mold", "INDUSTRIAL HYGIENE"],

["Opioid Awareness", "HEALTH/HUMAN RESOURCES"],
["Overhead Crane", "CRANES/RIGGING"],

["Pallet Jack", "FORKLIFTS"],
["Pedestal Crane", "CRANES/RIGGING"],
["PPE", "OTHER"],

["Record Keeping", "OTHER"],
["Rigger Signaler", "CRANES/RIGGING"],
["Roller Compactor", "EARTH MOVERS"],

["Scaffolding", "AERIAL LIFTS/AT HEIGHTS"],
["Scissor Lift", "AERIAL LIFTS/AT HEIGHTS"],
["Self-Defense", "OTHER"],
["Sexual Harassment", "HEALTH/HUMAN RESOURCES"],
["Signal Person", "CRANES/RIGGING"],
["Silica Awareness", "INDUSTRIAL HYGIENE"],
["Sit Down Forklift", "FORKLIFTS"],
["Skid Steer", "EARTH MOVERS"],
["Snowcat", "OTHER MOBILE EQUIPMENT"],
["Stand Up Forklift", "FORKLIFTS"],
["Street Sweeper", "OTHER MOBILE EQUIPMENT"],

["Telehandler", "FORKLIFTS"],
["Trench Safety", "EARTH MOVERS"],
["Trencher", "EARTH MOVERS"],

["Vacuum Truck", "OTHER MOBILE EQUIPMENT"],

["Walking & Working Surfaces", "OTHER"],
["Welding", "OTHER"],
["Workplace Civility", "HEALTH/HUMAN RESOURCES"],
["Workplace Violence", "HEALTH/HUMAN RESOURCES"],
["Workplace Stress", "HEALTH/HUMAN RESOURCES"]
]

product_group_array.each do |pg|
  product_group = ProductGroup.find_by(name: pg[0])
  tag = pg[1]
  product_group.tag_list.add(tag)
  product_group.save
end

####### CREATE CREDIT PACKS #######

CreditPack.create(
    name: '$79x25',
    price: 187500,
    credits: 197500,
    cad_price: 0,
    cad_credits: 0
)

CreditPack.create(
    name: '$79x50',
    price: 325000,
    credits: 395000,
    cad_price: 0,
    cad_credits: 0
)

CreditPack.create(
    name: '$79x100',
    price: 450000,
    credits: 790000,
    cad_price: 0,
    cad_credits: 0
)

CreditPack.create(
    name: '$99x25',
    price: 0,
    credits: 0,
    cad_price: 237500,
    cad_credits: 247500
)

CreditPack.create(
    name: '$99x50',
    price: 0,
    credits: 0,
    cad_price: 425000,
    cad_credits: 495000
)

CreditPack.create(
    name: '$99x100',
    price: 0,
    credits: 0,
    cad_price: 650000,
    cad_credits: 990000
)

CreditPack.create(
    name: '$29x25',
    price: 62500,
    credits: 72500,
    cad_price: 0,
    cad_credits: 0
)

CreditPack.create(
    name: '$29x50',
    price: 100000,
    credits: 145000,
    cad_price: 0,
    cad_credits: 0
)

CreditPack.create(
    name: '$29x100',
    price: 150000,
    credits: 290000,
    cad_price: 0,
    cad_credits: 0
)

CreditPack.create(
    name: '$39x25',
    price: 0,
    credits: 0,
    cad_price: 87500,
    cad_credits: 97500
)

CreditPack.create(
    name: '$39x50',
    price: 0,
    credits: 0,
    cad_price: 150000,
    cad_credits: 195000
)

CreditPack.create(
    name: '$39x100',
    price: 0,
    credits: 0,
    cad_price: 250000,
    cad_credits: 390000
)

####### CREATE GROUP TRAININGS #######
online_course_ids = OnlineCourse.all.pluck(:id)
online_course_ids.each do |id|
  GroupTraining.create(
    online_course_id: id
  )
end

####### CREATE KEYCHAIN GROUPS #######
uploader = KeychainGroupImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/keychain_group_image.jpg'))
uploaded_keychain_group_image = uploader.upload(file)

KeychainGroup.create(
  name: "CPR Keychain",
  description: "Life-saving protection that goes everywhere you go. These compact keychains each contain a single-use one-way valve CPR barrier mask. Always be prepared by keeping one in every vehicle and/or piece of machinery you own.

Each CPR Keychain includes:
• Nylon pouch with key ring
• Single-use one-way CPR barrier mask
• Available in black, orange and red
• FDA 510(k) approved",
  image_data: uploaded_keychain_group_image.to_json,
  # slug: ""
  keywords: "cpr, cpr keychain, cpr barrier mask",
  meta_description: "These compact keychains each contain a single-use one-way valve CPR barrier mask."
)

# Create image derivatives
KeychainGroup.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE KEYCHAINS #######
Keychain.create(
  name: "CPR Keychain (Pack of 5)",
  price: 1000,
  color: "Red, Orange, Black, Variety",
  cad_price: 1500,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  keychain_group_id: KeychainGroup.first.id,
  weight: 150
)
Keychain.create(
  name: "CPR Keychain (Pack of 50)",
  price: 5000,
  color: "Red, Orange, Black, Variety",
  cad_price: 6500,
  shipping_price: 1000,
  priority_shipping: 1500,
  intl_shipping: 1500,
  keychain_group_id: KeychainGroup.first.id,
  weight: 1500
)
Keychain.create(
  name: "CPR Keychain (Pack of 10)",
  price: 1500,
  color: "Red, Orange, Black, Variety",
  cad_price: 2000,
  shipping_price: 750,
  priority_shipping: 1000,
  intl_shipping: 1000,
  keychain_group_id: KeychainGroup.first.id,
  weight: 300
)
Keychain.create(
  name: "CPR Keychain (Pack of 100)",
  price: 10000,
  color: "Red, Orange, Black, Variety",
  cad_price: 15000,
  shipping_price: 1000,
  priority_shipping: 1500,
  intl_shipping: 1500,
  keychain_group_id: KeychainGroup.first.id,
  weight: 3000
)
Keychain.create(
  name: "CPR Keychain (Pack of 200)",
  price: 20000,
  color: "Red, Orange, Black, Variety",
  cad_price: 30000,
  shipping_price: 1000,
  priority_shipping: 1500,
  intl_shipping: 1500,
  keychain_group_id: KeychainGroup.first.id,
  weight: 6000
)
Keychain.create(
  name: "CPR Keychain (Pack of 25)",
  price: 2500,
  color: "Red, Orange, Black, Variety",
  cad_price: 3500,
  shipping_price: 1000,
  priority_shipping: 1500,
  intl_shipping: 1500,
  keychain_group_id: KeychainGroup.first.id,
  weight: 750
)

####### CREATE MACHINE TAG GROUPS #######
uploader = MachineTagGroupImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/machine_tag_group_image.jpg'))
uploaded_machine_tag_group_image = uploader.upload(file)

MachineTagGroup.create(
  name: "Pendant Warning Tags",
  description: "Our overhead crane pendant warning tags are made from a thick flexible plastic that won’t break easily. For OSHA and ANSI compliance (ANSI B30.16 Safety Code for hoists, and hoist manufacturer’s instructions), each wire rope or electric chain hoist should have one installed on the pendant. The pendant warning tag comes in English on one side, Spanish on the other. \r\n\r\nSize: approximately 9″ x 2.75″ and the thickness of a penny.\r\n\r\nInterested in unbranded pendant warning labels for resale? Contact us today or call (360) 930-9247.",
  image_data: uploaded_machine_tag_group_image.to_json,
  slug: "pendant-warning-tags",
  keywords: "Pendant Warning Tag",
  meta_description: "Overhead crane pendant warning tags made from a thick flexible plastic that won’t break easily."
)

# Create image derivatives
MachineTagGroup.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE MACHINE TAGS #######
MachineTag.create(
    name: "Pendant Warning Tag (Single)",
    price: 500,
    cad_price: 700,
    shipping_price: 500,
    priority_shipping: 1000,
    intl_shipping: 1000,
    machine_tag_group_id: MachineTagGroup.first.id,
    weight: 10
)

MachineTag.create(
    name: "Pendant Warning Tag (Pack of 5)",
    price: 2375,
    cad_price: 3000,
    shipping_price: 1000,
    priority_shipping: 1500,
    intl_shipping: 1500,
    machine_tag_group_id: MachineTagGroup.first.id,
    weight: 50
)

MachineTag.create(
    name: "Pendant Warning Tag (Pack of 10)",
    price: 4500,
    cad_price: 6000,
    shipping_price: 1000,
    priority_shipping: 1500,
    intl_shipping: 1500,
    machine_tag_group_id: MachineTagGroup.first.id,
    weight: 100
)

MachineTag.create(
    name: "Pendant Warning Tag (Pack of 25)",
    price: 10625,
    cad_price: 12500,
    shipping_price: 1000,
    priority_shipping: 1500,
    intl_shipping: 1500,
    machine_tag_group_id: MachineTagGroup.first.id,
    weight: 250
)

MachineTag.create(
    name: "Pendant Warning Tag (Pack of 50)",
    price: 20000,
    cad_price: 25000,
    shipping_price: 1000,
    priority_shipping: 1500,
    intl_shipping: 1500,
    machine_tag_group_id: MachineTagGroup.first.id,
    weight: 500
)

MachineTag.create(
    name: "Pendant Warning Tag (Pack of 100)",
    price: 37500,
    cad_price: 42500,
    shipping_price: 1000,
    priority_shipping: 1500,
    intl_shipping: 1500,
    machine_tag_group_id: MachineTagGroup.first.id,
    weight: 1000
)

####### CREATE BACKUPS #######
Backup.create(
  format: 'usb',
  price:  2500,
  cad_price: 3500,
  shipping_price: 1000,
  priority_shipping: 1500,
  intl_shipping: 1500
)

Backup.create(
  format: 'cd',
  price:  2500,
  cad_price: 3500,
  shipping_price: 1000,
  priority_shipping: 1500,
  intl_shipping: 1500
)

####### CREATE CERTIFICATES #######
uploader = CertificateImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/400x400.png'))
uploaded_certificate_image = uploader.upload(file)

Certificate.create(
  name: "Certificate of Completion",
  description: "<div>Both OSHA and OHS standards require proof of training. So leave no doubt about the authenticity of your OSHA-compliant or OHS-compliant operator training. Get your high-quality official certificate sent to you in the mail. Hand the certificate to your employer and get back to work, knowing you have what it takes to keep safe on the work site.<br><br></div><div>Available for <strong>OSHA-compliance</strong>, as well as <strong>Canadian OHS/CSA-compliance</strong>.<br><br></div><div><strong><em>*IMPORTANT NOTE: You must provide the information below, so we can verify your training before issuing a certificate!<br></em></strong><br></div><div><em>*Frame not included.</em></div>",
  image_data: uploaded_certificate_image.to_json,
  keywords: "safety training certificate, certificate of completion",
  meta_description: "OSHA and Canada compliant certificate of safety training completion",
  price: "700",
  cad_price: "925",
  shipping_price: "500",
  priority_shipping: "800",
  intl_shipping: "800"
)

# Create image derivatives
Certificate.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE CARD GROUPS #######
uploader = CardGroupImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/400x400.png'))
uploaded_card_group_image = uploader.upload(file)

CardGroup.create(
  name: "Official Wallet Cards",
  description: "<div>Leave no doubt as to the authenticity of your OSHA-compliant operator training. Let us send you our official, professionally-printed Hard Hat Training Series operator certification cards. You simply fill in the information and then have them sign the back. <strong>These cards are intended to be purchased by companies that are using our training kits or online courses to train their crew.<br><br></strong>Available for <strong>OSHA-compliance</strong>, as well as <strong>Canadian OHS/CSA-compliance</strong>.</div><div><strong><br><br></strong><em>*IMPORTANT NOTE: The image for this product is a sample.</em></div>",
  image_data: uploaded_card_group_image.to_json,
  keywords: "Blank Official Wallet Cards, safety training wallet cards, osha wallet cards",
  meta_description: "Blank Official Safety Training Wallet Cards"
)

CardGroup.create(
  name: "Hand Signal Cards",
  description: "Use these hand signal cards as a quick reference for important signals. Our hand signal cards fit in your wallet or pocket and provide the hand signal information you need while on a job site. Our plastic wallet cards are the most durable and have a professional gloss finish.

Each card is double-sided, with different information on each side, as specified below.",
  image_data: uploaded_card_group_image.to_json,
  keywords: "Hand Signal Cards, operator Hand Signal Cards, crane Hand Signal Cards, excavator Hand Signal Cards, forklift Hand Signal Cards, telehandler Hand Signal Cards",
  meta_description: "Use these hand signal cards as a quick reference for important signals."
)

# Create image derivatives
CardGroup.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE CARDS #######
Card.create(
  name: "Mobile Crane (English / Spanish)",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Overhead Crane / Telehandler",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Overhead Crane / Telehandler",
  price: 10000,
  cad_price: 13500,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "plastic"
)

Card.create(
  name: "Sit Down Forklift / Telehandler",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Sit Down Forklift / Telehandler",
  price: 4500,
  cad_price: 6000,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "plastic"
)

Card.create(
  name: "Sit Down Forklift / Telehandler",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Blank Wallet Cards",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Blank Wallet Cards",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Mobile Crane (English / Spanish)",
  price: 10000,
  cad_price: 13500,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "plastic"
)

Card.create(
  name: "Mobile Crane / Basic Hitches",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Mobile Crane / Basic Hitches",
  price: 4500,
  cad_price: 6000,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "plastic"
)

Card.create(
  name: "Mobile Crane / Basic Hitches",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Mobile Crane / Basic Hitches",
  price: 10000,
  cad_price: 13500,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "plastic"
)

Card.create(
  name: "Mobile Crane / Operator Warning",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Mobile Crane / Operator Warning",
  price: 4500,
  cad_price: 6000,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "plastic"
)

Card.create(
  name: "Mobile Crane / Operator Warning",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Mobile Crane / Operator Warning",
  price: 10000,
  cad_price: 13500,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "plastic"
)

Card.create(
  name: "Sit Down Forklift / Telehandler",
  price: 10000,
  cad_price: 13500,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "plastic"
)

Card.create(
  name: "Official Wallet Card (Plastic)",
  price: 500,
  cad_price: 800,
  shipping_price: 300,
  priority_shipping: 500,
  intl_shipping: 500,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "1",
  material: "plastic"
)

Card.create(
  name: "Official Wallet Card (Paper)",
  price: 200,
  cad_price: 275,
  shipping_price: 300,
  priority_shipping: 500,
  intl_shipping: 500,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "1",
  material: "paper"
)

Card.create(
  name: "Forklift (English / Spanish)",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Forklift (English / Spanish)",
  price: 4500,
  cad_price: 6000,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "plastic"
)

Card.create(
  name: "Forklift (English / Spanish)",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Forklift (English / Spanish)",
  price: 10000,
  cad_price: 13500,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "plastic"
)

Card.create(
  name: "Hoisting / Excavator",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Hoisting / Excavator",
  price: 4500,
  cad_price: 6000,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "plastic"
)

Card.create(
  name: "Hoisting / Excavator",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Hoisting / Excavator",
  price: 10000,
  cad_price: 13500,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "plastic"
)

Card.create(
  name: "Mobile Crane (English / Spanish)",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Mobile Crane (English / Spanish)",
  price: 4500,
  cad_price: 6000,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "plastic"
)

Card.create(
  name: "Overhead Crane / Forklift",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 500,
  intl_shipping: 500,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Overhead Crane / Forklift",
  price: 4500,
  cad_price: 6000,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "plastic"
)

Card.create(
  name: "Overhead Crane / Forklift",
  price: 2000,
  cad_price: 2750,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "paper"
)

Card.create(
  name: "Overhead Crane / Forklift",
  price: 10000,
  cad_price: 13500,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "25",
  material: "plastic"
)

Card.create(
  name: "Overhead Crane / Telehandler",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "paper"
)

Card.create(
  name: "Overhead Crane / Telehandler",
  price: 4500,
  cad_price: 6000,
  shipping_price: 500,
  priority_shipping: 800,
  intl_shipping: 800,
  card_group_id: CardGroup.second.id,
  weight: 30,
  details_required: false,
  standards_required: false,
  active: true,
  pack_qty: "10",
  material: "plastic"
)


####### CREATE SHIRTS #######
uploader = ShirtImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/400x400.png'))
uploaded_shirt_image = uploader.upload(file)

Shirt.create(
  name: "Building America",
  description: "It’s said, “the clothes make the man or woman.” This goes for trainers and operators too. Look the part in these high-quality Hard Hat Training articles. Global brand recognition!",
  image: uploaded_shirt_image.to_json,
  keywords: "construction t-shirt, safety training, safety shirt",
  meta_description: "It’s said, “the clothes make the man or woman.” This goes for trainers and operators too. Look the part in these high-quality Hard Hat Training articles.",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  weight: 60,
  sizes: "MD, LG, XL",
  colors: "Blue, Charcoal, Pink"
)

Shirt.create(
  name: "Moving Heaven and Earth",
  description: "It’s said, “the clothes make the man or woman.” This goes for trainers and operators too. Look the part in these high-quality Hard Hat Training articles. Global brand recognition!",
  image: uploaded_shirt_image.to_json,
  keywords: "construction t-shirt, safety training, safety shirt",
  meta_description: "It’s said, “the clothes make the man or woman.” This goes for trainers and operators too. Look the part in these high-quality Hard Hat Training articles.",
  price: 1000,
  cad_price: 1350,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  weight: 60,
  sizes: "MD, LG, XL",
  colors: "Orange, Red, Pink"
)

# Create image derivatives
Shirt.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE HATS #######
uploader = HatImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/400x400.png'))
uploaded_hat_image = uploader.upload(file)

Hat.create(
  name: "Navy with Velcro",
  description: "It’s said, “the clothes make the man or woman.” This goes for trainers and operators too. Look the part in these high-quality Hard Hat Training work caps. Global brand recognition!",
  image: uploaded_hat_image.to_json,
  keywords: "hat, baseball cap, hardhattraining, hard hat training",
  meta_description: "High quality hats, the Hard Hat Training way!",
  price: 1695,
  cad_price: 2275,
  shipping_price: 1000,
  priority_shipping: 1500,
  intl_shipping: 1500,
  weight: 160,
  size: "One-size-fits-all",
  style: "Baseball Cap",
  structure: "Structured",
  fastener: "Velcro"
)

# Create image derivatives
Hat.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE LANYARD GROUPS #######
uploader = LanyardGroupImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/400x400.png'))
uploaded_lanyard_group_image = uploader.upload(file)

LanyardGroup.create(
  name: "Black Lanyard",
  description: "It’s said, “the clothes make the man or woman.” This goes for trainers and operators too. Look the part in these high-quality Hard Hat Training articles. Global brand recognition!

Comes with removable key ring and clip
High quality material
Hard Hat Training Logo",
  image: uploaded_lanyard_group_image.to_json,
  keywords: "lanyard, safety training",
  meta_description: "High-quality lanyard with removable key ring and clip, featuring the Hard Hat Training logo!",
  active: true
)

# Create image derivatives
LanyardGroup.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE LANYARDS #######
Lanyard.create(
  name: "Black Lanyard x1",
  price: 200,
  cad_price: 300,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  lanyard_group_id: LanyardGroup.first.id,
  weight: 50,
  active: true
)

Lanyard.create(
  name: "Black Lanyard x5",
  price: 1000,
  cad_price: 1450,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  lanyard_group_id: LanyardGroup.first.id,
  weight: 250,
  active: true
)

Lanyard.create(
  name: "Black Lanyard x10",
  price: 1500,
  cad_price: 2175,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  lanyard_group_id: LanyardGroup.first.id,
  weight: 500,
  active: true
)

Lanyard.create(
  name: "Black Lanyard x20",
  price: 2000,
  cad_price: 3000,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  lanyard_group_id: LanyardGroup.first.id,
  weight: 1000,
  active: true
)

####### CREATE BUTTON GROUPS #######
uploader = ButtonGroupImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/400x400.png'))
uploaded_button_group_image = uploader.upload(file)

ButtonGroup.create(
  name: "An Arm And A Leg",
  description: "Show the world how you keep your workers and operators safe by wearing a Hard Hat Training button. 1″ Diameter",
  image: uploaded_button_group_image.to_json,
  keywords: "button, safety training, hard hat training",
  meta_description: "Show the world how you keep your workers and operators safe by wearing a Hard Hat Training button.",
  active: true
)

# Create image derivatives
ButtonGroup.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE BUTTONS #######
Button.create(
  name: "An Arm And A Leg x5",
  price: 500,
  cad_price: 725,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 30,
  active: true
)

Button.create(
  name: "An Arm And A Leg x10",
  price: 750,
  cad_price: 1100,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 60,
  active: true
)

Button.create(
  name: "Building America x5",
  price: 500,
  cad_price: 725,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 30,
  active: true
)

Button.create(
  name: "Building America x10",
  price: 750,
  cad_price: 1100,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 60,
  active: true
)

Button.create(
  name: "HardHatTraining.com x5",
  price: 500,
  cad_price: 725,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 30,
  active: true
)

Button.create(
  name: "HardHatTraining.com x10",
  price: 750,
  cad_price: 1100,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 60,
  active: true
)

Button.create(
  name: "Mix & Match x5",
  price: 500,
  cad_price: 725,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 30,
  active: true
)

Button.create(
  name: "Mix & Match x10",
  price: 750,
  cad_price: 1100,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 60,
  active: true
)

Button.create(
  name: "Moving Heaven And Earth x5",
  price: 500,
  cad_price: 725,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 30,
  active: true
)

Button.create(
  name: "Moving Heaven And Earth x10",
  price: 750,
  cad_price: 1100,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 60,
  active: true
)

Button.create(
  name: "Stop Doing It The Hard Way x5",
  price: 500,
  cad_price: 725,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 30,
  active: true
)

Button.create(
  name: "Stop Doing It The Hard Way x10",
  price: 750,
  cad_price: 1100,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  button_group_id: ButtonGroup.first.id,
  weight: 60,
  active: true
)

####### CREATE STICKERS #######
uploader = StickerImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/400x400.png'))
uploaded_sticker_image = uploader.upload(file)

Sticker.create!(
    name: "5 Stickers",
    description: "Show your Hard Hat Training pride with these stickers! Perfect for hard hats! This is a pack of 5.",
    image_data: uploaded_sticker_image.to_json,
    keywords: "safety stickers, hard hat training",
    meta_description: "Show your Hard Hat Training pride with these stickers! Perfect for hard hats!",
    active: true,
    price: 500,
    cad_price: 700,
    shipping_price: 500,
    priority_shipping: 1000,
    intl_shipping: 1000,
    weight: 10,
)

Sticker.create(
    name: "10 Stickers",
    description: "Show your Hard Hat Training pride with these stickers! Perfect for hard hats! This is a pack of 10.",
    image_data: uploaded_sticker_image.to_json,
    keywords: "safety stickers, hard hat training",
    meta_description: "Show your Hard Hat Training pride with these stickers! Perfect for hard hats!",
    active: true,
    price: 750,
    cad_price: 1050,
    shipping_price: 500,
    priority_shipping: 1000,
    intl_shipping: 1000,
    weight: 10,
)

Sticker.create(
    name: "20 Stickers",
    description: "Show your Hard Hat Training pride with these stickers! Perfect for hard hats! This is a pack of 20.",
    image_data: uploaded_sticker_image.to_json,
    keywords: "safety stickers, hard hat training",
    meta_description: "Show your Hard Hat Training pride with these stickers! Perfect for hard hats!",
    active: true,
    price: 1000,
    cad_price: 1400,
    shipping_price: 500,
    priority_shipping: 1000,
    intl_shipping: 1000,
    weight: 10,
)

# Create image derivatives
Sticker.find_each do |item|
  attacher = item.image_attacher

  next unless attacher.stored?

  attacher.create_derivatives

  begin
    attacher.atomic_persist            # persist changes if attachment has not changed in the meantime
  rescue Shrine::AttachmentChanged,    # attachment has changed
         ActiveRecord::RecordNotFound  # record has been deleted
    attacher.delete_derivatives        # delete now orphaned derivatives
  end
end

####### CREATE POSTER GROUPS #######
uploader = PosterGroupImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/300x400.png'))
uploaded_poster_group_image = uploader.upload(file)

PosterGroup.create(
  name: "Arc Flash",
  description: "Our posters are designed to catch the eye, increase retention and improve safety.",
  slug: "arc-flash",
  keywords: "arc flash, arc flash safety training, arc flash safety, safety training, construction safety, construction safety training",
  meta_description: "Safety posters, designed to catch the eye, increase retention and improve safety.",
  active: "true",
  image_data: uploaded_poster_group_image.to_json,
)

####### CREATE POSTERS #######
uploader = PosterImageUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/300x375.png'))
uploaded_poster_image = uploader.upload(file)

uploader2 = PosterDownloadUploader.new(:store)
file = File.new(Rails.root.join('app/assets/seeds/blank.pdf'))
uploaded_poster_download = uploader2.upload(file)

Poster.create(
  name: "Ashes, Ashes",
  description: "<div>Ashes, ashes… John falls down. Working near electricity. It's not a game.</div>",
  slug: "ashes-ashes",
  active: "true",
  poster_group_id: PosterGroup.first.id,
  display_format: "portrait",
  price: 1000,
  cad_price: 1500,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  image_data: uploaded_poster_image.to_json,
  download_data: uploaded_poster_download.to_json
)

DigitalPoster.create(
  price: 300,
  cad_price: 400,
  poster_id: Poster.first.id
)

Poster.create(
  name: "Flash Forward",
  description: "<div>Flash Forward<br>Idiom: To undergo a change of scene to a future point in time as a narrative device.<br><br>EXT. CEMETERY - MORNING (OVERCAST)<br>John’s wife, standing in front of the casket before it is lowered into<br>the ground, hands covering her face, sobs uncontrollably. Friends and<br>family march past in a solemn line. One of John’s co-workers, MIKE, a<br>tall, strong man, stops, hesitates, then wraps his arms around her,<br>squeezes tight, buries his head into her neck and sobs alongside her.<br><br>SCREEN FADES BLACK:<br>MIKE (V.O.)<br>I am so sorry...<br><br>INT. JOHN’S WORK - MORNING (DAYS EARLIER)<br>John, a lineman, prepares for the days’ work. He is in a hurry. He is<br>raised into the air and begins cutting the trees' limbs. He is very close<br>to the power lines.<br><br>MIKE approaches on the ground.<br><br>MIKE (SHOUTING THROUGH CUPPED HANDS)<br>Hey John, don’t you want to de-energize those first?<br>John looks over his shoulder and smiles. The smile, slightly mischievous,<br>certainly overconfident, reassures MIKE that they’ve done it this way<br>before, he’ll be careful, and it will be alright. As he turns back,<br>though, his pole saw contacts the line. There is a bright flash, a loud<br>explosion. John is, in a moment, engulfed in flames.<br><br>MIKE<br>John! Oh my--. John! Someone call 911!</div>",
  slug: "flash-forward",
  active: "true",
  poster_group_id: PosterGroup.first.id,
  display_format: "portrait",
  price: 1000,
  cad_price: 1500,
  shipping_price: 500,
  priority_shipping: 1000,
  intl_shipping: 1000,
  image_data: uploaded_poster_image.to_json,
  download_data: uploaded_poster_download.to_json
)

DigitalPoster.create(
  price: 300,
  cad_price: 400,
  poster_id: Poster.second.id
)

####### CREATE TRAIN THE TRAINERS #######
# TODO

####### CREATE RELATED ITEMS #######
# TODO
