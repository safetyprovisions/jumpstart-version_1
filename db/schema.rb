# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_11_181854) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ahoy_events", force: :cascade do |t|
    t.bigint "visit_id"
    t.bigint "user_id"
    t.string "name"
    t.jsonb "properties"
    t.datetime "time"
    t.index ["name", "time"], name: "index_ahoy_events_on_name_and_time"
    t.index ["properties"], name: "index_ahoy_events_on_properties", opclass: :jsonb_path_ops, using: :gin
    t.index ["user_id"], name: "index_ahoy_events_on_user_id"
    t.index ["visit_id"], name: "index_ahoy_events_on_visit_id"
  end

  create_table "ahoy_visits", force: :cascade do |t|
    t.string "visit_token"
    t.string "visitor_token"
    t.bigint "user_id"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.string "referring_domain"
    t.text "landing_page"
    t.string "browser"
    t.string "os"
    t.string "device_type"
    t.string "country"
    t.string "region"
    t.string "city"
    t.float "latitude"
    t.float "longitude"
    t.string "utm_source"
    t.string "utm_medium"
    t.string "utm_term"
    t.string "utm_content"
    t.string "utm_campaign"
    t.string "app_version"
    t.string "os_version"
    t.string "platform"
    t.datetime "started_at"
    t.index ["user_id"], name: "index_ahoy_visits_on_user_id"
    t.index ["visit_token"], name: "index_ahoy_visits_on_visit_token", unique: true
  end

  create_table "alt_names", force: :cascade do |t|
    t.text "name", null: false
    t.bigint "product_group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_group_id"], name: "index_alt_names_on_product_group_id"
  end

  create_table "announcements", force: :cascade do |t|
    t.datetime "published_at"
    t.string "announcement_type"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "backups", force: :cascade do |t|
    t.text "format", null: false
    t.integer "price", default: 0, null: false
    t.integer "shipping_price", default: 0, null: false
    t.integer "priority_shipping", default: 0, null: false
    t.integer "intl_shipping", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "order_item_id"
    t.integer "cad_price", default: 0, null: false
    t.boolean "active", default: true
    t.index ["order_item_id"], name: "index_backups_on_order_item_id"
  end

  create_table "bulk_price_levels", force: :cascade do |t|
    t.integer "qty", null: false
    t.integer "price", null: false
    t.integer "cad_price", null: false
    t.string "tier", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "button_groups", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_button_groups_on_slug", unique: true
  end

  create_table "buttons", force: :cascade do |t|
    t.text "name", null: false
    t.integer "price", null: false
    t.integer "cad_price", null: false
    t.integer "shipping_price", null: false
    t.integer "priority_shipping", null: false
    t.integer "intl_shipping", null: false
    t.bigint "button_group_id", null: false
    t.integer "weight", null: false
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pack_qty", default: "5"
    t.index ["button_group_id"], name: "index_buttons_on_button_group_id"
  end

  create_table "card_groups", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.index ["slug"], name: "index_card_groups_on_slug", unique: true
  end

  create_table "cards", force: :cascade do |t|
    t.text "name", null: false
    t.integer "price", null: false
    t.integer "cad_price", null: false
    t.integer "shipping_price", null: false
    t.integer "priority_shipping", null: false
    t.integer "intl_shipping", null: false
    t.bigint "card_group_id", null: false
    t.integer "weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "details_required", default: false
    t.boolean "standards_required", default: false
    t.boolean "active", default: true
    t.string "pack_qty", default: "1"
    t.text "material", default: "paper"
    t.index ["card_group_id"], name: "index_cards_on_card_group_id"
  end

  create_table "certificates", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.integer "price", null: false
    t.integer "cad_price", null: false
    t.integer "shipping_price", null: false
    t.integer "priority_shipping", null: false
    t.integer "intl_shipping", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.index ["slug"], name: "index_certificates_on_slug", unique: true
  end

  create_table "companies", force: :cascade do |t|
    t.string "name", null: false
    t.string "address", null: false
    t.string "city", null: false
    t.string "state", null: false
    t.string "zip", null: false
    t.integer "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "talent_name"
    t.string "talent_branch_id"
    t.string "identifier"
    t.integer "credits", default: 0, null: false
    t.string "preferred_currency", default: "usd", null: false
    t.integer "cad_credits", default: 0, null: false
    t.datetime "announcements_last_read_at"
    t.datetime "onboarding_completed_at"
    t.index ["email"], name: "index_companies_on_email", unique: true
    t.index ["reset_password_token"], name: "index_companies_on_reset_password_token", unique: true
  end

  create_table "companies_users", id: false, force: :cascade do |t|
    t.bigint "company_id", null: false
    t.bigint "user_id", null: false
    t.index ["company_id", "user_id"], name: "index_companies_users_on_company_id_and_user_id"
    t.index ["user_id", "company_id"], name: "index_companies_users_on_user_id_and_company_id"
  end

  create_table "company_admins", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_company_admins_on_company_id"
    t.index ["user_id", "company_id"], name: "index_company_admins_on_user_id_and_company_id", unique: true
    t.index ["user_id"], name: "index_company_admins_on_user_id"
  end

  create_table "company_courses", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "online_course_id"
    t.bigint "order_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "currency"
    t.bigint "company_id"
    t.bigint "train_the_trainer_id"
    t.index ["company_id"], name: "index_company_courses_on_company_id"
    t.index ["online_course_id"], name: "index_company_courses_on_online_course_id"
    t.index ["order_id"], name: "index_company_courses_on_order_id"
    t.index ["train_the_trainer_id"], name: "index_company_courses_on_train_the_trainer_id"
    t.index ["user_id"], name: "index_company_courses_on_user_id"
  end

  create_table "company_orders", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "company_id"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "po_number"
    t.index ["company_id"], name: "index_company_orders_on_company_id"
    t.index ["order_id"], name: "index_company_orders_on_order_id"
    t.index ["user_id"], name: "index_company_orders_on_user_id"
  end

  create_table "coupon_codes", force: :cascade do |t|
    t.string "code", null: false
    t.date "valid_from", null: false
    t.date "valid_to", null: false
    t.integer "flat_rate"
    t.integer "percentage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "scope", null: false
    t.integer "min_purchase_amt"
    t.integer "times_used", default: 0
    t.index ["code"], name: "index_coupon_codes_on_code", unique: true
  end

  create_table "credit_packs", force: :cascade do |t|
    t.string "name", null: false
    t.integer "price", default: 0, null: false
    t.integer "credits", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cad_price", default: 0, null: false
    t.integer "cad_credits", default: 0, null: false
  end

  create_table "digital_posters", force: :cascade do |t|
    t.integer "price", default: 300
    t.integer "cad_price", default: 400
    t.bigint "poster_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["poster_id"], name: "index_digital_posters_on_poster_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "group_trainings", force: :cascade do |t|
    t.bigint "online_course_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["online_course_id"], name: "index_group_trainings_on_online_course_id"
  end

  create_table "hats", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.integer "price", default: 1695
    t.integer "cad_price", default: 2275
    t.integer "shipping_price", default: 1000
    t.integer "priority_shipping", default: 1500
    t.integer "intl_shipping", default: 1500
    t.integer "weight", default: 160
    t.text "size", default: "S/M, M/L, L/XL"
    t.text "style", default: "Baseball Cap"
    t.text "structure", default: "Structured"
    t.text "fastener", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.index ["slug"], name: "index_hats_on_slug", unique: true
  end

  create_table "keychain_groups", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.text "keywords"
    t.text "meta_description"
    t.boolean "active", default: true
    t.index ["slug"], name: "index_keychain_groups_on_slug", unique: true
  end

  create_table "keychains", force: :cascade do |t|
    t.text "name", null: false
    t.integer "price", null: false
    t.text "color", null: false
    t.integer "cad_price", null: false
    t.integer "shipping_price", null: false
    t.integer "priority_shipping", null: false
    t.integer "intl_shipping", null: false
    t.bigint "keychain_group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "weight", default: 0
    t.boolean "active", default: true
    t.string "pack_qty", default: "1"
    t.index ["keychain_group_id"], name: "index_keychains_on_keychain_group_id"
  end

  create_table "kits", force: :cascade do |t|
    t.string "title", null: false
    t.integer "price", null: false
    t.integer "sale_price"
    t.date "sale_expiry"
    t.integer "language", null: false
    t.integer "standards", null: false
    t.integer "product_group_id", null: false
    t.text "sample_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "sale_start"
    t.string "market_type", default: "usa", null: false
    t.integer "cad_price", null: false
    t.integer "cad_sale_price"
    t.text "download", null: false
    t.text "additional_download"
    t.boolean "active", default: true
  end

  create_table "lanyard_groups", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_lanyard_groups_on_slug", unique: true
  end

  create_table "lanyards", force: :cascade do |t|
    t.text "name", null: false
    t.integer "price", null: false
    t.integer "cad_price", null: false
    t.integer "shipping_price", null: false
    t.integer "priority_shipping", null: false
    t.integer "intl_shipping", null: false
    t.bigint "lanyard_group_id", null: false
    t.integer "weight", null: false
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pack_qty", default: "1"
    t.index ["lanyard_group_id"], name: "index_lanyards_on_lanyard_group_id"
  end

  create_table "machine_tag_groups", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.index ["slug"], name: "index_machine_tag_groups_on_slug", unique: true
  end

  create_table "machine_tags", force: :cascade do |t|
    t.text "name", null: false
    t.integer "price", null: false
    t.integer "cad_price", null: false
    t.integer "shipping_price", null: false
    t.integer "priority_shipping", null: false
    t.integer "intl_shipping", null: false
    t.bigint "machine_tag_group_id"
    t.integer "weight", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.string "pack_qty", default: "1"
    t.index ["machine_tag_group_id"], name: "index_machine_tags_on_machine_tag_group_id"
  end

  create_table "non_catalog_course_accesses", force: :cascade do |t|
    t.bigint "online_course_id"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_non_catalog_course_accesses_on_company_id"
    t.index ["online_course_id"], name: "index_non_catalog_course_accesses_on_online_course_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.bigint "recipient_id"
    t.bigint "actor_id"
    t.datetime "read_at"
    t.string "action"
    t.bigint "notifiable_id"
    t.string "notifiable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "online_course_activations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "order_id"
    t.bigint "online_course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["online_course_id"], name: "index_online_course_activations_on_online_course_id"
    t.index ["order_id"], name: "index_online_course_activations_on_order_id"
    t.index ["user_id"], name: "index_online_course_activations_on_user_id"
  end

  create_table "online_courses", force: :cascade do |t|
    t.string "title", null: false
    t.integer "price", null: false
    t.integer "sale_price"
    t.date "sale_start", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.date "sale_expiry", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "language", null: false
    t.integer "standards", null: false
    t.integer "product_group_id", null: false
    t.integer "talent_course_id", null: false
    t.text "practical_evaluation_data", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "exam_data", null: false
    t.text "answer_key_data", null: false
    t.string "market_type", default: "usa", null: false
    t.integer "cad_price", null: false
    t.integer "cad_sale_price"
    t.boolean "public", default: true, null: false
    t.boolean "active", default: true
  end

  create_table "order_items", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "product_id"
    t.integer "quantity", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "order_item_type", default: "", null: false
    t.string "orderable_type"
    t.bigint "orderable_id"
    t.text "shipping_method", default: "none", null: false
    t.string "backup_for"
    t.text "specifics"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["orderable_type", "orderable_id"], name: "index_order_items_on_orderable_type_and_orderable_id"
    t.index ["product_id"], name: "index_order_items_on_product_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "user_id"
    t.string "status", default: "draft", null: false
    t.string "stripe_id"
    t.string "card_brand"
    t.string "card_last4"
    t.string "card_exp_month"
    t.string "card_exp_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "paid_at"
    t.string "shipment_id"
    t.string "to_state"
    t.string "to_country"
    t.datetime "download_start_time", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.text "shipping_method", default: "shipping_price", null: false
    t.datetime "order_placed_at"
    t.date "group_training_date"
    t.datetime "label_created"
    t.string "label_url"
    t.string "tracking_code"
    t.boolean "idaho_resident", default: false
    t.string "currency"
    t.bigint "ahoy_visit_id"
    t.string "to_street2"
    t.string "card_zip_code"
    t.integer "final_total"
    t.string "coupon_code"
    t.string "to_name_ciphertext"
    t.string "to_street1_ciphertext"
    t.string "to_city_ciphertext"
    t.string "to_zip_ciphertext"
    t.string "to_email_ciphertext"
    t.string "additional_info_ciphertext"
    t.string "to_name_bidx"
    t.string "to_street1_bidx"
    t.string "to_city_bidx"
    t.string "to_zip_bidx"
    t.string "to_email_bidx"
    t.string "additional_info_bidx"
    t.index ["additional_info_bidx"], name: "index_orders_on_additional_info_bidx"
    t.index ["to_city_bidx"], name: "index_orders_on_to_city_bidx"
    t.index ["to_email_bidx"], name: "index_orders_on_to_email_bidx"
    t.index ["to_name_bidx"], name: "index_orders_on_to_name_bidx"
    t.index ["to_street1_bidx"], name: "index_orders_on_to_street1_bidx"
    t.index ["to_zip_bidx"], name: "index_orders_on_to_zip_bidx"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "payment_terms", force: :cascade do |t|
    t.bigint "company_id", null: false
    t.integer "term", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_payment_terms_on_company_id"
  end

  create_table "poster_groups", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "image_data", null: false
    t.boolean "active", default: true
    t.index ["slug"], name: "index_poster_groups_on_slug", unique: true
  end

  create_table "posters", force: :cascade do |t|
    t.text "name", null: false
    t.text "image_data", null: false
    t.bigint "poster_group_id", null: false
    t.text "description", null: false
    t.text "download_data"
    t.text "display_format", default: "portrait"
    t.integer "price", null: false
    t.integer "cad_price", null: false
    t.integer "shipping_price", null: false
    t.integer "priority_shipping", null: false
    t.integer "intl_shipping", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.boolean "active", default: true
    t.index ["poster_group_id"], name: "index_posters_on_poster_group_id"
    t.index ["slug"], name: "index_posters_on_slug", unique: true
  end

  create_table "product_groups", force: :cascade do |t|
    t.string "name", null: false
    t.text "image_data", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "video", null: false
    t.text "overview", null: false
    t.text "training_standards", null: false
    t.text "facts", null: false
    t.text "fails", null: false
    t.string "slug"
    t.text "keywords"
    t.text "meta_description"
    t.boolean "active", default: true
    t.text "alt_text"
    t.index ["slug"], name: "index_product_groups_on_slug", unique: true
  end

  create_table "products", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.integer "price", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "shipping_price"
    t.integer "weight"
    t.integer "product_group_id", null: false
    t.integer "priority_shipping"
    t.integer "intl_shipping"
    t.integer "cad_price", default: 0, null: false
    t.integer "cad_sale_price"
    t.integer "sale_price"
    t.boolean "active", default: true
  end

  create_table "related_items", force: :cascade do |t|
    t.string "relatable_type"
    t.bigint "relatable_id"
    t.bigint "product_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_group_id"], name: "index_related_items_on_product_group_id"
    t.index ["relatable_type", "relatable_id"], name: "index_related_items_on_relatable_type_and_relatable_id"
  end

  create_table "services", force: :cascade do |t|
    t.bigint "user_id"
    t.string "provider"
    t.string "uid"
    t.string "access_token"
    t.string "access_token_secret"
    t.string "refresh_token"
    t.datetime "expires_at"
    t.text "auth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_services_on_user_id"
  end

  create_table "shirts", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.integer "price", default: 1000
    t.integer "cad_price", default: 1350
    t.integer "shipping_price", default: 500
    t.integer "priority_shipping", default: 1000
    t.integer "intl_shipping", default: 1000
    t.integer "weight", default: 60
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "sizes", default: "MD, LG, XL"
    t.text "colors"
    t.boolean "active", default: true
    t.index ["slug"], name: "index_shirts_on_slug", unique: true
  end

  create_table "stickers", force: :cascade do |t|
    t.text "name", null: false
    t.text "description", null: false
    t.text "image_data", null: false
    t.string "slug"
    t.text "keywords", null: false
    t.text "meta_description", null: false
    t.boolean "active", default: true
    t.integer "price", null: false
    t.integer "cad_price", null: false
    t.integer "shipping_price", default: 500
    t.integer "priority_shipping", default: 1000
    t.integer "intl_shipping", default: 1000
    t.integer "weight", default: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_stickers_on_slug", unique: true
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "train_the_trainers", force: :cascade do |t|
    t.string "title", null: false
    t.integer "price", null: false
    t.integer "sale_price"
    t.date "sale_start", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.date "sale_expiry", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.integer "language", null: false
    t.integer "standards", null: false
    t.integer "product_group_id", null: false
    t.integer "talent_course_id", null: false
    t.text "practical_evaluation_data", null: false
    t.text "exam_data", null: false
    t.text "answer_key_data", null: false
    t.bigint "kit_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cad_price", null: false
    t.integer "cad_sale_price"
    t.boolean "active", default: true
    t.index ["kit_id"], name: "index_train_the_trainers_on_kit_id"
  end

  create_table "ttt_activations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "order_id"
    t.bigint "train_the_trainer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_ttt_activations_on_order_id"
    t.index ["train_the_trainer_id"], name: "index_ttt_activations_on_train_the_trainer_id"
    t.index ["user_id"], name: "index_ttt_activations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name"
    t.string "last_name"
    t.datetime "announcements_last_read_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "company_id"
    t.boolean "superadmin", default: false
    t.boolean "active", default: true
    t.string "talent_user_id"
    t.string "talent_username"
    t.string "talent_password"
    t.string "preferred_currency", default: "usd", null: false
    t.integer "failed_attempts", default: 0, null: false
    t.datetime "locked_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "alt_names", "product_groups"
  add_foreign_key "backups", "order_items"
  add_foreign_key "buttons", "button_groups"
  add_foreign_key "cards", "card_groups"
  add_foreign_key "company_admins", "companies"
  add_foreign_key "company_admins", "users"
  add_foreign_key "company_courses", "companies"
  add_foreign_key "company_courses", "online_courses"
  add_foreign_key "company_courses", "orders"
  add_foreign_key "company_courses", "train_the_trainers"
  add_foreign_key "company_courses", "users"
  add_foreign_key "company_orders", "companies"
  add_foreign_key "company_orders", "orders"
  add_foreign_key "company_orders", "users"
  add_foreign_key "digital_posters", "posters"
  add_foreign_key "group_trainings", "online_courses"
  add_foreign_key "keychains", "keychain_groups"
  add_foreign_key "lanyards", "lanyard_groups"
  add_foreign_key "machine_tags", "machine_tag_groups"
  add_foreign_key "non_catalog_course_accesses", "companies"
  add_foreign_key "non_catalog_course_accesses", "online_courses"
  add_foreign_key "online_course_activations", "online_courses"
  add_foreign_key "online_course_activations", "orders"
  add_foreign_key "online_course_activations", "users"
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_items", "products"
  add_foreign_key "orders", "users"
  add_foreign_key "payment_terms", "companies"
  add_foreign_key "posters", "poster_groups"
  add_foreign_key "related_items", "product_groups"
  add_foreign_key "services", "users"
  add_foreign_key "train_the_trainers", "kits"
  add_foreign_key "ttt_activations", "orders"
  add_foreign_key "ttt_activations", "train_the_trainers"
  add_foreign_key "ttt_activations", "users"
end
