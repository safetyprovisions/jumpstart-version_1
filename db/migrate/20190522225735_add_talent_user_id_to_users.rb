class AddTalentUserIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :talent_user_id, :string
  end
end
