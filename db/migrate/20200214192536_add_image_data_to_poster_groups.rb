class AddImageDataToPosterGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :poster_groups, :image_data, :text, null: false
  end
end
