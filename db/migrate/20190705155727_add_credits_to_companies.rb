class AddCreditsToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :credits, :integer, default: 0, null: false
  end
end
