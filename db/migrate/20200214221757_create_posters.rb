class CreatePosters < ActiveRecord::Migration[5.2]
  def change
    create_table :posters do |t|
      t.text :name, null: false
      t.text :image_data, null: false
      t.references :poster_group, foreign_key: true, null: false
      t.text :format, null: false
      t.text :description, null: false
      t.text :download_data
      t.text :display_format, default: "portrait"
      t.integer :price, null: false
      t.integer :cad_price, null: false
      t.integer :shipping_price, null: false
      t.integer :priority_shipping, null: false
      t.integer :intl_shipping, null: false
      t.string :weight

      t.timestamps
    end
  end
end
