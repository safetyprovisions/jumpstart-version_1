class AddPackQtyToMachineTags < ActiveRecord::Migration[5.2]
  def change
    add_column :machine_tags, :pack_qty, :string, default: 1
  end
end
