class CreateTrainTheTrainers < ActiveRecord::Migration[5.2]
  def change
    create_table :train_the_trainers do |t|
      t.string :title, null: false
      t.integer :price, null: false
      t.integer :sale_price
      t.date :sale_start, null: false, default: -> { "CURRENT_TIMESTAMP" }
      t.date :sale_expiry, null: false, default: -> { "CURRENT_TIMESTAMP" }
      t.integer :language, null: false
      t.integer :standards, null: false
      t.integer :product_group_id, null: false
      t.integer :talent_course_id, null: false
      t.text :practical_evaluation_data, null: false
      t.text :exam_data, null: false
      t.text :answer_key_data, null: false
      t.string :market_type, null: false
      t.references :kit, foreign_key: true, null: false

      t.timestamps
    end
  end
end
