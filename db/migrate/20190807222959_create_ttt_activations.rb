class CreateTttActivations < ActiveRecord::Migration[5.2]
  def change
    create_table :ttt_activations do |t|
      t.references :user, foreign_key: true
      t.references :order, foreign_key: true
      t.references :train_the_trainer, foreign_key: true

      t.timestamps
    end
  end
end
