class AddActiveToTrainTheTrainers < ActiveRecord::Migration[5.2]
  def change
    add_column :train_the_trainers, :active, :boolean, default: true
  end
end
