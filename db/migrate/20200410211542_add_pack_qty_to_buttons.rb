class AddPackQtyToButtons < ActiveRecord::Migration[5.2]
  def change
    add_column :buttons, :pack_qty, :string, default: 5
  end
end
