class AddActiveToHats < ActiveRecord::Migration[5.2]
  def change
    add_column :hats, :active, :boolean, default: true
  end
end
