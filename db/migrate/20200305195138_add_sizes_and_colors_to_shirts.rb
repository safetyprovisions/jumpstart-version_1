class AddSizesAndColorsToShirts < ActiveRecord::Migration[5.2]
  def change
    add_column :shirts, :sizes, :text, default: "MD, LG, XL"
    add_column :shirts, :colors, :text
  end
end
