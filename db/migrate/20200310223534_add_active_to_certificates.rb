class AddActiveToCertificates < ActiveRecord::Migration[5.2]
  def change
    add_column :certificates, :active, :boolean, default: true
  end
end
