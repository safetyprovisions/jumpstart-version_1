class AddPackQtyToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :pack_qty, :string, default: 1
  end
end
