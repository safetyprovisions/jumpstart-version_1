class CreateGroupTrainings < ActiveRecord::Migration[5.2]
  def change
    create_table :group_trainings do |t|
      t.references :online_course, foreign_key: true, null: false

      t.timestamps
    end
  end
end
