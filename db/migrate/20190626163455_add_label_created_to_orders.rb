class AddLabelCreatedToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :label_created, :datetime, default: nil
  end
end
