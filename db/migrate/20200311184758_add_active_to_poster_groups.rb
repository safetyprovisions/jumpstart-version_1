class AddActiveToPosterGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :poster_groups, :active, :boolean, default: true
  end
end
