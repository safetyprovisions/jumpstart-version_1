class AddDefaultSaleDatesToOnlineCourses < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:online_courses, :sale_start, from: nil, to: ->{ 'CURRENT_TIMESTAMP' })
    change_column_default(:online_courses, :sale_expiry, from: nil, to: ->{ 'CURRENT_TIMESTAMP' })
  end
end
