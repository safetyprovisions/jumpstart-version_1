class AddCardZipCodeToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :card_zip_code, :string
  end
end
