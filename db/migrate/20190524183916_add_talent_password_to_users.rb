class AddTalentPasswordToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :talent_password, :string
  end
end
