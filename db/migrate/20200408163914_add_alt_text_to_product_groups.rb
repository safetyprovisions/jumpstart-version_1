class AddAltTextToProductGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :product_groups, :alt_text, :text
  end
end
