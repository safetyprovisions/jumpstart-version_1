class AddDetailsRequiredToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :details_required, :boolean, default: false
  end
end
