class AddAdditionalInfoToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :additional_info, :text
  end
end
