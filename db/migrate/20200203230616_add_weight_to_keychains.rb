class AddWeightToKeychains < ActiveRecord::Migration[5.2]
  def change
    add_column :keychains, :weight, :integer, default: 0
  end
end
