class AddUniqueIndexToAltNames < ActiveRecord::Migration[5.2]
  def change
    def change
      add_index :alt_names, [:name, :product_group_id], unique: true
    end
  end
end
