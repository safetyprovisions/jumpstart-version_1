class CreatePosterGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :poster_groups do |t|
      t.text :name, null: false
      t.text :description, null: false
      t.string :slug
      t.text :keywords, null: false
      t.text :meta_description, null: false

      t.timestamps
    end
    add_index :poster_groups, :slug, unique: true
  end
end
