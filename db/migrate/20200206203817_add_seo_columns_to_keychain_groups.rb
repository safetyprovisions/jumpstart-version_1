class AddSeoColumnsToKeychainGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :keychain_groups, :keywords, :text
    add_column :keychain_groups, :meta_description, :text
  end
end
