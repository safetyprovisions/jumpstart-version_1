class AddActiveToBackups < ActiveRecord::Migration[5.2]
  def change
    add_column :backups, :active, :boolean, default: true
  end
end
