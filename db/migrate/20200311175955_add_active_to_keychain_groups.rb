class AddActiveToKeychainGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :keychain_groups, :active, :boolean, default: true
  end
end
