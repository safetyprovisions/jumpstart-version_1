class AddExamColumnsToOnlineCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :online_courses, :exam_data, :text, null: false
    add_column :online_courses, :answer_key_data, :text, null: false
  end
end
