class AddSlugToProductGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :product_groups, :slug, :string
    add_index :product_groups, :slug, unique: true
  end
end
