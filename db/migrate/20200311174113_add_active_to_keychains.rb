class AddActiveToKeychains < ActiveRecord::Migration[5.2]
  def change
    add_column :keychains, :active, :boolean, default: true
  end
end
