class CreateCompanyAdmins < ActiveRecord::Migration[5.2]
  def change
    create_table :company_admins do |t|
      t.references :user, foreign_key: true
      t.references :company, foreign_key: true

      t.timestamps
    end
    add_index :company_admins, [:user_id, :company_id], unique: true
  end
end
