class CreateKeychains < ActiveRecord::Migration[5.2]
  def change
    create_table :keychains do |t|
      t.text :name, null: false
      t.integer :price, null: false
      t.text :color, null: false
      t.integer :cad_price, null: false
      t.integer :shipping_price, null: false
      t.integer :priority_shipping, null: false
      t.integer :intl_shipping, null: false
      t.references :keychain_group, foreign_key: true, null: false

      t.timestamps
    end
  end
end
