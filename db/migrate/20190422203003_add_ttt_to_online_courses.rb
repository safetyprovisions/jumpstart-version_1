class AddTttToOnlineCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :online_courses, :ttt, :boolean, default: false
  end
end
