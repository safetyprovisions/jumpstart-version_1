class ChangeOrdersShippingMethodDefault < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:orders, :shipping_method, from: "basic", to: "shipping_price")
  end
end
