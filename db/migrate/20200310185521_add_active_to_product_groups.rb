class AddActiveToProductGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :product_groups, :active, :boolean, default: true
  end
end
