class CreateShirts < ActiveRecord::Migration[5.2]
  def change
    create_table :shirts do |t|
      t.text :name, null: false
      t.text :description, null: false
      t.text :image_data, null: false
      t.string :slug
      t.text :keywords, null: false
      t.text :meta_description, null: false
      t.integer :price, default: 1000
      t.integer :cad_price, default: 1350
      t.integer :shipping_price, default: 500
      t.integer :priority_shipping, default: 1000
      t.integer :intl_shipping, default: 1000
      t.integer :weight, default: 60

      t.timestamps
    end
    add_index :shirts, :slug, unique: true
  end
end
