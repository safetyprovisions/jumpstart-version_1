class AddSpecificsToOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :order_items, :specifics, :text
  end
end
