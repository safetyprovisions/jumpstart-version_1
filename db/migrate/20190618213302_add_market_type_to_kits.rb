class AddMarketTypeToKits < ActiveRecord::Migration[5.2]
  def up
    add_column :kits, :market_type, :string, null: false, default: 'usa'
    remove_column :kits, :kit_type, :string
  end

  def down
    remove_column :kits, :market_type, :string
    add_column :kits, :kit_type, :string
  end
end
