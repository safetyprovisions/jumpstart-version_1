class ChangeColumnDefault < ActiveRecord::Migration[5.2]
  def change
    change_column_null :orders, :stripe_id, true
    change_column_null :orders, :card_brand, true
    change_column_null :orders, :card_last4, true
    change_column_null :orders, :card_exp_month, true
    change_column_null :orders, :card_exp_year, true
  end
end
