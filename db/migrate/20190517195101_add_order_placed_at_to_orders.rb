class AddOrderPlacedAtToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :order_placed_at, :datetime
  end
end
