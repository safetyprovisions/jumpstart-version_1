class CreateHats < ActiveRecord::Migration[5.2]
  def change
    create_table :hats do |t|
      t.text :name, null: false
      t.text :description, null: false
      t.text :image_data, null: false
      t.string :slug
      t.text :keywords, null: false
      t.text :meta_description, null: false
      t.integer :price, default: 1695
      t.integer :cad_price, default: 2275
      t.integer :shipping_price, default: 1000
      t.integer :priority_shipping, default: 1500
      t.integer :intl_shipping, default: 1500
      t.integer :weight, default: 160
      t.text :size, default: "S/M, M/L, L/XL"
      t.text :style, default: "Baseball Cap"
      t.text :structure, default: "Structured"
      t.text :fastener, null: false

      t.timestamps
    end
    add_index :hats, :slug, unique: true
  end
end
