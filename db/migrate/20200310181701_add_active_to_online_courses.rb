class AddActiveToOnlineCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :online_courses, :active, :boolean, default: true
  end
end
