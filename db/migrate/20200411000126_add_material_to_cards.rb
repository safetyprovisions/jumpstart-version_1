class AddMaterialToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :material, :text, default: "paper"
  end
end
