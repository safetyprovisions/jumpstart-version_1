class AddSlugToPosters < ActiveRecord::Migration[5.2]
  def change
    add_column :posters, :slug, :string
    add_index :posters, :slug, unique: true
  end
end
