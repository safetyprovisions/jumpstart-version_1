class AddCompanyToCompanyCourses < ActiveRecord::Migration[5.2]
  def change
    add_reference :company_courses, :company, foreign_key: true
  end
end
