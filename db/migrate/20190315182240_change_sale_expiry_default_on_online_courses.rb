class ChangeSaleExpiryDefaultOnOnlineCourses < ActiveRecord::Migration[5.2]
  def change
    change_column :online_courses, :sale_expiry, :date, default: '', null: false
  end
end
