class RemoveMarketTypeFromTrainTheTrainers < ActiveRecord::Migration[5.2]
  def change
    remove_column :train_the_trainers, :market_type, :string
  end
end
