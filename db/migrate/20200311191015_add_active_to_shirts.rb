class AddActiveToShirts < ActiveRecord::Migration[5.2]
  def change
    add_column :shirts, :active, :boolean, default: true
  end
end
