class RemoveTalentCourseIdFromProducts < ActiveRecord::Migration[5.2]
  def up
    remove_column :products, :talent_course_id, :string
    change_column_null(:products, :product_group_id, false)
  end

  def down
    add_column :products, :talent_course_id, :string
    change_column_null(:products, :product_group_id, true)
  end
end
