class AddShippingAddressToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :to_name, :string
    add_column :orders, :to_street1, :string
    add_column :orders, :to_city, :string
    add_column :orders, :to_state, :string
    add_column :orders, :to_zip, :string
    add_column :orders, :to_country, :string
    add_column :orders, :to_email, :string
  end
end
