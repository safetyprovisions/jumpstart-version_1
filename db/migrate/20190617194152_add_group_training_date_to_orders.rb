class AddGroupTrainingDateToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :group_training_date, :date
  end
end
