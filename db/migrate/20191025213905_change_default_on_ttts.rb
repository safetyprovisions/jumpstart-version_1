class ChangeDefaultOnTtts < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:train_the_trainers, :cad_price, from: 0, to: nil)
  end
end
