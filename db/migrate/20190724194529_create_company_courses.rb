class CreateCompanyCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :company_courses do |t|
      t.references :user, foreign_key: true
      t.references :online_course, foreign_key: true
      t.references :order, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
