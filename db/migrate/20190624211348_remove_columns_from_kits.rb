class RemoveColumnsFromKits < ActiveRecord::Migration[5.2]
  def up
    remove_column :kits, :image_data, :text
    remove_column :kits, :description, :text
  end

  def down
    add_column :kits, :image_data, :text, null: false
    add_column :kits, :description, :text, null: false
  end
end
