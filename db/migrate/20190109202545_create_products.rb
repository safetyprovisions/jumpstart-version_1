class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.text :description
      t.string :talent_course_id
      t.integer :price, default: 0, null: false

      t.timestamps
    end
  end
end
