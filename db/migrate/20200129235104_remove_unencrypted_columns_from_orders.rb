class RemoveUnencryptedColumnsFromOrders < ActiveRecord::Migration[5.2]
  def change
    remove_column :orders, :to_name, :string
    remove_column :orders, :to_street1, :string
    remove_column :orders, :to_city, :string
    remove_column :orders, :to_zip, :string
    remove_column :orders, :to_email, :string
    remove_column :orders, :additional_info, :text
  end
end
