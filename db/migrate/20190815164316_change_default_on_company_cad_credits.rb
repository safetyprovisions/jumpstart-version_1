class ChangeDefaultOnCompanyCadCredits < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:companies, :cad_credits, from: nil, to: 0)
    change_column_null(:companies, :cad_credits, false)
  end
end
