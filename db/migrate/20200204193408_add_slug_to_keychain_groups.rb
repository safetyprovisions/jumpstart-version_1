class AddSlugToKeychainGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :keychain_groups, :slug, :string
    add_index :keychain_groups, :slug, unique: true
  end
end
