class AddBackupForToOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :order_items, :backup_for, :string
  end
end
