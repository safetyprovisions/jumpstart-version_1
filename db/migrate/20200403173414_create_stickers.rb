class CreateStickers < ActiveRecord::Migration[5.2]
  def change
    create_table :stickers do |t|
      t.text :name, null: false
      t.text :description, null: false
      t.text :image_data, null: false
      t.string :slug
      t.text :keywords, null: false
      t.text :meta_description, null: false
      t.boolean :active, default: true
      t.integer :price, null: false
      t.integer :cad_price, null: false
      t.integer :shipping_price, default: 500
      t.integer :priority_shipping, default: 1000
      t.integer :intl_shipping, default: 1000
      t.integer :weight, default: 10

      t.timestamps
    end
    add_index :stickers, :slug, unique: true
  end
end
