class AddLabelUrlAndTrackingCodeToOrders < ActiveRecord::Migration[5.2]
  def up
    add_column :orders, :label_url, :string
    add_column :orders, :tracking_code, :string
  end

  def down
    remove_column :orders, :label_url, :string
    remove_column :orders, :tracking_code, :string
  end
end
