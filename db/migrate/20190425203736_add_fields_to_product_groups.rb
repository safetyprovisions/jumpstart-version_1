class AddFieldsToProductGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :product_groups, :video, :string
    add_column :product_groups, :overview, :text
    add_column :product_groups, :training_standards, :text
    add_column :product_groups, :facts, :text
    add_column :product_groups, :fails, :text
  end
end
