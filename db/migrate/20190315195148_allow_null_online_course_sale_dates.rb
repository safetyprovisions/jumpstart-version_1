class AllowNullOnlineCourseSaleDates < ActiveRecord::Migration[5.2]
  def change
    change_column :online_courses, :sale_start, :date, default: ''
    change_column :online_courses, :sale_expiry, :date, default: ''
  end
end
