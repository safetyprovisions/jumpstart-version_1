class AddCadCreditsToCreditPacks < ActiveRecord::Migration[5.2]
  def change
    add_column :credit_packs, :cad_credits, :integer
  end
end
