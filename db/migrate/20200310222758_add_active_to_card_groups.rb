class AddActiveToCardGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :card_groups, :active, :boolean, default: true
  end
end
