class AddPoNumberToCompanyOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :company_orders, :po_number, :string
  end
end
