class CreateCertificates < ActiveRecord::Migration[5.2]
  def change
    create_table :certificates do |t|
      t.text :name, null: false
      t.text :description, null: false
      t.text :image_data, null: false
      t.string :slug
      t.text :keywords, null: false
      t.text :meta_description, null: false
      t.integer :price, null: false
      t.integer :cad_price, null: false
      t.integer :shipping_price, null: false
      t.integer :priority_shipping, null: false
      t.integer :intl_shipping, null: false

      t.timestamps
    end
    add_index :certificates, :slug, unique: true
  end
end
