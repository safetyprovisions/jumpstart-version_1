class ChangeDefaultOnLanyardGroups < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:lanyard_groups, :active, from: nil, to: true)
  end
end
