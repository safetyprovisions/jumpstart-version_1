class AddCurrencyToCompanyCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :company_courses, :currency, :string
  end
end
