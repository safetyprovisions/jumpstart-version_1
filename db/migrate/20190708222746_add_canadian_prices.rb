class AddCanadianPrices < ActiveRecord::Migration[5.2]
  def up
    add_column :backups, :cad_price, :integer, default: 0, null: false

    add_column :credit_packs, :cad_price, :integer, default: 0, null: false

    add_column :kits, :cad_price, :integer, default: 0, null: false
    add_column :kits, :cad_sale_price, :integer

    add_column :online_courses, :cad_price, :integer, default: 0, null: false
    add_column :online_courses, :cad_sale_price, :integer

    add_column :products, :cad_price, :integer, default: 0, null: false
    add_column :products, :cad_sale_price, :integer
    add_column :products, :sale_price, :integer

    add_column :train_the_trainers, :cad_price, :integer, default: 0, null: false
    add_column :train_the_trainers, :cad_sale_price, :integer
  end

  def down
    remove_column :backups, :cad_price, :integer, default: 0, null: false

    remove_column :credit_packs, :cad_price, :integer, default: 0, null: false

    remove_column :kits, :cad_price, :integer, default: 0, null: false
    remove_column :kits, :cad_sale_price, :integer

    remove_column :online_courses, :cad_price, :integer, default: 0, null: false
    remove_column :online_courses, :cad_sale_price, :integer

    remove_column :products, :cad_price, :integer, default: 0, null: false
    remove_column :products, :cad_sale_price, :integer
    remove_column :products, :sale_price, :integer

    remove_column :train_the_trainers, :cad_price, :integer, default: 0, null: false
    remove_column :train_the_trainers, :sale_price, :integer
  end
end
