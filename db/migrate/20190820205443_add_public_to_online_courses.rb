class AddPublicToOnlineCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :online_courses, :public, :boolean, default: true, null: false
  end
end
