class CreateKits < ActiveRecord::Migration[5.2]
  def change
    create_table :kits do |t|
      t.string  :type, null:false
      t.string  :title, null:false
      t.text    :image_data, null:false
      t.text    :download_data, null:false
      t.text    :description, null:false
      t.integer :price, null:false, default: 0
      t.integer :sale_price
      t.date    :sale_expiry
      t.integer :language, null:false
      t.integer :standards, null:false
      t.integer :product_group_id, null:false
      t.text    :sample_data

      t.timestamps
    end
  end
end
