class RemoveDefaultFromOnlineCoursesCadPrice < ActiveRecord::Migration[5.2]
  def up
    change_column_default(:online_courses, :cad_price, from: 0, to: nil)
  end

  def down
    change_column_default(:online_courses, :cad_price, from: nil, to: 0)
  end
end
