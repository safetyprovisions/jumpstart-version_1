class AddKeywordsToProductGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :product_groups, :keywords, :text
  end
end
