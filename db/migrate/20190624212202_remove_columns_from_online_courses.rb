class RemoveColumnsFromOnlineCourses < ActiveRecord::Migration[5.2]
  def up
    remove_column :online_courses, :image_data, :text
    remove_column :online_courses, :description, :text
    remove_column :online_courses, :video, :string
  end

  def down
    add_column :online_courses, :image_data, :text, null: false
    add_column :online_courses, :description, :text, null: false
    add_column :online_courses, :video, :string
  end
end
