class AddIdahoResidentToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :idaho_resident, :boolean, default: false
  end
end
