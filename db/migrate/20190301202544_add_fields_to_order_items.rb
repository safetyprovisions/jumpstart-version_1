class AddFieldsToOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :order_items, :order_item_type, :string, null: false, default: ""
  end
end
