class CreateProductGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :product_groups do |t|
      t.string :name, null:false
      t.text :image_data, null:false

      t.timestamps
    end
  end
end
