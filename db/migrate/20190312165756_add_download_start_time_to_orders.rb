class AddDownloadStartTimeToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :download_start_time, :datetime, null: false, default: Time.now
  end
end
