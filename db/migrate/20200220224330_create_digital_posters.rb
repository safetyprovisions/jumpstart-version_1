class CreateDigitalPosters < ActiveRecord::Migration[5.2]
  def change
    create_table :digital_posters do |t|
      t.integer :price, default: 300
      t.integer :cad_price, default: 400
      t.references :poster, foreign_key: true

      t.timestamps
    end
  end
end
