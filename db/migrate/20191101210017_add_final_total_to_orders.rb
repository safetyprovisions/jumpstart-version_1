class AddFinalTotalToOrders < ActiveRecord::Migration[5.2]
  def up
    add_column :orders, :final_total, :integer
  end

  def down
    remove_column :orders, :final_total, :integer
  end
end
