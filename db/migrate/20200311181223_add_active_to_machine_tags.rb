class AddActiveToMachineTags < ActiveRecord::Migration[5.2]
  def change
    add_column :machine_tags, :active, :boolean, default: true
  end
end
