class AddShippingFieldsToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :priority_shipping, :integer
    add_column :products, :intl_shipping, :integer
  end
end
