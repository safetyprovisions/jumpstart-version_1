class AddOnboardingCompletedAtToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :onboarding_completed_at, :datetime
  end
end
