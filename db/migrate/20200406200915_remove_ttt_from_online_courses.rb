class RemoveTttFromOnlineCourses < ActiveRecord::Migration[5.2]
  def change
    remove_column :online_courses, :ttt, :boolean
  end
end
