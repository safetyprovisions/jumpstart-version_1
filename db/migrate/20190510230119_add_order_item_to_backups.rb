class AddOrderItemToBackups < ActiveRecord::Migration[5.2]
  def change
    add_reference :backups, :order_item, foreign_key: true
  end
end
