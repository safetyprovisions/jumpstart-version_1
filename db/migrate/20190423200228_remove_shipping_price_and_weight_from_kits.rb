class RemoveShippingPriceAndWeightFromKits < ActiveRecord::Migration[5.2]
  def change
    remove_column :kits, :shipping_price, :integer
    remove_column :kits, :weight, :integer
  end
end
