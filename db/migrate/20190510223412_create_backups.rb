class CreateBackups < ActiveRecord::Migration[5.2]
  def change
    create_table :backups do |t|
      t.text :format, null: false
      t.integer :price, null: false, default: 0
      t.integer :shipping_price, null: false, default: 0
      t.integer :priority_shipping, null: false, default: 0
      t.integer :intl_shipping, null: false, default: 0

      t.timestamps
    end
  end
end
