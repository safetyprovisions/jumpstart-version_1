class CreateKeychainGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :keychain_groups do |t|
      t.text :name, null: false
      t.text :description, null: false
      t.text :image_data, null: false

      t.timestamps
    end
  end
end
