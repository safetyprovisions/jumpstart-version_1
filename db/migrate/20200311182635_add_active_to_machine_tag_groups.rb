class AddActiveToMachineTagGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :machine_tag_groups, :active, :boolean, default: true
  end
end
