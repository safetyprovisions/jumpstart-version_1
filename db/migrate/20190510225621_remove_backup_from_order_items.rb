class RemoveBackupFromOrderItems < ActiveRecord::Migration[5.2]
  def change
    remove_column :order_items, :backup, :text
  end
end
