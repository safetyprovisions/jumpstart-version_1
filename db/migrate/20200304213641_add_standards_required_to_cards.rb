class AddStandardsRequiredToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :standards_required, :boolean, default: false
  end
end
