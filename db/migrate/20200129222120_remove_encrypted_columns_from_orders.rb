class RemoveEncryptedColumnsFromOrders < ActiveRecord::Migration[5.2]
  def change
    # remove blind indexes first
    remove_index "orders", name: "index_orders_on_to_name_bidx"
    remove_index "orders", name: "index_orders_on_to_street1_bidx"
    remove_index "orders", name: "index_orders_on_to_city_bidx"
    remove_index "orders", name: "index_orders_on_to_zip_bidx"
    remove_index "orders", name: "index_orders_on_to_email_bidx"
    remove_index "orders", name: "index_orders_on_additional_info_bidx"

    # remove encrypted data
    remove_column :orders, :to_name_ciphertext, :string
    remove_column :orders, :to_street1_ciphertext, :string
    remove_column :orders, :to_city_ciphertext, :string
    remove_column :orders, :to_zip_ciphertext, :string
    remove_column :orders, :to_email_ciphertext, :string
    remove_column :orders, :additional_info_ciphertext, :string

    remove_column :orders, :to_name_bidx, :string
    remove_column :orders, :to_street1_bidx, :string
    remove_column :orders, :to_city_bidx, :string
    remove_column :orders, :to_zip_bidx, :string
    remove_column :orders, :to_email_bidx, :string
    remove_column :orders, :additional_info_bidx, :string
  end
end
