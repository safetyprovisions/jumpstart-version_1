class AddActiveToPosters < ActiveRecord::Migration[5.2]
  def change
    add_column :posters, :active, :boolean, default: true
  end
end
