class AddMetaDescriptionToProductGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :product_groups, :meta_description, :text
  end
end
