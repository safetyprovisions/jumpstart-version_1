class AddMarketTypeToOnlineCourses < ActiveRecord::Migration[5.2]
  def up
    add_column :online_courses, :market_type, :string, default: "usa", null: false
  end

  def down
    remove_column :online_courses, :market_type, :string
  end
end
