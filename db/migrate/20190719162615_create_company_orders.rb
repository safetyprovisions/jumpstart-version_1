class CreateCompanyOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :company_orders do |t|
      t.references :user, foreign_key: true
      t.references :company, foreign_key: true
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
