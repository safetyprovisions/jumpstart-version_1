class AddKitTypeToKits < ActiveRecord::Migration[5.2]
  def change
    remove_column :kits, :type, :string
    add_column :kits, :kit_type, :string, null:false, default: 'usa'
  end
end
