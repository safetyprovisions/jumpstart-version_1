class AddSaleStartToKits < ActiveRecord::Migration[5.2]
  def change
    add_column :kits, :sale_start, :date
  end
end
