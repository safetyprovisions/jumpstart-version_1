class CreateCouponCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :coupon_codes do |t|
      t.string :code, null: false
      t.date :valid_from, null: false
      t.date :valid_to, null: false
      t.integer :flat_rate
      t.integer :percentage

      t.timestamps
    end
  end
end
