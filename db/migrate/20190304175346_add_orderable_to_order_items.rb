class AddOrderableToOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_reference :order_items, :orderable, polymorphic: true, index: true
  end
end
