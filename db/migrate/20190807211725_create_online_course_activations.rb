class CreateOnlineCourseActivations < ActiveRecord::Migration[5.2]
  def change
    create_table :online_course_activations do |t|
      t.references :user, foreign_key: true
      t.references :order, foreign_key: true
      t.references :online_course, foreign_key: true

      t.timestamps
    end
  end
end
