class AddTalentUsernameToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :talent_username, :string
  end
end
