class CreateButtons < ActiveRecord::Migration[5.2]
  def change
    create_table :buttons do |t|
      t.text :name, null: false
      t.integer :price, null: false
      t.integer :cad_price, null: false
      t.integer :shipping_price, null: false
      t.integer :priority_shipping, null: false
      t.integer :intl_shipping, null: false
      t.references :button_group, foreign_key: true, null: false
      t.integer :weight, null: false
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
