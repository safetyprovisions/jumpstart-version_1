class AddEncryptedFieldsToOrdersAgain < ActiveRecord::Migration[5.2]
  def change
    # encrypted data
    add_column :orders, :to_name_ciphertext, :string
    add_column :orders, :to_street1_ciphertext, :string
    add_column :orders, :to_city_ciphertext, :string
    add_column :orders, :to_zip_ciphertext, :string
    add_column :orders, :to_email_ciphertext, :string
    add_column :orders, :additional_info_ciphertext, :string

    # blind index
    add_column :orders, :to_name_bidx, :string
    add_index :orders,  :to_name_bidx

    add_column :orders, :to_street1_bidx, :string
    add_index :orders,  :to_street1_bidx

    add_column :orders, :to_city_bidx, :string
    add_index :orders,  :to_city_bidx

    add_column :orders, :to_zip_bidx, :string
    add_index :orders,  :to_zip_bidx

    add_column :orders, :to_email_bidx, :string
    add_index :orders,  :to_email_bidx

    add_column :orders, :additional_info_bidx, :string
    add_index :orders,  :additional_info_bidx
  end
end
