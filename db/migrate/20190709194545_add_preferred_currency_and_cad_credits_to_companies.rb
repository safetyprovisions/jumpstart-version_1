class AddPreferredCurrencyAndCadCreditsToCompanies < ActiveRecord::Migration[5.2]
  def up
    add_column :companies, :preferred_currency, :string, default: 'usd', null: false
    add_column :companies, :cad_credits, :integer
  end

  def down
    remove_column :companies, :preferred_currency, :string, default: 'usd', null: false
    remove_column :companies, :cad_credits, :integer
  end
end
