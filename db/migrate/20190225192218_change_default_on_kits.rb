class ChangeDefaultOnKits < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:kits, :price, from: 0, to: nil)
    change_column_default(:kits, :kit_type, from: "usa", to: nil)
  end
end
