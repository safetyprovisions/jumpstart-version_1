class CreateOnlineCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :online_courses do |t|
      t.string :title, null: false
      t.text :image_data, null: false
      t.text :description, null: false
      t.integer :price, null: false
      t.integer :sale_price
      t.date :sale_start
      t.date :sale_expiry
      t.integer :language, null: false
      t.integer :standards, null: false
      t.integer :product_group_id, null: false
      t.string :video
      t.integer :talent_course_id, null: false
      t.text :practical_evaluation_data, null: false

      t.timestamps
    end
  end
end
