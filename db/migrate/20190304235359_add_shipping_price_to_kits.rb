class AddShippingPriceToKits < ActiveRecord::Migration[5.2]
  def change
    add_column :kits, :shipping_price, :integer
    add_column :kits, :weight, :integer
  end
end
