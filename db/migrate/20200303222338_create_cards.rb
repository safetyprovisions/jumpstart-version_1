class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.text :name, null: false
      t.integer :price, null: false
      t.integer :cad_price, null: false
      t.integer :shipping_price, null: false
      t.integer :priority_shipping, null: false
      t.integer :intl_shipping, null: false
      t.references :card_group, foreign_key: true, null: false
      t.integer :weight

      t.timestamps
    end
  end
end
