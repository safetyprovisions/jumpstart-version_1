class AddTalentNameToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :talent_name, :string
  end
end
