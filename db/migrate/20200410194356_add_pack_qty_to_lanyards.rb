class AddPackQtyToLanyards < ActiveRecord::Migration[5.2]
  def change
    add_column :lanyards, :pack_qty, :string, default: 1
  end
end
