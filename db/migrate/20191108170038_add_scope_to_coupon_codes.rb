class AddScopeToCouponCodes < ActiveRecord::Migration[5.2]
  def change
    add_column :coupon_codes, :scope, :text, null: false
    add_column :coupon_codes, :min_purchase_amt, :integer
  end
end
