class AddNullFalseToCreditPacksCadCredits < ActiveRecord::Migration[5.2]
  def up
    change_column_null(:credit_packs, :cad_credits, false)
  end

  def down
    change_column_null(:credit_packs, :cad_credits, true)
  end
end
