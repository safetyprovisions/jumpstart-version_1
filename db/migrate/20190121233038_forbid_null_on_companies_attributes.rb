class ForbidNullOnCompaniesAttributes < ActiveRecord::Migration[5.2]
  def change
    change_column_null(:companies, :name, false)
    change_column_null(:companies, :address, false)
    change_column_null(:companies, :city, false)
    change_column_null(:companies, :state, false)
    change_column_null(:companies, :zip, false)
  end
end
