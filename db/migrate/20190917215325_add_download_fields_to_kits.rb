class AddDownloadFieldsToKits < ActiveRecord::Migration[5.2]
  def up
    add_column :kits, :download, :text, null: false
    add_column :kits, :additional_download, :text, null: false
    remove_column :kits, :download_data
    remove_column :kits, :additional_download_data
  end

  def down
    remove_column :kits, :download
    remove_column :kits, :additional_download
    add_column :kits, :download_data, :text
    add_column :kits, :additional_download_data, :text
  end
end
