class ForbidNullOnProductGroups < ActiveRecord::Migration[5.2]
  def change
    change_column_null(:product_groups, :video, false)
    change_column_null(:product_groups, :overview, false)
    change_column_null(:product_groups, :training_standards, false)
    change_column_null(:product_groups, :facts, false)
    change_column_null(:product_groups, :fails, false)
  end
end
