class AddActiveToKits < ActiveRecord::Migration[5.2]
  def change
    add_column :kits, :active, :boolean, default: true
  end
end
