class AddTalentBranchIdToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :talent_branch_id, :string
  end
end
