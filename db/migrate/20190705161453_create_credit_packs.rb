class CreateCreditPacks < ActiveRecord::Migration[5.2]
  def change
    create_table :credit_packs do |t|
      t.string :name, null: false
      t.integer :price, null: false
      t.integer :credits, null: false

      t.timestamps
    end
  end
end
