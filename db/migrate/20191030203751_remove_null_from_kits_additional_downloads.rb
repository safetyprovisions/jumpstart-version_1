class RemoveNullFromKitsAdditionalDownloads < ActiveRecord::Migration[5.2]
  def change
    change_column :kits, :additional_download, :text, null: true
  end
end
