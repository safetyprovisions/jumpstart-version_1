class ChangeDefaultsOnCreditPacks < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:credit_packs, :price, from: nil, to: 0)
    change_column_default(:credit_packs, :credits, from: nil, to: 0)
    change_column_default(:credit_packs, :cad_credits, from: nil, to: 0)
  end
end
