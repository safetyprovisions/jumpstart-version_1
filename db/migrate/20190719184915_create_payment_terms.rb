class CreatePaymentTerms < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_terms do |t|
      t.references :company, foreign_key: true, null: false
      t.integer :term, null: false

      t.timestamps
    end
  end
end
