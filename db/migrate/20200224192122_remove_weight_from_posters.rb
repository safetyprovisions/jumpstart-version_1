class RemoveWeightFromPosters < ActiveRecord::Migration[5.2]
  def change
    remove_column :posters, :weight, :string
  end
end
