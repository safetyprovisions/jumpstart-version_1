class AddAnnouncementsLastReadAtToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :announcements_last_read_at, :datetime
  end
end
