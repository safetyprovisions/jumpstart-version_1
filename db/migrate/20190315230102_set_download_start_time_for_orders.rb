class SetDownloadStartTimeForOrders < ActiveRecord::Migration[5.2]
  def change
    change_column :orders, :download_start_time, :datetime, default: -> { 'CURRENT_TIMESTAMP' }, null: false
  end
end
