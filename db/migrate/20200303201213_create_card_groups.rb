class CreateCardGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :card_groups do |t|
      t.text :name, null: false
      t.text :description, null: false
      t.text :image_data, null: false
      t.string :slug
      t.text :keywords, null: false
      t.text :meta_description, null: false

      t.timestamps
    end
    add_index :card_groups, :slug, unique: true
  end
end
