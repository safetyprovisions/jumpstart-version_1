class AddPackQtyToKeychains < ActiveRecord::Migration[5.2]
  def change
    add_column :keychains, :pack_qty, :string, default: 1
  end
end
