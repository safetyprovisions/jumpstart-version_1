class AddPreferredCurrencyToUsers < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :preferred_currency, :string, default: "usd", null: false
  end

  def down
    remove_column :users, :preferred_currency, :string, default: "usd", null: false
  end
end
