class CreateAltNames < ActiveRecord::Migration[5.2]
  def change
    create_table :alt_names do |t|
      t.text :name, null: false
      t.references :product_group, foreign_key: true, null: false

      t.timestamps
    end
  end
end
