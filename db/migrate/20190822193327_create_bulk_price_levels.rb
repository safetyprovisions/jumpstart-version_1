class CreateBulkPriceLevels < ActiveRecord::Migration[5.2]
  def change
    create_table :bulk_price_levels do |t|
      t.integer :qty, null: false
      t.integer :price, null: false
      t.integer :cad_price, null: false
      t.string :tier, null: false

      t.timestamps
    end
  end
end
