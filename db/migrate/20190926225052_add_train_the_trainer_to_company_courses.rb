class AddTrainTheTrainerToCompanyCourses < ActiveRecord::Migration[5.2]
  def up
    add_reference :company_courses, :train_the_trainer, foreign_key: true
  end

  def down
    remove_reference :company_courses, :train_the_trainer, foreign_key: true
  end
end
