class AddAdditionalDownloadDataToKits < ActiveRecord::Migration[5.2]
  def change
    add_column :kits, :additional_download_data, :text
  end
end
