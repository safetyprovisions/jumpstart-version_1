class AddShippingAndBackupFeldsToOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :order_items, :shipping_method, :text, default: "none", null: false
    add_column :order_items, :backup, :text, default: "none", null: false
  end
end
