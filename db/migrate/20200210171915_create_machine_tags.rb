class CreateMachineTags < ActiveRecord::Migration[5.2]
  def change
    create_table :machine_tags do |t|
      t.text :name, null: false
      t.integer :price, null: false
      t.integer :cad_price, null: false
      t.integer :shipping_price, null: false
      t.integer :priority_shipping, null: false
      t.integer :intl_shipping, null: false
      t.references :machine_tag_group, foreign_key: true
      t.integer :weight, null: false

      t.timestamps
    end
  end
end
