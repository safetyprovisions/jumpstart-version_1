class AddTimesUsedToCouponCodes < ActiveRecord::Migration[5.2]
  def change
    add_column :coupon_codes, :times_used, :integer, default: 0
  end
end
