class AddIdentifierToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :identifier, :string
  end
end
