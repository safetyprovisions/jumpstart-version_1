source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'rails', '~> 5.2.2'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.1', '>= 4.1.2'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
gem 'mini_magick', '~> 4.10', '>= 4.10.1'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # gem 'rspec-rails',        '~> 3.8', '>= 3.8.2'
  # gem 'factory_bot_rails',  '~> 4.11', '>= 4.11.1'
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara',           '~> 3.12'
  gem 'database_cleaner',   '~> 1.7'
  gem 'shoulda-matchers',   '~> 3.1'
  gem 'rails-controller-testing',
                            '~> 1.0' # Required by shoulda-matchers
  # gem 'selenium-webdriver'
  # gem 'chromedriver-helper' # Replaced by webdrivers
  gem 'webdrivers', '~> 4.2'
  gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'
  gem 'bullet', '~> 6.0' # Find and fix N+1 queries
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console',        '>= 3.3.0'
  gem 'listen',             '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'
  gem 'rails_real_favicon'
  # Display pageload speed, etc.
  gem 'rack-mini-profiler', '~> 1.1'
end

group :test do
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'webmock', '~> 3.7'
  gem 'vcr', '~> 5.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# gem 'activerecord_where_assoc', '~> 1.0' # No longer needed
gem 'acts-as-taggable-on', '~> 6.0'
gem 'administrate', '~> 0.10.0'
gem 'ahoy_matey', '~> 3.0' # Analytics and charts
gem 'aws-sdk-s3', '~> 1.30'
gem 'blind_index', '~> 1.0' # Decrypts (used w/ lockbox)
gem 'bootstrap', '~> 4.3'
gem 'chartkick', '~> 3.2'
gem 'data-confirm-modal', '~> 1.6'
gem 'devise', '~> 4.4'
gem 'devise-bootstrapped', github: 'excid3/devise-bootstrapped', branch: 'bootstrap4'
gem 'devise_masquerade', '~> 0.6.2'
gem 'easypost', '~> 3.0'
gem 'fastimage', '~> 2.1'
gem 'font-awesome-sass', '~> 5.5'
gem 'foreman', '~> 0.84.0'
gem 'friendly_id', '~> 5.2'
gem 'gravatar_image_tag', github: 'mdeering/gravatar_image_tag'
gem 'groupdate', '~> 4.1' # For Chartkick/Ahoy
gem 'image_processing', '~> 1.10', '>= 1.10.3' # For Shrine derivatives
gem 'jquery-rails', '~> 4.3.4'
gem 'local_time', '~> 2.0'
gem 'lockbox', '~> 0.3.1' # Encrypts sensitive columns
gem 'mail_form', '~> 1.7'
# gem 'mini_magick', '~> 4.8'
gem 'name_of_person', '~> 1.1'
gem 'omniauth-facebook', '~> 5.0'
gem 'omniauth-github', '~> 1.3'
gem 'omniauth-twitter', '~> 1.4'
gem 'pagy', '~> 3.5'
# gem 'postmark-rails', '~> 0.19.0' # Send emails
gem 'pundit', '~> 2.0'
# gem 'rails-controller-testing', '~> 1.0', '>= 1.0.4' # Required by shoulda-matchers
gem 'rack-attack', '~> 6.2'
gem 'ransack', '~> 2.3' # Search functionality
gem 'receipts', '~> 0.2.2'
gem 'rollbar' # Error monitoring & reporting
gem 'sendgrid', '~> 1.2' # Send emails
# gem 'shoulda-matchers', '~> 3.1', '>= 3.1.2'
gem 'shrine', '~> 3.2', '>= 3.2.1'
# gem 'shrine', '~> 3.0'
# gem 'shrine', '~> 2.19' # Original version
gem 'sidekiq', '~> 5.1'
# gem 'sitemap_generator', '~> 6.0' # Rolling my own now
gem 'stripe', '~> 4.5'
gem 'sucker_punch', '~> 2.1' # Email sending app recommended by @excid3
gem 'talentlms', github: 'dlepage/talentlms'
gem 'trix-rails', require: 'trix'
gem 'turnout', '~> 2.5' # Maintenance
gem 'webpacker', '>= 4.0'
gem 'whenever', require: false

### Stuff I Added After Reading Rails Testing For Beginners
gem 'pry'
gem 'money-rails'
# gem 'simple_form', '~> 4.1'
