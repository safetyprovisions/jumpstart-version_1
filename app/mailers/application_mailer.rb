class ApplicationMailer < ActionMailer::Base
  # default from: 'contact@hardhattrainingseries.com'
  default from: 'office@hardhattraining.com'
  layout 'mailer'
end

# NOTE Mailers act like Controllers for emails
