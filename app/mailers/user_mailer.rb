class UserMailer < ApplicationMailer
  def order_success_email
    @user  = params[:user]
    @order = Order.where(user_id: @user.id, status: "paid").last

    mail(to: @user.email, subject: "Hard Hat Training Series Order ##{ @order.id }")
  end

  def order_notification_email
    @user  = params[:user]
    @order = Order.where(user_id: @user.id, status: "paid").last

    mail(to: 'office@hardhattraining.com', subject: "Hard Hat Training Series - New Order ##{ @order.id } for #{ @user.name.full }")
  end

  def label_failure_email
    @order = Order.find(params[:order_id])

    mail(to: 'office@hardhattraining.com', subject: "Label Failure for Hard Hat Training Series Order ##{ @order }")
  end
end
