class OrderSuccessJob
  include SuckerPunch::Job

  def perform(user)
    UserMailer.with(user).order_success_email.deliver_now
    UserMailer.with(user).order_notification_email.deliver_now
  end
end
