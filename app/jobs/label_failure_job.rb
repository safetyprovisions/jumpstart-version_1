class LabelFailureJob
  include SuckerPunch::Job

  def perform(order_id)
    UserMailer.with(order_id).label_failure_email.deliver_now
  end
end
