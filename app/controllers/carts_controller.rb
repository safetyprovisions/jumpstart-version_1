class CartsController < ApplicationController
  # before_action :authenticate_user!
  before_action :current_user_check
  before_action :set_coupon_code
  before_action :revalidate_code
  before_action :set_order_currency

  def show
    # Filter out backups
    all_order_items = current_cart.order_items
    @order_items = all_order_items.where.not(orderable_type: "Backup").sort_by { |word| word.oi_name }
    @backup_items = all_order_items.where(orderable_type: "Backup")

    all_items = @order_items + @backup_items

    course_types   = %W[ Kit OnlineCourse TrainTheTrainer GroupTraining ]
    @courses_added = (all_order_items.pluck(:orderable_type) & course_types).any?
    if @courses_added == true
      @certificates = Certificate.where(active: true)
      # @cards        = Card.all
      @card_groups  = CardGroup.where(active: true)
      @lanyard_groups  = LanyardGroup.where(active: true)
    end

    shippables = %W[ Product Backup Keychain MachineTag Poster Certificate Card Shirt Hat Lanyard Button Sticker ]
    @shippable = all_items.pluck(:orderable_type) & shippables

    commentables = %W[ GroupTraining ]
    @commentable = @order_items.pluck(:orderable_type) & commentables
    # These comments are only for GroupTrainings; refactor if other commentables are added.
    @comments = 'Please provide a comma-separated list of your Group Training participants.'

    # Original Method
    # @usb_backup = Backup.find_by(format: "usb", active: true)
    # @cd_backup  = Backup.find_by(format: "cd", active: true)
    # New Method
    @backup_formats = Backup.where(active: true)

    if ProductGroup.where(name: 'Backup Methods').any?
      @backup_methods = Product.where(product_group_id: ProductGroup.find_by(name: 'Backup Methods').id)
    end

    @currency = session[:preferred_currency]

    if current_company.present?
      @invoicable = (PaymentTerm.where(company: current_company.id).any? ? true : false)
    end

    # Get all Product Group IDs
    product_group_ids = []
    items_with_product_groups = current_cart.order_items.where.not(orderable_type: "Backup").where.not(orderable_type: "Certificate").where.not(orderable_type: "Shirt").where.not(orderable_type: "Hat").where.not(orderable_type: "Sticker").includes([:orderable])
    items_with_product_groups.each do |oi|
      product_group_ids |=  [oi.orderable.product_group.id]
    end
    # Find their Related Items
    related_items = RelatedItem.where(product_group_id: product_group_ids)
    # related_items = RelatedItem.where(product_group_id: product_group_ids).pluck(:id)

    @related_items = related_items
          .select('DISTINCT relatable_type, relatable_id')
          .group(:relatable_type, :relatable_id)

    # # Get the IDs for the ones that are active
    # active_ids = []
    # related_items.each do |item|
    #   if RelatedItem.find(item).relatable.active == true
    #     active_ids |= [item]
    #   end
    #   active_ids
    # end
    # # Find the Related Items for the active Product Groups
    # active_related_items = RelatedItem.where(product_group_id: active_ids)

    # @related_items = active_related_items
    #       .select('DISTINCT relatable_type, relatable_id')
    #       .group(:relatable_type, :relatable_id)
  end

  private
    def set_coupon_code
      if current_cart.coupon_code.present?
        @coupon_code = CouponCode.find_by(code: current_cart.coupon_code)
      else
        @coupon_code = nil
      end
    end

    # Make sure coupon code still applies to current order_items
    def revalidate_code
      if current_cart.coupon_code.present?
        set_coupon_code
        item_types = current_cart.order_items.pluck(:orderable_type)

        if ((@coupon_code.scope == 'online_courses') && item_types.include?("OnlineCourse")) ||
          ((@coupon_code.scope == 'kits') && item_types.include?("Kit")) ||
          ((@coupon_code.scope == 'train_the_trainers') && item_types.include?("TrainTheTrainer")) ||
          ((@coupon_code.scope == 'order_total') && current_cart.subtotal(session[:preferred_currency]) >= @coupon_code.min_purchase_amt)
          true
        else
          current_cart.update(coupon_code: '')
          current_cart.save
        end
      end
    end

    def set_order_currency
      # if current_cart.currency == nil
      current_cart.update(currency: session[:preferred_currency])
      current_cart.save
      # end
    end

end
