class KeychainGroupsController < ApplicationController
  before_action :set_keychain_group, only: [:edit, :update, :destroy, :check_for_keychains, :delete_related_items]
  before_action :check_and_cleanup, only: [:destroy]

  def index
    @keychain_groups = KeychainGroup.all
  end

  def show
    @keychain_group = KeychainGroup.friendly.find(params[:id])
    @keychains = Keychain.where(keychain_group_id: @keychain_group.id, active: true).order(price: :asc)
  end

  def new
    @keychain_group = KeychainGroup.new
  end

  def edit
  end

  def create
    @keychain_group = KeychainGroup.new(keychain_group_params)
    @keychain_group.image_derivatives! # generate Shrine derivatives

    respond_to do |format|
      if @keychain_group.save
        format.html { redirect_to @keychain_group, notice: 'Keychain group was successfully created.' }
        format.json { render :show, status: :created, location: @keychain_group }
      else
        format.html { render :new }
        format.json { render json: @keychain_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @keychain_group.update(keychain_group_params)
        @keychain_group.image_derivatives! # generate Shrine derivatives
        @keychain_group.save
        format.html { redirect_to @keychain_group, notice: 'Keychain group was successfully updated.' }
        format.json { render :show, status: :ok, location: @keychain_group }
      else
        format.html { render :edit }
        format.json { render json: @keychain_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @keychain_group.destroy
    respond_to do |format|
      format.html { redirect_to keychain_groups_url, notice: 'Keychain group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_keychain_group
      @keychain_group = KeychainGroup.friendly.find(params[:id])
    end

    def check_and_cleanup
      check_for_keychains

      delete_related_items
    end

    def check_for_keychains
      if @keychain_group.keychains.any?
        flash[:alert] = 'Please remove any Keychains that are attached to this group before deleting the Keychain Group.'
        redirect_to @keychain_group
      end
    end

    def delete_related_items
      items_to_delete = RelatedItem.where(relatable_type: "KeychainGroup", relatable_id: @keychain_group.id)
      if items_to_delete.present?
        items_to_delete.each do |item|
          item.delete
        end
      end
    end

    def keychain_group_params
      params.require(:keychain_group).permit(:name, :description, :image, :keywords, :meta_description, :active)
    end
end
