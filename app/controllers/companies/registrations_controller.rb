# frozen_string_literal: true

class Companies::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    if current_user
      super
    else
      flash[:alert] = 'You must be logged in.'
      redirect_to new_user_session_path
    end
  end

  # POST /resource
  def create
    params[:company][:email] = current_user.email
    params[:company][:password] = Rails.application.credentials.company_key
    params[:company][:password_confirmation] = Rails.application.credentials.company_key

    set_name

    super

    if resource.save
      @company.employees << current_user
      CompanyAdmin.create(user_id: current_user.id, company_id: @company.id)
      submit_new_talent_branch
    end

  end

  def after_sign_up_path_for(resource)
    companies_homepage_path
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute, :name, :address, :city, :state, :zip, :parent_id, :email, :talent_name])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:attribute, :name, :address, :city, :state, :zip, :parent_id, :email, :talent_name])
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  private

    def new_talent_branch(name, description, disallow_global_login, group_id, language, timezone, signup_method, ecommerce_processor, currency, paypal_email, ecommerce_subscription, internal_announcement, external_announcement, creator_id, ecommerce_subscription_price, user_type, registration_email_restriction, users_limit, ecommerce_subscription_interval, ecommerce_credits)

      require_dependencies

      url = URI("https://safetyclasses.talentlms.com/api/v1/createbranch")

      make_secure_connection(url)

      @request = Net::HTTP::Post.new(url)

      make_http_request(url)

      @request["Accept"] = '*/*'
      @request["Host"] = 'safetyclasses.talentlms.com'
      @request["accept-encoding"] = 'gzip, deflate'
      @request["content-type"] = 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
      @request["Connection"] = 'close' # Use 'keep-alive' if we need to make more requests at some point
      @request.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"name\"\r\n\r\n" +
        "#{ name }" +
        content_block("description", description) +
        content_block("disallow_global_login", disallow_global_login) +
        content_block("group_id", group_id) +
        content_block("language", language) +
        content_block("timezone", timezone) +
        content_block("signup_method", signup_method) +
        content_block("ecommerce_processor", ecommerce_processor) +
        content_block("currency", currency) +
        content_block("paypal_email", paypal_email) +
        content_block("ecommerce_subscription", ecommerce_subscription) +
        content_block("internal_announcement", internal_announcement) +
        content_block("external_announcement", external_announcement) +
        content_block("creator_id", creator_id) +
        content_block("ecommerce_subscription_price", ecommerce_subscription_price) +
        content_block("user_type", user_type) +
        content_block("registration_email_restriction", registration_email_restriction) +
        content_block("users_limit", users_limit) +
        content_block("ecommerce_subscription_interval", ecommerce_subscription_interval) +
        content_block("ecommerce_credits", ecommerce_credits) +
        form_boundary + "--"

      parse_response

      if @parsed["error"] && (@parsed["error"]["message"] == "A branch with the same name already exists")

        @name += generate_chars(1)

        submit_new_talent_branch
      end

      resource.talent_branch_id = @parsed["id"]
      resource.talent_name = @parsed["name"]
      resource.save

    end

    def set_name
      @name = params[:company][:name]
      name_compressor = /(\w+)/
      @name = @name.scan(name_compressor).join.downcase
    end

    def require_dependencies
      require 'net/http'
      require 'json'
    end

    def generate_postman_token
      generate_chars(8) + '-' +
      generate_chars(4) + '-' +
      generate_chars(4) + '-' +
      generate_chars(4) + '-' +
      generate_chars(12)
    end

    def generate_chars(number)
      charset = Array(0..9) + Array("a".."z")
      Array.new(number) { charset.sample }.join
    end

    def make_secure_connection(url)
      @http = Net::HTTP.new(url.host, url.port)
      @http.use_ssl = true
    end

    def make_http_request(url)
      @request["Authorization"] = 'Basic NUFWWjF3MUFraHBBTFNkbmx6WE1OSDF5bmZ0NXExOg=='
      @request["cache-control"] = 'no-cache'
      @request["Postman-Token"] = generate_postman_token
    end

    def parse_response
      response = @http.request(@request)
      @parsed = JSON.parse(response.read_body)
    end

    def form_boundary
      "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW"
    end

    def content_block(content_type, content)
      form_boundary + "\r\nContent-Disposition: form-data; name=\"#{ content_type }\"\r\n\r\n" +
        "#{ content }"
    end

    def submit_new_talent_branch
      # Create New Talent Branch
      new_talent_branch(
              #name:
              @name,

              # description:
              "Online safety courses for #{ params[:company][:name] }",

              # disallow_global_login:
              'on',

              # group_id:
              '',

              # language:
              'en',

              # timezone:
              "-7",

              # signup_method:
              'direct',

              # ecommerce_processor:
              'stripe',
              # payment_processor: , # This field is returned, and ecommerce_processor is not.

              # currency:
              'dollar',

              # paypal_email:
              '',

              # ecommerce_subscription:
              '',

              # internal_announcement:
              '**IMPORTANT: We publish our courses to work on all computers and mobile devices. However, there are many variables on mobile devices that can affect playback. On occasion, there have been reports of courses not always recording progress properly. When possible, we ask users to take the courses on a desktop or laptop computer.',

              # external_announcement:
              "Online safety courses for #{ params[:company][:name] }",

              # creator_id:
              '',

              # ecommerce_subscription_price:
              '',

              # user_type:
              "Learner-Type",

              # registration_email_restriction:
              '',

              # users_limit:
              '',

              # ecommerce_subscription_interval:
              '',

              # ecommerce_credits:
              0
      )
    end
end
