# frozen_string_literal: true

class Companies::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]
  after_action :set_preferred_currency, only: [:new, :create]
  after_action :set_current_company, only: [:new, :create]
  before_action :clear_preferred_currency, only: [:destroy]

  # GET /resource/sign_in
  def new
    # super
    flash[:error] = "Not Authorized"
    redirect_to root_path
  end

  # POST /resource/sign_in
  def create
    super
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  # GET /resource/edit
  def edit
    super
  end

  def after_sign_in_path_for(resource)
      # if resource.sign_in_count == 1
         companies_homepage_path
      # else
      #    root_path
      # end
  end

  private

    def set_preferred_currency
      if current_company.present?
        session[:preferred_currency] = current_company.preferred_currency
      end
    end

    def set_current_company
      if current_company.present?
        session[:current_company] = current_company
      end
    end

    def clear_preferred_currency
      session[:preferred_currency] = nil
    end

    # def set_talent_branch_info
    #   require_dependencies

    #   # url = URI("https://safetyclasses.talentlms.com/api/v1/branches/id:#{ id }")
    #   url = URI("https://safetyclasses.talentlms.com/api/v1/branches/id:#{ current_company.talent_branch_id }")

    #   make_secure_connection(url)
    #   @request = Net::HTTP::Get.new(url)
    #   make_http_request(url)
    #   parse_response
    #   @branch = @parsed
    # end

    # def set_direct_purchases
    #   @direct_purchases = current_company.company_courses.where('quantity > 0')
    # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
