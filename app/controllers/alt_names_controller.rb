class AltNamesController < ApplicationController
  before_action :set_alt_name, only: [:show, :edit, :update, :destroy]
  # before_action :authenticate_user!
  before_action :verify_superadmin!

  def index
    # @alt_names = AltName.all
    ids = AltName.pluck(:product_group_id).uniq.sort
    @product_groups = ProductGroup.where(id: ids).order(name: :asc) # ProductGroup.find(ids)
  end

  def show
  end

  def new
    @alt_name = AltName.new
  end

  def edit
  end

  def create
    @alt_name = AltName.new(alt_name_params)

    respond_to do |format|
      if @alt_name.save
        format.html { redirect_to @alt_name, notice: 'Alt name was successfully created.' }
        format.json { render :show, status: :created, location: @alt_name }
      else
        format.html { render :new }
        format.json { render json: @alt_name.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @alt_name.update(alt_name_params)
        format.html { redirect_to @alt_name, notice: 'Alt name was successfully updated.' }
        format.json { render :show, status: :ok, location: @alt_name }
      else
        format.html { render :edit }
        format.json { render json: @alt_name.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @alt_name.destroy
    respond_to do |format|
      format.html { redirect_to alt_names_url, notice: 'Alt name was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_alt_name
      @alt_name = AltName.find(params[:id])
    end

    def alt_name_params
      params.require(:alt_name).permit(:name, :product_group_id)
    end
end
