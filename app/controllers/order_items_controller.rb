class OrderItemsController < ApplicationController
  before_action :authenticate_user!

  def create
    if (params[:orderable_type] == "Certificate") && (params[:trainee].empty? || params[:course_name].empty?)

      flash[:alert] = "You must provide all trainee information."
      redirect_back fallback_location: { action: "show", id: Certificate.find(params[:orderable_id]) }

    else
      @specifics = []
      # For Shirts, Hats
      if params[:size].present?
        @specifics << params[:size]
      end

      # For Blank Wallet Cards
      if params[:standards].present?
        @specifics << params[:standards]
      end

      # For Blank Wallet Cards
      if params[:pack_qty].present?
        @specifics << params[:pack_qty]
      end

      # For Hand Signal Cards
      if params[:card_type].present?
        @specifics << params[:card_type]
      end

      # For CPR Keychains, Shirts
      if params[:color].present?
        @specifics << params[:color]
      end

      # For things like Plastic Wallet Cards
      if params[:trainee].present?
        @specifics << "#{ params[:trainee] }, #{ params[:course_name] }, #{ params[:certificate][:training_date] }"
      end

      @specifics = @specifics.join(", ")

      @order_item = current_cart.order_items.find_or_initialize_by(
        orderable_id:   params[:orderable_id],
        orderable_type: params[:orderable_type],
        backup_for:     params[:backup_for],
        specifics:      @specifics ||= nil
      )
      @order_item.update(quantity: @order_item.quantity + 1)

      if params[:orderable_type] == "CreditPack"
        session[:return_to_assign] = true
      end

      set_reminder

      redirect_to cart_path
    end
  end

  def update
    @order_item = current_cart.order_items.find(params[:id])

    if params[:order_item][:quantity].present? && (params[:order_item][:quantity] == "0")
      if @order_item.orderable_type == "Kit" && @order_item.existing_backup.present?
        @order_item.existing_backup.delete # unless @order_item.orderable_type == "Backup"
      end
      @order_item.delete
    else
      @order_item.update(order_item_params)
    end

    set_reminder

    redirect_to cart_path
  end

  # def add_backup
  #   order_item = current_cart.order_items.find(params[:id])
  #   order_item.update(backup: params[:backup])
  #   # total: (order_item.total + params[:backup_price].to_i)

  #   redirect_to cart_path
  # end

  private

    def order_item_params
      params.require(:order_item).permit(:quantity)
    end

    def set_reminder
      if (@order_item.orderable_type == 'GroupTraining') && (@order_item.quantity == 1)
        flash[:alert] = "For Group Trainings, you must specify the number of attendees in the Quantity field."
      end
    end

end
