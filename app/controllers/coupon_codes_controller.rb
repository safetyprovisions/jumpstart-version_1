class CouponCodesController < ApplicationController
  before_action :verify_superadmin!
  before_action :set_coupon_code, only: [:show, :edit, :update, :destroy]

  def index
    @coupon_codes = CouponCode.all.order(code: :asc)
  end

  def show
  end

  def new
    @coupon_code = CouponCode.new
  end

  def edit
  end

  def create
    @coupon_code = CouponCode.new(coupon_code_params)

    respond_to do |format|
      if @coupon_code.save
        format.html { redirect_to @coupon_code, notice: 'Coupon code was successfully created.' }
        format.json { render :show, status: :created, location: @coupon_code }
      else
        format.html { render :new }
        format.json { render json: @coupon_code.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @coupon_code.update(coupon_code_params)
        format.html { redirect_to @coupon_code, notice: 'Coupon code was successfully updated.' }
        format.json { render :show, status: :ok, location: @coupon_code }
      else
        format.html { render :edit }
        format.json { render json: @coupon_code.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @coupon_code.destroy
    respond_to do |format|
      format.html { redirect_to coupon_codes_url, notice: 'Coupon code was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_coupon_code
      @coupon_code = CouponCode.find(params[:id])
    end

    def coupon_code_params
      params.require(:coupon_code).permit(:code, :valid_from, :valid_to, :flat_rate, :percentage, :scope, :min_purchase_amt)
    end
end
