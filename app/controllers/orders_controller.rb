class OrdersController < ApplicationController
  include Pagy::Backend

  before_action :authenticate_user!, except: [:change_shipping]
  before_action :set_order, only: [:show, :update]

  def index
    @pagy, @orders = pagy(current_user.orders.where.not(status: "draft").order(id: :desc))
    authorize @orders
  end

  def show
    @backups = @order.order_items.where(orderable_type: 'Backup')

    company_order = CompanyOrder.find_by(order_id: @order.id)
    if company_order.nil?
      @company_order_exists = false
    else
      @company_order_exists = true
      @company = CompanyOrder.find_by(order_id: @order.id).company
    end

    @online_course_activations = OnlineCourseActivation.where(order_id: @order.id, user_id: current_user)

    respond_to do |format|
      format.html
      format.pdf {
        send_data @order.receipt.render,
          filename: "safety-provisions-order-number-#{ @order.id }-receipt.pdf",
          type: "application/pdf",
          disposition: :inline # :attachment
      }
    end
  end

  def update
    @order.update(order_params)

    redirect_to cart_path
  end

  def download_validator(order=params[:order], item=params[:item], download_type=params[:download_type])
    if (download_type == 'kit_additional') || (download_type == 'kit_primary')
      this_item = Kit.find(item)
      download_time = Order.find(order).download_start_time

      if Time.now.between?(download_time, (download_time + 72.hours))
        create_filename(this_item, download_type)
        s3_downloader('hht-dotcom-wp', @filename)
      else
        no_download_for_you(order)
      end
    elsif download_type == 'digital_poster'
      this_poster = Poster.find(item)
      download_time = Order.find(order).download_start_time

      if Time.now.between?(download_time, (download_time + 72.hours))
        if Rails.env.production?
          s3_downloader('hht-dotcom-wp', "Poster/download/#{ this_poster.name.parameterize }/#{ this_poster.download.url.split("/")[-1] }")
        else
          redirect_to this_poster.download.url
        end
      else
        no_download_for_you(order)
      end
    elsif download_type == 'practical_evaluation'
      this_item = OnlineCourse.find(item)
      redirect_to this_item.download.url
    else
      no_download_for_you(order)
    end
  end

  def create_filename(kit, download_type)
    market = kit.market_type
    @filename = ''
    if market == 'tagalog'
      @filename += 'Tagalog/'
    elsif market == 'canada'
      @filename += 'Canada/'
    elsif market == 'spanish'
      @filename += 'Spanish/'
    else #if market == 'usa'
      @filename += 'USA/'
    end

    if download_type == 'kit_primary'
      @filename += kit.download
    else # download_type == 'kit_additional'
      @filename += kit.additional_download
    end
  end

  def s3_downloader(bucketName, key)
    require 'aws-sdk-s3'
    # (1) Create S3 object
    s3 = Aws::S3::Resource.new(region: 'us-west-2')
    # (2) Create the source object
    sourceObj = s3.bucket(bucketName).object(key)
    # (3) Download the file
    redirect_to sourceObj.presigned_url(:get, expires_in: 3600)
  end

  def unpaid_orders
    @unpaid_orders = Order.all.where.not(status: :paid).where.not(status: :draft).order(id: :asc)
    authorize @unpaid_orders
  end

  def invoiced
    order = Order.find(params[:invoiced_order])
    order.update(status: :invoiced)
    order.save
    redirect_to "#{url_for(current_user)}#dashboard-unpaid-orders"
  end

  def order_paid
    order = Order.find(params[:paid_order])
    order.update(status: :paid)
    order.save
    redirect_to "#{url_for(current_user)}#dashboard-unpaid-orders"
  end

  def labeled
    order = Order.find(params[:labeled_order])
    order.update(label_created: Time.zone.now)
    order.save
    redirect_to "#{url_for(current_user)}#dashboard-manual-labels"
  end

  def change_shipping
    @switch_shipping = ((session[:preferred_currency] == 'cad') ? "intl_shipping" : params[:shipping_method])
    current_cart.update(shipping_method: @switch_shipping)
    current_cart.save
    redirect_to cart_path
  end

  def verify_code
    attempted_code = CouponCode.find_by(code: params[:coupon_code].upcase)
    if attempted_code.present?
      if (attempted_code.scope == 'online_courses')
        if (current_cart.order_items.map { |oi| oi.orderable_type == 'OnlineCourse' }).any?
          coupon_code_success
        else
          coupon_code_failure('Online Courses')
        end
      end

      if (attempted_code.scope == 'kits')
        if (current_cart.order_items.map { |oi| oi.orderable_type == 'Kit' }).any?
          coupon_code_success
        else
          coupon_code_failure('DIY Kits')
        end
      end

      if (attempted_code.scope == 'train_the_trainers')
        if (current_cart.order_items.map { |oi| oi.orderable_type == 'TrainTheTrainer' }).any?
          coupon_code_success
        else
          coupon_code_failure('Train The Trainers')
        end
      end

      if (attempted_code.scope == 'order_total')
        if current_cart.total(session[:preferred_currency]) >= attempted_code.min_purchase_amt
          coupon_code_success
        else
          ot_coupon_code_failure(helpers.number_to_currency attempted_code.min_purchase_amt / 100)
        end
      end
    else
      flash[:error] = 'Invalid coupon code.'
      redirect_to cart_path
    end
  end

  # ot = order total
  def ot_coupon_code_failure(min_purchase_amt)
    flash[:error] = "Minimum purchase amt must be met: #{ min_purchase_amt }."
    redirect_to cart_path
  end

  def coupon_code_failure(course_type)
    flash[:error] = "This coupon code can only be used to purchase one or more #{ course_type }."
    redirect_to cart_path
  end

  def coupon_code_success
    current_cart.update(coupon_code: params[:coupon_code].upcase)
    current_cart.save
    flash[:notice] = 'Coupon code added!'
    redirect_to cart_path
  end

  def remove_coupon_code
    current_cart.update(coupon_code: '')
    current_cart.save
    flash[:notice] = 'Coupon code removed.'
    redirect_to cart_path
  end

  private

    def set_order
      if current_user.superadmin?
        @order = Order.find(params[:id])
      else
        @order = current_user.orders.includes(order_items: [:orderable]).find(params[:id])
      end
      authorize @order
    end

    def no_download_for_you(order)
      redirect_to order_path(order), notice: "This download link has expired."
    end

    def order_params
      params.require(:order).permit(:shipping_method, :order_placed_at, :coupon_code)
    end

end
