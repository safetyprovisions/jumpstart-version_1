class HomeController < ApplicationController
  before_action :preferred_currency_params, only: [:update_preferred_currency]
  before_action :set_categories, only: [
    :topics,
    # Kits
    :diy_training_kits,
    :osha_training_kits,
    :canadian_training_kits,
    :spanish_training_kits,
    :tagalog_training_kits,
    # Online Courses & Group Trainings
    :all_online_courses,
    :osha_online_courses,
    :canadian_online_courses,
    :spanish_online_courses,
    :tagalog_online_courses,
    # TTTs
    :all_train_the_trainer_courses,
    :osha_train_the_trainer_courses,
    :canadian_train_the_trainer_courses,
    :spanish_train_the_trainer_courses,
    :tagalog_train_the_trainer_courses,
    # Group Trainings
    :all_group_trainings,
    :osha_group_trainings,
    :canadian_group_trainings,
    :spanish_group_trainings,
    :tagalog_group_trainings
  ]

  before_action :get_all_online_courses, only: [:all_online_courses, :all_group_trainings]
  before_action :get_osha_online_courses, only: [:osha_online_courses, :osha_group_trainings]
  before_action :get_canadian_online_courses, only: [:canadian_online_courses, :canadian_group_trainings]
  before_action :get_spanish_online_courses, only: [:spanish_online_courses, :spanish_group_trainings]
  before_action :get_tagalog_online_courses, only: [:tagalog_online_courses, :tagalog_group_trainings]

  before_action :set_course_type_online, only: [
    :all_online_courses,
    :osha_online_courses,
    :canadian_online_courses,
    :spanish_online_courses,
    :tagalog_online_courses
  ]

  before_action :set_course_type_kit, only: [
    :diy_training_kits,
    :osha_training_kits,
    :canadian_training_kits,
    :spanish_training_kits,
    :tagalog_training_kits
  ]

  before_action :set_course_type_group_training, only: [
    :all_group_trainings,
    :osha_group_trainings,
    :canadian_group_trainings,
    :spanish_group_trainings,
    :tagalog_group_trainings
  ]

  before_action :set_course_type_train_the_trainer, only: [
    :all_train_the_trainer_courses,
    :osha_train_the_trainer_courses,
    :canadian_train_the_trainer_courses,
    :spanish_train_the_trainer_courses,
    :tagalog_train_the_trainer_courses
  ]

  before_action :set_market_type_all, only: [
    :diy_training_kits,
    :all_online_courses,
    :all_group_trainings,
    :all_train_the_trainer_courses
  ]

  before_action :set_market_type_osha, only: [
    :osha_training_kits,
    :osha_online_courses,
    :osha_group_trainings,
    :osha_train_the_trainer_courses
  ]

  before_action :set_market_type_canada, only: [
    :canadian_training_kits,
    :canadian_online_courses,
    :canadian_group_trainings,
    :canadian_train_the_trainer_courses
  ]

  before_action :set_market_type_spanish, only: [
    :spanish_training_kits,
    :spanish_online_courses,
    :spanish_group_trainings,
    :spanish_train_the_trainer_courses
  ]

  before_action :set_market_type_tagalog, only: [
    :tagalog_training_kits,
    :tagalog_online_courses,
    :tagalog_group_trainings,
    :tagalog_train_the_trainer_courses
  ]

  def index
    @categories = ActsAsTaggableOn::Tag.where('taggings_count > 0').order(name: :asc)

    @courses = ProductGroup.all.order(name: :asc)
  end

  def terms
  end

  def privacy
  end

  def catch_404
    raise ActionController::RoutingError.new(params[:path])
  end

  def about
  end

  def contact
  end

  def faq
  end

  def training_types
    get_bulk_price_levels
  end

  def onsite_training
    @contact = ContactForm.new
    if !params[:pg_name].blank?
      @interest = "We need training on #{ params[:pg_name] }."
    end
  end

  # Sends Onsite Training Request
  def send_contact_email
    @contact = ContactForm.new(params[:contact_form])
    @contact.request = request
    respond_to do |format|
      if @contact.deliver
        # re-initialize Home object for cleared form
        @contact = ContactForm.new
        params[:pg_name] = ''
        format.html { redirect_to onsite_request_path, notice: 'Your request has been sent.' }
        # format.json { render :show, status: :created, location: onsite_request_path }
      else
        format.html { redirect_to onsite_request_path, notice: 'Your request could not be sent for some reason.' }
        # format.json { render :show, status: :unprocessable_entity, location: onsite_request_path }
      end
    end
  end

  def search
    # Swag
    @button_group   = ButtonGroup.where(active: true).first
    @card_groups    = CardGroup.where(active: true).order(name: :asc)
    @button_group   = ButtonGroup.where(active: true).first
    @certificate    = Certificate.where(active: true).first
    @hat            = Hat.where(active: true).first
    @keychain_group = KeychainGroup.where(active: true).first
    @lanyard_group  = LanyardGroup.where(active: true).first
    @machine_tag_group  = MachineTagGroup.where(active: true).first
    @poster_group   = PosterGroup.where(active: true).first
    @shirt          = Shirt.where(active: true).first
    @sticker        = Sticker.where(active: true).first

    if params[:q1].present? || params[:q2].present?

      if params[:q1].present?
        @q = (params[:q1])
        qs = (params[:q1].include?(' ') ? params[:q1].split(' ') : params[:q1].split)
      else # if params[:q2].present?
        @q = (params[:q2])
        qs = (params[:q2].include?(' ') ? params[:q2].split(' ') : params[:q2].split)
      end

      # Exact results only
      # @q = (params[:q])
      exact_match = []

      product_group = ProductGroup.ransack(name_cont: @q).result.pluck(:id)
      alt_name      = AltName.ransack(name_cont: @q).result.pluck(:product_group_id)
      exact_match += product_group + alt_name

      @exact_match = ProductGroup.includes([:alt_names]).find(exact_match.uniq)

      # Multiple results based on each search term
      # qs = (params[:q].include?(' ') ? params[:q].split(' ') : params[:q].split)
      ids = []
      qs.each do |q|
        product_groups = ProductGroup.ransack(name_cont_any: q).result.pluck(:id)
        alt_names      = AltName.ransack(name_cont_any: q).result.pluck(:product_group_id)

        ids += product_groups + alt_names
        ids.delete_if { |x| exact_match.include?(x) }
      end

      results = ProductGroup.find(ids.uniq)
      @results = results.sort_by { |x| x.name }
    else
      @results = nil
    end
  end

  def update_preferred_currency
    set_session_currency_pref(params[:preferred_currency])

    if user_signed_in?
      remove_credit_packs
      check_shipping
    end

    if params[:source].present? && (params[:source] == 'company')
      change_preferred_currency(current_company)
    end

    if params[:source].present? && params[:source] == 'user'
      change_preferred_currency(current_user)
    end

    redirect_to params[:url], notice: 'Your preferred currency was successfully updated.'
  end

  def topics
    @courses = ProductGroup.where(active: true).order(name: :asc)
  end

  def diy_training_kits
    # @courses = ProductGroup.where(active: true).joins(:kits).distinct.order(name: :asc)
    @courses = ProductGroup.where(active: true).joins(:kits).where(kits: { active: true }).distinct.order(name: :asc)
  end

  def osha_training_kits
    @courses = ProductGroup.where(active: true).joins(:kits).where(kits: { standards: 'osha', active: true }).distinct.order(name: :asc)
  end

  def canadian_training_kits
    @courses = ProductGroup.where(active: true).joins(:kits).where(kits: { standards: 'csa_ansi', active: true }).distinct.order(name: :asc)
  end

  def spanish_training_kits
    @courses = ProductGroup.where(active: true).joins(:kits).where(kits: { language: 'spanish', active: true }).distinct.order(name: :asc)
  end

  def tagalog_training_kits
    @courses = ProductGroup.where(active: true).joins(:kits).where(kits: { language: 'tagalog', active: true }).distinct.order(name: :asc)
  end

  def all_online_courses
  end

  def osha_online_courses
  end

  def canadian_online_courses
  end

  def spanish_online_courses
  end

  def tagalog_online_courses
  end

  def all_train_the_trainer_courses
    @courses = ProductGroup.where(active: true).joins(:train_the_trainers).where(train_the_trainers: { active: true }).distinct.order(name: :asc)
  end

  def osha_train_the_trainer_courses
    @courses = ProductGroup.where(active: true).joins(:train_the_trainers).where(train_the_trainers: { standards: 'osha', active: true }).distinct.order(name: :asc)
  end

  def canadian_train_the_trainer_courses
    @courses = ProductGroup.where(active: true).joins(:train_the_trainers).where(train_the_trainers: { standards: 'csa_ansi', active: true }).distinct.order(name: :asc)
  end

  def spanish_train_the_trainer_courses
    @courses = ProductGroup.where(active: true).joins(:train_the_trainers).where(train_the_trainers: { language: 'spanish', active: true }).distinct.order(name: :asc)
  end

  def tagalog_train_the_trainer_courses
    @courses = ProductGroup.where(active: true).joins(:train_the_trainers).where(train_the_trainers: { language: 'tagalog', active: true }).distinct.order(name: :asc)
  end

  def all_group_trainings
  end

  def osha_group_trainings
  end

  def canadian_group_trainings
  end

  def spanish_group_trainings
  end

  def tagalog_group_trainings
  end

  def maintenance
    render layout: 'maintenance.html.erb'
  end

  private

    def get_all_online_courses
      @courses = ProductGroup.where(active: true).joins(:online_courses).where(online_courses: {active: true} ).distinct.order(name: :asc)
    end

    def get_osha_online_courses
      @courses = ProductGroup.where(active: true).joins(:online_courses).where(online_courses: { standards: 'osha', active: true }).distinct.order(name: :asc)
    end

    def get_canadian_online_courses
      @courses = ProductGroup.where(active: true).joins(:online_courses).where(online_courses: { standards: 'csa_ansi', active: true }).distinct.order(name: :asc)
    end

    def get_spanish_online_courses
      @courses = ProductGroup.where(active: true).joins(:online_courses).where(online_courses: { language: 'spanish', active: true }).distinct.order(name: :asc)
    end

    def get_tagalog_online_courses
      @courses = ProductGroup.where(active: true).joins(:online_courses).where(online_courses: { language: 'tagalog', active: true }).distinct.order(name: :asc)
    end

    def set_categories
      @categories = ActsAsTaggableOn::Tag.where('taggings_count > 0').order(name: :asc)
      @title = @categories.pluck(:name)
    end

    def set_market_type_all
      @market_type = "all"
    end

    def set_market_type_osha
      @market_type = "usa"
    end

    def set_market_type_canada
      @market_type = "canada"
    end

    def set_market_type_spanish
      @market_type = "spanish"
    end

    def set_market_type_tagalog
      @market_type = "tagalog"
    end

    def set_course_type_online
      @course_type = "onlinecourse"
    end

    def set_course_type_kit
      @course_type = "kit"
    end

    def set_course_type_group_training
      @course_type = "grouptraining"
    end

    def set_course_type_train_the_trainer
      @course_type = "trainthetrainer"
    end

    # TODO Is this check necessary? Won't intl_shipping get set at checkout?
    def check_shipping
      if current_cart.present? && params[:preferred_currency] == 'cad'
        current_cart.update(shipping_method: 'intl_shipping')
        current_cart.save
        # redirect_to cart_path
      else
        current_cart.update(shipping_method: 'shipping_price')
        current_cart.save
      end
    end

    def preferred_currency_params
      params.permit(:url, :value, :preferred_currency, :utf8, :authenticity_token, :source)
    end

    def set_session_currency_pref(currency)
      session[:preferred_currency] = currency
    end

    def change_preferred_currency(model)
      model.preferred_currency = params[:preferred_currency]
      model.save
    end

    def remove_credit_packs
      if current_cart.present? && current_cart.order_items.present?
        oicp = current_cart.order_items.where(orderable_type: "CreditPack")

        oicp.map(&:destroy)
      end
    end
end
