class DigitalPostersController < ApplicationController
  before_action :set_digital_poster, only: [:show, :edit, :update, :destroy]

  # GET /digital_posters
  # GET /digital_posters.json
  def index
    @digital_posters = DigitalPoster.all
  end

  # GET /digital_posters/1
  # GET /digital_posters/1.json
  def show
  end

  # GET /digital_posters/new
  def new
    @digital_poster = DigitalPoster.new
  end

  # GET /digital_posters/1/edit
  def edit
  end

  # POST /digital_posters
  # POST /digital_posters.json
  def create
    @digital_poster = DigitalPoster.new(digital_poster_params)

    respond_to do |format|
      if @digital_poster.save
        format.html { redirect_to @digital_poster, notice: 'Digital poster was successfully created.' }
        format.json { render :show, status: :created, location: @digital_poster }
      else
        format.html { render :new }
        format.json { render json: @digital_poster.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /digital_posters/1
  # PATCH/PUT /digital_posters/1.json
  def update
    respond_to do |format|
      if @digital_poster.update(digital_poster_params)
        format.html { redirect_to @digital_poster, notice: 'Digital poster was successfully updated.' }
        format.json { render :show, status: :ok, location: @digital_poster }
      else
        format.html { render :edit }
        format.json { render json: @digital_poster.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /digital_posters/1
  # DELETE /digital_posters/1.json
  def destroy
    @digital_poster.destroy
    respond_to do |format|
      format.html { redirect_to digital_posters_url, notice: 'Digital poster was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_digital_poster
      @digital_poster = DigitalPoster.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def digital_poster_params
      params.require(:digital_poster).permit(:price, :cad_price, :poster_id)
    end
end
