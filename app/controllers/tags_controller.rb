class TagsController < ApplicationController
  before_action :verify_superadmin!
  before_action :set_tag, only: [:edit, :update]

  def edit
  end

  def update

    respond_to do |format|
      if @tag.update(name: params[:name].upcase)
        format.html { redirect_to current_user, notice: 'Tag was successfully updated.' }
        format.json { render current_user, status: :ok, location: @tag }
      else
        format.html { render :edit }
        format.json { render json: @tag.errors, status: :unprocessable_entity }
      end
    end

  end

  private

    def set_tag
      @tag = ActsAsTaggableOn::Tag.find(params[:id])
    end

    # def tag_params
    #   params.require(:acts_as_taggable_on_tag).permit(:id, :name)
    # end

end
