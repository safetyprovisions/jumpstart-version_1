class ShirtsController < ApplicationController
  before_action :set_shirt, only: [:show, :edit, :update, :destroy]

  def index
    if user_signed_in? && current_user.superadmin?
      @shirts = Shirt.all.order(name: :asc)
    else
      @shirts = Shirt.where(active: true).order(name: :asc)
    end
  end

  def show
    @colors = @shirt.colors.present? ? @shirt.colors.split(", ") : ""
    @sizes  = @shirt.sizes.present? ? @shirt.sizes.split(", ") : ""
  end

  def new
    @shirt = Shirt.new
  end

  def edit
  end

  def create
    @shirt = Shirt.new(shirt_params)
    @shirt.image_derivatives! # generate derivatives

    respond_to do |format|
      if @shirt.save
        format.html { redirect_to @shirt, notice: 'Shirt was successfully created.' }
        format.json { render :show, status: :created, location: @shirt }
      else
        format.html { render :new }
        format.json { render json: @shirt.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @shirt.update(shirt_params)
        @shirt.image_derivatives! # generate Shrine derivatives
        @shirt.save
        format.html { redirect_to @shirt, notice: 'Shirt was successfully updated.' }
        format.json { render :show, status: :ok, location: @shirt }
      else
        format.html { render :edit }
        format.json { render json: @shirt.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @shirt.destroy
    respond_to do |format|
      format.html { redirect_to shirts_url, notice: 'Shirt was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_shirt
      @shirt = Shirt.friendly.find(params[:id])
    end

    def shirt_params
      params.require(:shirt).permit(:name, :description, :image, :slug, :keywords, :meta_description, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :weight, :sizes, :colors, :active)
    end
end
