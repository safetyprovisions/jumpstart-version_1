class PaymentTermsController < ApplicationController
  before_action :set_payment_term, only: [:show, :edit, :update, :destroy]

  def index
    @payment_terms = PaymentTerm.all
    authorize @payment_terms
  end

  def show
  end

  def new
    @payment_term = PaymentTerm.new
    authorize @payment_term
    @companies = Company.all.order(name: :asc).pluck(:id, :name, :city, :state)
  end

  def edit
  end

  def create
    @payment_term = PaymentTerm.new(payment_term_params)
    authorize @payment_term

    respond_to do |format|
      if @payment_term.save
        format.html { redirect_to @payment_term, notice: 'Payment term was successfully created.' }
        format.json { render :show, status: :created, location: @payment_term }
      else
        format.html { render :new }
        format.json { render json: @payment_term.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @payment_term.update(payment_term_params)
        format.html { redirect_to @payment_term, notice: 'Payment term was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment_term }
      else
        format.html { render :edit }
        format.json { render json: @payment_term.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @payment_term.destroy
    respond_to do |format|
      format.html { redirect_to payment_terms_url, notice: 'Payment term was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_payment_term
      @payment_term = PaymentTerm.find(params[:id])
      authorize @payment_term
    end

    def payment_term_params
      params.require(:payment_term).permit(:company_id, :term)
    end
end
