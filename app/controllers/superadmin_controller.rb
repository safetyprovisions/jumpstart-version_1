class SuperadminController < ApplicationController

  def assign_non_catalog_course
    # if (params[:company].to_i.class == Integer) && (params[:course].to_i.class == Integer)
      company_id = params[:company].to_i
      flash[:notice] = ''
      company = Company.find_by(id: company_id)
      if company.nil?
        flash[:notice] += "There is no Company with that ID."
      end

      course_id = params[:course].to_i
      course = OnlineCourse.find_by(talent_course_id: course_id)
      if course.nil?
        flash[:notice] += " There is no Online Course with that Talent Course ID."
      end

      if flash[:notice].present?
        redirect_to current_user
      else
        flash.clear
        ncca = NonCatalogCourseAccess.create(company: company, online_course: course)
        if ncca.save
          flash[:notice] = "#{ company.name } now has access to #{ course.title }."
          redirect_to current_user
        else # TODO This doesn't actually check if they already have access… What if the save just fails?
          flash[:alert] = "#{ company.name } already has access to #{ course.title }!"
          redirect_to current_user
        end
      end
    # else
    #   flash[:alert] = 'Company and Course must both be numbers.'
    #   redirect_to root_path
    # end
  end

  def reset_download_window
    order_id = params[:order].to_i
    order = Order.find_by(id: order_id)

    if order.nil?
      flash[:alert] = "No order exists with that ID."
    else
      order.update(download_start_time: Time.zone.now)
      flash[:notice] = "Download window has been reset!"
    end

    redirect_to current_user
  end

  # private

end
