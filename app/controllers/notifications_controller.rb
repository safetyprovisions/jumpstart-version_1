class NotificationsController < ApplicationController
  def index
    @notifications = current_user.notifications
    authorize @notifications
  end
end
