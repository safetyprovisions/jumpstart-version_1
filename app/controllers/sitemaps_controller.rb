class SitemapsController < ApplicationController

  layout :false
  before_action :init_sitemap

  def index
    @button_groups      = ButtonGroup.all
    @card_groups        = CardGroup.all
    @certificates       = Certificate.all
    @hats               = Hat.all
    @keychain_groups    = KeychainGroup.all
    @lanyard_groups     = LanyardGroup.all
    @machine_tag_groups = MachineTagGroup.all
    @poster_groups      = PosterGroup.all
    @posters            = Poster.all
    @product_groups     = ProductGroup.all
    @shirts             = Shirt.all
    @stickers           = Sticker.all
  end

  private

    def init_sitemap
      headers['Content-Type'] = 'application/xml'
    end

end
