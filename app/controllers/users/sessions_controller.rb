# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]
  after_action :set_preferred_currency, only: [:new, :create]
  after_action :set_current_company, only: [:new, :create]
  after_action :set_superadmin_credits, only: [:new, :create]
  before_action :clear_preferred_currency, only: [:destroy]
  before_action :set_redirect

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    super
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  private

    def set_preferred_currency
      if current_user.present?
        session[:preferred_currency] = current_user.preferred_currency
      end
    end

    def set_superadmin_credits
      if current_user.present? && current_user.superadmin?
        session[:credits] = 999999
      end
    end

    def set_current_company
      if current_user.present? && current_user.superadmin?
        session[:current_company] = '1'
      end

      if current_user.present?
        session[:current_company] = current_user.companies.last
      end
    end

    def clear_preferred_currency
      session[:preferred_currency] = nil
    end

    def set_redirect
      if params[:login_redirect].present?
        session[:login_redirect] = params[:login_redirect]
      end
      if params[:button].present?
        session[:login_redirect] += '#' + params[:button]
      end
    end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
