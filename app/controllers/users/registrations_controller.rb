# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]
  before_action :set_redirect

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  def create
    super

    # NOTE Removed for testing due to error. Seems to be fine.
    # if super
    #   # if params[:login_redirect].present?
    #   #   session[:login_redirect] = params[:login_redirect]
    #   # end

    #   # redirect_to user_path(current_user.id)
    #   if current_company.present?
    #     current_company.employees << current_user

    #     flash[:notice] = "#{ current_user.name.full } has been added as an employee of #{ current_company.name}."
    #   end
    # end
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  def update
    # Update info in Talent
    new_email    = params[:user][:email]
    talent_user_edit(user_id: current_user.talent_user_id,
                     email:   new_email
                     )
    super
  end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  private

    def set_redirect
      if params[:login_redirect].present? && params[:button].present?
        session[:login_redirect] = (params[:login_redirect] + '#' + params[:button])
      end

      if params[:login_redirect].present? && !params[:button].present?
        session[:login_redirect] = params[:login_redirect]
      end
    end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
