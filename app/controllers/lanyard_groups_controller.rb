class LanyardGroupsController < ApplicationController
  before_action :set_lanyard_group, only: [:show, :edit, :update, :destroy]

  def index
    @lanyard_groups = LanyardGroup.all
  end

  def show
    @lanyards = Lanyard.where(lanyard_group_id: @lanyard_group.id, active: true).order(price: :asc)
  end

  def new
    @lanyard_group = LanyardGroup.new
  end

  def edit
  end

  def create
    @lanyard_group = LanyardGroup.new(lanyard_group_params)
      @lanyard_group.image_derivatives! # generate Shrine derivatives

    respond_to do |format|
      if @lanyard_group.save
        format.html { redirect_to @lanyard_group, notice: 'Lanyard group was successfully created.' }
        format.json { render :show, status: :created, location: @lanyard_group }
      else
        format.html { render :new }
        format.json { render json: @lanyard_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @lanyard_group.update(lanyard_group_params)
        @lanyard_group.image_derivatives! # generate Shrine derivatives
        @lanyard_group.save
        format.html { redirect_to @lanyard_group, notice: 'Lanyard group was successfully updated.' }
        format.json { render :show, status: :ok, location: @lanyard_group }
      else
        format.html { render :edit }
        format.json { render json: @lanyard_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @lanyard_group.destroy
    respond_to do |format|
      format.html { redirect_to lanyard_groups_url, notice: 'Lanyard group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_lanyard_group
      @lanyard_group = LanyardGroup.friendly.find(params[:id])
    end

    def lanyard_group_params
      params.require(:lanyard_group).permit(:name, :description, :image, :image_data, :slug, :keywords, :meta_description, :active)
    end
end
