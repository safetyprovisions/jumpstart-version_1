class ButtonGroupsController < ApplicationController
  before_action :set_button_group, only: [:show, :edit, :update, :destroy]

  def index
    if user_signed_in? && current_user.superadmin?
      @button_groups = ButtonGroup.all.order(name: :asc)
    else
      @button_groups = ButtonGroup.where(active: true).order(name: :asc)
    end
  end

  def show
    @buttons = Button.where(button_group_id: @button_group.id, active: true).order(price: :asc)
  end

  def new
    @button_group = ButtonGroup.new
  end

  def edit
  end

  def create
    @button_group = ButtonGroup.new(button_group_params)
    @button_group.image_derivatives! # generate Shrine derivatives

    respond_to do |format|
      if @button_group.save
        format.html { redirect_to @button_group, notice: 'Button group was successfully created.' }
        format.json { render :show, status: :created, location: @button_group }
      else
        format.html { render :new }
        format.json { render json: @button_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @button_group.update(button_group_params)
        @button_group.image_derivatives! # generate Shrine derivatives
        @button_group.save
        format.html { redirect_to @button_group, notice: 'Button group was successfully updated.' }
        format.json { render :show, status: :ok, location: @button_group }
      else
        format.html { render :edit }
        format.json { render json: @button_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @button_group.destroy
    respond_to do |format|
      format.html { redirect_to button_groups_url, notice: 'Button group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_button_group
      @button_group = ButtonGroup.friendly.find(params[:id])
    end

    def button_group_params
      params.require(:button_group).permit(:name, :description, :image, :image_data, :slug, :keywords, :meta_description, :active)
    end
end
