class ButtonsController < ApplicationController
  before_action :set_button, only: [:show, :edit, :update, :destroy]

  def index
    # @buttons = Button.all.includes([:button_group]).order(price: :asc)

    @button_groups = ButtonGroup.all.order(name: :asc).pluck(:id, :name)
  end

  def show
  end

  def new
    @button = Button.new
  end

  def edit
  end

  def create
    @button = Button.new(button_params)

    respond_to do |format|
      if @button.save
        format.html { redirect_to @button, notice: 'Button was successfully created.' }
        format.json { render :show, status: :created, location: @button }
      else
        format.html { render :new }
        format.json { render json: @button.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @button.update(button_params)
        format.html { redirect_to @button, notice: 'Button was successfully updated.' }
        format.json { render :show, status: :ok, location: @button }
      else
        format.html { render :edit }
        format.json { render json: @button.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @button.destroy
    respond_to do |format|
      format.html { redirect_to buttons_url, notice: 'Button was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_button
      @button = Button.find(params[:id])
    end

    def button_params
      params.require(:button).permit(:name, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :button_group_id, :weight, :active, :pack_qty)
    end
end
