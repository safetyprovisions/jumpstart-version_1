class OnlineCoursesController < ApplicationController
  before_action :set_online_course, only: [:show, :edit, :update, :destroy]

  def index
    # @online_courses = OnlineCourse.all.order(title: :asc, language: :asc)
    # authorize @online_courses

    # @public_courses = @online_courses.where(public: true)
    # @video_courses = @online_courses.where(public: false)

    @product_groups = ProductGroup.all.includes([:online_courses]).order(name: :asc)
  end

  def show
  end

  def new
    @online_course = OnlineCourse.new
    authorize @online_course
  end

  def edit
  end

  def create
    @online_course = OnlineCourse.new(online_course_params)
    authorize @online_course

    respond_to do |format|
      if @online_course.save
        @group_training = GroupTraining.create(online_course_id: @online_course.id)

        # if @group_training.save
          format.html { redirect_to @online_course, notice: 'Online course was successfully created.' }
          format.json { render :show, status: :created, location: @online_course }
        else
          format.html { render :new }
          format.json { render json: @online_course.errors, status: :unprocessable_entity }
        end
      # end
    end
  end

  def update
    respond_to do |format|
      if @online_course.update(online_course_params)
        format.html { redirect_to online_courses_path, notice: 'Online course was successfully updated.' }
        format.json { render :index, status: :ok, location: @online_course }
      else
        format.html { render :edit }
        format.json { render json: @online_course.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @online_course.destroy
    respond_to do |format|
      format.html { redirect_to online_courses_url, notice: 'Online course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_online_course
      @online_course = OnlineCourse.find(params[:id])
      authorize @online_course
    end

    def online_course_params
      params.require(:online_course).permit(
        :active,
        :answer_key,
        :cad_price,
        :cad_sale_price,
        :description,
        :exam,
        :language,
        :market_type,
        :practical_evaluation,
        :price,
        :product_group_id,
        :public,
        :sale_expiry,
        :sale_price,
        :sale_start,
        :standards,
        :talent_course_id,
        :title,
        :video,
      )
    end

end
