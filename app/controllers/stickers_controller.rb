class StickersController < ApplicationController
  before_action :set_sticker, only: [:show, :edit, :update, :destroy]

  def index
    if user_signed_in? && current_user.superadmin?
      @stickers = Sticker.all.order(price: :asc)
    else
      @stickers = Sticker.where(active: true).order(price: :asc)
    end
  end

  def show
  end

  def new
    @sticker = Sticker.new
  end

  def edit
  end

  def create
    @sticker = Sticker.new(sticker_params)
    @sticker.image_derivatives! # generate Shrine derivatives

    respond_to do |format|
      if @sticker.save
        format.html { redirect_to @sticker, notice: 'Sticker was successfully created.' }
        format.json { render :show, status: :created, location: @sticker }
      else
        format.html { render :new }
        format.json { render json: @sticker.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @sticker.update(sticker_params)
        @sticker.image_derivatives! # generate Shrine derivatives
        @sticker.save
        format.html { redirect_to @sticker, notice: 'Sticker was successfully updated.' }
        format.json { render :show, status: :ok, location: @sticker }
      else
        format.html { render :edit }
        format.json { render json: @sticker.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @sticker.destroy
    respond_to do |format|
      format.html { redirect_to stickers_url, notice: 'Sticker was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_sticker
      @sticker = Sticker.friendly.find(params[:id])
    end

    def sticker_params
      params.require(:sticker).permit(:name, :description, :image, :image_data, :slug, :keywords, :meta_description, :active, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :weight)
    end
end
