class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :exception

  before_action :redirect_to_root!

  # NOTE Commented out for maintenance mode
  # before_action :set_active_online_course_count
  # before_action :set_current_company
  # before_action :configure_permitted_parameters, if: :devise_controller?
  # before_action :masquerade_user!
  # before_action :set_bulk_price_levels
  # before_action :verify_session_params
  # before_action :set_direct_purchases
  # before_action :set_company
  # before_action :redirect_subdomain
  # before_action :set_menu_items
  # # after_action :login_redirect

  after_action :track_action

  # BEGIN ERROR HANDLING
  rescue_from NotAuthorizedError do
    flash[:alert] = 'You are not authorized.'
    redirect_to root_path
  end

  rescue_from ActiveRecord::RecordNotFound do
    flash[:alert] = 'This record does not exist.'
    redirect_to root_path
  end

  # rescue_from ActionController::BadRequest do
    # serve a 400 Bad Request response.
  # end

  def set_company
    if current_user.present?
      if current_user.superadmin? && Company.any?
        @company = Company.first
      end

      if current_user.companies.any?
        @company = current_user.companies.last
      end
    elsif current_company.present?
      @company = current_company
    else
      # redirect_to
    end

    # if @company.present?
      session[:current_company] = @company
    # end
  end

  def verify_admin!
    if !(current_user.admin?)
      flash[:alert] = 'You are not authorized.'
      redirect_to root_path
    end
  end

  def set_current_company
    if user_signed_in? && current_user.companies.any?
      session[:current_company] = current_user.companies.last
      @company = session[:current_company]
    end
  end

  def get_bulk_price_levels
    set_bulk_price_levels
  end

  def verify_superadmin!
    if !(current_user.present? && current_user.superadmin?)
      flash[:alert] = 'You are not authorized.'
      redirect_to root_path
    end
  end

  def no_employees_error!
    if current_company.employees.count == 0
      flash[:alert] = 'Add some employees first.'
      redirect_to add_employee_path
    end
  end

  rescue_from ActionController::RoutingError, with: :error_render_method

  def error_render_method
    @attempting_url = request.path

    if @attempting_url == ("." || "/.")
      flash[:alert] = 'Not a valid path.'
      redirect_to root_path
    else
      string = @attempting_url.split('/')[-1]

      filename = string.split('.')[0]

      if filename.match(/[-_(%20)]/)
        filename = filename.split(/[-_(%20)]/)
      end

      filename.class == Array ? filename.join(' ') : filename

      redirect_to search_path(q: filename)
    end
  end
  # END ERROR HANDLING

  def generate_chars(number)
    charset = Array(0..9) + Array("a".."z")
    Array.new(number) { charset.sample }.join
  end

  def remove_employee
    if current_user.present? && (current_user.admin? || current_user.superadmin?)
      @current_employees = current_user.companies.last.employees
      @redirect = manage_employees_path
    elsif current_company.present?
      @current_employees = current_company.employees
      @redirect = companies_manage_employees_path
    else
      raise NotAuthorizedError
    end

    unless @current_employees.find_by_id(params[:emp]).blank?
      ee = User.find(params[:emp])
      # ee.update!(admin: false)
      # ee.save
      if CompanyAdmin.find_by(user: ee, company: current_company)
        CompanyAdmin.find_by(user: ee, company: current_company).delete
      end

      @current_employees.delete(params[:emp])
    end
    flash[:alert] = "This employee has been removed from your Company."
    redirect_to @redirect
  end

  def toggle_employee_status
    if (current_user.present? && current_user.admin?)
      @redirect = manage_employees_path
    elsif current_company.present?
      @redirect = companies_manage_employees_path
    else
      raise NotAuthorizedError
    end

    ee = User.find(params[:employee])

    if ee.active?
      ee.toggle(:active)

      talent_user_status(ee.talent_user_id, "inactive")
    else
      ee.toggle(:active)

      talent_user_status(ee.talent_user_id, "active")
    end
    if ee.save
      redirect_to @redirect
    else
      redirect_to @redirect, alert: "Something went wrong. Please try again."
    end
  end

  # NOTE This controls the redirect after sign in!
  def after_sign_in_path_for(resource)
    if resource.class == User
      if session[:login_redirect].present?
        @login_redirect = session[:login_redirect]
        session[:login_redirect] = nil
        @login_redirect
      else
        user_path(resource)
      end
    elsif resource.class == Company
      company_path(resource)
    end
  end

  def current_cart
    return if !user_signed_in?
    current_user.orders.draft.first_or_initialize
  end
  helper_method :current_cart

  def company_signed_in?
    current_company
  end

  def logged_in?
    current_user
  end

  def reset_password
    user = User.find(params[:employee])
    user.update_attributes(password: "HardHatTraining1", password_confirmation: "HardHatTraining1")

    flash[:notice] = "Password has been reset for #{ user.name.full }."
    redirect_to manage_employees_path
  end

  def fake_email(fullname)
    @append_num = 0
    increment_fake_email(fullname)
  end

  def increment_fake_email(fullname)
    formatted_name = fullname.downcase.sub(' ', '_')
    username = "#{ formatted_name }#{ @append_num }@noemail.com"

    # Check if email is already in use
    @parsed = find_user(username)

    # If not, return the fake email
    if @parsed["error"]["message"] == "The requested user does not exist"
      username
    else # If not, increment digit and check again
      @append_num = @append_num + 1
      increment_fake_email(username)
    end
  end

  def make_assignable
    this_order = Order.find(params[:order])

    create_company_course(this_order)
    create_company_order(this_order)

    redirect_to this_order
  end

  def get_all_employees
    if current_company.present?
      @all_employees = current_company.employees
      # current_company.branches.each do |branch|
      #   @all_employees << branch.employees
      # end
    elsif current_user.present? && (current_user.admin? || current_user.superadmin?)
      # @all_employees = current_user.companies.last.employees

      all_companies = CompanyAdmin.where(user: current_user).pluck(:company_id)
      @employee_ids = []

      all_companies.each do |co|
        company = Company.find(co)
        company.employees.each do |ee|
          @employee_ids << ee.id
        end
      end
      @all_employees = User.where(id: @employee_ids )
    else
      raise NotAuthorizedError
    end

    # Paginate All Employees
    @pagy, @employees = pagy(@all_employees.includes(:users_companies, :companies).order(last_name: :asc, first_name: :asc))
  end

  # BEGIN SHARED TALENT METHODS

  # Optional Params:
  # bio, credits, email, first_name, last_name, login, password, timezone, user_id
  # Required Params: user_id
  def talent_user_edit(**keyword_args)
    # If User updates via Devise
    if params[:user] && params[:user][:email] != nil
      params[:user_id] = current_user.talent_user_id
      params[:email]   = params[:user][:email]
    end

    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/edituser")

    make_secure_connection(url)

    @request = Net::HTTP::Post.new(url)
    @request["content-type"] = 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'

    make_http_request(url)

    @request.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"user_id\"\r\n\r\n#{ params[:user_id] }" +
      content_block("email", params[:email]) +
      content_block("login", params[:login]) +
      content_block("password", params[:password]) +
      "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"

    parse_response
  end

  def talent_user_status(talent_user_id, status) # All Required
    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/usersetstatus/user_id:#{ talent_user_id },status:#{ status }")

    make_secure_connection(url)
    @request = Net::HTTP::Get.new(url)
    make_http_request(url)
    parse_response
  end

  def get_talent_user
    if current_user.admin? || current_user.superadmin?
      @user_to_get = User.find(params[:id])
    else
      @user_to_get = current_user
    end

    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/users/id:#{ @user_to_get.talent_user_id }")

    make_secure_connection(url)

    @request = Net::HTTP::Get.new(url)

    make_http_request(url)

    @request["Accept"] = '*/*'
    @request["Host"] = 'safetyclasses.talentlms.com'
    @request["accept-encoding"] = 'gzip, deflate'
    @request["Connection"] = 'keep-alive'
    @request["cache-control"] = 'no-cache'

    parse_response
  end

  def find_user(email)
    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/users/email:example@example.com?email=#{ email }")

    make_secure_connection(url)

    @request = Net::HTTP::Get.new(url)

    make_http_request(url)

    @request["Accept"] = '*/*'
    @request["Postman-Token"] = 'ea2d6811-9c2b-4812-b9dc-ad51402c0b0d,062ec00b-19ed-4a3f-bd55-74af6bad9758'
    @request["Host"] = 'safetyclasses.talentlms.com'
    @request["accept-encoding"] = 'gzip, deflate'
    @request["Connection"] = 'keep-alive'
    @request["cache-control"] = 'no-cache'

    parse_response
  end

  def new_talent_user(first_name, last_name, email, login, password, company_name, city, state, zip_code, restrict_email)

    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/usersignup")

    make_secure_connection(url)

    @request = Net::HTTP::Post.new(url)
    @request["content-type"] = 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'

    make_http_request(url)

    @request.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"first_name\"\r\n\r\n#{ first_name }" +
    content_block("last_name", last_name) +
    content_block("email", email) +
    content_block("login", login) +
    content_block("password", password) +
    content_block("custom_field_1", company_name) +
    content_block("custom_field_2", city) +
    content_block("custom_field_3", state) +
    content_block("custom_field_4", zip_code) +
    content_block("restrict_email", restrict_email) +
    content_block("branch_id", "10") +
    "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"

    parse_response
  end

  def set_autogenerated_password(user)
    password = Devise.friendly_token.first(8)

    if user.encrypted_password.blank?
      user.password              = password
      user.password_confirmation = password
    else
      password
    end
  end

  def require_dependencies
    require 'net/http'
    require 'json'
  end

  def make_secure_connection(url)
    @http = Net::HTTP.new(url.host, url.port)
    @http.use_ssl = true
  end

  def make_http_request(url)
    @request["Authorization"] = 'Basic NUFWWjF3MUFraHBBTFNkbmx6WE1OSDF5bmZ0NXExOg=='
    @request["cache-control"] = 'no-cache'
  end

  def parse_response
    response = @http.request(@request)
    @parsed = JSON.parse(response.read_body)
    @parsed
  end

  def form_boundary
    "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW"
  end

  def content_block(content_type, content)
    form_boundary + "\r\nContent-Disposition: form-data; name=\"#{ content_type }\"\r\n\r\n" +
      "#{ content }"
  end

  def talent_login(email)
    email.tr( "@!#.$%&'*+-/=?^_`{|}~", '' ) + generate_code(4)
  end

  def generate_code(number)
    charset = Array(0..9)
    Array.new(number) { charset.sample }.join
  end

  def add_user_to_course(user_id, course_id)
    add_user_to_talent_course(user_id, course_id)

    if ((@parsed.class == Hash) && (@parsed["error"].present?) && (@parsed["error"]["message"] == "The requested user is already enrolled in this course")) || ((@parsed.class == Array) && (!@parsed[0].nil?) && (@parsed[0]["role"] == "learner"))

      send_user_to_course(user_id, course_id.to_i)
    elsif ((@parsed["error"].present?) && (@parsed["error"]["message"] == "The requested user is no longer available"))

      redirect_back fallback_location: orders_path, notice: "The requested user is no longer available."
    else # Error handling
      redirect_back fallback_location: orders_path, notice: "Cannot connect with the Learning Management System at this time. Try again later."
    end
  end

  def add_user_to_talent_course(user_id, course_id)
    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/addusertocourse")

    make_secure_connection(url)
    @request = Net::HTTP::Post.new(url)
    make_http_request(url)

    @request["Accept"] = '*/*'
    @request["Host"] = 'safetyclasses.talentlms.com'
    @request["accept-encoding"] = 'gzip, deflate'
    @request["content-type"] = 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
    @request["Connection"] = 'keep-alive'
    @request["cache-control"] = 'no-cache'

    @request.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"user_id\"\r\n\r\n" +
      "#{ user_id }" +
      content_block("course_id", course_id) +
      content_block("role", "learner") +
      form_boundary + "--"

    parse_response
  end

  def get_talent_course(id)
    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/courses/id:#{ id }")

    make_secure_connection(url)
    @request = Net::HTTP::Get.new(url)
    make_http_request(url)
    parse_response
  end

  # END SHARED TALENT METHODS

  def set_price_and_credits
    if current_user.present? && current_user.admin?

      if session[:preferred_currency] == 'usd'
        if @course.present?
          @the_price = @course.price
        end

        if current_company.present?
          session[:credits] = current_company.credits
        else
          session[:credits] = current_user.companies.last.credits
        end
      else # ie, if session[:preferred_currency] == 'cad'
        if @course.present?
          @the_price = @course.cad_price
        end

        if current_company.present?
          session[:credits] = current_company.cad_credits
        else
          session[:credits] = current_user.companies.last.cad_credits
        end
      end
    end
  end

  def mark_all_as_read
    if params[:source] == 'company'
      @return_page = companies_homepage_path

      current_company.update!(announcements_last_read_at: Time.zone.now)
      current_company.save
    else # params[:source] == 'user'
      @return_page = current_user

      current_user.update!(announcements_last_read_at: Time.zone.now)
      current_user.save
    end

    redirect_to @return_page
  end

  def current_user_check
    if !current_user.present?
      flash[:alert] = 'You must log into a User account to make a purchase.'
      redirect_to new_user_session_path
    end
  end

  private

    def redirect_subdomain
      if request.host == 'www.hardhattrainingseries.com'
        redirect_to 'https://hardhattrainingseries.com' + request.fullpath, :status => 301
      end
    end

    def verify_session_params
      if (session[:preferred_currency] == nil) && !(current_user.present? || current_company.present?)
        session[:preferred_currency] = 'usd'
      end

      if session[:preferred_currency] == nil
        session[:preferred_currency] = 'usd'
      end

      if session[:credits] == nil
        session[:credits] = 0
      end

      if current_company.present? && session[:current_company].blank?
        session[:current_company] == current_company
      end
    end

    def set_direct_purchases
      if current_company.present?
        all_direct_purchases = current_company.company_courses.where('quantity > 0')
        @dp_online_courses = all_direct_purchases.where.not(online_course_id: nil).includes(:online_course)
        @dp_train_the_trainers = all_direct_purchases.where.not(train_the_trainer_id: nil).includes(:train_the_trainer)
      # elsif current_user.present? && current_user.admin?
      #   @direct_purchases = current_user.companies.last.company_courses.where('quantity > 0')
      # else
        # @direct_purchases = CompanyCourse.none
      end
    end
     # @courses = ProductGroup.joins(:kits).where(kits: { standards: 'osha' }).distinct

    def load_credit_packs
      if session[:preferred_currency] == 'usd'
        @usd = true
        all_credit_packs = CreditPack.where(cad_credits: 0)
        tier1_credit_packs = all_credit_packs.where("name like ?", "%" + '79' + "%")
        tier2_credit_packs = all_credit_packs.where("name like ?", "%" + '29' + "%")
      else # session[:preferred_currency] == 'cad'
        @usd = false
        all_credit_packs = CreditPack.where(credits: 0)
        tier1_credit_packs = all_credit_packs.where("name like ?", "%" + '99' + "%")
        tier2_credit_packs = all_credit_packs.where("name like ?", "%" + '39' + "%")
      end

      @tier1_credit_packs = sort_by_price(tier1_credit_packs)
      @tier2_credit_packs = sort_by_price(tier2_credit_packs)
    end

    def create_company_course(this_order)
      oi_courses = this_order.order_items.where(orderable_type: "OnlineCourse")

      oi_ttts = this_order.order_items.where(orderable_type: "TrainTheTrainer")

      if oi_courses.any?
        oi_courses.each do |course|
          @company_course = CompanyCourse.new(
            user:          current_user,
            company:       current_user.companies.last,
            order:         this_order,
            online_course: course.orderable,
            quantity:      course.quantity,
            currency:      session[:preferred_currency]
            )
          @company_course.save
        end
      end

      if oi_ttts.any?
        oi_ttts.each do |ttt|
          @company_course = CompanyCourse.new(
            user:              current_user,
            company:           current_user.companies.last,
            order:             this_order,
            train_the_trainer: ttt.orderable,
            quantity:          ttt.quantity,
            currency:          session[:preferred_currency]
            )
          @company_course.save
        end
      end
    end

    def create_company_order(this_order)
      @company_order = CompanyOrder.new(
        user:    current_user,
        company: current_user.companies.last,
        order:   this_order,
        po_number: params[:po_number]
        )
      @company_order.save
    end

    def set_bulk_price_levels
      # This sets the figures in a few views (Documentation, ProductGroup modal, and app/views/home/training_types), but it does NOT change the calculations. Those are handled in order_item.rb#total
      bulk_price_levels = BulkPriceLevel.all

      if session[:preferred_currency] == 'usd'
        tier_one_levels = bulk_price_levels.where(tier: '1').order(price: :desc)
        tier_two_levels = bulk_price_levels.where(tier: '2').order(price: :desc)
      else # 'cad'
        tier_one_levels = bulk_price_levels.where(tier: '1').order(cad_price: :desc)
        tier_two_levels = bulk_price_levels.where(tier: '2').order(cad_price: :desc)
      end

      @tier1_level1 = tier_one_levels.first
      @tier1_level2 = tier_one_levels.second
      @tier1_level3 = tier_one_levels.third
      @tier1_level4 = tier_one_levels.fourth

      @tier2_level1 = tier_two_levels.first
      @tier2_level2 = tier_two_levels.second
      @tier2_level3 = tier_two_levels.third
      @tier2_level4 = tier_two_levels.fourth
    end

    def sort_by_price(the_credit_packs)
      cp_hash = {}
      the_credit_packs.each do |cp|
        cp_hash[cp.id] = cp.savings
      end
      cp_hash = cp_hash.sort_by{|k, v| v}
      sorted_keys = []
      cp_hash.each do |array|
        sorted_keys << array[0]
      end

      the_credit_packs.find(sorted_keys)
    end

    def set_menu_items
      # Set links for _navigation.html.erb
      @button_url            = ButtonGroup.first
      @certificate_url       = Certificate.first
      @hand_signal_cards_url = CardGroup.second
      @keychain_url          = KeychainGroup.first
      @lanyard_url           = LanyardGroup.first
      @machine_tag_url       = MachineTagGroup.first
      @wallet_card_url       = CardGroup.first
      @tagalog_kits_exist    = Kit.where(language: "tagalog").any?
      @tagalog_online_courses_exist = OnlineCourse.where(language: "tagalog").any?
      @tagalog_train_the_trainers_exist = TrainTheTrainer.where(language: "tagalog").any?
    end

    def redirect_to_root!
      redirect_to root_path unless (request.original_url == root_url)
    end

  protected

    def set_active_online_course_count
      if Rails.env.test?
        @active_online_course_count = OnlineCourse.where(active: true).count
        @active_train_the_trainer_count = TrainTheTrainer.where(active: true).count
      end
    end

    def track_action
      ahoy.track "Ran action", request.path_parameters
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :last_name, :first_name])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :last_name, :first_name])
    end
end
