class CardGroupsController < ApplicationController
  before_action :set_card_group, only: [:show, :edit, :update, :destroy, :check_for_cards, :delete_related_items]
  before_action :check_and_cleanup, only: [:destroy]

  def index
    @card_groups = CardGroup.all
  end

  def show
    @cards = Card.where(card_group_id: @card_group.id, active: true).order(name: :asc)

    if (@cards.where(standards_required: true).any?) || (@cards.where(details_required: true).any?)
      @info_required = true
    else
      @info_required = false
    end

    @quantities = @cards.pluck(:pack_qty).uniq

    @materials  = @cards.pluck(:material).uniq
  end

  def new
    @card_group = CardGroup.new
  end

  def edit
  end

  def create
    @card_group = CardGroup.new(card_group_params)
    @card_group.image_derivatives! # generate Shrine derivatives

    respond_to do |format|
      if @card_group.save
        format.html { redirect_to @card_group, notice: 'Card group was successfully created.' }
        format.json { render :show, status: :created, location: @card_group }
      else
        format.html { render :new }
        format.json { render json: @card_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @card_group.update(card_group_params)
        @card_group.image_derivatives! # generate Shrine derivatives
        @card_group.save
        format.html { redirect_to @card_group, notice: 'Card group was successfully updated.' }
        format.json { render :show, status: :ok, location: @card_group }
      else
        format.html { render :edit }
        format.json { render json: @card_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @card_group.destroy
    respond_to do |format|
      format.html { redirect_to card_groups_url, notice: 'Card group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_card_group
      @card_group = CardGroup.friendly.find(params[:id])
    end

    def check_and_cleanup
      check_for_cards

      delete_related_items
    end

    def check_for_cards
      if @card_group.cards.any?
        flash[:alert] = 'Please remove any Cards that are attached to this group before deleting the Card Group.'
        redirect_to @card_group
      end
    end

    def delete_related_items
      items_to_delete = RelatedItem.where(relatable_type: "CardGroup", relatable_id: @card_group.id)
      if items_to_delete.present?
        items_to_delete.each do |item|
          item.delete
        end
      end
    end

    def card_group_params
      params.require(:card_group).permit(:name, :description, :image, :image_data, :slug, :keywords, :meta_description, :active)
    end
end
