class CertificatesController < ApplicationController
  before_action :set_certificate, only: [:show, :edit, :update, :destroy]

  def index
    @certificates = Certificate.all
  end

  def show
  end

  def new
    @certificate = Certificate.new
  end

  def edit
  end

  def create
    @certificate = Certificate.new(certificate_params)
    @certificate.image_derivatives! # generate Shrine derivatives

    respond_to do |format|
      if @certificate.save
        format.html { redirect_to @certificate, notice: 'Certificate was successfully created.' }
        format.json { render :show, status: :created, location: @certificate }
      else
        format.html { render :new }
        format.json { render json: @certificate.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @certificate.update(certificate_params)
        @certificate.image_derivatives! # generate Shrine derivatives
        @certificate.save
        format.html { redirect_to @certificate, notice: 'Certificate was successfully updated.' }
        format.json { render :show, status: :ok, location: @certificate }
      else
        format.html { render :edit }
        format.json { render json: @certificate.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @certificate.destroy
    respond_to do |format|
      format.html { redirect_to certificates_url, notice: 'Certificate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_certificate
      @certificate = Certificate.friendly.find(params[:id])
    end

    def certificate_params
      params.require(:certificate).permit(:name, :description, :image, :slug, :keywords, :meta_description, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :active)
    end
end
