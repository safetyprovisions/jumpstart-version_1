class MachineTagsController < ApplicationController
  before_action :set_machine_tag, only: [:show, :edit, :update, :destroy]

  def index
    # @machine_tags = MachineTag.all.order(price: :asc)

    @machine_tag_groups = MachineTagGroup.all.order(name: :asc).pluck(:id, :name)
  end

  def show
  end

  def new
    @machine_tag = MachineTag.new
  end

  def edit
  end

  def create
    @machine_tag = MachineTag.new(machine_tag_params)

    respond_to do |format|
      if @machine_tag.save
        format.html { redirect_to @machine_tag, notice: 'Machine tag was successfully created.' }
        format.json { render :show, status: :created, location: @machine_tag }
      else
        format.html { render :new }
        format.json { render json: @machine_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @machine_tag.update(machine_tag_params)
        format.html { redirect_to @machine_tag, notice: 'Machine tag was successfully updated.' }
        format.json { render :show, status: :ok, location: @machine_tag }
      else
        format.html { render :edit }
        format.json { render json: @machine_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @machine_tag.destroy
    respond_to do |format|
      format.html { redirect_to machine_tags_url, notice: 'Machine tag was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_machine_tag
      @machine_tag = MachineTag.find(params[:id])
    end

    def machine_tag_params
      params.require(:machine_tag).permit(:name, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :machine_tag_group_id, :weight, :active, :pack_qty)
    end
end
