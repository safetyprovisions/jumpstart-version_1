class KeychainsController < ApplicationController
  before_action :set_keychain, only: [:show, :edit, :update, :destroy]

  def index
    # @keychains = Keychain.all.order(keychain_group_id: :asc, price: :asc)

    @keychain_groups = KeychainGroup.all.order(name: :asc).pluck(:id, :name)
  end

  def show
  end

  def new
    @keychain = Keychain.new
  end

  def edit
  end

  def create
    @keychain = Keychain.new(keychain_params)

    respond_to do |format|
      if @keychain.save
        format.html { redirect_to @keychain, notice: 'Keychain was successfully created.' }
        format.json { render :show, status: :created, location: @keychain }
      else
        format.html { render :new }
        format.json { render json: @keychain.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @keychain.update(keychain_params)
        format.html { redirect_to @keychain, notice: 'Keychain was successfully updated.' }
        format.json { render :show, status: :ok, location: @keychain }
      else
        format.html { render :edit }
        format.json { render json: @keychain.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @keychain.destroy
    respond_to do |format|
      format.html { redirect_to keychains_url, notice: 'Keychain was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_keychain
      @keychain = Keychain.find(params[:id])
    end

    def keychain_params
      params.require(:keychain).permit(:name, :price, :color, :cad_price, :shipping_price, :shipping_price, :priority_shipping, :intl_shipping, :keychain_group_id, :weight, :active, :pack_qty)
    end
end
