class PostersController < ApplicationController
  before_action :set_poster, only: [:show, :edit, :update, :destroy]

  def index
    # @posters = Poster.all.order(name: :asc)

    @poster_groups = PosterGroup.all.order(name: :asc).pluck(:id, :name)
  end

  def show
  end

  def new
    @poster = Poster.new
  end

  def edit
  end

  def create
    @poster = Poster.new(poster_params)
    @poster.image_derivatives! # generate Shrine derivatives

    respond_to do |format|
      if @poster.save
        @digital_poster = DigitalPoster.create(poster: @poster)
        format.html { redirect_to @poster, notice: 'Poster was successfully created.' }
        format.json { render :show, status: :created, location: @poster }
      else
        format.html { render :new }
        format.json { render json: @poster.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @poster.update(poster_params)
        @poster.image_derivatives! # generate Shrine derivatives
        @poster.save
        format.html { redirect_to @poster, notice: 'Poster was successfully updated.' }
        format.json { render :show, status: :ok, location: @poster }
      else
        format.html { render :edit }
        format.json { render json: @poster.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if @poster.digital_poster.destroy
      @poster.destroy
      respond_to do |format|
        format.html { redirect_to posters_url, notice: 'Poster was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      flash[:alert] = "Digital version could not be deleted."
      redirect_to @poster
    end
  end

  private
    def set_poster
      @poster = Poster.friendly.find(params[:id])
    end

    def poster_params
      params.require(:poster).permit(:name, :image, :image_data, :poster_group_id, :description, :download, :download_data, :display_format, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :active)
    end
end
