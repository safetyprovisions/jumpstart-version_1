class RelatedItemsController < ApplicationController
  before_action :set_related_item, only: [:show, :edit, :update, :destroy]

  def index
    @related_items = RelatedItem.all

    related_product_groups = RelatedItem.all.pluck(:product_group_id)
    @product_groups = ProductGroup.includes([:related_items]).where(id: related_product_groups).order(name: :asc)
  end

  def show
  end

  def new
    if params[:relatable_type].present?
      model = params[:relatable_type].constantize
      @relatable  = model.find(params[:relatable_id])

      related_product_groups = RelatedItem.where(
        relatable_type: params[:relatable_type],
        relatable_id:   params[:relatable_id]
      ).pluck(:product_group_id)

      @product_groups = ProductGroup.where(
        id: related_product_groups
      ).order(name: :asc).pluck(:name).join(", ")

      # @relations  = RelatedItem.where(
      #   relatable_type: params[:relatable_type],
      #   relatable_id:   params[:relatable_id]
      # )
    else
      @relatable  = nil
    end
    @related_item = RelatedItem.new
  end

  def edit
    @relatable = @related_item.relatable
  end

  def create
    @related_item = RelatedItem.new(related_item_params)

    respond_to do |format|
      if @related_item.save
        format.html { redirect_to @related_item, notice: 'Related item was successfully created.' }
        format.json { render :show, status: :created, location: @related_item }
      else
        format.html { render :new }
        format.json { render json: @related_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @related_item.update(related_item_params)
        format.html { redirect_to @related_item, notice: 'Related item was successfully updated.' }
        format.json { render :show, status: :ok, location: @related_item }
      else
        format.html { render :edit }
        format.json { render json: @related_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @related_item.destroy
    respond_to do |format|
      format.html { redirect_to related_items_url, notice: 'Related item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_related_item
      @related_item = RelatedItem.find(params[:id])
    end

    def related_item_params
      params.require(:related_item).permit(:relatable_id, :relatable_type, :product_group_id)
    end
end
