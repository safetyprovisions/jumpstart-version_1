class CheckoutsController < ApplicationController

  require 'easypost'

  if Rails.env.production?
    EasyPost.api_key = ENV['EASYPOST_LIVE_KEY']
  else
    EasyPost.api_key = Rails.application.credentials.easypost_test_key
  end

  before_action :address_params, only: [:create]
  before_action :authenticate_user!
  before_action :require_nonempty_cart!, except: [:make_assignable]
  # before_action :set_currency, only: [:show, :create]

  def show
    @pref_curr = session[:preferred_currency]
  end

  def create

    # if @currency == 'usd'
    if session[:preferred_currency] == 'usd'
      # @total_by_currency = current_cart.total_with_taxes
      @charge_currency = 'usd'
    else
      # @total_by_currency = current_cart.cad_total
      @charge_currency = 'cad'
    end

    @total_by_currency = current_cart.total_with_taxes(session[:preferred_currency])

    # NOTE params[:stripeToken] gets generated automatically via JS and submitted with the Stripe form on Checkout.
    if params[:stripeToken].present?
      # STRIPE
      @charge = Stripe::Charge.create(
        amount: @total_by_currency,
        currency: @charge_currency,
        # currency: session[:preferred_currency],
        source: params[:stripeToken],
        description: "Order ##{ current_cart.id } from HardHatTrainingSeries.com",
        metadata: {
          order_id: "Order ##{ current_cart.id }",
          # customer_name: "#{ current_user.name.full }",
          # user_email: "#{ current_user.email }",
          order_items: "#{ current_cart.order_items.map{|x| x.orderable.oi_name + ', ' + x.orderable_type + ', Qty: ' + x.quantity.to_s} }"
        },
      )

      if @charge.id.blank?
        redirect_to cart_checkout_path, notice: "Connection to Stripe server failed. This happens from time to time. Please try again."
      end
    else
      redirect_to cart_checkout_path, notice: "We're sorry, there was an error getting info from Stripe. Please try again."
    end

    # Create shipping label, if necessary
    # NOTE No @charge is created because we skipped the preceding block.
    shipping_handler

    # Apply any purchased credits, if necessary
    credit_check

    # Track Coupon Code used, if any
    track_coupon_code

    if current_user.admin?
      this_order = current_user.orders.where.not(status: "draft").last

      # Create a CompanyCourse for each OnlineCourse or TrainTheTrainer that was purchased
      create_company_course(this_order)

      # Add this Order to User's Company records.
      create_company_order(this_order)
    end

    # Send email with Order number
    OrderSuccessJob.perform_async(user: current_user)

    if session[:return_to_assign] == true
      redirect_to assign_course_path, notice: "You may now assign courses."
    else
      redirect_to current_user.orders.last, notice: "Your order was completed."
    end

    rescue Stripe::CardError => e
      flash.alert = e.message
      render action: :show

    rescue Stripe::StripeError => e
      flash.alert = "Something went wrong. Here's what we know from Stripe: #{ e.message }. You should probably just try again in a minute."
      render action: :show
  end

  def add_shipping_address
    if params[:idaho_resident] == "true"
      current_cart.update(idaho_resident: true)
    else
      current_cart.update(idaho_resident: false)
    end

    if (params[:address_provided] == "true")
      if validate_params && verify_deliverability
        if !params["group_training_date(1i)"].nil?
          require 'date'
          year  = params["group_training_date(1i)"].to_i
          month = params["group_training_date(2i)"].to_i
          day   = params["group_training_date(3i)"].to_i

          params[:group_training_date] = Date.new(year,month,day).to_formatted_s(:iso8601)
        end

        if helpers.taxable_states.include?(params[:to_state])
          params[:idaho_resident] = true
        else
          params[:idaho_resident] = false
        end

        current_cart.update(
          to_name:         params[:to_name],
          to_street1:      params[:to_street1],
          to_street2:      params[:to_street2],
          to_city:         params[:to_city],
          to_state:        params[:to_state],
          to_zip:          params[:to_zip],
          to_country:      params[:to_country],
          to_email:        params[:to_email],
          additional_info: params[:additional_info],
          group_training_date: params[:group_training_date],
          order_placed_at: Time.zone.now,
          idaho_resident:  params[:idaho_resident]
        )

        redirect_to cart_checkout_path
      else
        flash.alert = "All shipping address fields must be valid."

        redirect_to cart_path
      end
    else
      redirect_to cart_checkout_path
    end
  end

  private

    def track_coupon_code
      if current_user.orders.last.coupon_code.present?
        this_code = CouponCode.find_by(code: current_user.orders.last.coupon_code)
        this_code.times_used += 1
        this_code.save
      end
    end

    def verify_deliverability
      verifiable_address = EasyPost::Address.create(
        verify:     ["delivery"],
        street1: params[:to_street1],
        street2: params[:to_street2],
        city:    params[:to_city],
        state:   params[:to_state],
        zip:     params[:to_zip],
        country: params[:to_country],
      )

      verifiable_address.verifications["delivery"]["success"]
    end

    def validate_params
      @proceed = true

      if current_cart.order_items.pluck(:orderable_type).include?("GroupTraining")
        array = [
          params[:additional_info]
        ]

        if array.include?("")
          @proceed = false
        end
      end

      if params[:address_provided] == "true"
        array = [
          params[:to_name],
          params[:to_street1],
          params[:to_city],
          params[:to_state],
          params[:to_zip],
          params[:to_country],
          params[:to_email]
        ]

        if array.include?("")
          @proceed = false
        end
      end

      @proceed
    end

    def shipping_handler
      if ( current_user.orders.last.weight_total > 0 ) && ( session[:preferred_currency] == 'usd' )
        if current_user.orders.last.weight_total > 15
          # Set label_created to nil
          processed_cart_updater('')
        else
          begin
            # EASYPOST
            from_address = EasyPost::Address.create(
              name:    'Safety Provisions, Inc',
              street1: '218 Dividend Dr',
              city:    'Rexburg',
              state:   'ID',
              zip:     '83440',
              country: 'US',
              email:   'info@safetyprovision.com'
            )

            to_address = EasyPost::Address.create(
              name:    current_cart.to_name,
              street1: current_cart.to_street1,
              street2: current_cart.to_street2,
              city:    current_cart.to_city,
              state:   current_cart.to_state,
              zip:     current_cart.to_zip,
              country: current_cart.to_country,
              email:   current_cart.to_email,
            )

            parcel = EasyPost::Parcel.create(
              # length: 10,
              # width: 10,
              # height: 6,
              weight: current_user.orders.last.weight_total,
            )

            shipment = EasyPost::Shipment.create(
              to_address: to_address,
              from_address: from_address,
              parcel: parcel
            )

            # Automatically choose the lowest rate
            shipment.buy(
              rate: shipment.lowest_rate
            )

            @shipping = shipment.id
            # END EASYPOST
          rescue EasyPost::Error => e
            LabelFailureJob.perform_async(order_id: current_cart.id)
          end

          current_cart.update(
            # PNG version of label
            label_url:       shipment.postage_label.label_url,
            # USPS tracking code
            tracking_code:   shipment.tracking_code
          )

          processed_cart_updater(Time.zone.now)
        end
      else
        @shipping = 'none'

        processed_cart_updater(DateTime.new(1999,9,9))
      end
    end

    def credit_check
      # If current_cart contains any order_items that are CreditPacks
      @this_order = current_user.orders.where.not(status: "draft").last
      if @this_order.order_items.where(orderable_type: "CreditPack").any?
        total_credits = 0
        # Get the total number of credits and add them to current_company.credits
        @this_order.order_items.where(orderable_type: "CreditPack").each do |oi|
          if session[:preferred_currency] == 'usd'
            total_credits += (oi.orderable.credits * oi.quantity)
          else
            total_credits += (oi.orderable.cad_credits * oi.quantity)
          end
        end
        # Add credits in our system
        if session[:preferred_currency] == 'usd'
          current_company.credits += total_credits
        else
          current_company.cad_credits += total_credits
        end
        current_company.save
      end
    end

    def processed_cart_updater(label_creation_time)
      if (current_company.present? && current_company.invoicable?) || (current_user.admin? && current_user.companies.last.invoicable?)
        current_cart.update(
          shipment_id:     @shipping,
          status:          :to_be_invoiced,
          order_placed_at: Time.zone.now,
          label_created:   label_creation_time,
          currency:        @charge_currency,
          download_start_time: Time.zone.now,
          final_total:     @total_by_currency
        )
      else
        current_cart.update(
          stripe_id:       @charge.id,
          shipment_id:     @shipping,
          status:          :paid,
          paid_at:         Time.zone.now,
          card_brand:      @charge.source.brand,
          card_last4:      @charge.source.last4,
          card_exp_month:  @charge.source.exp_month,
          card_exp_year:   @charge.source.exp_year,
          order_placed_at: Time.zone.now,
          label_created:   label_creation_time,
          currency:        @charge_currency,
          download_start_time: Time.zone.now,
          card_zip_code:   @charge.source.address_zip,
          final_total:     @total_by_currency
        )
      end
    end

    def require_nonempty_cart!
      redirect_to cart_path if current_cart.order_items.none?
    end

    def address_params
      params.permit(:to_name, :to_street1, :to_street2, :to_city, :to_state, :to_zip, :to_country, :to_email, :additional_info, :order_placed_at, :group_training_date, :label_created, :po_number, :address_provided)
    end

end


# Capture ZIP after successful Stripe charge:
# @charge.source.address_zip
