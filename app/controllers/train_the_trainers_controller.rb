class TrainTheTrainersController < ApplicationController
  before_action :set_train_the_trainer, only: [:show, :edit, :update, :destroy]
  before_action :set_dropdown_options, only: [:new, :update, :edit, :create]

  def index
    # @train_the_trainers = TrainTheTrainer.all.includes(:kit).order(title: :asc)
    # authorize @train_the_trainers

    @product_groups = ProductGroup.all.includes([:train_the_trainers]).order(name: :asc)
  end

  def show
    @practical_evaluation = JSON.parse(@train_the_trainer.practical_evaluation_data)["metadata"]["filename"]
    @answer_key = JSON.parse(@train_the_trainer.answer_key_data)["metadata"]["filename"]
    @exam = JSON.parse(@train_the_trainer.exam_data)["metadata"]["filename"]
  end

  def new
    @train_the_trainer = TrainTheTrainer.new
    authorize @train_the_trainer
  end

  def edit
  end

  def create
    @train_the_trainer = TrainTheTrainer.new(train_the_trainer_params)
    authorize @train_the_trainer

    respond_to do |format|
      if @train_the_trainer.save
        format.html { redirect_to @train_the_trainer, notice: 'Train the trainer was successfully created.' }
        format.json { render :show, status: :created, location: @train_the_trainer }
      else
        format.html { render :new }
        format.json { render json: @train_the_trainer.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @train_the_trainer.update(train_the_trainer_params)
        format.html { redirect_to @train_the_trainer, notice: 'Train the trainer was successfully updated.' }
        format.json { render :show, status: :ok, location: @train_the_trainer }
      else
        format.html { render :edit }
        format.json { render json: @train_the_trainer.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @train_the_trainer.destroy
    authorize @train_the_trainer
    respond_to do |format|
      format.html { redirect_to train_the_trainers_url, notice: 'Train the trainer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_train_the_trainer
      @train_the_trainer = TrainTheTrainer.find(params[:id])
      authorize @train_the_trainer
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def train_the_trainer_params
      params.require(:train_the_trainer).permit(:title, :price, :sale_price, :sale_start, :sale_expiry, :language, :standards, :product_group_id, :talent_course_id, :practical_evaluation, :exam, :answer_key, :kit_id, :cad_price, :cad_sale_price, :active)
    end

    def set_dropdown_options
      @kits = Kit.all.order(title: :asc)
      @product_groups = ProductGroup.all.order(name: :asc)
      # @online_courses = OnlineCourse.where(ttt: true).order(title: :asc)
    end
end
