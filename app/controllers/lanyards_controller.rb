class LanyardsController < ApplicationController
  before_action :set_lanyard, only: [:show, :edit, :update, :destroy]

  def index
    # @lanyards = Lanyard.all.includes([:lanyard_group])

    @lanyard_groups = LanyardGroup.all.order(name: :asc).pluck(:id, :name)
  end

  def show
  end

  def new
    @lanyard = Lanyard.new
  end

  def edit
  end

  def create
    @lanyard = Lanyard.new(lanyard_params)

    respond_to do |format|
      if @lanyard.save
        format.html { redirect_to @lanyard, notice: 'Lanyard was successfully created.' }
        format.json { render :show, status: :created, location: @lanyard }
      else
        format.html { render :new }
        format.json { render json: @lanyard.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @lanyard.update(lanyard_params)
        format.html { redirect_to @lanyard, notice: 'Lanyard was successfully updated.' }
        format.json { render :show, status: :ok, location: @lanyard }
      else
        format.html { render :edit }
        format.json { render json: @lanyard.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @lanyard.destroy
    respond_to do |format|
      format.html { redirect_to lanyards_url, notice: 'Lanyard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_lanyard
      @lanyard = Lanyard.find(params[:id])
    end

    def lanyard_params
      params.require(:lanyard).permit(:name, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :lanyard_group_id, :weight, :active, :pack_qty)
    end
end
