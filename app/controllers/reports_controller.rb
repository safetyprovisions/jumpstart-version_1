class ReportsController < ApplicationController
  # before_action :authenticate_user!
  before_action :verify_superadmin!, only: [:sales]
  before_action :no_employees_error!, only: [:employee_list]

  def employee_list
    if current_user.present? && policy(current_user).create?
      employees = current_user.companies.last.employees.order(last_name: :asc)
    end

    if current_company.present? && policy(current_company).main?
      employees = current_company.employees.order(last_name: :asc)
    end

    ee_list = ''
    employees.each do |ee|
      ee_list += "#{ ee.name.last }, #{ ee.name.first }\t\t\t\t #{ ee.email }\n\n________________________________________________________________________________\n\n"
    end

    send_data ee_list, filename: "employees-#{ Date.today.strftime("%Y-%m-%d") }.txt"
  end

  def sales
    require 'date'
    from_year  = params["from(1i)"].to_i
    from_month = params["from(2i)"].to_i
    from_day   = params["from(3i)"].to_i

    params[:from] = Date.new(from_year,from_month,from_day)

    to_year  = params["to(1i)"].to_i
    to_month = params["to(2i)"].to_i
    to_day   = params["to(3i)"].to_i

    params[:to] = Date.new(to_year,to_month,to_day)

    sales = Order.where('created_at > ? AND created_at < ?', params[:from], params[:to]).where.not(status: 'draft').order(created_at: :asc)

    sale_list = ''
    # sales.each do |sale|
    #   sale_list += "Order ##{ sale.id }, #{ helpers.readable_time(sale.created_at) }, #{ helpers.number_to_currency(sale.final_total / 100) }\n\n________________________________________________________________________________\n\n"
    # end

    paid_orders = sales.where('status IN (?)', %w[ paid shipped completed ])
    paid_orders_total = paid_orders.pluck(:final_total).sum
    sale_list += "PAID ORDERS\n\nTotal Payments Received: #{ helpers.number_to_currency(paid_orders_total / 100) }\n\n________________________________________________________________________________\n\n"

    paid_orders.each do |sale|
      sale_list += "Order ##{ sale.id }, #{ helpers.readable_time(sale.created_at) }, #{ helpers.number_to_currency(sale.final_total / 100) }\n\n________________________________________________________________________________\n\n"
    end

    invoiced_orders = sales.where(status: 'invoiced')
    invoiced_orders_total = invoiced_orders.pluck(:final_total).sum
    sale_list += "\n\nINVOICED ORDERS\n\nTotal Payments Invoiced: #{ helpers.number_to_currency(invoiced_orders_total / 100) }\n\n________________________________________________________________________________\n\n"

    invoiced_orders.each do |sale|
      sale_list += "Order ##{ sale.id }, #{ helpers.readable_time(sale.created_at) }, #{ helpers.number_to_currency(sale.final_total / 100) }\n\n________________________________________________________________________________\n\n"
    end

    to_be_invoiced_orders = sales.where(status: 'to_be_invoiced')
    to_be_invoiced_orders_total = to_be_invoiced_orders.pluck(:final_total).sum
    sale_list += "\n\nORDERS TO BE INVOICED\n\nTotal To Be Invoiced: #{ helpers.number_to_currency(to_be_invoiced_orders_total / 100) }\n\n________________________________________________________________________________\n\n"

    to_be_invoiced_orders.each do |sale|
      sale_list += "Order ##{ sale.id }, #{ helpers.readable_time(sale.created_at) }, #{ helpers.number_to_currency(sale.final_total / 100) }\n\n________________________________________________________________________________\n\n"
    end

    send_data sale_list, filename: "sales-as-of-#{ Date.today.strftime("%Y-%m-%d") }.txt"
  end

  def analytics
    visits = Order.joins(:ahoy_visit)

    @domains = visits.group("referring_domain").count
    # @cities  = visits.group("city").count
    @countries  = visits.group("country").count
    @devices = visits.group("device_type").count
  end

end
