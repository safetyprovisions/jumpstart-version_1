class BackupsController < ApplicationController
  before_action :set_backup, only: [:show, :edit, :update, :destroy]
  # after_action :format_format, only: [:create, :update]

  def index
    @backups = Backup.all
    authorize @backups
  end

  def show
  end

  def new
    @backup = Backup.new
    authorize @backup
  end

  def edit
  end

  def create
    @backup = Backup.new(backup_params)
    authorize @backup
    @backup.format = params[:backup][:format].downcase

    respond_to do |format|
      if @backup.save
        format.html { redirect_to @backup, notice: 'Backup was successfully created.' }
        format.json { render :show, status: :created, location: @backup }
      else
        format.html { render :new }
        format.json { render json: @backup.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @backup.update(backup_params)
        format.html { redirect_to @backup, notice: 'Backup was successfully updated.' }
        format.json { render :show, status: :ok, location: @backup }
      else
        format.html { render :edit }
        format.json { render json: @backup.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @backup.destroy
    respond_to do |format|
      format.html { redirect_to backups_url, notice: 'Backup was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # def format_format
    #   format = format.downcase
    # end

    def set_backup
      @backup = Backup.find(params[:id])
      authorize @backup
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def backup_params
      params.require(:backup).permit(:format, :price, :shipping_price, :priority_shipping, :intl_shipping, :cad_price, :active)
    end
end
