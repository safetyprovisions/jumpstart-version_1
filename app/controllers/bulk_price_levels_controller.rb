class BulkPriceLevelsController < ApplicationController
  before_action :set_bulk_price_level, only: [:show, :edit, :update, :destroy]

  def index
    @bulk_price_levels = BulkPriceLevel.all.order(qty: :asc)
    # authorize @bulk_price_levels
    @tiers = @bulk_price_levels.pluck(:tier).uniq.sort
    # @groups_by_tier = BulkPriceLevel.group(:tier).order(qty: :asc)
  end

  def show
  end

  def new
    @bulk_price_level = BulkPriceLevel.new
  end

  def edit
  end

  def create
    @bulk_price_level = BulkPriceLevel.new(bulk_price_level_params)

    respond_to do |format|
      if @bulk_price_level.save
        format.html { redirect_to @bulk_price_level, notice: 'Bulk price level was successfully created.' }
        format.json { render :show, status: :created, location: @bulk_price_level }
      else
        format.html { render :new }
        format.json { render json: @bulk_price_level.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @bulk_price_level.update(bulk_price_level_params)
        format.html { redirect_to @bulk_price_level, notice: 'Bulk price level was successfully updated.' }
        format.json { render :show, status: :ok, location: @bulk_price_level }
      else
        format.html { render :edit }
        format.json { render json: @bulk_price_level.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @bulk_price_level.destroy
    respond_to do |format|
      format.html { redirect_to bulk_price_levels_url, notice: 'Bulk price level was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_bulk_price_level
      @bulk_price_level = BulkPriceLevel.find(params[:id])
    end

    def bulk_price_level_params
      params.require(:bulk_price_level).permit(:qty, :price, :cad_price, :tier)
    end
end
