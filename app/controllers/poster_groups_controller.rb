class PosterGroupsController < ApplicationController
  before_action :set_poster_group, only: [:show, :edit, :update, :destroy, :check_for_posters, :delete_related_items]
  before_action :check_and_cleanup, only: [:destroy]

  def index
    @poster_groups = PosterGroup.where(active: true).order(name: :asc)
  end

  def show
    @posters = Poster.where(poster_group_id: @poster_group.id, active: true).order(name: :asc)
    related = RelatedItem.find_by(relatable_type: "PosterGroup", relatable_id: @poster_group.id)
    if related.present?
      @training = ProductGroup.find(related.product_group_id)
    end
  end

  def new
    @poster_group = PosterGroup.new
  end

  def edit
  end

  def create
    @poster_group = PosterGroup.new(poster_group_params)

    respond_to do |format|
      if @poster_group.save
        format.html { redirect_to @poster_group, notice: 'Poster group was successfully created.' }
        format.json { render :show, status: :created, location: @poster_group }
      else
        format.html { render :new }
        format.json { render json: @poster_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @poster_group.update(poster_group_params)
        format.html { redirect_to @poster_group, notice: 'Poster group was successfully updated.' }
        format.json { render :show, status: :ok, location: @poster_group }
      else
        format.html { render :edit }
        format.json { render json: @poster_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @poster_group.destroy
    respond_to do |format|
      format.html { redirect_to poster_groups_url, notice: 'Poster group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_poster_group
      @poster_group = PosterGroup.friendly.find(params[:id])
    end

    def check_and_cleanup
      check_for_posters

      delete_related_items
    end

    def check_for_posters
      if @poster_group.posters.any?
        flash[:alert] = 'Please remove any Posters that are attached to this group before deleting the Poster Group.'
        redirect_to @poster_group
      end
    end

    def delete_related_items
      items_to_delete = RelatedItem.where(relatable_type: "PosterGroup", relatable_id: @poster_group.id)
      if items_to_delete.present?
        items_to_delete.each do |item|
          item.delete
        end
      end
    end

    def poster_group_params
      params.require(:poster_group).permit(:name, :description, :slug, :keywords, :meta_description, :image, :active)
    end
end
