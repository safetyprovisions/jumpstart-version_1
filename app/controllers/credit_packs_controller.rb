class CreditPacksController < ApplicationController
  before_action :verify_superadmin!, except: :index
  before_action :set_credit_pack, only: [:show, :edit, :update, :destroy]
  before_action :load_credit_packs, only: [:index]

  def index

  end

  def show
  end

  def new
    @credit_pack = CreditPack.new
    authorize @credit_pack
  end

  def edit
  end

  def create
    @credit_pack = CreditPack.new(credit_pack_params)
    authorize @credit_pack

    respond_to do |format|
      if @credit_pack.save
        format.html { redirect_to @credit_pack, notice: 'Credit pack was successfully created.' }
        format.json { render :show, status: :created, location: @credit_pack }
      else
        format.html { render :new }
        format.json { render json: @credit_pack.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @credit_pack.update(credit_pack_params)
        format.html { redirect_to @credit_pack, notice: 'Credit pack was successfully updated.' }
        format.json { render :show, status: :ok, location: @credit_pack }
      else
        format.html { render :edit }
        format.json { render json: @credit_pack.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @credit_pack.destroy
    respond_to do |format|
      format.html { redirect_to credit_packs_url, notice: 'Credit pack was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_credit_pack
      @credit_pack = CreditPack.find(params[:id])
      authorize @credit_pack
    end

    def credit_pack_params
      params.require(:credit_pack).permit(:name, :price, :credits, :cad_price, :cad_credits)
    end
end
