class MachineTagGroupsController < ApplicationController
  before_action :set_machine_tag_group, only: [:show, :edit, :update, :destroy, :check_for_machine_tags, :delete_related_items]
  before_action :check_and_cleanup, only: [:destroy]

  def index
    @machine_tag_groups = MachineTagGroup.all
  end

  def show
    # @machine_tag_group = MachineTagGroup.friendly.find(params[:id])
    @machine_tags = MachineTag.where(machine_tag_group_id: @machine_tag_group.id, active: true).order(price: :asc)
  end

  def new
    @machine_tag_group = MachineTagGroup.new
  end

  def edit
  end

  def create
    @machine_tag_group = MachineTagGroup.new(machine_tag_group_params)
    @machine_tag_group.image_derivatives! # generate Shrine derivatives

    respond_to do |format|
      if @machine_tag_group.save
        format.html { redirect_to @machine_tag_group, notice: 'Machine tag group was successfully created.' }
        format.json { render :show, status: :created, location: @machine_tag_group }
      else
        format.html { render :new }
        format.json { render json: @machine_tag_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @machine_tag_group.update(machine_tag_group_params)
        @machine_tag_group.image_derivatives! # generate Shrine derivatives
        @machine_tag_group.save
        format.html { redirect_to @machine_tag_group, notice: 'Machine tag group was successfully updated.' }
        format.json { render :show, status: :ok, location: @machine_tag_group }
      else
        format.html { render :edit }
        format.json { render json: @machine_tag_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @machine_tag_group.destroy
    respond_to do |format|
      format.html { redirect_to machine_tag_groups_url, notice: 'Machine tag group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_machine_tag_group
      @machine_tag_group = MachineTagGroup.friendly.find(params[:id])
    end

    def check_and_cleanup
      check_for_machine_tags

      delete_related_items
    end

    def check_for_machine_tags
      if @machine_tag_group.machine_tags.any?
        flash[:alert] = 'Please remove any Machine Tags that are attached to this group before deleting the Machine Tag Group.'
        redirect_to @machine_tag_group
      end
    end

    def delete_related_items
      items_to_delete = RelatedItem.where(relatable_type: "MachineTagGroup", relatable_id: @machine_tag_group.id)
      if items_to_delete.present?
        items_to_delete.each do |item|
          item.delete
        end
      end
    end

    def machine_tag_group_params
      params.require(:machine_tag_group).permit(:name, :description, :image, :image_data, :keywords, :meta_description, :active)
    end
end
