class HatsController < ApplicationController
  before_action :set_hat, only: [:show, :edit, :update, :destroy]

  def index
    @hats = Hat.where(active: true).order(name: :asc)
  end

  def show
    @sizes = @hat.size.split(", ")
  end

  def new
    @hat = Hat.new
  end

  def edit
  end

  def create
    @hat = Hat.new(hat_params)

    respond_to do |format|
      if @hat.save
        @hat.image_derivatives! # generate derivatives
        @hat.save
        format.html { redirect_to @hat, notice: 'Hat was successfully created.' }
        format.json { render :show, status: :created, location: @hat }
      else
        format.html { render :new }
        format.json { render json: @hat.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @hat.update(hat_params)
        @hat.image_derivatives! # generate Shrine derivatives
        @hat.save
        format.html { redirect_to @hat, notice: 'Hat was successfully updated.' }
        format.json { render :show, status: :ok, location: @hat }
      else
        format.html { render :edit }
        format.json { render json: @hat.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @hat.destroy
    respond_to do |format|
      format.html { redirect_to hats_url, notice: 'Hat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_hat
      @hat = Hat.friendly.find(params[:id])
    end

    def hat_params
      params.require(:hat).permit(:name, :description, :image, :image_data, :slug, :keywords, :meta_description, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :weight, :size, :style, :structure, :fastener, :active)
    end
end
