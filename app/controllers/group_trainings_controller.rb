class GroupTrainingsController < ApplicationController
  before_action :set_group_training, only: [:show, :edit, :update, :destroy]

  def index
    @group_trainings = GroupTraining.all
    authorize @group_trainings
  end

  def show
  end

  def new
    @group_training = GroupTraining.new
    authorize @group_training
  end

  def edit
  end

  def create
    @group_training = GroupTraining.new(group_training_params)
    authorize @group_training

    respond_to do |format|
      if @group_training.save
        format.html { redirect_to @group_training, notice: 'Group training was successfully created.' }
        format.json { render :show, status: :created, location: @group_training }
      else
        format.html { render :new }
        format.json { render json: @group_training.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @group_training.update(group_training_params)
        format.html { redirect_to @group_training, notice: 'Group training was successfully updated.' }
        format.json { render :show, status: :ok, location: @group_training }
      else
        format.html { render :edit }
        format.json { render json: @group_training.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @group_training.destroy
    respond_to do |format|
      format.html { redirect_to group_trainings_url, notice: 'Group training was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_group_training
      @group_training = GroupTraining.find(params[:id])
      authorize @group_training
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_training_params
      params.require(:group_training).permit(:online_course_id)
    end
end
