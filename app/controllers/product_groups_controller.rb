class ProductGroupsController < ApplicationController
  before_action :set_product_group, only: [:show, :edit, :update, :destroy, :add_tag, :remove_tag, :check_for_existing_children, :delete_related_items]
  before_action :check_and_cleanup, only: [:destroy]
  before_action :set_categories, only: [:edit, :update]
  before_action :get_alt_names, only: [:show]

  def index
    @product_groups = ProductGroup.all.order(name: :asc)
    authorize @product_groups
  end

  def show
    @alt_text = ((@product_group.alt_text.nil? || @product_group.alt_text.empty?) ? @product_group.name : @product_group.alt_text)

    get_bulk_price_levels

    get_alt_names

    @kits = @product_group.kits.where(active:true)

    # @kits               = Kit.where(product_group_id: @product_group_id)

    # TODO Is it necessary to set ttt: false?
    all_online_courses  = OnlineCourse.where(product_group_id: @product_group_id, active:true)

    @online_courses     = all_online_courses.where(public: true)

    if user_signed_in? && current_user.admin?
      check_company = (current_company.present? ? current_company : current_user.companies.last)
      if NonCatalogCourseAccess.where(company: check_company).any?
        non_cat_courses = NonCatalogCourseAccess.where(company: check_company).pluck(:online_course_id)
        non_public_courses = all_online_courses.where(public: false)

        @online_courses += non_public_courses.where(id: non_cat_courses)
      end
    end

    if user_signed_in? && current_user.superadmin?
      @online_courses = all_online_courses
    end

    @train_the_trainers = TrainTheTrainer.where(active: true).includes(:product_group, :kit).where(product_group_id: @product_group_id)

    @products           = Product.where(product_group_id: @product_group_id)

    @group_trainings = GroupTraining.none

    # TODO This block makes no sense. What is it doing?
    # I think it's checking to see if there are any GroupTrainings that have the OnlineCourse, and if so, assign them to the @group_trainings variable.
    @online_courses.each do |course|
      course_matches = GroupTraining.where(online_course_id: course.id)
      if course_matches.count > 0
        course_matches.each do |match|
          @group_trainings += GroupTraining.where(online_course_id: course.id)
        end
      end
      @group_trainings
    end

    if @product_group.tag_list.empty?
      flash.now[:notice] = 'This Product Group will not be listed on the Homepage until you add at least one Category.'
    end

    # if session[:preferred_currency] == 'usd'
    #   @currency = 'usd'
    # else
    #   @currency = 'cad'
    # end
  end

  def new
    @product_group = ProductGroup.new
    authorize @product_group
  end

  def edit
    # @categories = ActsAsTaggableOn::Tag.all.order(name: :asc)
  end

  def create
    @product_group = ProductGroup.new(product_group_params)
    @product_group.image_derivatives! # generate Shrine derivatives
    authorize @product_group

    respond_to do |format|
      if @product_group.save
        format.html { redirect_to edit_product_group_path(@product_group, anchor: 'product-group-categories'), notice: 'Saved. You must now add at least one Category.' }
        format.json { render :edit, status: :created, location: @product_group }
      else
        format.html { render :new }
        format.json { render json: @product_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @product_group.tag_list.empty?
        format.html { redirect_to edit_product_group_path(@product_group), notice: 'You must add at least one Category!' }
      else
        if @product_group.update(product_group_params)
          @product_group.image_derivatives! # generate Shrine derivatives
          @product_group.save
          format.html { redirect_to @product_group, notice: 'Product group was successfully updated.' }
          format.json { render :show, status: :ok, location: @product_group }
        else
          format.html { render :edit }
          format.json { render json: @product_group.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def destroy
    @product_group.destroy
    authorize @product_group
    respond_to do |format|
      format.html { redirect_to product_groups_url, notice: 'Product group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add_tag
    @product_group.tag_list.add(params[:tag].upcase)
    # @product_group.save

    # redirect_to edit_product_group_path

    respond_to do |format|
      if @product_group.save
        format.html { redirect_to edit_product_group_path(@product_group), notice: 'Tag was successfully added.' }
        format.json { render :edit, status: :created, location: @product_group }
      else
        format.html { render :edit }
        format.json { render json: @product_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove_tag
    @product_group.tag_list.remove(params[:tag])
    # @product_group.save

    # redirect_to edit_product_group_path

    respond_to do |format|
      if @product_group.save
        format.html { redirect_to edit_product_group_path(@product_group), notice: 'Tag was successfully removed.' }
        format.json { render :edit, status: :created, location: @product_group }
      else
        format.html { render :edit }
        format.json { render json: @product_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_tag_global
    new_name = params[:tag_to_add].upcase
    new_tag = ActsAsTaggableOn::Tag.new(name: new_name, taggings_count: 0)
    authorize new_tag, policy_class: ProductGroupPolicy

    if new_tag.save
      flash[:notice] = "Category #{ new_name } was added."
      redirect_to user_path(current_user)
    else
      flash[:alert] = " A new category named #{ new_name } could not be added. Does it already exist?"
      redirect_to user_path(current_user)
    end
  end

  def remove_tag_global
    tag_to_delete = ActsAsTaggableOn::Tag.find(params[:tag_id])
    authorize tag_to_delete, policy_class: ProductGroupPolicy
    tag_to_delete.delete

    redirect_to user_path(current_user)
  end

  private

    def get_alt_names
      @alt_names = AltName.where(product_group_id: @product_group.id)
    end

    def set_categories
      @categories = ActsAsTaggableOn::Tag.all.order(name: :asc)
    end

    def set_product_group
      # @product_group = ProductGroup.find(params[:id])
      @product_group = ProductGroup.friendly.find(params[:id])
      authorize @product_group
      @product_group_id = ProductGroup.friendly.find(params[:id]).id
    end

    def check_and_cleanup
      check_for_existing_children

      delete_related_items
    end

    def check_for_existing_children
      existing_children = []
      if @product_group.kits.any?
        existing_children << "Kits"
      end
      if @product_group.online_courses.any?
        existing_children << "Online Courses"
      end
      if @product_group.train_the_trainers.any?
        existing_children << "Train The Trainers"
      end
      unless existing_children.empty?
        flash[:alert] = "Please remove any #{ existing_children.join("/")} that are attached to this group before deleting the Product Group."
        redirect_to @product_group
      end
    end

    def delete_related_items
      items_to_delete = RelatedItem.where(product_group_id: @product_group.id)
      if items_to_delete.present?
        items_to_delete.each do |item|
          item.delete
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_group_params
      params.require(:product_group).permit(:name, :image, :image_data, :tag, :tag_to_add, :video, :overview, :training_standards, :facts, :fails, :keywords, :meta_description, :active, :alt_text)
    end
end
