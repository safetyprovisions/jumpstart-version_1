class KitsController < ApplicationController
  before_action :set_kit, only: [:show, :edit, :update, :destroy]
  # before_action :set_product_groups, only: [:new, :edit]

  def index
    @kits = Kit.all.order(title: :asc)
    authorize @kits

    @product_groups = ProductGroup.all.includes([:kits]).order(name: :asc)
  end

  def show
  end

  def new
    @kit = Kit.new
    authorize @kit
    # @product_groups = ProductGroup.all.order(name: :asc)
  end

  def edit
  end

  def create
    @kit = Kit.new(kit_params)
    authorize @kit

    respond_to do |format|
      if @kit.save
        format.html { redirect_to @kit, notice: 'Kit was successfully created.' }
        format.json { render :show, status: :created, location: @kit }
      else
        format.html { render :new }
        format.json { render json: @kit.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @kit.update(kit_params)
        format.html { redirect_to @kit, notice: 'Kit was successfully updated.' }
        format.json { render :show, status: :ok, location: @kit }
      else
        format.html { render :edit }
        format.json { render json: @kit.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @kit.destroy
    respond_to do |format|
      format.html { redirect_to kits_url, notice: 'Kit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_kit
      @kit = Kit.find(params[:id])
      authorize @kit
    end

    # def set_product_groups
    #   @product_groups = ProductGroup.all.order(name: :asc)
    # end

    def kit_params
      params.require(:kit).permit(:market_type, :title, :download, :additional_download, :price, :sale_price, :sale_start, :sale_expiry, :language, :standards, :product_group_id, :sample, :cad_price, :cad_sale_price, :active)
    end
end
