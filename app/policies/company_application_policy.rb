class CompanyApplicationPolicy < ApplicationPolicy
  attr_reader :current_company, :record

  def initialize(current_company, record)
    @current_company = current_company
    @record = record
  end

  def branch_view?
    company_signed_in?
  end

  def add_employee?
    branch_view?
  end

  def manage_employees?
    company_signed_in?
  end

  def toggle_admin?
    false
  end

  def toggle_employee_status?
    branch_view?
  end

  def remove_employee?
    false
  end

  def assign_course?
    branch_view?
  end

  # HELPER METHODS

  def record_is_current_company?
    current_company.present? && (current_company == record)
  end

  def record_is_in_downline?
    current_company.present? && current_company.branches.include?(record)
  end

  # class Scope
  #   attr_reader :user, :scope

  #   def initialize(user, scope)
  #     @user  = user
  #     @scope = scope
  #   end

  #   def resolve
  #     if user.superadmin?
  #       scope.all
  #     end
  #   end
  # end
end
