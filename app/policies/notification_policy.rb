class NotificationPolicy < UserApplicationPolicy

  # METHOD MATCHERS

  def index?
    user.present?
  end

end
