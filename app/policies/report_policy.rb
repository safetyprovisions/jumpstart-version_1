class ReportPolicy < UserApplicationPolicy

  # METHOD MATCHERS

  def employee_list?
    policy(user).create? || policy(company).main?
  end

end
