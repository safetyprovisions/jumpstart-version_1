class ProductGroupPolicy < UserApplicationPolicy

  # METHOD MATCHERS

  def index?
    user_is_superadmin?
  end

  def show?
    true
  end

  def new?
    user_is_superadmin?
  end

  def edit?
    user_is_superadmin?
  end

  def update?
    user_is_superadmin?
  end

  def create?
    user_is_superadmin?
  end

  def destroy?
    user_is_superadmin?
  end

  def add_tag?
    user_is_superadmin?
  end

  def remove_tag?
    user_is_superadmin?
  end

  def add_tag_global?
    user_is_superadmin?
  end

  def remove_tag_global?
    user_is_superadmin?
  end

end
