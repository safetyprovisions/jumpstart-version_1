class UserApplicationPolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  # HELPER METHODS

  def user_is_admin?
    user.present? && user.admin?
  end

  def user_is_superadmin?
    user.present? && user.superadmin?
  end

  def user_is_employees_admin?
    user.present? && shared_company? && user.admin? && !record.superadmin?
  end

    def shared_company?
      user.companies.pluck(:id) & record.companies.pluck(:id) != []
    end

  def user_is_current_user?
    user.present? && user == record
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
