class UserPolicy < UserApplicationPolicy

  # METHOD MATCHERS

  def show?
    # User can view their own data
    user_is_current_user? || user_is_superadmin? || user_is_employees_admin?
  end

  def create?
    user_is_admin? || user_is_superadmin?
  end


  def existence_check?
    true
  end

  def talent_user_delete? # Are arguments required?
    user_is_admin? || user_is_superadmin?
  end

  def labels_required?
    user_is_superadmin?
  end

  # def remove_employee?
  #   user_is_admin? || user_is_superadmin?
  # end

  def toggle_employee_status?
    user_is_admin? || user_is_superadmin?
  end


  # class Scope < Scope
  #   def resolve
  #     scope.all
  #   end
  # end

end
