class CompanyPolicy < CompanyApplicationPolicy

# METHOD MATCHERS

  def index?
    # Handled manually in CompaniesController
  end

  def main?
    # current_company.present? || user.superadmin?
    true
  end

  def branch_view?
    current_company.present?
  end

  def add_employee?
    current_company.present?
  end

  def manage_employees?
    current_company.present?
  end

  def toggle_admin?
    current_company.present?
  end

  def toggle_employee_status?
    current_company.present?
  end

  # def remove_employee?
  #   current_company.present?
  # end

  def show?
    record_is_current_company? || record_is_in_downline?
  end

  def edit?
    current_company.present?
  end

  def update?
    current_company.present?
  end

  def assign_course?
    current_company.present?
  end

  def company_reports?
    current_company.present?
  end

end
