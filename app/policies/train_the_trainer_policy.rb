class TrainTheTrainerPolicy < UserApplicationPolicy

  # METHOD MATCHERS

  def index?
    user_is_superadmin?
  end

  def show?
    user_is_superadmin?
  end

  def new?
    user_is_superadmin?
  end

  def edit?
    user_is_superadmin?
  end

  def update?
    user_is_superadmin?
  end

  def create?
    user_is_superadmin?
  end

  def destroy?
    user_is_superadmin?
  end

end
