class TalentPolicy < UserApplicationPolicy

  # METHOD MATCHERS

  def online_course_validator?
    user.present?
  end

  def assign_talent_user_id_and_return?
    user.present?
  end

  def assign_talent_user_id?
    user.present?
  end

  def get_cert?
    user.present?
  end

  def log_into_talent?
    user.present?
  end

  def talent_user_login?
    user.present?
  end

end
