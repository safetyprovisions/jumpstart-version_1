class OrderPolicy < UserApplicationPolicy

  # METHOD MATCHERS

  def index?
    user.present? || user_is_admin? || user_is_superadmin?
  end

  def labeled
    index?
  end

  def show?
    (user.present? && record.user == user ) ||
    user_is_admin? ||
    user_is_superadmin?
  end

  def update?
    user_is_superadmin?
  end

  def unpaid_orders?
    user_is_superadmin?
  end
end
