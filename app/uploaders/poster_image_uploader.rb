class PosterImageUploader < Shrine
  # require "image_processing/mini_magick"

  plugin :remove_invalid # remove and delete files that failed validation
  plugin :validation_helpers
  plugin :store_dimensions
  plugin :derivatives

  def generate_location(io, context)
      "Poster/#{ super }"
  end

  Attacher.derivatives do |original|
    magick = ImageProcessing::MiniMagick.source(original)

    {
      large:  magick.resize_to_limit!(658, 822),
      medium: magick.resize_to_limit!(475, 593),
      small:  magick.resize_to_limit!(300, 374),
    }
  end

  Attacher.validate do
    validate_min_size 1, message: "must not be empty" # Validates presence?
    validate_max_size 0.075*1024*1024, message: "is too large (max is 75 KB)" # Validates maximum file size
    validate_extension_inclusion %w[jpg jpeg gif png tiff tif]
    validate_max_width 300 # Validates maximum file width
    validate_max_height 375 # Validates maximum file height
  end
end
