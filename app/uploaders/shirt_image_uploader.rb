class ShirtImageUploader < Shrine
  plugin :remove_invalid # remove and delete files that failed validation
  plugin :validation_helpers
  plugin :store_dimensions

  def generate_location(io, context)
      "Shirt/#{ super }"
  end

  Attacher.validate do
    validate_min_size 1, message: "must not be empty" # Validates presence?
    validate_max_size 0.075*1024*1024, message: "is too large (max is 75 KB)" # Validates maximum file size
    validate_extension_inclusion %w[jpg jpeg gif png tiff tif]
    validate_max_width 400 # Validates maximum file width
    validate_max_height 400 # Validates maximum file height
  end

  Attacher.derivatives do |original|
    magick = ImageProcessing::MiniMagick.source(original)

    # generate the thumbnails you want here
    {
      small:  magick.resize_to_limit!(200, 200),
      # medium: magick.resize_to_limit!(500, 500),
      large:  magick.resize_to_limit!(400, 400),
    }
  end
end
