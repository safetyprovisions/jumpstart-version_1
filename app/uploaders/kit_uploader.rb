class KitUploader < Shrine
  plugin :remove_invalid
  plugin :validation_helpers

  def generate_location(io, context)
    case context[:record].market_type
    when ""
      "NotUploaded/#{ super }"
    when "usa"
      "USA/Kits/#{ context[:record].title }/Download/#{ [super, *context[:metadata]["filename"]].join("/") }"
    when "canada"
      "Canada/Kits/#{ context[:record].title }/Download/#{ [super, *context[:metadata]["filename"]].join("/") }"
    when "spanish"
      "Spanish/Kits/#{ context[:record].title }/Download/#{ [super, *context[:metadata]["filename"]].join("/") }"
    when "tagalog"
      "Tagalog/Kits/#{ context[:record].title }/Download/#{ [super, *context[:metadata]["filename"]].join("/") }"
    end
  end

  Attacher.validate do
    validate_min_size 1, message: "must not be empty" # Validates presence?
    validate_extension_inclusion %w[zip]
  end
end
