class PosterDownloadUploader < Shrine
  plugin :remove_invalid # remove and delete files that failed validation
  plugin :validation_helpers
  plugin :store_dimensions

  def generate_location(io, context)
    # "Poster/download/#{ super }"
    # "Poster/download/#{ [super, *context[:metadata]["filename"]].join("/") }"
    "Poster/download/#{ context[:record].name.parameterize }/#{ [super, *context[:metadata]["filename"]].join("/") }"
  end

  Attacher.validate do
    validate_min_size 1, message: "must not be empty" # Validates presence?
    # validate_max_size 0.5*1024*1024, message: "is too large (max is 500 KB)" # Validates maximum file size
    validate_max_size 75*1024*1024, message: "is too large (max is 75 MB)" # Validates maximum file size
    validate_extension_inclusion %w[pdf]
  end
end
