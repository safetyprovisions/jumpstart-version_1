class TrainTheTrainerExamUploader < Shrine
  plugin :remove_invalid # remove and delete files that failed validation
  plugin :validation_helpers

  def generate_location(io, context)
    "TrainTheTrainer/#{ context[:record].title }/Exam/#{ [super, *context[:metadata]["filename"]].join("/") }"
  end

  Attacher.validate do
    validate_min_size 1, message: "must not be empty" # Validates presence?
    validate_extension_inclusion %w[doc docx pdf]
  end
end
