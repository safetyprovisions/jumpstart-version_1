class Order < ApplicationRecord
  visitable :ahoy_visit

  encrypts :to_name
  blind_index :to_name

  encrypts :to_street1
  blind_index :to_street1

  encrypts :to_city
  blind_index :to_city

  encrypts :to_zip
  blind_index :to_zip

  encrypts :to_email
  blind_index :to_email

  encrypts :additional_info
  blind_index :additional_info


  belongs_to :user

  has_many :order_items, dependent: :destroy

  scope :draft,          ->{ where(status: :draft)          }
  scope :paid,           ->{ where(status: :paid)           }
  scope :shipped,        ->{ where(status: :shipped)        }
  scope :complete,       ->{ where(status: :complete)       }
  scope :refunded,       ->{ where(status: :refunded)       }
  scope :invoiced,       ->{ where(status: :invoiced)       }
  scope :to_be_invoiced, ->{ where(status: :to_be_invoiced) }

  def subtotal(*args)
    # order_items.map(&:total).sum
    subtotal = 0
    order_items.each do |oi|
      subtotal += oi.total(args[0])
    end

    subtotal
  end

  # def cad_subtotal
  #   order_items.map(&:cad_total).sum
  # end

  def shipping_total
    if shipping_method == "shipping_price" # Basic
      order_items.map(&:shipping_price).sum
    elsif shipping_method == "priority_shipping"
      order_items.map(&:priority_shipping).sum
    elsif shipping_method == "intl_shipping"
      order_items.map(&:intl_shipping).sum
    else
      0
    end
  end

  def weight_total
    # Divide by 10 because weight is calculated to one decimal place by EasyPost.
    ((order_items.map(&:weight).sum.to_f) / 10)
  end

  def calculate_taxes
    @taxes = taxable_subtotal_with_shipping * 0.06
  end

  def amt_from_percentage(oi_type, percentage, currency)
    formatted_oi_type = oi_type.split('_').map { |x| x.capitalize }.join.delete_suffix('s')
    self.order_items.where(orderable_type: formatted_oi_type).map { |x| x.total(currency) }.sum * (percentage.to_f / 100)
  end

  def percentage_off_total(percentage, currency)
    self.order_items.map { |x| x.total(currency) }.sum * (percentage.to_f / 100)
  end

  def total(*args)
    # subtotal + shipping_total
    if coupon_code.present?
      this_code = CouponCode.find_by(code: coupon_code)

      if this_code.flat_rate.present?
        total = subtotal_with_shipping(args[0]) - this_code.flat_rate
      end

      if this_code.percentage.present?
        this_scope = this_code.scope
        if this_scope == "order_total"
          subtotal = subtotal_with_shipping(args[0])
          total = subtotal - (subtotal * (this_code.percentage.to_f / 100))
        else
          total = subtotal_with_shipping(args[0]) - (amt_from_percentage(this_scope, this_code.percentage, args[0]))
        end
      end

      total
    else
      subtotal_with_shipping(args[0])
    end
  end

  # def cad_total
  #   total = order_items.map(&:cad_total).sum + order_items.map(&:intl_shipping).sum + order_items.map(&:intl_shipping_price).sum
  #   total.ceil
  # end

  def total_with_taxes(*args)
    if idaho_resident == true
      calculate_taxes
      ((total(args[0]) + @taxes).to_f).ceil
    else
      total(args[0]).ceil
    end
  end

  def refundable?
    stripe_id? && !refunded?
  end

  def refunded?
    status == "refunded"
  end

  def receipt
    @amount = ActionController::Base.helpers.number_to_currency(final_total / 100.0)

    line_items_array = []

    # Always include these items at the top:
    line_items_array << ["Status", status.capitalize]
    line_items_array << ["Date", readable_time(transaction_date).to_s]
    line_items_array << ["Account Billed", "#{ user.name } (#{ user.email })"]

    # Create Order Item string for the array below:
    order_items_string = ""
    order_items.each do |oi|
      qty_and_break = ', Qty: ' + oi.quantity.to_s + "\n"

      if oi.orderable_type == 'Backup'
        order_items_string += oi.oi_name.upcase + ' Backup for ' + OrderItem.find(oi.backup_for).orderable.title + ' Kit' + qty_and_break
      elsif (oi.orderable_type == 'Certificate') ||
            (oi.orderable_type == 'Card')
        order_items_string += oi.oi_name + " for " + oi.specifics + qty_and_break
      elsif (oi.orderable_type == 'Keychain')
        order_items_string += oi.oi_name + " " + oi.specifics + qty_and_break
      elsif (oi.orderable_type == 'Shirt')
        order_items_string += oi.oi_name + " " + oi.orderable_type + ", " + oi.specifics + qty_and_break
      elsif (oi.orderable_type == 'MachineTag') || (oi.orderable_type == 'Lanyard')
        order_items_string += oi.oi_name + qty_and_break
      elsif (oi.orderable_type == 'Poster') ||
            (oi.orderable_type == 'DigitalPoster') ||
            (oi.orderable_type == 'Button')
        order_items_string += oi.oi_name + ' (' + oi.orderable_type + ')' + qty_and_break
      elsif (oi.orderable_type == 'Sticker')
        order_items_string += oi.oi_name + qty_and_break
      else
        order_items_string += oi.oi_name + ' (' + oi.orderable_type + '/' + oi.orderable.language.capitalize + '/' + oi.orderable.standards.upcase + ')' + qty_and_break
      end
    end

    line_items_array << ["Order Item(s)", order_items_string]

    # Set Optional Line Items
    if coupon_code.present?
      line_items_array << ["Coupon Code",    "#{ coupon_code }"]
    end

    # Always include these items at the bottom:
    line_items_array << ["Amount", "#{ @amount } #{ currency.upcase }"]
    line_items_array << ["Charged to", "#{ card_brand } ending in #{ card_last4 }"]
    # line_items_array << ["Transaction ID", id]

    Receipts::Receipt.new(
      id: id,
      subheading: "RECEIPT FOR ORDER #%{id}",
      product: "Hard Hat Training",
      company: {
        name: "Safety Provisions, Inc.",
        address: "218 Dividend Dr, Suite 4, Rexburg, ID 83442",
        email: "office@hardhattraining.com",
        logo: Rails.root.join("app/assets/images/icon-clear.png") #
      },
      line_items: line_items_array
    )
  end

  def transaction_date
    paid_at.blank? ? created_at : paid_at
  end

  private

    def readable_time(transaction_date)
      transaction_date.in_time_zone('Mountain Time (US & Canada)').strftime("%a %b %e %Y, %l:%M %p MDT")
    end

    # def taxable?(state)
    #   # taxable_states = ['ID'] # Moved to application helper
    #   # chomp removes carriage returns and newlines from the end of lines, gsub removes any whitespace chars from the string.
    #   taxable_states.include?(state.upcase.chomp.gsub(/\s+/, ''))
    # end

    def subtotal_with_shipping(*args)
      subtotal(args[0]) + shipping_total
    end

    def taxable_subtotal_with_shipping
      # Exclude: OnlineCourses and GroupTrainings (we are providing a service), TrainTheTrainers (they pay for the service, and we provide the Kit for free), CreditPacks (there is no deliverable, physical or otherwise)
      taxable_items = order_items.where.not(orderable_type: 'OnlineCourse').where.not(orderable_type: 'TrainTheTrainer').where.not(orderable_type: 'GroupTraining').where.not(orderable_type: 'CreditPack')
      taxable_subtotal = taxable_items.map(&:total).sum

      taxable_subtotal + shipping_total
    end

    # def country_must_be_provided
    #   if to_country.blank?
    #     flash.alert = "You must select a State or Province."
    #     redirect_to cart_path
    #   end
    # end

    # def state_or_province_provided?
    #   to_state.present?
    # end

end
