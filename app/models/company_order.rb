class CompanyOrder < ApplicationRecord
  belongs_to :user
  belongs_to :company
  belongs_to :order

  validates_presence_of :user, :company, :order
end
