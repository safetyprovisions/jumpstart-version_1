class KeychainGroup < ApplicationRecord
  include KeychainGroupImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  has_many :keychains

  acts_as_taggable_on :tags

  validates_presence_of :name, :image, :description

  extend FriendlyId
  friendly_id :name, use: :slugged

  def active_keychain_count
    self.keychains.where(active: true).count
  end
end
