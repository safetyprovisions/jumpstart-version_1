class CreditPack < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  validate :some_info_is_provided?

  def some_info_is_provided?
    if (price == 0) && (cad_price == 0)
      errors.add(:price, "and cad_price cannot both be blank.")
    end
  end

  # VALIDATE USD
  validates :credits, numericality: { only_integer: true, greater_than: 0, message: '(USD) must be greater than 0 if Price (USD) is set.' }, if: :price_is_set?

  validates :price, numericality: { only_integer: true, greater_than: 0, message: '(USD) must be greater than 0 if Credits (USD) is set.' }, if: :credits_is_set?

  def price_is_set?
    price > 0
  end

  def credits_is_set?
    credits > 0
  end

  # VALIDATE CAD
  validates :cad_credits, numericality: { only_integer: true, greater_than: 0, message: '(CAD) must be greater than 0 if Price (CAD) is set.' }, if: :cad_price_is_set?

  validates :cad_price, numericality: { only_integer: true, greater_than: 0, message: 'must be greater than 0 if CAD Credits is set.' }, if: :cad_credits_is_set?

  def cad_price_is_set?
    cad_price > 0
  end

  def cad_credits_is_set?
    cad_credits > 0
  end

  # CAN'T SET BOTH PRICE AND CAD PRICE
  validates :price, numericality: { equal_to: 0, message: "(USD) must be 0 if Price (CAD) is set." }, if: :cad_price_is_set?
  validates :cad_price, numericality: { equal_to: 0, message: "must be 0 if Price (USD) is set." }, if: :price_is_set?

  # CAN'T SET BOTH CREDITS AND CAD CREDITS
  validates :credits, numericality: { equal_to: 0, message: "(USD) must be 0 if Credits (CAD) is set." }, if: :cad_credits_is_set?
  validates :cad_credits, numericality: { equal_to: 0, message: "must be 0 if Credits (USD) is set." }, if: :credits_is_set?

  has_many :order_items, as: :orderable

  def oi_name
    name
  end

  def shipping_price
    0
  end

  # def product_group
  #   "Bulk Credits"
  # end

  def language
    "N/A"
  end

  def standards
    "N/A"
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def weight
    0
  end

  def savings
    credits - price
  end

  def cad_savings
    cad_credits - cad_price
  end

  # def view_price
  #   if session[:preferred_currency] == 'usd'
  #     price
  #   else
  #     cad_price
  #   end
  # end

end
