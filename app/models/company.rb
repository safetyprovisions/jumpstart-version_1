class Company < ApplicationRecord
  include CompanyOnboarding

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable #, :validatable

  belongs_to :parent, class_name: "Company", optional: true

  has_one :payment_term

  has_many :non_catalog_course_accesses
  has_many :branches, class_name: "Company", foreign_key: :parent_id
  has_many :company_courses
  has_many :company_orders
  has_and_belongs_to_many :employees, -> { distinct }, class_name: "User", foreign_key: :company_id

  # Email is added automatically, so validations should not be necessary.
  # EMAIL_VALIDATOR = /[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/
  # validates :email, presence: true, format: { with: EMAIL_VALIDATOR, message: "must be a valid email format" }

  validates :name, presence: true, length: { minimum: 3, message: "must be at least 3 characters" }

  validates_presence_of :address, :city, :zip

  STATE_VALIDATOR = /([a-zA-Z]){2}/
  validates :state, presence: true, length: { is: 2, message: "must be two letters" }, format: { with: STATE_VALIDATOR, message: "must be two letters" }

  def full_address
    address + ' ' + city + ', ' + state + ' ' + zip
  end

  def invoicable?
    PaymentTerm.where(company: self.id).any?
  end

  def has_credits?
    credits > 0
  end

  def has_cad_credits?
    cad_credits > 0
  end

  def has_direct_purchases?
    company_courses.where('quantity > 0').any?
  end

  def admins
    CompanyAdmin.where(company_id: self).joins(:user)
  end
end
