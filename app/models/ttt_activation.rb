class TttActivation < ApplicationRecord
  belongs_to :user
  belongs_to :order
  belongs_to :train_the_trainer

  validates_presence_of :user, :order, :train_the_trainer
end
