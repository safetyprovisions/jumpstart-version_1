class ContactForm < MailForm::Base
  attribute :name ,      :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :phone,     :validate => /\A(\d{10})\z/i
  attribute :company,   :validate => true

  attribute :note #,   :validate => true
  attribute :nickname,  :captcha  => true
  attribute :training_type,   :validate => true
  attribute :equipment_type,   :validate => true
  attribute :number_of_operators,   :validate => true
  attribute :number_of_trainers,   :validate => true
  attribute :location,   :validate => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Onsite Training Request",
      :to => "office@hardhattraining.com",
      :from => %("#{ name }" <#{ email }>)
    }
  end
end
