class ButtonGroup < ApplicationRecord
  include ButtonGroupImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  has_many :buttons

  extend FriendlyId
  friendly_id :name, use: :slugged

  validates_presence_of :name, :description, :image, :keywords, :meta_description

  def active_button_count
    self.buttons.where(active: true).count
  end
end
