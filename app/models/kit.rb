class Kit < ApplicationRecord

  # monetize :price
  # monetize :sale_price, allow_nil: true
  # monetize :shipping_price

  belongs_to :product_group
  has_many :order_items, as: :orderable
  has_many :train_the_trainers, dependent: :destroy

  validates :price, numericality: { only_integer: true, greater_than: 0 }, presence: true

  validates :cad_price, numericality: { only_integer: true, greater_than: 0 }, presence: true

  validates_presence_of :market_type, :title, :download, :language, :standards, :product_group_id, :sample

  # Uncomment if we want to disallow duplicate records with the same market_type, language, and standards.
  # validates_uniqueness_of :market_type, scope: [:language, :standards]

  include KitSampleUploader::Attachment.new(:sample)

  # include KitUploader::Attachment.new(:download)
  # include KitUploader::Attachment.new(:additional_download)

  enum language:  [:english, :spanish, :tagalog]
  enum standards: [:osha, :csa_ansi, :n_a]

  # scope :is_osha_compliant,   ->{ where(standards: :osha) }

  def is_on_sale(pref_curr)
    if (sale_start.present? && sale_expiry.present?) && (Date.today > sale_start) && (Date.today < sale_expiry)
      true if (pref_curr == 'usd' && sale_price.present?) || (pref_curr == 'cad' && cad_sale_price.present?)
    end
  end

  def filename(data)
    if Rails.env.test?
      "filename"
    else
      data.split(',')[2].split('"').last
    end
  end

  def is_osha?
    standards == 'osha'
  end

  def oi_name
    title
  end

  # Kits don't get shipped. USBs do.
  def shipping_price
    0
  end

  # Kits don't weigh anything. USBs do.
  def weight
    0
  end

  # def view_price
  #   if session[:preferred_currency] == 'usd'
  #     price
  #   else
  #     cad_price
  #   end
  # end

  def current_price
    if !sale_price.blank? && !sale_start.blank? && !sale_expiry.blank?
      if Time.now.between?(sale_start, sale_expiry)
        sale_price
      else
        price
      end
    else
      price
    end
  end

  def cad_current_price
    if !cad_sale_price.blank? && !sale_start.blank? && !sale_expiry.blank?
      if Time.now.between?(sale_start, sale_expiry)
        cad_sale_price
      else
        cad_price
      end
    else
      cad_price
    end
  end

end
