class NonCatalogCourseAccess < ApplicationRecord
  belongs_to :online_course
  belongs_to :company

  validates_presence_of :online_course_id, :company_id
  validates_uniqueness_of :online_course_id, scope: :company_id
end
