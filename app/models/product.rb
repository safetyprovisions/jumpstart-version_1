# UNUSED

class Product < ApplicationRecord

  # monetize :price, as: "price_cents"
  # monetize :shipping_price, as: "shipping_price_cents", allow_nil: true

  belongs_to :product_group

  validates :cad_price, numericality: { only_integer: true, greater_than: 0 }
  validates :price, numericality: { only_integer: true, greater_than: 0 }

  validates_presence_of :name

  has_many :order_items, as: :orderable

  def oi_name
    name
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def language
    "None"
  end

  def standards
    "None"
  end

  def intl_shipping
    1500
  end

end
