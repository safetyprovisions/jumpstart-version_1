class Keychain < ApplicationRecord
  belongs_to :keychain_group

  has_many :order_items, as: :orderable
  # has_many :related_items, as: :relatable

  validates_presence_of :name, :price, :color, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :keychain_group_id

  def product_group
    keychain_group
  end

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    name
  end
end
