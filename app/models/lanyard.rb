class Lanyard < ApplicationRecord
  belongs_to :lanyard_group

  has_many :order_items, as: :orderable

  validates_presence_of :name, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :lanyard_group_id

  def product_group
    lanyard_group
  end

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    name
  end
end
