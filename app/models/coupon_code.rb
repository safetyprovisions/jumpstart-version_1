class CouponCode < ApplicationRecord
  before_validation :format_code

  enum application: [:kits, :online_courses, :order_total, :train_the_trainers]

  validates_presence_of :valid_from, :valid_to

  validates :min_purchase_amt, numericality: { only_integer: true, greater_than: 0, message: 'must be greater than 0 if scope is order total.' }, if: :scope_is_order_total?

  def scope_is_order_total?
    scope == 'order_total'
  end

  # validates_uniqueness_of :code
  validates :code, presence: true, uniqueness: true

  if (validates_presence_of :scope)
    validate :only_one?
  end

  def only_one?
    count = 0
    count += 1 if flat_rate.present?
    count += 1 if percentage.present?
    if count == 1
      true
    else
      errors.add(:percentage, "cannot be set at the same time as Flat Rate. Choose one.")
      false
    end
  end

  # Only validate if percentage is blank
  validates :flat_rate, presence: true, numericality: { only_integer: true, greater_than: 100, message: '(USD) must be greater than 100 (cents) if Percentage is blank.' }, if: :percentage_is_not_set?

  def percentage_is_not_set?
    percentage.blank?
  end

  # Only validate if flat_rate is blank
  validates :percentage, presence: true, numericality: { only_integer: true, greater_than: 0, message: '(USD) must be greater than 0 if Flat Rate is blank.' }, if: :flat_rate_is_not_set?

  def flat_rate_is_not_set?
    flat_rate.blank?
  end

  private
    def format_code
      if self.code.present?
        self.code = self.code.upcase
      end
    end
end
