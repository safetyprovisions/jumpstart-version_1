class DigitalPoster < ApplicationRecord
  belongs_to :poster

  has_many :order_items, as: :orderable

  def product_group
    poster.product_group
  end

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    poster.name
  end

  def shipping_price
    0
  end

  def weight
    0
  end
end
