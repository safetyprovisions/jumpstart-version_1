class Sticker < ApplicationRecord
  include StickerImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :order_items, as: :orderable

  validates_presence_of :name, :description, :image, :keywords, :meta_description, :price, :cad_price

  def product_group
    # machine_tag_group
    ""
  end

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    name
  end
end
