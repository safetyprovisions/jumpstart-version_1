class Certificate < ApplicationRecord
  include CertificateImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :order_items, as: :orderable

  validates_presence_of :name, :description, :image, :keywords, :meta_description, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping

  def product_group
    ""
  end

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    name
  end

  def weight
    10
  end
end
