class AltName < ApplicationRecord
  belongs_to :product_group

  validates :product_group_id, presence: true
  validates :name, presence: true, uniqueness: { scope: :product_group_id }
end
