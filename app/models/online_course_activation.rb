# Used to track WHO has been assigned WHICH ONLINE COURSE from WHICH ORDER.

class OnlineCourseActivation < ApplicationRecord
  belongs_to :user
  belongs_to :order
  belongs_to :online_course

  validates_presence_of :user, :order, :online_course
end
