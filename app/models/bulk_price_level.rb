class BulkPriceLevel < ApplicationRecord

  if (validates_presence_of :qty, :price, :cad_price, :tier)
    validate :only_four
  end
  validates_uniqueness_of :qty, scope: :tier
  validates_numericality_of :qty, only_integer: true, greater_than: 0, message: 'value must be greater than 0.'
  validates_numericality_of :price, :cad_price, only_integer: true, greater_than: 500, message: 'value must be greater than 50 cents.'

  # Can we also validate that each subsequent level has a higher qty and lower price than the previous?

  private

    def only_four
      if tier == '1'
        if BulkPriceLevel.where(tier: 1).count == 4
          errors.add(:tier, "1 cannot have more than 4 price levels.")
        end
      elsif tier == '2'
        if BulkPriceLevel.where(tier: 2).count == 4
          errors.add(:tier, "2 cannot have more than 4 price levels.")
        end
      end
    end
end
