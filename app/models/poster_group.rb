class PosterGroup < ApplicationRecord
  include PosterGroupImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :posters

  validates_presence_of :name, :description, :keywords, :meta_description, :image
end
