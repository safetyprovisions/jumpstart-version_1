class Button < ApplicationRecord
  belongs_to :button_group

  has_many :order_items, as: :orderable

  def product_group
    button_group
  end

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    name
  end
end
