class GroupTraining < ApplicationRecord
  belongs_to :online_course
  # belongs_to :product_group

  has_many :order_items, as: :orderable

  validates :online_course, presence: true, uniqueness: true



  def is_on_sale(pref_curr)
    if (online_course.sale_start.present? && online_course.sale_expiry.present?) && (Date.today > online_course.sale_start) && (Date.today < online_course.sale_expiry)
      true if (pref_curr == 'usd' && online_course.sale_price.present?) || (pref_curr == 'cad' && online_course.cad_sale_price.present?)
    end
  end

  def name
    online_course.title
  end

  def market_type
    online_course.market_type
  end

  def oi_name
    online_course.oi_name
  end

  def language
    online_course.language
  end

  def standards
    online_course.standards
  end

  def price
    online_course.price
  end

  def cad_price
    online_course.cad_price
  end

  def shipping_price
    online_course.shipping_price
  end

  def weight
    online_course.weight
  end

  def product_group
    online_course.product_group
  end

  def current_price
    online_course.price
  end

  def cad_current_price
    online_course.cad_price
  end

  # def view_price
  #   if session[:preferred_currency] == 'usd'
  #     price
  #   else
  #     cad_price
  #   end
  # end

end
