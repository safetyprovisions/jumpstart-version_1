# I think this just makes current_company available in Pundit policies.

class CompanyContext
  attr_reader :current_company, :branches

  def initialize(current_company, branches)
    @current_company = current_company
    @branches = current_company.branches
  end
end
