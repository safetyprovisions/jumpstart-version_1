module CompanyOnboarding
  extend ActiveSupport::Concern

  included do
    # Makes this helper available for use below.
    attr_reader :current_company
  end

  def should_see_onboarding?
    onboarding_completed_at == nil
  end

  def has_employee?
    employees.count >= 1
  end

  def has_admin?
    CompanyAdmin.where(company: self).any?
  end

  def has_order?
    CompanyOrder.where(company: self).any?
  end

  def onboarding_percent
    return 100 if onboarding_completed_at?

    steps = [:has_employee?, :has_admin?, :has_order?]
    complete = steps.select{ |step| send(step) }
    percent = complete.length / steps.length.to_f * 100

    update(onboarding_completed_at: Time.zone.now) if percent == 100
    percent
  end
end
