class MachineTag < ApplicationRecord
  belongs_to :machine_tag_group

  has_many :order_items, as: :orderable
  # has_many :related_items, as: :relatable

  def product_group
    machine_tag_group
  end

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    name
  end
end
