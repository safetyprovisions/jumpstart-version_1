class LanyardGroup < ApplicationRecord
  include LanyardGroupImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  has_many :lanyards

  extend FriendlyId
  friendly_id :name, use: :slugged

  # has_many :order_items, as: :orderable

  validates_presence_of :name, :description, :image, :image_data, :keywords, :meta_description

  def active_lanyard_count
    self.lanyards.where(active: true).count
  end
end
