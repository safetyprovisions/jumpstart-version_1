class MachineTagGroup < ApplicationRecord
  include MachineTagGroupImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  has_many :machine_tags

  extend FriendlyId
  friendly_id :name, use: :slugged

  validates_presence_of :name, :description, :image, :keywords, :meta_description

  def active_machine_tag_count
    self.machine_tags.where(active: true).count
  end
end
