# This tracks OnlineCourses and TrainTheTrainers and whether all purchased seats have been assigned, as well as which order they were purchased through.

class CompanyCourse < ApplicationRecord
  # Users who have been assigned the course:
  belongs_to :user

  belongs_to :online_course, optional: true
  belongs_to :train_the_trainer, optional: true
  belongs_to :order
  belongs_to :company

  validates_absence_of :train_the_trainer, if: :online_course_is_present?
  validates_absence_of :online_course, if: :train_the_trainer_is_present?

  validates_presence_of :train_the_trainer, if: :online_course_is_absent?
  validates_presence_of :online_course, if: :train_the_trainer_is_absent?

  # Quantity can't go below 0
  validates :quantity, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, presence: true
  validates_presence_of :user, :order, :currency, :company


  def online_course_is_present?
    !(online_course.blank?)
  end

  def train_the_trainer_is_present?
    !(train_the_trainer.blank?)
  end

  def online_course_is_absent?
    online_course.blank?
  end

  def train_the_trainer_is_absent?
    train_the_trainer.blank?
  end
end
