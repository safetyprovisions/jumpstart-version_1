class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, and :omniauthable
  devise :masqueradable, :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :omniauthable, :timeoutable, :lockable

  validates_presence_of [:first_name, :last_name, :email]
  # validate :password_complexity

  has_and_belongs_to_many :companies, optional: true, dependent: :nullify

  has_person_name

  has_many :notifications, foreign_key: :recipient_id
  has_many :orders
  has_many :services
  has_many :visits, class_name: "Ahoy::Visit"

  def admin?
    CompanyAdmin.where(user: self).any?
  end

  def admin
    CompanyAdmin.where(user: self).any?
  end

  # Save in case we need this later.
  # def admin_for_company?(company_id)
  #   CompanyAdmin.where(user: self, company: company_id).any?
  # end

  ####### BEGIN CSV IMPORTING METHODS #######

  def self.import(file, company)
    counter = 0

    CSV.foreach(file.path, headers: true, header_converters: :symbol) do |row|

      user = User.assign_from_row(row)

      company.employees << user

      if user.save
        counter += 1
      else
        puts "#{ user.email } - #{ user.errors.full_messages.join(",")}"
      end
    end

    counter
  end

  def self.assign_from_row(row)
    user = User.where(email: row[:email]).first_or_initialize

    user.assign_attributes row.to_hash.slice(:first_name, :last_name)

    user.update(
      password:              "HardHatTraining1",
      password_confirmation: "HardHatTraining1"
    )

    user
  end
  ####### END CSV IMPORTING METHODS #######

  private

    # def password_complexity
    #   return if password.blank?

      # if Rails.env.production?
        ## Original Method
        # password =~ /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,70}$/
        # errors.add :password, "Complexity requirement not met. Length should be 8-70 characters and include: 1 uppercase, 1 lowercase, 1 digit and 1 special character"
        ## My Method
        # password =~ /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,70}$/
        # errors.add :password, "Complexity requirement not met. Length should be 8-70 characters and include: 1 uppercase, 1 lowercase, and 1 digit"
      # end
    # end

end
