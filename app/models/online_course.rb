class OnlineCourse < ApplicationRecord
  # scope :active, -> { where(active: true) }

  # monetize :price
  # monetize :sale_price, allow_nil: true

  belongs_to :product_group
  has_many :non_catalog_course_accesses
  has_many :order_items, as: :orderable
  # has_many :train_the_trainers, dependent: :destroy
  has_many :group_trainings, dependent: :destroy

  validates_presence_of :title, :talent_course_id, :practical_evaluation, :exam, :answer_key, :product_group_id, :language, :standards, :cad_price, :price

  validates_uniqueness_of :title, scope: [:language, :standards, :market_type, :talent_course_id], message: "already exists with this combination of language, standards, market type, and Talent Course ID."

  validates :cad_price, numericality: { only_integer: true, greater_than: 0 }

  validates :price, numericality: { only_integer: true, greater_than: 0 }

  include PracticalEvaluationUploader::Attachment.new(:practical_evaluation)
  include ExamUploader::Attachment.new(:exam)
  include AnswerKeyUploader::Attachment.new(:answer_key)

  enum language:  [:english, :spanish, :tagalog]
  enum standards: [:osha, :csa_ansi, :n_a]

  def is_on_sale(pref_curr)
    if (sale_start.present? && sale_expiry.present?) && (Date.today > sale_start) && (Date.today < sale_expiry)
      true if (pref_curr == 'usd' && sale_price.present?) || (pref_curr == 'cad' && cad_sale_price.present?)
    end
  end

  def non_public?
    self.public == false
  end

  def oi_name
    title
  end

  def filename(data)
    data.split(',')[2].split('"').last
  end

  def current_price
    if !sale_price.blank? && !sale_start.blank? && !sale_expiry.blank?
      if Time.now.between?(sale_start, sale_expiry)
        sale_price
      else
        price
      end
    else
      price
    end
  end

  def cad_current_price
    if !cad_sale_price.blank? && !sale_start.blank? && !sale_expiry.blank?
      if Time.now.between?(sale_start, sale_expiry)
        cad_sale_price
      else
        cad_price
      end
    else
      cad_price
    end
  end

  def shipping_price
    0
  end

  def weight
    0
  end

  def product_group
    # if Rails.env.development? # When running seeds.rb
    #   ProductGroup.first
    # else
      ProductGroup.find(product_group_id)
    # end
  end

  # def view_price
  #   if session[:preferred_currency] == 'usd'
  #     price
  #   else
  #     cad_price
  #   end
  # end

end
