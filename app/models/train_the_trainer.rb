class TrainTheTrainer < ApplicationRecord
  belongs_to :kit
  # belongs_to :online_course
  belongs_to :product_group

  validates_presence_of :answer_key, :exam, :kit_id, :language, :market_type, :practical_evaluation, :product_group_id, :standards, :talent_course_id, :title
  validates_uniqueness_of :title, scope: [:language, :standards, :product_group_id, :talent_course_id, :kit_id], message: "already exists with this language, standards, product group, kit, and Talent course ID."

  validates :cad_price, numericality: { only_integer: true, greater_than: 0 }, presence: true
  validates :price, numericality: { only_integer: true, greater_than: 0 }, presence: true

  has_many :order_items, as: :orderable

  include TrainTheTrainerPracticalEvaluationUploader::Attachment.new(:practical_evaluation)
  include TrainTheTrainerExamUploader::Attachment.new(:exam)
  include TrainTheTrainerAnswerKeyUploader::Attachment.new(:answer_key)

  enum language:  [:english, :spanish, :tagalog]
  enum standards: [:osha, :csa_ansi, :n_a]

  def market_type
    kit.market_type
  end

  def oi_name
    title
  end

  def shipping_price
    0 # TrainTheTrainers don't cost, Backups do.
  end

  def weight
    0 # TrainTheTrainer don't have weight, Backups do.
  end

  def filename(data)
    data.split(',')[2].split('"').last
  end

  # def language
  #   kit.language
  # end

  # def standards
  #   kit.standards
  # end

  def current_price
    if !sale_price.blank? && !sale_start.blank? && !sale_expiry.blank?
      if Time.now.between?(sale_start, sale_expiry)
        sale_price
      else
        price
      end
    else
      price
    end
  end

  def cad_current_price
    if !cad_sale_price.blank? && !sale_start.blank? && !sale_expiry.blank?
      if Time.now.between?(sale_start, sale_expiry)
        cad_sale_price
      else
        cad_price
      end
    else
      cad_price
    end
  end

  # def view_price
  #   if session[:preferred_currency] == 'usd'
  #     price
  #   else
  #     cad_price
  #   end
  # end

  def is_on_sale(pref_curr)
    if (sale_start.present? && sale_expiry.present?) && (Date.today > sale_start) && (Date.today < sale_expiry)
      true if (pref_curr == 'usd' && sale_price.present?) || (pref_curr == 'cad' && cad_sale_price.present?)
    end
  end

end

