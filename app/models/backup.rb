class Backup < ApplicationRecord

  before_save :to_lowercase

  validates :format, uniqueness: true, presence: true
  validates_presence_of :cad_price, :intl_shipping, :priority_shipping, :shipping_price, :price, :format
  validates_numericality_of :cad_price, :intl_shipping, :priority_shipping, :shipping_price, :price, only_integer: true, greater_than: 500, message: 'value must be greater than 50 cents.'
  # validates :cad_price, numericality: { only_integer: true, greater_than: 0 }

  has_many :order_items, as: :orderable

  def oi_name
    format.upcase
  end

  def product_group
    "Backup"
  end

  def language
    "N/A"
  end

  def standards
    "N/A"
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def weight
    30 # 3 ounces
  end

  def to_lowercase
    self.format = self.format.downcase
  end

end
