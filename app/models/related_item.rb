class RelatedItem < ApplicationRecord
  belongs_to :relatable, polymorphic: true
  belongs_to :product_group

  # scope :active, -> { self.relatable.active? }
end
