class Announcement < ApplicationRecord
  TYPES = %w{ company company_setup new_user update user new_employee }

  after_initialize :set_defaults

  validates_presence_of :description
  validates_presence_of :name

  validates :announcement_type, presence: true, inclusion: { in: TYPES }

  def set_defaults
    self.published_at      ||= Time.zone.now
    self.announcement_type ||= TYPES.first
  end

end
