class CardGroup < ApplicationRecord
  include CardGroupImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  has_many :cards

  extend FriendlyId
  friendly_id :name, use: :slugged

  validates_presence_of :name, :description, :image, :keywords, :meta_description

  def active_card_count
    self.cards.where(active: true).count
  end
end
