class ProductGroup < ActiveRecord::Base # ApplicationRecord

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :alt_names
  has_many :kits
  has_many :online_courses
  has_many :related_items
  has_many :train_the_trainers

  acts_as_taggable_on :tags

  validates_presence_of :name, :image, :video, :overview, :training_standards, :facts, :fails

  # The old way
  include ProductGroupImageUploader::Attachment.new(:image) # adds an `image` virtual attribute
  # The new way
  # include ProductGroupImageUploader::Attachment(:image)

  def has_kits?
    self.kits.present?
  end

  def has_osha_kits?
    self.kits.map { |k| k.is_osha? }.any?
  end
end
