class OrderItem < ApplicationRecord

  belongs_to :order

  # Establishes the Orderable relationship
  belongs_to :orderable, polymorphic: true

  validates :quantity, numericality: { greater_than: 0 }, presence: true

  # after_save :destroy_if_zero
  # before_save :group_training_reminder

  def oi_name
    if orderable_type == 'Product'
      Product.find(orderable_id).name
    elsif orderable_type == 'Kit'
      Kit.find(orderable_id).title
    elsif orderable_type == 'OnlineCourse'
      OnlineCourse.find(orderable_id).title
    elsif orderable_type == 'TrainTheTrainer'
      TrainTheTrainer.find(orderable_id).title
    elsif orderable_type == 'GroupTraining'
      GroupTraining.find(orderable_id).name
    elsif orderable_type == 'Backup'
      Backup.find(orderable_id).format
    elsif orderable_type == 'CreditPack'
      CreditPack.find(orderable_id).name
    elsif orderable_type == 'Keychain'
      Keychain.find(orderable_id).name
    elsif orderable_type == 'MachineTag'
      MachineTag.find(orderable_id).name
    elsif orderable_type == 'Poster'
      Poster.find(orderable_id).name
    elsif orderable_type == 'DigitalPoster'
      DigitalPoster.find(orderable_id).poster.name
    elsif orderable_type == 'Certificate'
      Certificate.find(orderable_id).name
    elsif orderable_type == 'Card'
      Card.find(orderable_id).name
    elsif orderable_type == 'Shirt'
      Shirt.find(orderable_id).name
    elsif orderable_type == 'Hat'
      Hat.find(orderable_id).name
    elsif orderable_type == 'Lanyard'
      Lanyard.find(orderable_id).name
    elsif orderable_type == 'Button'
      Button.find(orderable_id).name
    elsif orderable_type == 'Sticker'
      Sticker.find(orderable_id).name
    else
      # Error handling for blank order_item_types
      'Other'
    end
  end

  def group_training_date
    GroupTraining.find(orderable_id).group_training_date
  end

  def total(*args)
    @pref_curr = args[0]
    @original_item = orderable_type.constantize.find(orderable_id)
    if orderable_type == 'GroupTraining'

      # Must match application_controller.rb#get_bulk_price_levels
        bulk_price_levels = BulkPriceLevel.all

        if @pref_curr == 'usd'
          tier_one_levels = bulk_price_levels.where(tier: '1').order(price: :desc)
          tier_two_levels = bulk_price_levels.where(tier: '2').order(price: :desc)
        else # 'cad'
          tier_one_levels = bulk_price_levels.where(tier: '1').order(cad_price: :desc)
          tier_two_levels = bulk_price_levels.where(tier: '2').order(cad_price: :desc)
        end

        @tier1_level1 = tier_one_levels.first
        @tier1_level2 = tier_one_levels.second
        @tier1_level3 = tier_one_levels.third
        @tier1_level4 = tier_one_levels.fourth

        @tier2_level1 = tier_two_levels.first
        @tier2_level2 = tier_two_levels.second
        @tier2_level3 = tier_two_levels.third
        @tier2_level4 = tier_two_levels.fourth
      # end

      if @pref_curr == 'usd'
        if @original_item.price == @tier1_level1.price # Tier 1 ($79)

          if ((@tier1_level1.qty)..(@tier1_level2.qty - 1)).include?(quantity)
            quantity * @tier1_level1.price
          elsif ((@tier1_level2.qty)..(@tier1_level3.qty - 1)).include?(quantity)
            quantity * @tier1_level2.price
          elsif ((@tier1_level3.qty)..(@tier1_level4.qty - 1)).include?(quantity)
            quantity * @tier1_level3.price
          else # if quantity >= @tier1_level4.qty
            quantity * @tier1_level4.price
          end

        else # Tier 2 ($29)

          if ((@tier2_level1.qty)..(@tier2_level2.qty - 1)).include?(quantity)
            quantity * @tier2_level1.price
          elsif ((@tier2_level2.qty)..(@tier2_level3.qty - 1)).include?(quantity)
            quantity * @tier2_level2.price
          elsif ((@tier2_level3.qty)..(@tier2_level4.qty - 1)).include?(quantity)
            quantity * @tier2_level3.price
          else # if quantity >= @tier2_level4.qty
            quantity * @tier2_level4.price
          end
        end
      else # @pref_curr == 'cad'
        if @original_item.cad_price == @tier1_level1.cad_price # Tier 1 ($99)

          if ((@tier1_level1.qty)..(@tier1_level2.qty - 1)).include?(quantity)
            quantity * @tier1_level1.cad_price
          elsif ((@tier1_level2.qty)..(@tier1_level3.qty - 1)).include?(quantity)
            quantity * @tier1_level2.cad_price
          elsif ((@tier1_level3.qty)..(@tier1_level4.qty - 1)).include?(quantity)
            quantity * @tier1_level3.cad_price
          else # if quantity >= @tier1_level4.qty
            quantity * @tier1_level4.cad_price
          end

        else # Tier 2 ($39)

          if ((@tier2_level1.qty)..(@tier2_level2.qty - 1)).include?(quantity)
            quantity * @tier2_level1.cad_price
          elsif ((@tier2_level2.qty)..(@tier2_level3.qty - 1)).include?(quantity)
            quantity * @tier2_level2.cad_price
          elsif ((@tier2_level3.qty)..(@tier2_level4.qty - 1)).include?(quantity)
            quantity * @tier2_level3.cad_price
          else # if quantity >= @tier2_level4.qty
            quantity * @tier2_level4.cad_price
          end
        end
      end

    else
      if @pref_curr == 'usd'
        quantity * @original_item.current_price
      else
        quantity * @original_item.cad_current_price
      end
    end
  end

  # def cad_total
  #   @original_item = orderable_type.constantize.find(orderable_id)

  #   if orderable_type == 'GroupTraining'

  #     @online_course = OnlineCourse.find(@original_item.online_course_id)

  #     if @online_course.cad_price == 9900

  #       if (25..49).include?(quantity)
  #         quantity * 9500
  #       elsif (50..99).include?(quantity)
  #         quantity * 8500
  #       elsif quantity > 99
  #         quantity * 6500
  #       else # ( ie, if (1..24).include?(quantity) )
  #         quantity * 9900
  #       end

  #     else # $39 courses

  #       if (25..49).include?(quantity)
  #         quantity * 3500
  #       elsif (50..99).include?(quantity)
  #         quantity * 3000
  #       elsif quantity > 99
  #         quantity * 2500
  #       else # ( ie, if (1..24).include?(quantity) )
  #         quantity * 3900
  #       end

  #     end

  #   else
  #     quantity * @original_item.cad_current_price
  #   end
  # end

  def shipping_price
    quantity * orderable_type.constantize.find(orderable_id).shipping_price
  end

  def priority_shipping
    # This doesn't account for all shippables!
    # (orderable_type == 'Backup') ? (quantity * orderable_type.constantize.find(orderable_id).priority_shipping) : 0

    shippable_types = ["Backup", "Keychain", "MachineTag", "Poster", "Certificate", "Card", "Shirt", "Hat", "Lanyard", "Button", "Sticker"]
    if shippable_types.include?(orderable_type)
      amt = quantity * orderable_type.constantize.find(orderable_id).priority_shipping
      # Not sure why I had this…
      # if order.currency == 'cad'
      #   amt += (quantity * 500)
      # end
    else
      amt = 0
    end

    amt
  end

  def intl_shipping
    # This doesn't account for all shippables!
    # if orderable_type == 'Backup'

    shippable_types = ["Backup", "Keychain", "MachineTag", "Poster", "Certificate", "Card", "Shirt", "Hat", "Lanyard", "Button", "Sticker"]
    if shippable_types.include?(orderable_type)
      amt = quantity * orderable_type.constantize.find(orderable_id).intl_shipping
      # Not sure why I had this…
      # if order.currency == 'cad'
      #   amt += (quantity * 500)
      # end
    else
      amt = 0
    end

    amt
  end

  def intl_shipping_price
    if (orderable_type == 'Backup') || (orderable_type == 'Product')
      amt = quantity * orderable_type.constantize.find(orderable_id).intl_shipping
      if order.currency == 'cad'
        amt += (quantity * 500)
      end
    else
      amt = 0
    end

    amt
  end

  def weight
    quantity * orderable.weight
  end

  def existing_backup
    OrderItem.find_by(backup_for: id)
  end

  private

    # This is being handled in OrderItemsController#update
    # def destroy_if_zero
    #   destroy if quantity.zero?

    #   if existing_backup
    #     existing_backup.destroy
    #   end
    # end

    # def group_training_reminder
    #   if (orderable_type == 'GroupTraining') && (quantity == 1)
    #     session[:group_training_reminder] = true
    #   end
    # end

end
