class Hat < ApplicationRecord
  include HatImageUploader::Attachment.new(:image) # adds an `image` virtual attribute

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :order_items, as: :orderable

  validates_presence_of :name, :description, :image, :keywords, :meta_description, :fastener, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :weight, :size

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    name
  end
end
