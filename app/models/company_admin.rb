class CompanyAdmin < ApplicationRecord
  belongs_to :user
  belongs_to :company

  validates :company, uniqueness: { scope: :user }, presence: true
  validates_presence_of :user
end
