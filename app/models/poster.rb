class Poster < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  include PosterImageUploader::Attachment.new(:image) # adds an `image` virtual attribute
  include PosterDownloadUploader::Attachment.new(:download) # adds an `image` virtual attribute

  belongs_to :poster_group

  has_one :digital_poster

  has_many :order_items,   as: :orderable
  # has_many :related_items, as: :relatable

  validates_presence_of :name, :image, :poster_group_id, :description, :download, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping

  def product_group
    poster_group
  end

  def language
    ""
  end

  def standards
    ""
  end

  def current_price
    price
  end

  def cad_current_price
    cad_price
  end

  def oi_name
    name
  end

  def weight
    # Ie, one lb (16 oz. x 10). This tells the shipping label generator not to make a label since it's 1 lb or more.
    160
  end
end
