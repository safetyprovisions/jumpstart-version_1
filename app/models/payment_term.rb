class PaymentTerm < ApplicationRecord
  belongs_to :company

  # enum term:  [:thirty, :forty_five, :sixty]

  validates_presence_of :term
  validates_numericality_of :term

  validates :company, presence: true, uniqueness: true

end
