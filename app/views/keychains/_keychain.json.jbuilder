json.extract! keychain, :id, :name, :price, :color, :cad_price, :shipping_price, :shipping_price, :priority_shipping, :intl_shipping, :keychain_group_id, :created_at, :updated_at
json.url keychain_url(keychain, format: :json)
