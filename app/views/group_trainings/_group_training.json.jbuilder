json.extract! group_training, :id, :online_course_id, :created_at, :updated_at
json.url group_training_url(group_training, format: :json)
