json.extract! coupon_code, :id, :code, :valid_from, :valid_to, :flat_rate, :percentage, :created_at, :updated_at
json.url coupon_code_url(coupon_code, format: :json)
