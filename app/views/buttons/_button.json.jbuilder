json.extract! button, :id, :name, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :button_group_id, :weight, :active, :created_at, :updated_at
json.url button_url(button, format: :json)
