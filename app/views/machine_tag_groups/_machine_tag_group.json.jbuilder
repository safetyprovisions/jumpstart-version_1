json.extract! machine_tag_group, :id, :name, :description, :image_data, :image, :slug, :keywords, :meta_description, :created_at, :updated_at
json.url machine_tag_group_url(machine_tag_group, format: :json)
