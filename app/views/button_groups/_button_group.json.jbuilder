json.extract! button_group, :id, :name, :description, :image_data, :slug, :keywords, :meta_description, :active, :created_at, :updated_at
json.url button_group_url(button_group, format: :json)
