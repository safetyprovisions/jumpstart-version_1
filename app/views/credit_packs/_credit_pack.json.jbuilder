json.extract! credit_pack, :id, :name, :price, :credits, :created_at, :updated_at
json.url credit_pack_url(credit_pack, format: :json)
