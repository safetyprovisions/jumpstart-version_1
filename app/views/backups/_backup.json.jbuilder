json.extract! backup, :id, :format, :price, :shipping_price, :priority_shipping, :intl_shipping, :created_at, :updated_at
json.url backup_url(backup, format: :json)
