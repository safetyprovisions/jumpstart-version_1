json.extract! kit, :id, :type, :title, :image_data, :download_data, :description, :price, :sale_price, :sale_expiry, :language, :standards, :product_group_id, :sample_data, :created_at, :updated_at
json.url kit_url(kit, format: :json)
