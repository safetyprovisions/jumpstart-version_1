json.extract! lanyard_group, :id, :name, :description, :image_data, :slug, :keywords, :meta_description, :active, :created_at, :updated_at
json.url lanyard_group_url(lanyard_group, format: :json)
