json.extract! keychain_group, :id, :name, :description, :image_data, :created_at, :updated_at
json.url keychain_group_url(keychain_group, format: :json)
