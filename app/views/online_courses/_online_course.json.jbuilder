json.extract! online_course, :id, :title, :image_data, :description, :price, :sale_price, :sale_start, :sale_expiry, :language, :standards, :product_group_id, :video, :talent_course_id, :practical_evaluation_data, :created_at, :updated_at
json.url online_course_url(online_course, format: :json)
