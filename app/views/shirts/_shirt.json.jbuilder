json.extract! shirt, :id, :name, :description, :image_data, :slug, :keywords, :meta_description, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :weight, :created_at, :updated_at
json.url shirt_url(shirt, format: :json)
