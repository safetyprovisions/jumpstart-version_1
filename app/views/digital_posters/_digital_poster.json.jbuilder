json.extract! digital_poster, :id, :price, :cad_price, :poster_id, :created_at, :updated_at
json.url digital_poster_url(digital_poster, format: :json)
