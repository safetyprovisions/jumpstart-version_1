json.extract! train_the_trainer, :id, :title, :price, :sale_price, :sale_start, :sale_expiry, :language, :standards, :product_group_id, :talent_course_id, :practical_evaluation_data, :exam_data, :answer_key_data, :market_type, :kit_id, :created_at, :updated_at
json.url train_the_trainer_url(train_the_trainer, format: :json)
