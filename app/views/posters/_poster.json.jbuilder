json.extract! poster, :id, :name, :image_data, :poster_group_id, :format, :description, :download_data, :display_format, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :weight, :created_at, :updated_at
json.url poster_url(poster, format: :json)
