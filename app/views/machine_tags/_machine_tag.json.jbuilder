json.extract! machine_tag, :id, :name, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :machine_tag_group_id, :weight, :created_at, :updated_at
json.url machine_tag_url(machine_tag, format: :json)
