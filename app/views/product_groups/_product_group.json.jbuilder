json.extract! product_group, :id, :name, :image_data, :created_at, :updated_at
json.url product_group_url(product_group, format: :json)
