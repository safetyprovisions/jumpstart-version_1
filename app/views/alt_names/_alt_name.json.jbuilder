json.extract! alt_name, :id, :name, :product_group_id, :created_at, :updated_at
json.url alt_name_url(alt_name, format: :json)
