json.extract! certificate, :id, :name, :description, :image_data, :slug, :keywords, :meta_description, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :created_at, :updated_at
json.url certificate_url(certificate, format: :json)
