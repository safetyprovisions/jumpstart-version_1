json.extract! card, :id, :name, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :card_group_id, :weight, :created_at, :updated_at
json.url card_url(card, format: :json)
