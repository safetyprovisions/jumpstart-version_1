json.extract! related_item, :id, :relatable_id, :product_group_id, :created_at, :updated_at
json.url related_item_url(related_item, format: :json)
