json.extract! poster_group, :id, :name, :description, :slug, :keywords, :meta_description, :created_at, :updated_at
json.url poster_group_url(poster_group, format: :json)
