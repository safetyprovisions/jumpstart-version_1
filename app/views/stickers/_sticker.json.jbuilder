json.extract! sticker, :id, :name, :description, :image_data, :slug, :keywords, :meta_description, :active, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :weight, :created_at, :updated_at
json.url sticker_url(sticker, format: :json)
