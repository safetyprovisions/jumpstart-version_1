json.extract! card_group, :id, :name, :description, :image_data, :slug, :keywords, :meta_description, :created_at, :updated_at
json.url card_group_url(card_group, format: :json)
