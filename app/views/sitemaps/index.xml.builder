xml.instruct! :xml, version: '1.0', encoding: "UTF-8"
xml.tag! 'urlset', 'xmlns' => "http://www.sitemaps.org/schemas/sitemap/0.9" do

  xml.tag! 'url' do
    xml.tag! 'loc', root_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', about_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', contact_us_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', documentation_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', faq_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', onsite_request_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', privacy_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', terms_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', training_topics_url
  end

  xml.tag! 'url' do
    xml.tag! 'loc', training_types_url
  end

  @button_groups.each do |button_group|
    xml.tag! 'url' do
      xml.tag! 'loc', button_group_url(button_group)
      xml.lastmod button_group.updated_at.strftime("%F")
    end
  end

  @card_groups.each do |card_group|
    xml.tag! 'url' do
      xml.tag! 'loc', card_group_url(card_group)
      xml.lastmod card_group.updated_at.strftime("%F")
    end
  end

  @certificates.each do |certificate|
    xml.tag! 'url' do
      xml.tag! 'loc', certificate_url(certificate)
      xml.lastmod certificate.updated_at.strftime("%F")
    end
  end

  @hats.each do |hat|
    xml.tag! 'url' do
      xml.tag! 'loc', hat_url(hat)
      xml.lastmod hat.updated_at.strftime("%F")
    end
  end

  @keychain_groups.each do |keychain_group|
    xml.tag! 'url' do
      xml.tag! 'loc', keychain_group_url(keychain_group)
      xml.lastmod keychain_group.updated_at.strftime("%F")
    end
  end

  @lanyard_groups.each do |lanyard_group|
    xml.tag! 'url' do
      xml.tag! 'loc', lanyard_group_url(lanyard_group)
      xml.lastmod lanyard_group.updated_at.strftime("%F")
    end
  end

  @machine_tag_groups.each do |machine_tag_group|
    xml.tag! 'url' do
      xml.tag! 'loc', machine_tag_group_url(machine_tag_group)
      xml.lastmod machine_tag_group.updated_at.strftime("%F")
    end
  end

  @poster_groups.each do |poster_group|
    xml.tag! 'url' do
      xml.tag! 'loc', poster_group_url(poster_group)
      xml.lastmod poster_group.updated_at.strftime("%F")
    end
  end

  @posters.each do |poster|
    xml.tag! 'url' do
      xml.tag! 'loc', poster_url(poster)
      xml.lastmod poster.updated_at.strftime("%F")
    end
  end

  @product_groups.each do |product_group|
    xml.tag! 'url' do
      xml.tag! 'loc', product_group_url(product_group)
      xml.lastmod product_group.updated_at.strftime("%F")
    end
  end

  @shirts.each do |shirt|
    xml.tag! 'url' do
      xml.tag! 'loc', shirt_url(shirt)
      xml.lastmod shirt.updated_at.strftime("%F")
    end
  end

  @stickers.each do |sticker|
    xml.tag! 'url' do
      xml.tag! 'loc', sticker_url(sticker)
      xml.lastmod sticker.updated_at.strftime("%F")
    end
  end

end
