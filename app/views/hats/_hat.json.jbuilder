json.extract! hat, :id, :name, :description, :image_data, :slug, :keywords, :meta_description, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :weight, :size, :style, :structure, :fastener, :created_at, :updated_at
json.url hat_url(hat, format: :json)
