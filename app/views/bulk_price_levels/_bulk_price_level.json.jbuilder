json.extract! bulk_price_level, :id, :qty, :price, :cad_price, :tier, :created_at, :updated_at
json.url bulk_price_level_url(bulk_price_level, format: :json)
