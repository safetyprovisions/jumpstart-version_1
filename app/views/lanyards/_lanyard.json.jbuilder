json.extract! lanyard, :id, :name, :price, :cad_price, :shipping_price, :priority_shipping, :intl_shipping, :lanyard_group_id, :weight, :active, :created_at, :updated_at
json.url lanyard_url(lanyard, format: :json)
