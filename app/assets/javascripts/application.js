// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require popper
//= require bootstrap
//= require data-confirm-modal
//= require local-time
//= require activestorage
//= require turbolinks
//= require trix
//= require chartkick
//= require Chart.bundle
//= require_tree .

// Copy Contents To Clipboard
$(document).ready(function(){
  $("a[name=copy_pre]").click(function() {
    var id = $(this).attr('id');
    var el = document.getElementById(id);
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
    document.execCommand('copy');
    // alert("Contents copied to clipboard.");
    window.open("https://ship.pirateship.com/ship/single");
    return false;
  });
});

// Hide Spinner when Turbolinks loads
$(document).on("turbolinks:load", function(){
  $(".spinner").hide();
});

// Show Spinner when button is clicked; remove it if on the Checkout page and an error msg is displayed after a 1 sec wait.
$(document).on('click','.btn-spinner',async function(){
  var error_block = document.getElementById("card-errors");
  var upload_field = document.getElementById("image-file-upload");

  $(".spinner").show();

  // Define sleep()
  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  await sleep(1000);

  // If on Checkout page (the only place error_block shows up), AND error_block is not empty after 1 second, hide the spinner.
  if ((error_block != null) && (error_block.innerHTML != "")) {
    $(".spinner").hide();
  }

  // If on /new page still, remove spinner.
  if (upload_field != null) {
    $(".spinner").hide();
  }

});
