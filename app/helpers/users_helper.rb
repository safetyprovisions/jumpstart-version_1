module UsersHelper

  def admin_btn(title, url)
      button_to title, url, class: "btn btn-link"
  end
end