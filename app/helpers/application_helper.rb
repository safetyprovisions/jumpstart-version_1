module ApplicationHelper
  include Pagy::Frontend

  def standards_styling(standards)
    standards.gsub('_', '/').upcase
  end

  def remove_underscores(string)
    string.gsub('_', ' ')
  end

  def onboarding_step_icon(step_completed)
    color = step_completed ? "text-success" : "text-muted"
    content_tag :i, nil, class: ["fas", "fa-check", color]
  end

  def bulk_prices
    bulk_price_levels = BulkPriceLevel.all

    if session[:preferred_currency] == 'usd'
      tier_one_levels = bulk_price_levels.where(tier: '1').order(price: :desc)
      tier_two_levels = bulk_price_levels.where(tier: '2').order(price: :desc)
    else # 'cad'
      tier_one_levels = bulk_price_levels.where(tier: '1').order(cad_price: :desc)
      tier_two_levels = bulk_price_levels.where(tier: '2').order(cad_price: :desc)
    end

    @tier1_level1 = tier_one_levels.first
    @tier1_level2 = tier_one_levels.second
    @tier1_level3 = tier_one_levels.third
    @tier1_level4 = tier_one_levels.fourth

    @tier2_level1 = tier_two_levels.first
    @tier2_level2 = tier_two_levels.second
    @tier2_level3 = tier_two_levels.third
    @tier2_level4 = tier_two_levels.fourth
  end

  def bootstrap_class_for(flash_type)
    {
      success: "alert-success",
      error: "alert-danger",
      alert: "alert-warning",
      notice: "alert-info"
    }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end

  def usd_is_set?
    session[:preferred_currency] == 'usd'
  end

  def cad_is_set?
    session[:preferred_currency] == 'cad'
  end

  def kebabify(str)
    regex = /\W/
    str = str.gsub(regex, ' ')
    str.split.map(&:downcase).join('-')
  end

  def taxable_states
    ['ID']
  end

  def market_options
    [
      ["USA", "usa"],
      ["Canada", "canada"],
      ["Spanish", "spanish"],
      ["Tagalog", "tagalog"]
    ]
  end

  def flag_helper(flag_type, course)
    image_tag "flags/#{ flag_type }.png",
    alt: "#{ flag_type }",
    class: "#{ (course.kits.exists?(market_type: flag_type) || (course.online_courses.exists?(market_type: flag_type))) ? '' : 'flag-inactive' }"
  end

  def readable_time(transaction_date)
    transaction_date.in_time_zone('Mountain Time (US & Canada)').strftime("%a %b %e %Y, %l:%M %p MDT")
  end

  def select_states_and_provinces
    [
      ["Select…", ""],
      # ["", ""],
      # ["--UNITED STATES--", ""],
      ["Alabama", "AL"],
      ["Alaska", "AK"],
      ["Arizona", "AZ"],
      ["Arkansas", "AR"],
      ["California", "CA"],
      ["Colorado", "CO"],
      ["Connecticut", "CT"],
      ["Delaware", "DE"],
      ["Florida", "FL"],
      ["Georgia", "GA"],
      ["Hawaii", "HI"],
      ["Idaho", "ID"],
      ["Illinois", "IL"],
      ["Indiana", "IN"],
      ["Iowa", "IA"],
      ["Kansas", "KS"],
      ["Kentucky", "KY"],
      ["Louisiana", "LA"],
      ["Maine", "ME"],
      ["Maryland", "MD"],
      ["Massachusetts", "MA"],
      ["Michigan", "MI"],
      ["Minnesota", "MN"],
      ["Mississippi", "MS"],
      ["Missouri", "MO"],
      ["Montana", "MT"],
      ["Nebraska", "NE"],
      ["Nevada", "NV"],
      ["New Hampshire", "NH"],
      ["New Jersey", "NJ"],
      ["New Mexico", "NM"],
      ["New York", "NY"],
      ["North Carolina", "NC"],
      ["North Dakota", "ND"],
      ["Ohio", "OH"],
      ["Oklahoma", "OK"],
      ["Oregon", "OR"],
      ["Pennsylvania", "PA"],
      ["Rhode Island", "RI"],
      ["South Carolina", "SC"],
      ["Suth Dakota", "SD"],
      ["Tennessee", "TN"],
      ["Texas", "TX"],
      ["Utah", "UT"],
      ["Vermont", "VT"],
      ["Virginia", "VA"],
      ["Washington", "WA"],
      ["West Virginia", "WV"],
      ["Wisconsin", "WI"],
      ["Wyoming", "WY"],

      # ["", ""],
      # ["--CANADA--", ""],
      ["Alberta", "AB"],
      ["British Columbia", "BC"],
      ["Manitoba", "MB"],
      ["New Brunswick", "NB"],
      ["Newfoundland & Labrador", "NL"],
      ["Nova Scotia", "NS"],
      ["Northwest Territories", "NT"],
      ["Nunavut", "NU"],
      ["Ontario", "ON"],
      ["Prince Edward Island", "PE"],
      ["Quebec", "QC"],
      ["Saskatchewan", "SK"],
      ["Yukon Territory", "YT"]
    ]
  end

  ######## BEGIN TALENT API METHODS ########

  ### BRANCHES

  def talent_branches # Currently Unused
    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/branches")

    make_secure_connection(url)
    @request = Net::HTTP::Get.new(url)
    make_http_request(url)
    parse_response
  end

  ### USERS

  # NOTE This is currently hardcoded for a test email!
  #      It must be able to accept at least one param (email/id/username)!
  #      See Note 1, below.
  def talent_user # Currently Unused
    require_dependencies

    # url = URI("https://safetyclasses.talentlms.com/api/v1/users/email:#{ email }")
    url = URI("https://safetyclasses.talentlms.com/api/v1/users/email:atest@hardhatraining.com")

    make_secure_connection(url)
    @request = Net::HTTP::Get.new(url)
    make_http_request(url)
    parse_response
  end

  def talent_users # Currently Unused
    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/users")

    make_secure_connection(url)
    @request = Net::HTTP::Get.new(url)
    make_http_request(url)
    parse_response
  end

  ### COURSES

  def talent_courses # Currently Unused
    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/courses")

    make_secure_connection(url)
    @request = Net::HTTP::Get.new(url)
    make_http_request(url)
    parse_response
  end

  def talent_new_course # Currently Unused, Needs Refactoring
    require_dependencies

    url = URI("https://safetyclasses.talentlms.com/api/v1/createcourse")

    make_secure_connection(url)

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
    request["Authorization"] = 'Basic SURzMjlTS2NZWEthWFhoY1dDakZYSWF0UHpKOWozOg=='
    request["cache-control"] = 'no-cache'
    request["Postman-Token"] = '56207f21-fbb9-4e34-8037-5a1a1f08a89d'
    request.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"name\"\r\n\r\ncourse name\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"description\"\r\n\r\ncourse description\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"code\"\r\n\r\ncourse code\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"price\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"time_limit\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"category_id\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"creator_id\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"custom_field_1\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"

    response = http.request(request)
    puts response.read_body
  end

  ######## END TALENT API METHODS ########

  # Meta tags helper methods
  def meta_title(title)
    content_for(:title, title)
  end

  def meta_description(description)
    content_for(:meta_description, description)
  end

  def meta_keywords(keywords)
    content_for(:meta_keywords, keywords)
  end

  def meta_author(author)
    content_for(:meta_author, author)
  end

  def generate_chars(number)
    charset = Array(0..9) + Array("a".."z")
    Array.new(number) { charset.sample }.join
  end

  private

    # def require_dependencies
    #   require 'net/http'
    #   require 'json'
    # end

    # def generate_chars(number)
    #   charset = Array(0..9) + Array("a".."z")
    #   Array.new(number) { charset.sample }.join
    # end

    # def generate_postman_token
    #   generate_chars(8) + '-' +
    #   generate_chars(4) + '-' +
    #   generate_chars(4) + '-' +
    #   generate_chars(4) + '-' +
    #   generate_chars(12)
    # end

    # def make_secure_connection(url)
    #   @http = Net::HTTP.new(url.host, url.port)
    #   @http.use_ssl = true
    # end

    # def make_http_request(url)
    #   @request["Authorization"] = 'Basic NUFWWjF3MUFraHBBTFNkbmx6WE1OSDF5bmZ0NXExOg=='
    #   @request["cache-control"] = 'no-cache'
    #   @request["Postman-Token"] = generate_postman_token
    # end

    # def parse_response
    #   response = @http.request(@request)
    #   parsed = JSON.parse(response.read_body)

    #   parsed
    # end

end


# NOTES

# "A. Test"
# user_id: 55,
# email: atest@hardhatraining.com

# 1.
# Use the ** syntax to get keyword arguments by themselves:

# def dual_argument_capturing_method(*args, **keyword_args)
#   {args: args, keyword_args: keyword_args}
# end

# dual_argument_capturing_method(1, 2, 3, key: "value")
# => {:args=>[1, 2, 3], :keyword_args=>{:key=>"value"}}

# 2.
# We can make a switcher, and provide a method key.
# @request = Net::HTTP::Get.new(url)
# @request = Net::HTTP::Post.new(url)
