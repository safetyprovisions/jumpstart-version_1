module CompaniesHelper

  def company_btn(title, url)
    button_to title, url, class: "btn btn-link"
  end
end
