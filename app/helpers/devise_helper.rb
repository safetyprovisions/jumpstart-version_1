module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    # Remove password errors, since it will be set automatically
    resource.errors[:password].pop(resource.errors[:password].length)

    messages = resource.errors.full_messages.map { |msg| content_tag(:div, msg) }.join
    html = <<-HTML
    <div class="alert alert-danger">
      #{messages}
    </div>
    HTML

    html.html_safe
  end
end
