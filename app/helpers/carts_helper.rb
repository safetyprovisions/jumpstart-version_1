module CartsHelper
  # Identify the specific_order_item
  def oi_identification(order_item)
    @this_oi = order_item.orderable_type.constantize.find(order_item.orderable_id)
  end

  # Specify where the href links to on the Cart page
  def oi_link_location(order_item)
    non_group_items = %W[ Certificate Poster Shirt Hat Sticker ]

    if order_item.orderable_type == "CreditPack"
      @link_location = credit_packs_path
    # elsif order_item.orderable_type == "Certificate"
    elsif non_group_items.include?(order_item.orderable_type)
      @link_location = @this_oi
    else
      @link_location = @this_oi.product_group
    end
  end

end
