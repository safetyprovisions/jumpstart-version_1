require "administrate/base_dashboard"

class OrderDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    # ahoy_visit: Field::BelongsTo.with_options(class_name: "Ahoy::Visit"),
    user: Field::BelongsTo,
    order_items: Field::HasMany,
    id: Field::Number,
    final_total: Field::Number,
    currency: Field::String,
    status: Field::String,
    stripe_id: Field::String,
    card_brand: Field::String,
    card_last4: Field::String,
    card_exp_month: Field::String,
    card_exp_year: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    paid_at: Field::DateTime,
    shipment_id: Field::String,
    to_name: Field::String,
    to_street1: Field::String,
    to_city: Field::String,
    to_state: Field::String,
    to_zip: Field::String,
    to_country: Field::String,
    to_email: Field::String,
    download_start_time: Field::DateTime,
    additional_info: Field::Text,
    shipping_method: Field::Text,
    order_placed_at: Field::DateTime,
    group_training_date: Field::DateTime,
    label_created: Field::DateTime,
    label_url: Field::String,
    tracking_code: Field::String,
    idaho_resident: Field::Boolean,
    # ahoy_visit_id: Field::Number,
    to_street2: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    # :ahoy_visit,
    :user,
    :order_items,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    # :ahoy_visit,
    :user,
    :order_items,
    :id,
    :final_total,
    :currency,
    :status,
    :stripe_id,
    :card_brand,
    :card_last4,
    :card_exp_month,
    :card_exp_year,
    :created_at,
    :updated_at,
    :paid_at,
    :shipment_id,
    :to_name,
    :to_street1,
    :to_city,
    :to_state,
    :to_zip,
    :to_country,
    :to_email,
    :download_start_time,
    :additional_info,
    :shipping_method,
    :order_placed_at,
    :group_training_date,
    :label_created,
    :label_url,
    :tracking_code,
    :idaho_resident,
    # :ahoy_visit_id,
    :to_street2,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    # :ahoy_visit,
    :user,
    :order_items,
    :status,
    :stripe_id,
    :card_brand,
    :card_last4,
    :card_exp_month,
    :card_exp_year,
    :paid_at,
    :shipment_id,
    :to_name,
    :to_street1,
    :to_city,
    :to_state,
    :to_zip,
    :to_country,
    :to_email,
    :download_start_time,
    :additional_info,
    :shipping_method,
    :order_placed_at,
    :group_training_date,
    :label_created,
    :label_url,
    :tracking_code,
    :idaho_resident,
    :currency,
    # :ahoy_visit_id,
    :to_street2,
  ].freeze

  # Overwrite this method to customize how orders are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(order)
  #   "Order ##{order.id}"
  # end
end
