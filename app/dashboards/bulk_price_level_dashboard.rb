require "administrate/base_dashboard"

class BulkPriceLevelDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    qty: Field::Number,
    price: Field::Number,
    cad_price: Field::Number,
    tier: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :qty,
    :price,
    :cad_price,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :qty,
    :price,
    :cad_price,
    :tier,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :qty,
    :price,
    :cad_price,
    :tier,
  ].freeze

  # Overwrite this method to customize how bulk price levels are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(bulk_price_level)
  #   "BulkPriceLevel ##{bulk_price_level.id}"
  # end
end
