require "administrate/base_dashboard"

class BackupDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    order_items: Field::HasMany,
    id: Field::Number,
    format: Field::Text,
    price: Field::Number,
    shipping_price: Field::Number,
    priority_shipping: Field::Number,
    intl_shipping: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    order_item_id: Field::Number,
    cad_price: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :order_items,
    :id,
    :format,
    :price,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :order_items,
    :id,
    :format,
    :price,
    :shipping_price,
    :priority_shipping,
    :intl_shipping,
    :created_at,
    :updated_at,
    :order_item_id,
    :cad_price,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :order_items,
    :format,
    :price,
    :shipping_price,
    :priority_shipping,
    :intl_shipping,
    :order_item_id,
    :cad_price,
  ].freeze

  # Overwrite this method to customize how backups are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(backup)
  #   "Backup ##{backup.id}"
  # end
end
