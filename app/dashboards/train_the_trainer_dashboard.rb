require "administrate/base_dashboard"

class TrainTheTrainerDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    kit: Field::BelongsTo,
    product_group: Field::BelongsTo,
    order_items: Field::HasMany,
    id: Field::Number,
    title: Field::String,
    price: Field::Number,
    sale_price: Field::Number,
    sale_start: Field::DateTime,
    sale_expiry: Field::DateTime,
    language: Field::String.with_options(searchable: false),
    standards: Field::String.with_options(searchable: false),
    talent_course_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    cad_price: Field::Number,
    cad_sale_price: Field::Number,
    practical_evaluation_data: Field::Text,
    exam_data: Field::Text,
    answer_key_data: Field::Text,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :kit,
    :product_group,
    :order_items,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :kit,
    :product_group,
    :order_items,
    :id,
    :title,
    :price,
    :sale_price,
    :sale_start,
    :sale_expiry,
    :language,
    :standards,
    :talent_course_id,
    :created_at,
    :updated_at,
    :cad_price,
    :cad_sale_price,
    :practical_evaluation_data,
    :exam_data,
    :answer_key_data,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :kit,
    :product_group,
    :order_items,
    :title,
    :price,
    :sale_price,
    :sale_start,
    :sale_expiry,
    :language,
    :standards,
    :talent_course_id,
    :cad_price,
    :cad_sale_price,
    :practical_evaluation_data,
    :exam_data,
    :answer_key_data,
  ].freeze

  # Overwrite this method to customize how train the trainers are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(train_the_trainer)
  #   "TrainTheTrainer ##{train_the_trainer.id}"
  # end
end
