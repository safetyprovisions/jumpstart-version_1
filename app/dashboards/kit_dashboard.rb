require "administrate/base_dashboard"

class KitDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    product_group: Field::BelongsTo,
    order_items: Field::HasMany,
    id: Field::Number,
    title: Field::String,
    price: Field::Number,
    sale_price: Field::Number,
    sale_expiry: Field::DateTime,
    language: Field::String.with_options(searchable: false),
    standards: Field::String.with_options(searchable: false),
    sample_data: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    sale_start: Field::DateTime,
    market_type: Field::String,
    cad_price: Field::Number,
    cad_sale_price: Field::Number,
    s3_url: Field::Text,
    download: Field::Text,
    additional_download: Field::Text,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :product_group,
    :order_items,
    :id,
    :title,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :product_group,
    :order_items,
    :id,
    :title,
    :price,
    :sale_price,
    :sale_expiry,
    :language,
    :standards,
    :sample_data,
    :created_at,
    :updated_at,
    :sale_start,
    :market_type,
    :cad_price,
    :cad_sale_price,
    :s3_url,
    :download,
    :additional_download,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :product_group,
    :order_items,
    :title,
    :price,
    :sale_price,
    :sale_expiry,
    :language,
    :standards,
    :sample_data,
    :sale_start,
    :market_type,
    :cad_price,
    :cad_sale_price,
    :s3_url,
    :download,
    :additional_download,
  ].freeze

  # Overwrite this method to customize how kits are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(kit)
  #   "Kit ##{kit.id}"
  # end
end
