require "administrate/base_dashboard"

class CouponCodeDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    code: Field::String,
    valid_from: Field::DateTime,
    valid_to: Field::DateTime,
    flat_rate: Field::Number,
    percentage: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    scope: Field::Text,
    min_purchase_amt: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :code,
    :valid_from,
    :valid_to,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :code,
    :valid_from,
    :valid_to,
    :flat_rate,
    :percentage,
    :created_at,
    :updated_at,
    :scope,
    :min_purchase_amt,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :code,
    :valid_from,
    :valid_to,
    :flat_rate,
    :percentage,
    :scope,
    :min_purchase_amt,
  ].freeze

  # Overwrite this method to customize how coupon codes are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(coupon_code)
  #   "CouponCode ##{coupon_code.id}"
  # end
end
