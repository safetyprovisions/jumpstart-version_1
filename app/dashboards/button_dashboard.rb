require "administrate/base_dashboard"

class ButtonDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    button_group: Field::BelongsTo,
    order_items: Field::HasMany,
    id: Field::Number,
    name: Field::Text,
    price: Field::Number,
    cad_price: Field::Number,
    shipping_price: Field::Number,
    priority_shipping: Field::Number,
    intl_shipping: Field::Number,
    weight: Field::Number,
    active: Field::Boolean,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :button_group,
    :order_items,
    :id,
    :name,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :button_group,
    :order_items,
    :id,
    :name,
    :price,
    :cad_price,
    :shipping_price,
    :priority_shipping,
    :intl_shipping,
    :weight,
    :active,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :button_group,
    :order_items,
    :name,
    :price,
    :cad_price,
    :shipping_price,
    :priority_shipping,
    :intl_shipping,
    :weight,
    :active,
  ].freeze

  # Overwrite this method to customize how buttons are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(button)
  #   "Button ##{button.id}"
  # end
end
