require "administrate/base_dashboard"

class KeychainDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    keychain_group: Field::BelongsTo,
    # order_items: Field::HasMany,
    id: Field::Number,
    name: Field::Text,
    price: Field::Number,
    color: Field::Text,
    cad_price: Field::Number,
    shipping_price: Field::Number,
    priority_shipping: Field::Number,
    intl_shipping: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    weight: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :keychain_group,
    # :order_items,
    :id,
    :name,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :keychain_group,
    # :order_items,
    :id,
    :name,
    :price,
    :color,
    :cad_price,
    :shipping_price,
    :priority_shipping,
    :intl_shipping,
    :created_at,
    :updated_at,
    :weight,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :keychain_group,
    # :order_items,
    :name,
    :price,
    :color,
    :cad_price,
    :shipping_price,
    :priority_shipping,
    :intl_shipping,
    :weight,
  ].freeze

  # Overwrite this method to customize how keychains are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(keychain)
  #   "Keychain ##{keychain.id}"
  # end
end
