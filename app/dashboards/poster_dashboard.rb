require "administrate/base_dashboard"

class PosterDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    poster_group: Field::BelongsTo,
    digital_poster: Field::HasOne,
    order_items: Field::HasMany,
    id: Field::Number,
    name: Field::Text,
    image_data: Field::Text,
    description: Field::Text,
    download_data: Field::Text,
    display_format: Field::Text,
    price: Field::Number,
    cad_price: Field::Number,
    shipping_price: Field::Number,
    priority_shipping: Field::Number,
    intl_shipping: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    slug: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :poster_group,
    :digital_poster,
    :order_items,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :poster_group,
    :digital_poster,
    :order_items,
    :id,
    :name,
    :image_data,
    :description,
    :download_data,
    :display_format,
    :price,
    :cad_price,
    :shipping_price,
    :priority_shipping,
    :intl_shipping,
    :created_at,
    :updated_at,
    :slug,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :poster_group,
    :digital_poster,
    :order_items,
    :name,
    :image_data,
    :description,
    :download_data,
    :display_format,
    :price,
    :cad_price,
    :shipping_price,
    :priority_shipping,
    :intl_shipping,
    :weight,
    :slug,
  ].freeze

  # Overwrite this method to customize how posters are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(poster)
  #   "Poster ##{poster.id}"
  # end
end
