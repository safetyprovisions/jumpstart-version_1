require "administrate/base_dashboard"

class OnlineCourseDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    non_catalog_course_accesses: Field::HasMany,
    order_items: Field::HasMany,
    group_trainings: Field::HasMany,
    product_group: Field::BelongsTo,
    id: Field::Number,
    title: Field::String,
    price: Field::Number,
    sale_price: Field::Number,
    sale_start: Field::DateTime,
    sale_expiry: Field::DateTime,
    language: Field::String.with_options(searchable: false),
    standards: Field::String.with_options(searchable: false),
    talent_course_id: Field::Number,
    practical_evaluation_data: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    ttt: Field::Boolean,
    exam_data: Field::Text,
    answer_key_data: Field::Text,
    market_type: Field::String,
    cad_price: Field::Number,
    cad_sale_price: Field::Number,
    public: Field::Boolean,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :non_catalog_course_accesses,
    :order_items,
    :group_trainings,
    :product_group,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :non_catalog_course_accesses,
    :order_items,
    :group_trainings,
    :product_group,
    :id,
    :title,
    :price,
    :sale_price,
    :sale_start,
    :sale_expiry,
    :language,
    :standards,
    :talent_course_id,
    :practical_evaluation_data,
    :created_at,
    :updated_at,
    :ttt,
    :exam_data,
    :answer_key_data,
    :market_type,
    :cad_price,
    :cad_sale_price,
    :public,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :non_catalog_course_accesses,
    :order_items,
    :group_trainings,
    :product_group,
    :title,
    :price,
    :sale_price,
    :sale_start,
    :sale_expiry,
    :language,
    :standards,
    :talent_course_id,
    :practical_evaluation_data,
    :ttt,
    :exam_data,
    :answer_key_data,
    :market_type,
    :cad_price,
    :cad_sale_price,
    :public,
  ].freeze

  # Overwrite this method to customize how online courses are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(online_course)
  #   "OnlineCourse ##{online_course.id}"
  # end
end
