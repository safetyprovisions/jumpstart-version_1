require "administrate/base_dashboard"

class DigitalPosterDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    poster: Field::BelongsTo,
    order_items: Field::HasMany,
    id: Field::Number,
    price: Field::Number,
    cad_price: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :poster,
    :order_items,
    :id,
    :price,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :poster,
    :order_items,
    :id,
    :price,
    :cad_price,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :poster,
    :order_items,
    :price,
    :cad_price,
  ].freeze

  # Overwrite this method to customize how digital posters are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(digital_poster)
  #   "DigitalPoster ##{digital_poster.id}"
  # end
end
