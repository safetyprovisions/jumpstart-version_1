require "administrate/base_dashboard"

class ProductGroupDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    kits: Field::HasMany,
    online_courses: Field::HasMany,
    alt_names: Field::HasMany,
    id: Field::Number,
    name: Field::String,
    image_data: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    video: Field::String,
    overview: Field::Text,
    training_standards: Field::Text,
    facts: Field::Text,
    fails: Field::Text,
    slug: Field::String,
    keywords: Field::Text,
    meta_description: Field::Text,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :kits,
    :online_courses,
    :alt_names,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :kits,
    :online_courses,
    :alt_names,
    :id,
    :name,
    :image_data,
    :created_at,
    :updated_at,
    :video,
    :overview,
    :training_standards,
    :facts,
    :fails,
    :slug,
    :keywords,
    :meta_description,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :kits,
    :online_courses,
    :alt_names,
    :name,
    :image_data,
    :video,
    :overview,
    :training_standards,
    :facts,
    :fails,
    :slug,
    :keywords,
    :meta_description,
  ].freeze

  # Overwrite this method to customize how product groups are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(product_group)
  #   "ProductGroup ##{product_group.id}"
  # end
end
