require "administrate/base_dashboard"

class LanyardGroupDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    order_items: Field::HasMany,
    id: Field::Number,
    name: Field::Text,
    description: Field::Text,
    image_data: Field::Text,
    slug: Field::String,
    keywords: Field::Text,
    meta_description: Field::Text,
    active: Field::Boolean,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :order_items,
    :id,
    :name,
    :description,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :order_items,
    :id,
    :name,
    :description,
    :image_data,
    :slug,
    :keywords,
    :meta_description,
    :active,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :order_items,
    :name,
    :description,
    :image_data,
    :slug,
    :keywords,
    :meta_description,
    :active,
  ].freeze

  # Overwrite this method to customize how lanyard groups are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(lanyard_group)
  #   "LanyardGroup ##{lanyard_group.id}"
  # end
end
